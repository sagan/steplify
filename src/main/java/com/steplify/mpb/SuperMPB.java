package com.steplify.mpb;

import com.google.gson.Gson;
import com.google.gson.internal.Primitives;

public class SuperMPB {

    //Used for communicating with UI Layer and REST calls
    public String toJSON()
    {
        Gson gson = new Gson();
        String json = gson.toJson(this);
        return json;
    }

    public static <T> T fromJson(String json, Class<T> classOfT)
    {
        Gson gson = new Gson();
        return gson.fromJson(json, classOfT);
    }

}
