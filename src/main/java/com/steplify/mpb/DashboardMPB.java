package com.steplify.mpb;

import com.steplify.dto.*;
import java.util.*;

public class DashboardMPB extends SuperMPB {

    public Map<String, String> mapCDsProjectType;
    public List<ProjectMemberDTO> alUserMemberships;
    public Map<Integer, ProjectTaskCountsMPB> mapProjectTaskCounts;
    public CompanyUserDTO dtoCompanyUser;

}

