package com.steplify.mpb;

import com.steplify.dto.CodeDecodeDTO;
import com.steplify.dto.TopicDTO;
import com.steplify.dto.UserPrincipalDTO;

import java.util.List;
import java.util.Map;

public class AdminTopicViewMPB extends SuperMPB {

    public List<CodeDecodeDTO> alCodesTopicAuthority;
    public List<TopicDTO> lstTopics;
    public int iProjectID;

}

