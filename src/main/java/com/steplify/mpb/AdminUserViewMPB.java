package com.steplify.mpb;

import com.steplify.dto.*;
import com.steplify.utility.*;
import java.util.List;
import java.util.Map;

public class AdminUserViewMPB extends SuperMPB {

    public List<CodeDecodeDTO> alCodesMembershipRole;
    public List<CodeDecodeDTO> alCodesMembershipStatus;
    public List<ProjectMemberDTO> lstMemberships;
    public boolean bUserHasProjectAdmin;
    public Map<Integer, UserPrincipalDTO> mapUsers;
    public int iProjectID;

}

