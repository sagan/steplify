package com.steplify.mpb;

import com.steplify.utility.*;
import org.apache.log4j.Logger;

import javax.servlet.http.*;
import java.util.*;

public class SortDashboardMPB extends SuperMPB {

    public boolean bOrderReverse = false;
    public boolean bOrderByMembershipCreateDate = false;
    public boolean bOrderByProjectAddress = false;
    public boolean bOrderByProjectID = false;

    private static Logger _LOGGER = Logger.getLogger("SortDashboardMPB");

    //Assumes inputs are either in JSP-RequestParameters or RequestCookies
    public static SortDashboardMPB deriveSort(HttpServletRequest request, HttpServletResponse response)
    {
        SortDashboardMPB sortMPB = new SortDashboardMPB();
        String jsonSort = null;

        try
        {
            //Check if a new sort field is passed in
            String sSort = request.getParameter("sort");
            if (sSort == null)
            {
                //Nothing was passed in, get it from the cookie
                Map<String, Cookie> mapCookies = AppSession.getCookies(request);
                Cookie sortCookie = mapCookies.get("DASHBOARD_SORT");

                if (sortCookie != null)
                {
                    jsonSort = sortCookie.getValue();
                    sortMPB = SortDashboardMPB.fromJson(jsonSort, SortDashboardMPB.class);
                }
                else
                {
                    //Here is our default sort, no cookie, no parameter passed in
                    sortMPB.bOrderByMembershipCreateDate = true;
                    sortMPB.bOrderReverse = true;
                }
            }
            else if ("membership_date".equals(sSort))
            {
                sortMPB.bOrderByMembershipCreateDate = true;
            }
            else if (sSort != null && "address".equals(sSort))
            {
                sortMPB.bOrderByProjectAddress = true;
            }
            else if (sSort != null && "project_id".equals(sSort))
            {
                sortMPB.bOrderByProjectID = true;
            }

            //Check if we are supposed to reverse the sort
            String sReverse = request.getParameter("reverse");
            if (sReverse != null)
            {
                sortMPB.bOrderReverse = !sortMPB.bOrderReverse;
            }

            //Set the sort for the next time, give them 1 year
            AppSession.setCookie(response, "DASHBOARD_SORT", sortMPB.toJSON(), 365);
        }
        catch (Exception ex)
        {
            //Better than blowing up, i.e. cookie has bad (or an old) format
            sortMPB = new SortDashboardMPB();
            sortMPB.bOrderByMembershipCreateDate = true;
            sortMPB.bOrderReverse = true;
            _LOGGER.error(ex.getMessage());
            _LOGGER.error("jsonSort=" + jsonSort);
        }

        return sortMPB;
    }
}

