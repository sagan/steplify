package com.steplify.mpb;

import com.steplify.dto.CodeDecodeDTO;
import com.steplify.dto.*;
import java.util.List;
import java.util.Map;

public class TaskManagerViewMPB extends SuperMPB {

    public List<CodeDecodeDTO> alCodesTaskStatus;
    public List<TopicDTO> lstTopics;
    public Map<Integer, List<TaskDTO>> mapTopicTasks;
    public ProjectDTO dtoProject;
    public Map<String, String> mapCDsTopicAuthority;
    public int iFilterUserID;

}

