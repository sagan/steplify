package com.steplify.controller;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.steplify.dao.*;
import com.steplify.dto.*;
import com.steplify.mpb.*;
import com.steplify.utility.*;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.*;
import java.util.Date;

public class AdminUserServlet extends SuperServlet
{
    private static Logger _LOGGER = Logger.getLogger("AdminUserServlet");

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //Get the logged in user from SuperServlet > Request
        UserPrincipalDTO loggedInUserDTO = getLoggedInUserDTO(request);

        if (request.getRequestURI().equals("/admin_user/view"))
        {
            AdminUserViewMPB mpbAdminUserView = new AdminUserViewMPB();
            String sID = request.getParameter("pid");
            if (sID == null)
            {
                throw new RuntimeException("Missing input PID");
            }

            //Assume we are planning to edit an existing project
            int iProjectID = Integer.parseInt(sID);
            mpbAdminUserView.iProjectID = iProjectID;

            MembershipAuthEnum enumAuth = authorityCheckForUserAndProject(loggedInUserDTO.get_id(), iProjectID);
            mpbAdminUserView.bUserHasProjectAdmin = (enumAuth == MembershipAuthEnum.ADMIN) ? true : false;

            //This will set the Memberships and Users on the MPB
            getMembershipsAndUsers(enumAuth, mpbAdminUserView, loggedInUserDTO, iProjectID);

            //Get the necessary CodeDecodes for the UI
            List<CodeDecodeDTO> alCodesMembershipRole = CodeDecode.getList(CodeDecode._CAT_MEMBERSHIP_ROLE);
            mpbAdminUserView.alCodesMembershipRole = alCodesMembershipRole;
            List<CodeDecodeDTO> alCodesMembershipStatus = CodeDecode.getList(CodeDecode._CAT_MEMBERSHIP_STATUS);
            mpbAdminUserView.alCodesMembershipStatus = alCodesMembershipStatus;

            //Return everything to the JSP to render
            request.setAttribute("mpbAdminUserView", mpbAdminUserView);
            request.setAttribute("objTransResponse", new TransResponse());
            request.setAttribute("dtoLoggedInUser", loggedInUserDTO);
            request.getRequestDispatcher("/admin_user.jsp").forward(request, response);
            return;
        }
        else if (request.getRequestURI().equals("/admin_user/user_search/ajax"))
        {
            String sUsernamePrefix = request.getParameter("_s_username_prefix");

            //This is an AJAX call, so no need for page manipulation
            getSearchCompanyUsernames(response, loggedInUserDTO, sUsernamePrefix);
            return;
        }
        else
        {
            throw new RuntimeException("URL mismatch");
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //Get the logged in user from SuperServlet > Request
        UserPrincipalDTO loggedInUserDTO = getLoggedInUserDTO(request);

        if (request.getRequestURI().equals("/admin_user/membership"))
        {
            String jsonIn = getPostParametersAsJSON(request);
            MembershipUserMPB mpbMembershipUser = MembershipUserMPB.fromJson(jsonIn, MembershipUserMPB.class);

            //Vars used in both add and update scenarios
            MembershipAuthEnum enumAuth = null;
            TransResponse tr = null;
            int iProjectID;

            //Check if this is an add or update
            if (mpbMembershipUser._id == 0)
            {
                //Add a new membership, verify they have AUTH to add to this project
                iProjectID = mpbMembershipUser._id_project;
                enumAuth = authorityCheckForUserAndProject(loggedInUserDTO.get_id(), iProjectID);
                tr = postAddMembership(enumAuth, loggedInUserDTO, mpbMembershipUser);
            }
            else
            {
                //Update an existing membership
                ProjectMemberDTO dtoMembershipFromDB = null;
                Connection conn = SuperDuperDAO.getConnection();
                try
                {
                    //Get the Membership from the DB, since we cant trust the user's didn't manipulate the hidden-fields
                    dtoMembershipFromDB = ProjectMemberDAO.selectMembershipByID(conn, mpbMembershipUser._id);
                }
                finally
                {
                    SuperDuperDAO.close(conn, false);
                }

                //Use the ProjectID from the DB to verify they have AUTH to update this project
                //This way, the Membership they want to update is guaranteed to be part of the Project we use for the AUTH check
                iProjectID = dtoMembershipFromDB.get_id_project();
                enumAuth = authorityCheckForUserAndProject(loggedInUserDTO.get_id(), iProjectID);
                tr = postUpdateMembership(enumAuth, loggedInUserDTO, mpbMembershipUser, dtoMembershipFromDB);
            }

            //When save is complete, leave the user on the same page, in case they need to do more edits
            AdminUserViewMPB mpbAdminUserView = new AdminUserViewMPB();
            mpbAdminUserView.iProjectID = iProjectID;
            mpbAdminUserView.bUserHasProjectAdmin = (enumAuth == MembershipAuthEnum.ADMIN) ? true : false;

            //This will set the Memberships and Users on the MPB, for display
            getMembershipsAndUsers(enumAuth, mpbAdminUserView, loggedInUserDTO, iProjectID);

            //Get the necessary CodeDecodes for the UI
            List<CodeDecodeDTO> alCodesMembershipRole = CodeDecode.getList(CodeDecode._CAT_MEMBERSHIP_ROLE);
            mpbAdminUserView.alCodesMembershipRole = alCodesMembershipRole;
            List<CodeDecodeDTO> alCodesMembershipStatus = CodeDecode.getList(CodeDecode._CAT_MEMBERSHIP_STATUS);
            mpbAdminUserView.alCodesMembershipStatus = alCodesMembershipStatus;

            //Return everything to the JSP to render
            request.setAttribute("mpbAdminUserView", mpbAdminUserView);
            request.setAttribute("objTransResponse", tr);
            request.setAttribute("dtoLoggedInUser", loggedInUserDTO);

            //If there were no errors, lets notify the user all was good
            if (tr.isSuccess())
            {
                tr.addWarning("Updated on: " + UI.datetimeString(new Date()));
            }

            request.getRequestDispatcher("/admin_user.jsp").forward(request, response);
            return;
        }
        else
        {
            throw new RuntimeException("URL mismatch");
        }
    }

    ///////////////////Split out for Unit Testing////////////////////

    public void getMembershipsAndUsers(MembershipAuthEnum enumAuth, AdminUserViewMPB mpbAdminUserView, UserPrincipalDTO dtoUser, int iProjectID)
    {
        List<ProjectMemberDTO> lstMemberships;

        //Make sure the user is allowed to at least read this project
        if (enumAuth != MembershipAuthEnum.ADMIN)
        {
            String sMsg = "Access Denied - Membership for UserID=" + dtoUser.get_id() + " for ProjectID=" + iProjectID;
            _LOGGER.error(sMsg);
            throw new RuntimeException(sMsg);
        }

        //Get a DB connection, do the work, and close it
        Connection connection = SuperDuperDAO.getConnection();
        try
        {
            //And select from the DB, those who are REMOVED will still be in the ResultSet
            lstMemberships = ProjectMemberDAO.selectUserMembershipsForProject(connection, iProjectID);
            if (lstMemberships == null || lstMemberships.size() == 0)
            {
                //For some reason we could not find any memberships
                _LOGGER.error("There were no project memberships = " + iProjectID);
                //Do not proceed
                throw new RuntimeException("There were no project memberships, this was unexpected");
            }
            else
            {
                //Initialize the return variables
                mpbAdminUserView.lstMemberships = new ArrayList<ProjectMemberDTO>();
                mpbAdminUserView.mapUsers = new HashMap<Integer, UserPrincipalDTO>();

                for (ProjectMemberDTO dtoMembership : lstMemberships)
                {
                    //Everyone can see everyone, but wont be able to edit everyone
                    mpbAdminUserView.lstMemberships.add(dtoMembership);
                    //Also build a hashmap of ALL the users for UI needs
                    mpbAdminUserView.mapUsers.put(dtoMembership.getUserPrincipal().get_id(), dtoMembership.getUserPrincipal());
                }
            }
        }
        finally
        {
            SuperDuperDAO.close(connection, false);
        }
    }

    public void getSearchCompanyUsernames(HttpServletResponse response, UserPrincipalDTO loggedInUserDTO, String sUsernamePrefix) throws ServletException, IOException
    {
        //Get the Users for the Company of the current user, based on autocomplete prefix
        List<String> lstUsernames = null;

        Connection conn = SuperDuperDAO.getConnection();
        try
        {
            //Get all the Topics/Tasks of the Company the current user is tied to
            CompanyUserDTO dtoCompanyUser = CompanyDAO.selectCompanyUserByUserID(conn, loggedInUserDTO.get_id());
            lstUsernames =  UserPrincipalDAO.selectUsernamesByCompanyAndNamePrefix(conn, dtoCompanyUser.get_id_company(), sUsernamePrefix);
        }
        catch (Exception ex)
        {
            _LOGGER.error("AUTOCOMPLETE FAILED");
            _LOGGER.error(ex);
            lstUsernames = new ArrayList<String>();
        }
        finally
        {
            SuperDuperDAO.close(conn, false);
        }

        //ship the data back
        Gson gson = new Gson();
        String json = gson.toJson(lstUsernames);
        response.getWriter().print(json);
        response.setStatus(HttpServletResponse.SC_OK);
    }

    public TransResponse postAddMembership(MembershipAuthEnum enumAuth, UserPrincipalDTO dtoUserCurrent, MembershipUserMPB mpbMembershipUser)
    {
        //Make sure the user is allowed to update the membership
        if (enumAuth != MembershipAuthEnum.ADMIN)
        {
            String sMsg = "Access Denied - Membership addition by UserID=" + dtoUserCurrent.get_id() + " on ProjectID=" + mpbMembershipUser._id_project;
            _LOGGER.error(sMsg);
            throw new RuntimeException(sMsg);
        }

        TransResponse tr = new TransResponse();
        try
        {
            ProjectMemberDTO dtoMembershipForDB = new ProjectMemberDTO();

            //Here is where I need to do any last minute UI conversions
            CodeDecodeDTO cdPMemberRole = CodeDecode.getByValue(CodeDecode._CAT_MEMBERSHIP_ROLE, mpbMembershipUser._s_cd_role);
            if (cdPMemberRole != null)
            {
                dtoMembershipForDB.set_s_cd_role(cdPMemberRole.get_s_code());
            }
            else
            {
                dtoMembershipForDB.setRoleToOther();
            }

            //Is this new user going to be an admin?
            dtoMembershipForDB.set_b_is_project_admin(mpbMembershipUser._b_is_project_admin);

            //Set the membership to be newly invited
            dtoMembershipForDB.setStatusInvited();
            //Link the membership to the project
            dtoMembershipForDB.set_id_project(mpbMembershipUser._id_project);

            //See if the membership is valid
            dtoMembershipForDB.validateFieldsBase(tr);

            //So far so good, lets save it up
            if (tr.isSuccess())
            {
                //Get a DB connection, do the work, and close it
                Connection connection = SuperDuperDAO.getConnection();
                try
                {
                    //Check if the requested user exists, but first validate the username
                    String passwordForNewUser = null;
                    UserPrincipalDTO dtoUP = new UserPrincipalDTO();
                    dtoUP.setNewUsername(mpbMembershipUser._s_username);
                    dtoUP = UserPrincipalDAO.selectUserByName(connection, mpbMembershipUser._s_username);

                    if (dtoUP == null)
                    {
                        dtoUP = new UserPrincipalDTO();
                        dtoUP.setNewUsername(mpbMembershipUser._s_username);
                        passwordForNewUser = dtoUP.generateNewPasswordBackup();
                        dtoUP.setNewPassword(passwordForNewUser);
                        dtoUP.setStatusActive();

                        int iCount = UserPrincipalDAO.insert(connection, dtoUP);
                        if (iCount != 1)
                        {
                            //For some reason we could insert the new user
                            _LOGGER.error("User Creation Failed: iCount=" + iCount);
                            throw new Exception("User can not be created, please try again");
                        }
                    }

                    //set the REQUESTED user to be the member
                    dtoMembershipForDB.set_id_user_principal(dtoUP.get_id());
                    //Set the CURRENT user to be the creator
                    dtoMembershipForDB.set_id_created_by(dtoUserCurrent.get_id());
                    dtoMembershipForDB.set_id_updated_by(dtoUserCurrent.get_id());
                    //See if the membership is valid
                    dtoMembershipForDB.validateFieldsAll(tr);

                    if (tr.isSuccess())
                    {
                        //And update the DB
                        int iCountPP = ProjectMemberDAO.insertMembership(connection, dtoMembershipForDB);
                        _LOGGER.info(formatAppEvent(dtoUserCurrent, "USER_MEMBER_ADD", ImmutableMap.of("project_id", String.valueOf(dtoMembershipForDB.get_id_project()))));

                        if (iCountPP != 1)
                        {
                            //For some reason we could not insert the new project
                            _LOGGER.error("Membership insert failed: iCountPP=" + iCountPP);
                            //Do not proceed
                            throw new Exception("Membership could not be saved, please try again");
                        }
                        else
                        {
                            //Add the new ProjectMember to all the existing ProjectTopics
                            createTopicMembershipsForNewProjectMember(tr, connection, dtoMembershipForDB);

                            //Send an email to let the user know they have been added
                            sendProjectMembershipNewEmail(connection, dtoUP, dtoUserCurrent, passwordForNewUser);

                            //Send an email to others on the project alerting them a new membership is added
                            sendProjectMembershipAlertOthersEmail(connection, dtoMembershipForDB);
                        }
                    }
                    else
                    {
                        throw new Exception("Save failed, please try again...");
                    }
                }
                catch (Exception ex)
                {
                    SuperDuperDAO.close(connection, false);

                    //SQLIntegrityConstraintViolationException doesnt get thrown
                    if (ex.getMessage().toLowerCase().contains("duplicate"))
                    {
                        throw new Exception("User is already a member of this project");
                    }
                    else
                    {
                        _LOGGER.error(ex.getMessage());
                        throw ex;
                    }
                }
                finally
                {
                    SuperDuperDAO.close(connection, true);
                }
            }
        }
        catch (Exception ex)
        {
            //Hopefully just bad input data
            tr.addException(ex.getMessage());
        }
        finally
        {
            return tr;
        }
    }

    public TransResponse postUpdateMembership(MembershipAuthEnum enumAuth, UserPrincipalDTO dtoUserCurrent, MembershipUserMPB mpbMembershipUser, ProjectMemberDTO dtoMembershipFromDB)
    {
        //Make sure the user is allowed to update the membership
        if (enumAuth != MembershipAuthEnum.ADMIN)
        {
            String sMsg = "Access Denied - Membership update by UserID=" + dtoUserCurrent.get_id() + " on MembershipID=" + dtoMembershipFromDB.get_id();
            _LOGGER.error(sMsg);
            throw new RuntimeException(sMsg);
        }

        TransResponse tr = new TransResponse();
        try
        {
            //Here is where I need to do any last minute UI conversions
            CodeDecodeDTO cdPMemberStatus = CodeDecode.getByValue(CodeDecode._CAT_MEMBERSHIP_STATUS, mpbMembershipUser._s_cd_status);
            if (cdPMemberStatus != null)
            {
                dtoMembershipFromDB.set_s_cd_status(cdPMemberStatus.get_s_code());
            }
            else
            {
                dtoMembershipFromDB.set_s_cd_status(null);
            }

            CodeDecodeDTO cdPMemberRole = CodeDecode.getByValue(CodeDecode._CAT_MEMBERSHIP_ROLE, mpbMembershipUser._s_cd_role);
            if (cdPMemberRole != null)
            {
                dtoMembershipFromDB.set_s_cd_role(cdPMemberRole.get_s_code());
            }
            else
            {
                dtoMembershipFromDB.set_s_cd_role(null);
            }

            //See if the ADMIN checkbox is changed, not everyone has Security Authorization
            if (dtoMembershipFromDB.get_b_is_project_admin() != mpbMembershipUser._b_is_project_admin)
            {
                //The creator of the project will always be ADMIN, it cannot be changed
                //As well as you cant add or remove it from yourself
                if (dtoMembershipFromDB.get_id_user_principal() == dtoMembershipFromDB.get_id_created_by()
                        || dtoMembershipFromDB.get_id_user_principal() == dtoUserCurrent.get_id())
                {
                    tr.addException("You cannot change those admin rights.");
                }
                else
                {
                    dtoMembershipFromDB.set_b_is_project_admin(mpbMembershipUser._b_is_project_admin);
                }
            }

            //Guard against the loggedInUser accidentally removing themselves.
            if (dtoMembershipFromDB.isStatusRemoved() &&
                    dtoMembershipFromDB.get_id_user_principal() == dtoUserCurrent.get_id())
            {
                throw new RuntimeException("You cannot remove yourself from the project");
            }

            //See if the membership is valid
            dtoMembershipFromDB.validateFieldsAll(tr);

            //So far so good, lets save it up
            if (tr.isSuccess())
            {
                //Get a DB connection, do the work, and close it
                Connection connection = SuperDuperDAO.getConnection();
                try
                {
                    //Set the current user to be the updater
                    dtoMembershipFromDB.set_id_updated_by(dtoUserCurrent.get_id());
                    //And update the DB
                    int iCountPP = ProjectMemberDAO.updateMembership(connection, dtoMembershipFromDB);
                    _LOGGER.info(formatAppEvent(dtoUserCurrent, "USER_MEMBER_UPDATE", ImmutableMap.of("project_id", String.valueOf(dtoMembershipFromDB.get_id_project()))));

                    if (iCountPP != 1)
                    {
                        //For some reason we could not insert the new project
                        _LOGGER.error("Membership update failed: iCountPP=" + iCountPP);
                        //Do not proceed
                        throw new Exception("Membership could not be saved, please try again");
                    }
                    else
                    {
                        //ProjectMember updated ok, verify membership permissions
                        updateTopicMembershipsForProjectMember(tr, connection, dtoUserCurrent, dtoMembershipFromDB);
                    }
                }
                catch (Exception ex)
                {
                    SuperDuperDAO.close(connection, false);
                    throw ex;
                }
                finally
                {
                    SuperDuperDAO.close(connection, true);
                }
            }
        }
        catch (Exception ex)
        {
            //Hopefully just bad input data
            tr.addException(ex.getMessage());
        }
        finally
        {
            return tr;
        }
    }

    private void createTopicMembershipsForNewProjectMember(TransResponse tr, Connection connection, ProjectMemberDTO dtoNewProjectMember)
    {
        List<TopicDTO> lstTopics = TopicDAO.selectTopicsByProjectID(connection, dtoNewProjectMember.get_id_project());

        for (TopicDTO dtoTopic: lstTopics)
        {
            TopicMemberDTO dtoTopicMember = new TopicMemberDTO();
            dtoTopicMember.set_id_created_by(dtoNewProjectMember.get_id_created_by());
            dtoTopicMember.set_id_updated_by(dtoNewProjectMember.get_id_updated_by());
            dtoTopicMember.set_id_user_principal(dtoNewProjectMember.get_id_user_principal());
            dtoTopicMember.set_id_topic(dtoTopic.get_id());
            dtoTopicMember.set_id_project(dtoTopic.get_id_project());

            //Rule based Membership permission on UserAdd
            //if (dtoTopic.get_b_auto_add_members())
            if(dtoNewProjectMember.get_b_is_project_admin())
            {
                dtoTopicMember.setAuthorityWriter();
            }
            else
            {
                //You are basically a nobody
                dtoTopicMember.setAuthorityNone();
            }

            dtoTopicMember.validateFields(tr);

            //If all fields are ok, insert the row
            if (tr.isSuccess())
            {
                int iCount = TopicMemberDAO.insertTopicMember(connection, dtoTopicMember);
                if (iCount != 1)
                {
                    throw new RuntimeException("TopicMember could not be saved, please try again");
                }
            }
            else
            {
                throw new RuntimeException("TopicMember could not be validated, please try again");
            }
        }
    }

    private void updateTopicMembershipsForProjectMember(TransResponse tr, Connection connection, UserPrincipalDTO dtoUserCurrent, ProjectMemberDTO dtoProjectMember)
    {
        List<TopicMemberDTO> lstTopicMembers = TopicMemberDAO.selectTopicMembersByUserAndProject(connection, dtoProjectMember.get_id_user_principal(), dtoProjectMember.get_id_project());

        for (TopicMemberDTO dtoTopicMember: lstTopicMembers)
        {
            dtoTopicMember.set_id_updated_by(dtoUserCurrent.get_id());

            //Rule based Membership permission on UserAdd
            if(dtoProjectMember.get_b_is_project_admin())
            {
                dtoTopicMember.setAuthorityWriter();
            }
            else
            {
                //You are basically a nobody
                dtoTopicMember.setAuthorityNone();
            }

            dtoTopicMember.validateFields(tr);

            //If all fields are ok, insert the row
            if (tr.isSuccess())
            {
                int iCount = TopicMemberDAO.updateTopicMember(connection, dtoTopicMember);
                if (iCount != 1)
                {
                    throw new RuntimeException("TopicMember could not be saved, please try again");
                }
            }
            else
            {
                throw new RuntimeException("TopicMember could not be validated, please try again");
            }
        }
    }


    ////////////////Email related methods

    private void sendProjectMembershipNewEmail(Connection connection, UserPrincipalDTO dtoNewUser, UserPrincipalDTO dtoReferralUser, String passwordForNewUser)
    {
        try
        {
            //Send the actual email
            Emailer.sendNewMembership(dtoNewUser.get_s_username(), dtoReferralUser.get_s_username(), passwordForNewUser);
        }
        catch (Exception ex)
        {
            //Just log it, but dont re-throw it; we dont want an email to stop the app from functioning
            _LOGGER.error(ex.getMessage());
        }
    }

    private void sendProjectMembershipAlertOthersEmail(Connection connection, ProjectMemberDTO dtoMember)
    {
        try
        {
            //Get the project
            ProjectDTO dtoProject = ProjectDAO.selectProjectWithMembersByID(connection, dtoMember.get_id_project());

            //Derive the email address TO list
            ArrayList<String> alTo = new ArrayList<String>();

            Map<Integer, ProjectMemberDTO> mapPMs = new HashMap<Integer, ProjectMemberDTO>();
            for (ProjectMemberDTO dtoPM: dtoProject.getMembers())
            {
                //We dont email anyone who is removed from the project, ever if they were the original referral party
                if (!dtoPM.isStatusRemoved())
                {
                    //Send to ADMINs, and the referring party
                    if (dtoPM.get_b_is_project_admin() || (dtoPM.get_id_user_principal() == dtoMember.get_id_created_by()))
                    {
                        alTo.add(dtoPM.getUserPrincipal().get_s_username());
                    }
                }

                //Derive a map for quick access, when we are done looping
                mapPMs.put(dtoPM.getUserPrincipal().get_id(), dtoPM);
            };

            //Get the actual users involved
            UserPrincipalDTO dtoUserActive = mapPMs.get(dtoMember.get_id_user_principal()).getUserPrincipal();
            UserPrincipalDTO dtoUserReferral = mapPMs.get(dtoMember.get_id_created_by()).getUserPrincipal();

            //Send the actual email
            Emailer.sendProjectMembershipAlertOthers(alTo, dtoUserActive, dtoUserReferral, dtoProject);
        }
        catch (Exception ex)
        {
            //Just log it, but dont re-throw it; we dont want an email to stop the app from functioning
            _LOGGER.error(ex.getMessage());
        }
    }

}
