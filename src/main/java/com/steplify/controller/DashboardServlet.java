package com.steplify.controller;

import com.google.common.collect.ImmutableMap;
import com.steplify.dao.*;
import com.steplify.dto.*;
import com.steplify.mpb.*;
import com.steplify.utility.*;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DashboardServlet extends SuperServlet
{
    private static Logger _LOGGER = Logger.getLogger("DashboardServlet");

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //Get the logged in user from SuperServlet > Request
        UserPrincipalDTO loggedInUserDTO = getLoggedInUserDTO(request);

        if (request.getRequestURI().equals("/dashboard/profile/ajax"))
        {
            //This is an AJAX call, so no need for page manipulation
            getProfile(response, loggedInUserDTO);
        }
        else
        {
            //The default route is to show the DASHBOARD
            request.setAttribute("dtoLoggedInUser", loggedInUserDTO);

            //Determine the sort
            SortDashboardMPB sortMPB = SortDashboardMPB.deriveSort(request, response);

            //Is the request for the only the archive, or normal/non-archived project memberships
            String sArchive = request.getParameter("archive");
            boolean bFetchArchived = false;

            if ("true".equals(sArchive))
            {
                bFetchArchived = true;
            }

            //So the JSP can implement logic on this context
            request.setAttribute("bFetchArchived", bFetchArchived);

            //Get all the data we need in one call; this is so we can unit test it too
            DashboardMPB mpbDashboard = getDashboardUserData(loggedInUserDTO, sortMPB, bFetchArchived);
            request.setAttribute("mpbDashboard", mpbDashboard);

            request.getRequestDispatcher("/dashboard.jsp").forward(request, response);
            return;
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //Get the logged in user from SuperServlet > Request
        UserPrincipalDTO loggedInUserDTO = getLoggedInUserDTO(request);

        if (request.getRequestURI().equals("/dashboard/password/ajax"))
        {

            String jsonIn = getRequestBodyPayload(request);
            SetNewPasswordMPB mpb = SetNewPasswordMPB.fromJson(jsonIn, SetNewPasswordMPB.class);
            postSetNewPassword(response, loggedInUserDTO, mpb);
        }
        else if (request.getRequestURI().equals("/dashboard/profile/ajax"))
        {
            String jsonIn = getRequestBodyPayload(request);
            UserProfileDTO dtoProfile = UserProfileDTO.fromJson(jsonIn, UserProfileDTO.class);
            postSetProfile(response, loggedInUserDTO, dtoProfile);
            //This is an AJAX call, so no need for page manipulation
        }
        else if (request.getRequestURI().equals("/dashboard/archive/ajax"))
        {
            String jsonIn = getRequestBodyPayload(request);
            ProjectMembershipArchiveMPB mpbProjectMembershipArchive = ProjectMembershipArchiveMPB.fromJson(jsonIn, ProjectMembershipArchiveMPB.class);
            postArchiveProject(response, loggedInUserDTO, mpbProjectMembershipArchive);
            //This is an AJAX call, so no need for page manipulation
        }
        else
        {
            throw new RuntimeException("URL mismatch");
        }
    }

    ///////////////////Split out for Unit Testing////////////////////

    public void postSetNewPassword(HttpServletResponse response, UserPrincipalDTO dtoUser, SetNewPasswordMPB mpb) throws ServletException, IOException
    {
        //Make sure we have the minimum info to check
        if (mpb._s_password1 != null && mpb._s_password2 != null)
        {
            try
            {
                if (mpb._s_password1.equals(mpb._s_password2))
                {
                    //The current user is always logged in
                    UserPrincipalDTO dto = dtoUser;

                    //This will make sure its a valid password
                    dto.setNewPassword(mpb._s_password1);

                    int iCount = 0;
                    //Get a DB connection, do the work, and close it
                    Connection connection = SuperDuperDAO.getConnection();
                    try
                    {
                        iCount = UserPrincipalDAO.updateUserPassword(connection, dto);

                        //And reset any backup passwords too, so new account passwords wont work anymore
                        dto.generateNewPasswordBackup();
                        UserPrincipalDAO.updateUserBackupPassword(connection, dto);
                        _LOGGER.info(formatAppEvent(dtoUser, "PASSWORD_UPDATE", null));
                    }
                    catch (Exception ex)
                    {
                        SuperDuperDAO.close(connection, false);
                        throw ex;
                    }
                    finally
                    {
                        SuperDuperDAO.close(connection, true);
                    }

                    if (iCount == 1)
                    {
                        response.setStatus(HttpServletResponse.SC_OK);
                        return;
                    }
                    else
                    {
                        //For some reason we could update user
                        _LOGGER.error("Password Update Failed: iCount=" + iCount);
                        throw new Exception("Password Update Failed");
                    }
                }
                else
                {
                    //Stop here
                    throw new Exception("The new passwords do not match.");
                }
            }
            catch (Exception ex)
            {
                //Probably bad input data
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().print(ex.getMessage());
                return;
            }
        }
        else
        {
            //We dont expect this, unless someone is hacking, or the front-end has a bug
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().print("Missing Password data");
            _LOGGER.error("Missing Password data, is there a front-end bug?");
            return;
        }
    }

    public UserProfileDTO getProfile(HttpServletResponse response, UserPrincipalDTO dtoUser) throws ServletException, IOException
    {
        UserProfileDTO dtoProfile = null;
        try
        {
            //The current user's profile
            dtoProfile = dtoUser.getUserProfile();

            String jsonOut;
            if (dtoProfile != null)
            {
                jsonOut = dtoProfile.toJSON();
            }
            else
            {
                jsonOut = SuperDuperDTO.nullJSON();
            }

            //response.setContentType("application/json");
            response.getWriter().print(jsonOut);
            response.setStatus(HttpServletResponse.SC_OK);
        }
        catch (Exception ex)
        {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().print(ex.getMessage());
            _LOGGER.error("UNEXPECTED - No idea why this would happen???");
        }

        return dtoProfile;
    }

    public void postSetProfile(HttpServletResponse response, UserPrincipalDTO dtoUser, UserProfileDTO dtoProfile) throws ServletException, IOException
    {
        try
        {
            //The profile should never be sent as null
            if (dtoProfile == null)
            {
                //For some reason we got a null object
                _LOGGER.error("Posted profile object was null");
                throw new Exception("Posted profile object was null");
            }

            if (dtoProfile.get_id_created_by() != dtoUser.get_id())
            {
                _LOGGER.error("Profile does not belong to logged in user");
                throw new Exception("Profile does not belong to logged in user");
            }

            if (dtoProfile.get_s_url_personal_img() != null &&
                    !dtoProfile.get_s_url_personal_img().equals("") &&
                    !UI.startsWithHttp(dtoProfile.get_s_url_personal_img()))
            {
                _LOGGER.error("Personal Picture/Image URL must start with http://");
                throw new Exception("Personal Picture/Image URL must start with http://");
            }

            if (dtoProfile.get_s_url_company_img() != null &&
                    !dtoProfile.get_s_url_company_img().equals("") &&
                    !UI.startsWithHttp(dtoProfile.get_s_url_company_img()))
            {
                _LOGGER.error("Company Icon/Image URL must start with http://");
                throw new Exception("Company Icon/Image URL must start with http://");
            }

            if (dtoProfile.get_s_url_website() != null &&
                    !dtoProfile.get_s_url_website().equals("") &&
                    !UI.startsWithHttp(dtoProfile.get_s_url_website()))
            {
                _LOGGER.error("Website URL must start with http://");
                throw new Exception("Website URL must start with http://");
            }

            int iCountUpdate = 0;
            //Get a DB connection, do the work, and close it
            Connection connection = SuperDuperDAO.getConnection();
            try
            {
                //Set the current user's with the new profile
                iCountUpdate = UserPrincipalDAO.updateUserProfile(connection, dtoProfile);
                _LOGGER.info(formatAppEvent(dtoUser, "PROFILE_UPDATE", null));
            }
            catch (Exception ex)
            {
                SuperDuperDAO.close(connection, false);
                throw ex;
            }
            finally
            {
                SuperDuperDAO.close(connection, true);
            }

            if (iCountUpdate == 1)
            {
                response.setStatus(HttpServletResponse.SC_OK);
            }
            else
            {
                //For some reason we couldnt update the user
                _LOGGER.error("User profile update failed");
                throw new Exception("User profile update failed");
            }
        }
        catch (Exception ex)
        {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().print(ex.getMessage());
        }
    }

    public DashboardMPB getDashboardUserData(UserPrincipalDTO loggedInUserDTO, SortDashboardMPB sortMPB, boolean bFetchArchived)
    {
        //Get a DB connection, do the work, and close it
        Connection connection = SuperDuperDAO.getConnection();

        try
        {
            DashboardMPB mpbDash = new DashboardMPB();
            mpbDash.mapCDsProjectType = CodeDecode.getLookupMap(CodeDecode._CAT_PROJECT_TYPE);

            //Get the user's memberships
            List<ProjectMemberDTO> alUserMemberships = ProjectMemberDAO.selectProjectMembershipsForUser(connection, loggedInUserDTO.get_id(), sortMPB, bFetchArchived);

            //Only keep the non-removed memberships
            mpbDash.alUserMemberships = new ArrayList<ProjectMemberDTO>();
            for (ProjectMemberDTO dtoPM : alUserMemberships)
            {
                if (!dtoPM.isStatusRemoved())
                {
                    mpbDash.alUserMemberships.add(dtoPM);
                }
            }

            //Get the Counts for each project
            Date lateDate = new Date();
            mpbDash.mapProjectTaskCounts = TaskDAO.selectTaskCountsByProjectForUser(connection, loggedInUserDTO.get_id(), lateDate);

            //Get the users company info
            mpbDash.dtoCompanyUser = CompanyDAO.selectCompanyUserByUserID(connection, loggedInUserDTO.get_id());

            return mpbDash;
        }
        finally
        {
            SuperDuperDAO.close(connection, false);
        }
    }

    public void postArchiveProject(HttpServletResponse response, UserPrincipalDTO dtoUser, ProjectMembershipArchiveMPB  mpb) throws ServletException, IOException
    {
        //Make sure we have the minimum info to check
        if (mpb._id != 0)
        {
            try
            {
                int iCount = 0;
                //Get a DB connection, do the work, and close it
                Connection connection = SuperDuperDAO.getConnection();
                try
                {
                    ProjectMemberDTO dtoPM = ProjectMemberDAO.selectMembershipByID(connection, mpb._id);

                    //YOu can only archive your own membership
                    if (dtoPM.get_id_user_principal() == dtoUser.get_id())
                    {
                        //THis same method does both archiving and un-archiving
                        if (mpb._b_set_archived)
                        {
                            dtoPM.set_dt_archive_date(new Date());
                        }
                        else
                        {
                            dtoPM.set_dt_archive_date(null);
                        }

                        iCount = ProjectMemberDAO.updateMembershipArchive(connection, dtoPM);
                        _LOGGER.info(formatAppEvent(dtoUser, "PROJECT_ARCHIVE", ImmutableMap.of("project_id", String.valueOf(dtoPM.get_id_project()))));
                    }
                    else
                    {
                        throw new Exception("Cannot archive another users membership");
                    }
                }
                catch (Exception ex)
                {
                    SuperDuperDAO.close(connection, false);
                    throw ex;
                }
                finally
                {
                    SuperDuperDAO.close(connection, true);
                }

                if (iCount == 1)
                {
                    response.setStatus(HttpServletResponse.SC_OK);
                    return;
                }
                else
                {
                    //For some reason we could update user
                    _LOGGER.error("Archive Membership Update Failed: iCount=" + iCount);
                    throw new Exception("Archive Membership Update Failed");
                }
            }
            catch (Exception ex)
            {
                //Probably bad input data
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().print(ex.getMessage());
                return;
            }
        }
        else
        {
            //We dont expect this, unless someone is hacking, or the front-end has a bug
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().print("Missing ProjectID data");
            _LOGGER.error("Missing ProjectID data, is there a front-end bug?");
            return;
        }
    }


}
