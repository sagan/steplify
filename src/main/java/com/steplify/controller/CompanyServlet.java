package com.steplify.controller;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.steplify.dao.*;
import com.steplify.dto.*;
import com.steplify.mpb.CompanyTopicTaskExportMPB;
import com.steplify.utility.CodeDecode;
import com.steplify.utility.TransResponse;
import com.steplify.utility.UI;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.util.List;

public class CompanyServlet extends SuperServlet
{
    private static Logger _LOGGER = Logger.getLogger("CompanyServlet");

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //Get the logged in user from SuperServlet > Request
        UserPrincipalDTO loggedInUserDTO = getLoggedInUserDTO(request);

        if (request.getRequestURI().equals("/company/users"))
        {
            TransResponse tr = new TransResponse();
            forwardToCompanyUsersJSP(request, response, tr);
            return;
        }
        else if (request.getRequestURI().equals("/company/topic_task"))
        {
            TransResponse tr = new TransResponse();
            forwardToTopicTaskJSP(request, response, tr);
            return;
        }
        else if (request.getRequestURI().equals("/company/topic_task_export/ajax"))
        {
            CompanyTopicTaskExportMPB mpbExport = new CompanyTopicTaskExportMPB();
            mpbExport._s_cd_project_type = request.getParameter("_s_cd_project_type");

            //This is an AJAX call, so no need for page manipulation
            getTopicTaskTemplates(response, loggedInUserDTO, false, mpbExport);
            return;
        }
        else
        {
            //default page
            TransResponse tr = new TransResponse();
            forwardToCompanyJSP(request, response, tr);
            return;
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //Get the logged in user from SuperServlet > Request
        UserPrincipalDTO loggedInUserDTO = getLoggedInUserDTO(request);

        if (request.getRequestURI().equals("/company/update"))
        {
            String jsonIn = getPostParametersAsJSON(request);

            //When save is complete, leave the user on the same page, in case they need to do more edits
            forwardToCompanyJSP(request, response, null);
            return;
        }
        else if (request.getRequestURI().equals("/company/topic_task_export"))
        {
            String jsonIn = getPostParametersAsJSON(request);
            CompanyTopicTaskExportMPB mpbExport = CompanyTopicTaskExportMPB.fromJson(jsonIn, CompanyTopicTaskExportMPB.class);

            //This is an EXPORT call, so no need for page manipulation
            getTopicTaskTemplates(response, loggedInUserDTO, true, mpbExport);
            return;
        }
        else if (request.getRequestURI().equals("/company/topic_task_import/ajax"))
        {
            String sProjectTypeCD = request.getParameter("_s_cd_project_type");
            String jsonIn = getRequestBodyPayload(request);
            Gson gson = new Gson();
            Type listType = new TypeToken<List<TopicDTO>>() {}.getType();
            List<TopicDTO> lstTopics = gson.fromJson(jsonIn, listType);

            //This is an AJAX call, so no need for page manipulation
            saveTopicTaskTemplates(response, loggedInUserDTO, sProjectTypeCD, lstTopics);
            return;
        }
        else
        {
            throw new RuntimeException("URL mismatch");
        }
    }

    private void forwardToCompanyJSP(HttpServletRequest request, HttpServletResponse response, TransResponse tr) throws ServletException, IOException
    {
        //Get the logged in user from SuperServlet > Request
        UserPrincipalDTO loggedInUserDTO = getLoggedInUserDTO(request);

        //Get the company ID/Name and User
        CompanyDTO dtoCompany = null;
        CompanyUserDTO dtoCompanyUser = null;
        Connection conn = SuperDuperDAO.getConnection();
        try
        {
            //Get the Company the current user is tied to
            dtoCompanyUser = CompanyDAO.selectCompanyUserByUserID(conn, loggedInUserDTO.get_id());
            dtoCompany = CompanyDAO.selectCompanyByID(conn, dtoCompanyUser.get_id_company());

        }
        catch (Exception ex)
        {
            _LOGGER.error(ex.getMessage());
        }
        finally
        {
            SuperDuperDAO.close(conn, false);
        }

        //Return everything to the JSP to render
        request.setAttribute("dtoCompanyUser", dtoCompanyUser);
        request.setAttribute("dtoCompany", dtoCompany);
        request.setAttribute("objTransResponse", tr);
        request.setAttribute("dtoLoggedInUser", loggedInUserDTO);
        request.getRequestDispatcher("/company.jsp").forward(request, response);
    }

    private void forwardToCompanyUsersJSP(HttpServletRequest request, HttpServletResponse response, TransResponse tr) throws ServletException, IOException
    {
        //Get the logged in user from SuperServlet > Request
        UserPrincipalDTO loggedInUserDTO = getLoggedInUserDTO(request);

        //Get the Users for the Company of the current user
        List<UserPrincipalDTO> lstUserPrincipal = null;
        CompanyDTO dtoCompany = null;
        Connection conn = SuperDuperDAO.getConnection();
        try
        {
            //Get all the Users of the Company the current user is tied to
            CompanyUserDTO dtoCompanyUser = CompanyDAO.selectCompanyUserByUserID(conn, loggedInUserDTO.get_id());
            lstUserPrincipal = UserPrincipalDAO.selectUsersByCompanyID(conn, dtoCompanyUser.get_id_company());

            //Get the Company the current user is tied to
            dtoCompany = CompanyDAO.selectCompanyByID(conn, dtoCompanyUser.get_id_company());

        }
        catch (Exception ex)
        {
            _LOGGER.error(ex.getMessage());
        }
        finally
        {
            SuperDuperDAO.close(conn, false);
        }

        //Return everything to the JSP to render
        request.setAttribute("lstUserPrincipal", lstUserPrincipal);
        request.setAttribute("dtoCompany", dtoCompany);
        request.setAttribute("objTransResponse", tr);
        request.setAttribute("dtoLoggedInUser", loggedInUserDTO);
        request.getRequestDispatcher("/company_users.jsp").forward(request, response);
    }

    private void forwardToTopicTaskJSP(HttpServletRequest request, HttpServletResponse response, TransResponse tr) throws ServletException, IOException
    {
        //Get the logged in user from SuperServlet > Request
        UserPrincipalDTO loggedInUserDTO = getLoggedInUserDTO(request);

        //Get the company ID/Name and User
        CompanyDTO dtoCompany = null;
        CompanyUserDTO dtoCompanyUser = null;
        Connection conn = SuperDuperDAO.getConnection();
        try
        {
            //Get the Company the current user is tied to
            dtoCompanyUser = CompanyDAO.selectCompanyUserByUserID(conn, loggedInUserDTO.get_id());
            dtoCompany = CompanyDAO.selectCompanyByID(conn, dtoCompanyUser.get_id_company());
        }
        catch (Exception ex)
        {
            _LOGGER.error(ex.getMessage());
        }
        finally
        {
            SuperDuperDAO.close(conn, false);
        }

        //Get the ProjectTypes
        List<CodeDecodeDTO> alCodesProjectType = CodeDecode.getList(CodeDecode._CAT_PROJECT_TYPE);

        //Return everything to the JSP to render
        request.setAttribute("dtoCompanyUser", dtoCompanyUser);
        request.setAttribute("dtoCompany", dtoCompany);
        request.setAttribute("alCodesProjectType", alCodesProjectType);
        request.setAttribute("objTransResponse", tr);
        request.setAttribute("dtoLoggedInUser", loggedInUserDTO);
        request.getRequestDispatcher("/company_topic_task.jsp").forward(request, response);
    }


    public void getTopicTaskTemplates(HttpServletResponse response, UserPrincipalDTO loggedInUserDTO, boolean bAsFile, CompanyTopicTaskExportMPB mpbExport) throws ServletException, IOException
    {
        try
        {
            //Get the Users for the Company of the current user
            List<TopicDTO> lstTopicTasks = null;

            //Here is where I need to do any last UI minute conversions
            CodeDecodeDTO cdProjectType = CodeDecode.getByValue(CodeDecode._CAT_PROJECT_TYPE, mpbExport._s_cd_project_type);
            if (cdProjectType == null)
            {
                throw new RuntimeException(mpbExport._s_cd_project_type);
            }

            Connection conn = SuperDuperDAO.getConnection();
            try
            {
                //Get all the Topics/Tasks of the Company the current user is tied to
                CompanyUserDTO dtoCompanyUser = CompanyDAO.selectCompanyUserByUserID(conn, loggedInUserDTO.get_id());
                lstTopicTasks =  CompanyDAO.selectCompanyTopicTaskList(conn, dtoCompanyUser.get_id_company(), cdProjectType.get_s_code());
            }
            finally
            {
                SuperDuperDAO.close(conn, false);
            }

            //download as file, vs normal data
            if (bAsFile)
            {
                response.setContentType("application/octet-stream");
                String sFilename = "attachment;filename=" + mpbExport._s_cd_project_type + "_topic_task_template.json";
                response.setHeader("Content-Disposition", sFilename);
            }

            //do the export
            Gson gson = new Gson();
            String json = gson.toJson(lstTopicTasks);
            response.getWriter().print(json);

            _LOGGER.info(formatAppEvent(loggedInUserDTO, "TOPIC_TASK_EXPORT_SUCCESS", null));
            response.setStatus(HttpServletResponse.SC_OK);

        }
        catch (Exception ex)
        {
            //Not good!
            _LOGGER.info(formatAppEvent(loggedInUserDTO, "TOPIC_TASK_EXPORT_FAILED", null));

            //This will take the user to the general error screen
            throw new RuntimeException(ex);
        }
    }

    public void saveTopicTaskTemplates(HttpServletResponse response, UserPrincipalDTO loggedInUserDTO, String sProjectTypeCD, List<TopicDTO> lstTopics) throws ServletException, IOException
    {
        try
        {
            //Here is where I need to do any last UI minute conversions
            CodeDecodeDTO cdProjectType = CodeDecode.getByValue(CodeDecode._CAT_PROJECT_TYPE, sProjectTypeCD);
            if (cdProjectType == null)
            {
                //If it was a bogus ProjectTypeCode, dont proceed
                throw new RuntimeException(sProjectTypeCD);
            }

            int iTotalTasksNew = 0;
            int iTotalTasksOld = 0;
            int iTotalTasksUnused = 0;
            Connection conn = SuperDuperDAO.getConnection();
            try
            {
                //Get the Company the current user is tied to; make sure its a real user assignment
                CompanyUserDTO dtoCompanyUser = CompanyDAO.selectCompanyUserByUserID(conn, loggedInUserDTO.get_id());
                if (!dtoCompanyUser.isAssignedCompanyUser())
                {
                    throw new RuntimeException("Access Denied - you are not a member of this company");
                }

                //Delete the old TopicTaskTemplates for this user's company and ProjectType
                iTotalTasksOld = CompanyDAO.deleteCompanyTopicTaskList(conn, dtoCompanyUser.get_id_company(), cdProjectType.get_s_code());

                int iTopicOrderCount = 10;
                for (TopicDTO dtoTopicTemplate : lstTopics)
                {
                    for (TaskDTO dtoTaskTemplate : dtoTopicTemplate.getTasks())
                    {
                        // convert input into CompanyTopicTaskDTO
                        //Save each one
                        CompanyTopicTaskDTO dtoCTT = new CompanyTopicTaskDTO();
                        dtoCTT.set_id_created_by(loggedInUserDTO.get_id());
                        dtoCTT.set_id_updated_by(loggedInUserDTO.get_id());
                        dtoCTT.set_id_company(dtoCompanyUser.get_id_company());
                        dtoCTT.set_s_cd_project_type(cdProjectType.get_s_code());
                        dtoCTT.set_i_topic_order(iTopicOrderCount);
                        dtoCTT.set_s_topic_name(dtoTopicTemplate.get_s_name());
                        dtoCTT.set_s_task_name(dtoTaskTemplate.get_s_name());

                        if (dtoCTT.isValid())
                        {
                            //Save it, else drop it and keep going
                            CompanyDAO.insertCompanyTopicTask(conn, dtoCTT);
                            iTotalTasksNew++;
                        }
                        else
                        {
                            iTotalTasksUnused++;
                        }
                    }

                    //For each Topic, increment the order and cap it; start at 10 to 94, and default to 95
                    iTopicOrderCount = iTopicOrderCount + 3;
                    if (iTopicOrderCount >= 95)
                    {
                        iTopicOrderCount = 95;
                    }
                }
            }
            catch (Exception ex)
            {
                SuperDuperDAO.close(conn, false);
                throw ex;
            }
            finally
            {
                SuperDuperDAO.close(conn, true);
            }

            //Ship off the 200OK with a useful info message
            response.getWriter().print("Summary - OldTasks="+iTotalTasksOld + ", NewTasks="+iTotalTasksNew + ", UnusedTasks="+iTotalTasksUnused);
            response.setStatus(HttpServletResponse.SC_OK);
            _LOGGER.info(formatAppEvent(loggedInUserDTO, "TOPIC_TASK_SAVE_SUCCESS", null));

        }
        catch (Exception ex)
        {
            //Not good!
            _LOGGER.info(formatAppEvent(loggedInUserDTO, "TOPIC_TASK_SAVE_FAILED", null));

            //This will take the user to the general error screen
            throw new RuntimeException(ex);
        }
    }


}
