package com.steplify.controller;

import org.apache.log4j.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import org.apache.http.impl.client.*;
import org.apache.http.*;
import org.apache.http.client.methods.*;
import org.apache.http.util.*;

public class ProxyServlet extends SuperServlet
{
    private static Logger _LOGGER = Logger.getLogger("ProxyServlet");

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //You must be logged in to even get this far in the stack
        if (request.getRequestURI().equals("/proxy/get"))
        {
            String sURL = request.getParameter("url");
            getRequest(response, sURL);
            //This is an AJAX call, so no need for page manipulation
        }
        else
        {
            throw new RuntimeException("URL mismatch");
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        if (request.getRequestURI().equals("/proxy/*"))
        {
            throw new RuntimeException("Not Implemented");
        }
        else
        {
            throw new RuntimeException("URL mismatch");
        }
    }

    ///////////////////Split out for Unit Testing////////////////////

    public void getRequest(HttpServletResponse response, String sURL) throws ServletException, IOException
    {
        try
        {
            //We need a URL to fetch
            if (sURL == null)
            {
                throw new Exception("No URL was passed in, so proxy cant continue");
            }

            //Lets go get the URL
            DefaultHttpClient httpclient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(sURL);
            HttpResponse theResponse = httpclient.execute(httpGet);

            //Get the content
            HttpEntity theEntity = theResponse.getEntity();
            if (theEntity.getContentLength() > 500000)
            {
                //500KB
                throw new Exception("Content Length too large, not supported");
            }
            String sResponseBody = EntityUtils.toString(theEntity);

            //Send back the content
            response.setStatus(theResponse.getStatusLine().getStatusCode());
            response.setContentType(theEntity.getContentType().getValue());
            response.getWriter().print(sResponseBody);

            //Clean up
            EntityUtils.consume(theEntity);
            httpGet.releaseConnection();
        }
        catch (Exception ex)
        {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().print(ex.getMessage());
            _LOGGER.error(ex.getMessage());
        }
    }


}
