package com.steplify.controller;

import com.steplify.dao.ProjectMemberDAO;
import com.steplify.dao.SuperDuperDAO;
import com.steplify.dto.ProjectMemberDTO;
import com.steplify.dto.UserPrincipalDTO;
import com.steplify.mpb.AuthProjectMemberMPB;
import com.steplify.utility.AppSession;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.*;
import java.sql.Connection;
import java.util.Map;
import com.google.gson.*;

public class SuperServlet extends HttpServlet {

    private static Logger _LOGGER = Logger.getLogger("SuperServlet");

    //DO NOT USE INSTANCE VARs ON A SERVLET, thus we use get/setLoggedInUserDTO()

    //This is where every request starts, so lets put common stuff here
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //For logging the time the request took
        long lTimeStart = System.currentTimeMillis();

        //Lets see if the user is already logged in
        UserPrincipalDTO loggedInUserDTO = AppSession.getCurrentUser(request, response);
        setLoggedInUserDTO(request, loggedInUserDTO);

        //Log the request, and hold the URI for later filtering
        logRequestStart(request, loggedInUserDTO);
        String sURI = request.getRequestURI();

        if (loggedInUserDTO != null)
        {
            try
            {
                super.service(request, response);

                //Log the time the request took
                long lTimeDiff = System.currentTimeMillis() - lTimeStart;
                logRequestEnd(request, loggedInUserDTO, lTimeDiff);
            }
            catch (RuntimeException ex)
            {
                logError(request, loggedInUserDTO, ex);

                //This catch block is to genericize all unexpected 500 RuntimeExceptions
                if (sURI.contains("ajax"))
                {
                    //So the user sees something graceful during an AZAJ flow, as opposed to the error500.jsp content
                    response.reset();
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    response.getWriter().print("An unexpected error occurred, please try again...");
                }
                else
                {
                    //This take you to the web.xml <error-code>500</error-code> handler
                    throw ex;
                }
            }
        }
        else
        {
            //Not logged in, they can only get to these servlet
            if (sURI.startsWith("/login"))
            {
                super.service(request, response);

                //Log the time the request took
                long lTimeDiff = System.currentTimeMillis() - lTimeStart;
                logRequestEnd(request, loggedInUserDTO, lTimeDiff);
            }
            else if (sURI.startsWith("/test"))
            {
                super.service(request, response);
            }
            else
            {
                Exception ex = new Exception("Permission Denied - page/request requires user to be logged in, throwing SecurityException");
                logError(request, loggedInUserDTO, ex);

                //This take you to the web.xml <exception-type>java.lang.SecurityException</exception-type> handler
                throw new SecurityException("Permission Denied - " + sURI);
            }
        }
    }

    ///////////////// Cant use instance variables in a servlet, so tack it on the request

    protected UserPrincipalDTO getLoggedInUserDTO(HttpServletRequest request)
    {
        UserPrincipalDTO dto = (UserPrincipalDTO)request.getAttribute("LOGGED_IN_USER");
        return dto;
    }

    private void setLoggedInUserDTO(HttpServletRequest request, UserPrincipalDTO dto)
    {
        request.setAttribute("LOGGED_IN_USER", dto);
    }

    ///////////////// Handle logging for this class only

    private void logRequestStart(HttpServletRequest request, UserPrincipalDTO dtoUser)
    {
        try
        {
            StringBuilder sbMsg = new StringBuilder();
            sbMsg.append("HTTP_REQ=").append("START").append(", ");
            sbMsg.append("USER=").append(dtoUser).append(", ");
            sbMsg.append("METHOD=").append(request.getMethod()).append(", ");
            sbMsg.append("URI=").append(request.getRequestURI()).append(", ");
            sbMsg.append("QUERY_STRING=").append(request.getQueryString());
            _LOGGER.info(sbMsg.toString());
        }
        catch (Exception ex)
        {
            //Dont let an error happen here, just keep going
        }
    }

    private void logRequestEnd(HttpServletRequest request, UserPrincipalDTO dtoUser, long lTimeMS)
    {
        try
        {
            StringBuilder sbMsg = new StringBuilder();
            sbMsg.append("HTTP_REQ=").append("200OK").append(", ");
            sbMsg.append("USER=").append(dtoUser).append(", ");
            sbMsg.append("METHOD=").append(request.getMethod()).append(", ");
            sbMsg.append("URI=").append(request.getRequestURI()).append(", ");
            sbMsg.append("QUERY_STRING=").append(request.getQueryString()).append(", ");
            sbMsg.append("TIME_MS=").append(lTimeMS);
            _LOGGER.info(sbMsg.toString());
        }
        catch (Exception ex)
        {
            //Dont let an error happen here, just keep going
        }
    }

    private void logError(HttpServletRequest request, UserPrincipalDTO dtoUser, Exception anEX)
    {
        try
        {
            StringBuilder sbMsg = new StringBuilder();
            sbMsg.append("HTTP_REQ=").append("500EX").append(", ");
            sbMsg.append("USER=").append(dtoUser).append(", ");
            sbMsg.append("METHOD=").append(request.getMethod()).append(", ");
            sbMsg.append("URI=").append(request.getRequestURI()).append(", ");
            sbMsg.append("QUERY_STRING=").append(request.getQueryString()).append(", ");
            sbMsg.append("EX_MSG=").append("\"").append(anEX.getMessage()).append("\"");
            _LOGGER.error(sbMsg.toString());
        }
        catch (Exception ex)
        {
            //Dont let an error happen here, just keep going
        }
    }

    ///////////////// Generic Logging for App

    protected String formatAppEvent(UserPrincipalDTO dtoUser, String sEvent, Map<String, String> mapEventArgs)
    {
        StringBuilder sbMsg = new StringBuilder();

        try
        {
            sbMsg.append("APP_EVENT=").append(sEvent).append(", ");
            sbMsg.append("USER=").append(dtoUser).append(", ");

            //Sometimes there isnt too much to report...
            if (mapEventArgs != null)
            {
                for (Map.Entry<String, String> entry : mapEventArgs.entrySet()) {
                    sbMsg.append(entry.getKey()).append("=").append(entry.getValue()).append(", ");
                }
            }

            //So the last KV has no comma
            sbMsg.append("EOM=").append("EOL");
        }
        catch (Exception ex)
        {
            //Dont let an error happen here, just keep going
        }

        return sbMsg.toString();
    }

    //////////////App specific permissions here

    //This is specific to PROJECT MembershipAuthority (as opposed to Topic level authority)
    public enum MembershipAuthEnum
    {
        NONE, MEMBER, ADMIN;
    }

    //Used by most consumers for simple check of authority
    public MembershipAuthEnum authorityCheckForUserAndProject(int iUserID, int iProjectID)
    {
        AuthProjectMemberMPB mpbAuth = getAuthMembershipForUserAndProject(iUserID, iProjectID);
        return mpbAuth.enumAuth;
    }

    //Use directly in case you also need to get the ProjectMemberDTO as a return value
    public AuthProjectMemberMPB getAuthMembershipForUserAndProject(int iUserID, int iProjectID)
    {
        MembershipAuthEnum enumAuth = MembershipAuthEnum.NONE;
        ProjectMemberDTO dtoMember = null;

        //Get a DB connection, do the work, and close it
        Connection connection = SuperDuperDAO.getConnection();
        try
        {
            //And select from the DB
            dtoMember = ProjectMemberDAO.selectMembershipByUserAndProjectID(connection, iUserID, iProjectID);
            if (dtoMember == null || dtoMember.isStatusRemoved())
            {
                //The user has no valid membership on this project
                enumAuth = MembershipAuthEnum.NONE;
            }
            else if(dtoMember.get_b_is_project_admin())
            {
                //The user is an ADMIN, that has NOT been removed from the project
                enumAuth = MembershipAuthEnum.ADMIN;
            }
            else
            {
                //Non ADMINs get the default READ access
                enumAuth = MembershipAuthEnum.MEMBER;
            }
        }
        finally
        {
            SuperDuperDAO.close(connection, false);
        }

        //Create the MPB to return multiple values back to the caller
        AuthProjectMemberMPB mpbAuth = new AuthProjectMemberMPB();
        mpbAuth.enumAuth = enumAuth;
        mpbAuth.dtoProjectMember = dtoMember;

        return mpbAuth;
    }

    //////////////////Need to add common utility methods here

    //Use this when actual JSON is being sent, not form submit parameter data
    public static String getRequestBodyPayload(HttpServletRequest request) throws IOException
    {
        String body = null;
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader theReader = null;

        //If you call request.getParameterMap() before this, request.getInputStream will be empty
        InputStream inputStream = request.getInputStream();
        if (inputStream != null)
        {
            theReader = new BufferedReader(new InputStreamReader(inputStream));
            String sLine = null;
            while ((sLine = theReader.readLine()) != null)
            {
                stringBuilder.append(sLine);
            }
        }

        body = stringBuilder.toString();
        return body;
    }

    //Use this when actual FORM-POST-SUBMIT is sent, but you want it in a JSON format
    @SuppressWarnings("unchecked")
    public static String getPostParametersAsJSON(HttpServletRequest request) throws IOException {
        JsonObject jso = new JsonObject();
        Map<String,String[]> params = request.getParameterMap();

        for (Map.Entry<String,String[]> entry : params.entrySet())
        {
            String v[] = entry.getValue();
            Object o = (v.length == 1) ? v[0] : v;
            jso.addProperty(entry.getKey(), o.toString());
        }

        return jso.toString();
    }


}
