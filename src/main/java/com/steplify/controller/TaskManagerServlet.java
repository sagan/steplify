package com.steplify.controller;

import com.google.common.collect.ImmutableMap;
import com.steplify.dao.*;
import com.steplify.dto.*;
import com.steplify.mpb.*;
import com.steplify.utility.CodeDecode;
import com.steplify.utility.Emailer;
import com.steplify.utility.TransResponse;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.util.*;

public class TaskManagerServlet extends SuperServlet
{
    private static Logger _LOGGER = Logger.getLogger("TaskManagerServlet");

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //Get the logged in user from SuperServlet > Request
        UserPrincipalDTO loggedInUserDTO = getLoggedInUserDTO(request);

        if (request.getRequestURI().equals("/task_manager/view"))
        {
            TaskManagerViewMPB mpbAdminTopicView = new TaskManagerViewMPB();
            String sID = request.getParameter("pid");
            if (sID == null)
            {
                throw new RuntimeException("Missing input PID");
            }

            //Assume we are planning to edit an existing project
            int iProjectID = Integer.parseInt(sID);
            AuthProjectMemberMPB authMPB = getAuthMembershipForUserAndProject(loggedInUserDTO.get_id(), iProjectID);
            MembershipAuthEnum enumAuth = authMPB.enumAuth;
            ProjectMemberDTO dtoMember = authMPB.dtoProjectMember;

            //If a member is INVITED, and they have come to view the project, lets make them ACTIVE.
            TransResponse tr = null;
            if (dtoMember != null && dtoMember.isStatusInvited())
            {
                tr = updateMembershipToActive(dtoMember);
            }
            else
            {
                tr = new TransResponse();
            }

            forwardToBaseJSP(request, response, tr, enumAuth, iProjectID);
            return;
        }
        else if (request.getRequestURI().equals("/task_manager/task/ajax"))
        {
            //This is an AJAX call
            String sTaskID = request.getParameter("tkid");
            int iTaskID = Integer.parseInt(sTaskID);
            getTask(response, loggedInUserDTO, iTaskID);
        }
        else
        {
            throw new RuntimeException("URL mismatch");
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //Get the logged in user from SuperServlet > Request
        UserPrincipalDTO loggedInUserDTO = getLoggedInUserDTO(request);

        if (request.getRequestURI().equals("/admin_topic/task_manager"))
        {
            throw new RuntimeException("Post not implemented");
        }
        else if (request.getRequestURI().equals("/task_manager/task_andor_comment/ajax"))
        {
            //This is an AJAX call
            String jsonIn = getRequestBodyPayload(request);
            TaskMPB mpbTask = TaskMPB.fromJson(jsonIn, TaskMPB.class);
            TaskDTO dtoTask = addUpdateTask(response, loggedInUserDTO, mpbTask);

            //If the Task was created successfully, and there is a comment, then lets add the Comment
            if (dtoTask != null && mpbTask._s_comment != null && !mpbTask._s_comment.trim().isEmpty())
            {
                TaskCommentDTO dtoComment = new TaskCommentDTO();
                dtoComment.set_id_task(dtoTask.get_id());
                dtoComment.set_s_comment(mpbTask._s_comment);
                addComment(response, loggedInUserDTO, dtoComment);
            }
        }
        else
        {
            throw new RuntimeException("URL mismatch");
        }
    }

    private void forwardToBaseJSP(HttpServletRequest request, HttpServletResponse response, TransResponse tr, MembershipAuthEnum enumAuth, int iProjectID) throws ServletException, IOException
    {
        //Get the logged in user from SuperServlet > Request
        UserPrincipalDTO loggedInUserDTO = getLoggedInUserDTO(request);

        _LOGGER.info(formatAppEvent(loggedInUserDTO, "TASK_MANAGER_VIEW", ImmutableMap.of("project_id", String.valueOf(iProjectID))));

        TaskManagerViewMPB mpbTaskManagerView = getFullProjectData(enumAuth, loggedInUserDTO, iProjectID);

        //Lets remove any topics this user is not allowed to see, to fail-safe the JSP to not show the wrong topics
        List<TopicDTO> lstTopicsAllowedToSee = getTopicsUserIsAllowedToSee(loggedInUserDTO, mpbTaskManagerView.lstTopics);
        mpbTaskManagerView.lstTopics = lstTopicsAllowedToSee;

        //Get the necessary CodeDecodes for the UI
        mpbTaskManagerView.alCodesTaskStatus = CodeDecode.getList(CodeDecode._CAT_TASK_STATUS);
        mpbTaskManagerView.mapCDsTopicAuthority = CodeDecode.getLookupMap(CodeDecode._CAT_TOPIC_AUTHORITY);

        //See if we should filter on a specific user
        try
        {
            String sFilterUserID = request.getParameter("filter_user_id");
            if (sFilterUserID != null)
            {
                mpbTaskManagerView.iFilterUserID = Integer.parseInt(sFilterUserID);
            }
        }
        catch (Exception ex)
        {
            //Just use the default, and no one will be filtered out
            mpbTaskManagerView.iFilterUserID = 0;
        }

        //Return everything to the JSP to render
        request.setAttribute("mpbTaskManagerView", mpbTaskManagerView);
        request.setAttribute("objTransResponse", tr);
        request.setAttribute("dtoLoggedInUser", loggedInUserDTO);
        request.getRequestDispatcher("/task_manager.jsp").forward(request, response);
    }

    ///////////////////Split out for Unit Testing////////////////////

    public TaskManagerViewMPB getFullProjectData(MembershipAuthEnum enumAuth, UserPrincipalDTO dtoUser,  int iProjectID)
    {
        if (enumAuth == MembershipAuthEnum.NONE)
        {
            String sMsg = "Access Denied - Membership for UserID=" + dtoUser.get_id() + " for ProjectID=" + iProjectID;
            _LOGGER.error(sMsg);
            throw new RuntimeException(sMsg);
        }

        TaskManagerViewMPB mpbTaskManagerView = new TaskManagerViewMPB();

        //Get a DB connection, do the work, and close it
        Connection connection = SuperDuperDAO.getConnection();
        try
        {
            //Get the project from the DB
            mpbTaskManagerView.dtoProject = ProjectDAO.selectProjectWithMembersByID(connection, iProjectID);
            if (mpbTaskManagerView.dtoProject == null)
            {
                //For some reason we could not insert the new project
                _LOGGER.error("Project could not be found=" + iProjectID);
                //Do not proceed
                throw new RuntimeException("Project could not be found, please try again");
            }

            //Get the Topics for the Project
            mpbTaskManagerView.lstTopics = TopicDAO.selectTopicsWithMembersForProject(connection, iProjectID);
            if (mpbTaskManagerView.lstTopics == null)
            {
                //For some reason we got NULL back, instead of a collection type
                _LOGGER.error("There were null topics = " + iProjectID);
                //Do not proceed
                throw new RuntimeException("There were null topics, this was unexpected");
            }
            else
            {
                //Looks like we got what we need, lets go...
                //Its ok to have empty list of topics, maybe they just created the project
            }

            //Get the TASKS for each TOPIC
            mpbTaskManagerView.mapTopicTasks = new HashMap<Integer, List<TaskDTO>>();
            for (TopicDTO dtoTopic : mpbTaskManagerView.lstTopics)
            {
                int iTopicID = dtoTopic.get_id();
                List<TaskDTO> lstTasks = TaskDAO.selectTasksWithCommentsForTopic(connection, iTopicID);
                if (lstTasks == null)
                {
                    //For some reason we got NULL back, instead of a collection type
                    _LOGGER.error("There were null tasks = " + iTopicID);
                    //Do not proceed
                    throw new RuntimeException("There were null tasks, this was unexpected");
                }
                else
                {
                    //Looks like we got what we need, lets go...
                    //Its ok to have empty list of tasks for a topic, maybe they just created the topic
                }
                mpbTaskManagerView.mapTopicTasks.put(iTopicID, lstTasks);
            }
        }
        finally
        {
            SuperDuperDAO.close(connection, false);
        }

        return mpbTaskManagerView;
    }

    private List<TopicDTO> getTopicsUserIsAllowedToSee(UserPrincipalDTO dtoUser, List<TopicDTO> lstTopics)
    {
        //Not the most efficient, but functional
        List<TopicDTO> lstTopicsAllowedToSee = new ArrayList<TopicDTO>();

        for (TopicDTO dtoTopic : lstTopics)
        {
            List<TopicMemberDTO> dtoMemberships = dtoTopic.getTopicMembers();
            for (TopicMemberDTO dtoTM : dtoMemberships)
            {
                if (dtoTM.get_id_user_principal() == dtoUser.get_id())
                {
                    if (dtoTM.isAuthorityNone())
                    {
                        //Dont add it to the new list, they have no permission to view this topic
                    }
                    else
                    {
                        //Add it to the list, since they have read or write permission
                        lstTopicsAllowedToSee.add(dtoTopic);
                        //No need to look at more memberships within this topic
                        break;
                    }
                }
            }
        }

        return lstTopicsAllowedToSee;
    }


    public void getTask(HttpServletResponse response, UserPrincipalDTO dtoUser, int iTaskID) throws ServletException, IOException
    {
        try
        {
            //Get a DB connection, do the work, and close it
            Connection connection = SuperDuperDAO.getConnection();
            try
            {
                //Lets get the task from the DB
                TaskDTO dtoTask = TaskDAO.selectTaskByID(connection, iTaskID);

                //Make sure the user is allowed to at least view the topic
                TopicMemberDTO dtoTopicMember = TopicMemberDAO.selectTopicMemberByUserAndTopic(connection, dtoUser.get_id(), dtoTask.get_id_topic());
                if (dtoTopicMember == null || dtoTopicMember.isAuthorityNone())
                {
                    String sMsg = "Access Denied - Topic read by UserID=" + dtoUser.get_id() + " on TaskID=" + iTaskID;
                    _LOGGER.error(sMsg);
                    throw new RuntimeException("Access Denied, you don't have any permission for this topic");
                }

                //Make sure the user is allowed access the project this task is tied to; since they may be REMOVED
                MembershipAuthEnum enumAuth = authorityCheckForUserAndProject(dtoUser.get_id(), dtoTask.get_id_project());
                if (enumAuth == MembershipAuthEnum.NONE)
                {
                    String sMsg = "Access Denied - Membership for UserID=" + dtoUser.get_id() + " for ProjectID=" + dtoTask.get_id_project();
                    _LOGGER.error(sMsg);
                    throw new RuntimeException(sMsg);
                }

                //Convert the DTO to an MPB, so the UI can handle it
                TaskMPB mpbTaskOut = new TaskMPB();
                mpbTaskOut._id = dtoTask.get_id();
                mpbTaskOut._id_topic = dtoTask.get_id_topic();
                mpbTaskOut._s_name = dtoTask.get_s_name();
                mpbTaskOut._s_description = dtoTask.get_s_description();

                //Turn the status code into a value
                CodeDecodeDTO cdTaskStatusStatus = CodeDecode.getByCode(CodeDecode._CAT_TASK_STATUS, dtoTask.get_s_cd_status());
                if (cdTaskStatusStatus != null)
                {
                    mpbTaskOut._s_cd_status = cdTaskStatusStatus.get_s_decode();
                }
                else
                {
                    mpbTaskOut._s_cd_status = null;
                }

                //Get the Username from the assigned UserID
                UserPrincipalDTO dtoUP = UserPrincipalDAO.selectUserByID(connection, dtoTask.get_id_assigned_user());
                if (dtoUP != null)
                {
                    mpbTaskOut._s_assigned_user = dtoUP.get_s_username();
                }
                else
                {
                    mpbTaskOut._s_assigned_user = null;
                }

                //Convert the DueDate to a String the HTML5 UI can handle
                try
                {
                    mpbTaskOut._s_dt_due = new java.text.SimpleDateFormat("yyyy-MM-dd").format(dtoTask.get_dt_due());
                }
                catch (Exception ex)
                {
                    mpbTaskOut._s_dt_due = null;
                }

                String jsonOut = mpbTaskOut.toJSON();

                //response.setContentType("application/json");
                response.getWriter().print(jsonOut);
                response.setStatus(HttpServletResponse.SC_OK);
            }
            finally
            {
                SuperDuperDAO.close(connection, false);
            }
        }
        catch (Exception ex)
        {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().print(ex.getMessage());
        }
    }


    public TaskDTO addUpdateTask(HttpServletResponse response, UserPrincipalDTO dtoUser, TaskMPB mpbTask) throws ServletException, IOException
    {
        //The return object
        TaskDTO dtoTask = null;

        try
        {
            //The task should never be sent as null
            if (mpbTask == null)
            {
                //For some reason we got a null object
                _LOGGER.error("Posted task object was null");
                throw new Exception("Posted task object was null");
            }

            //Get a DB connection, do the work, and close it
            Connection connection = SuperDuperDAO.getConnection();
            try
            {
                if (mpbTask._id != 0)
                {
                    //This is an UPDATE
                    //If a TaskID was given, assume its already in the DB; lets go get it
                    dtoTask = TaskDAO.selectTaskByID(connection, mpbTask._id);
                    dtoTask.set_id_updated_by(dtoUser.get_id());
                }
                else
                {
                    //This is an ADD
                    dtoTask = new TaskDTO();
                    dtoTask.set_id_topic(mpbTask._id_topic);
                    dtoTask.set_id_created_by(dtoUser.get_id());
                    dtoTask.set_id_updated_by(dtoUser.get_id());
                }

                if (dtoTask.get_id_topic() == 0)
                {
                    //Either the are hacking AJAX, or the GET request originally failed
                    String sMsg = "Missing TopicID, close form and please try again";
                    _LOGGER.error(sMsg);
                    throw new RuntimeException(sMsg);
                }

                //Make sure the user is allowed to update the topic
                TopicMemberDTO dtoTopicMember = TopicMemberDAO.selectTopicMemberByUserAndTopic(connection, dtoUser.get_id(), dtoTask.get_id_topic());
                if (dtoTopicMember != null && dtoTopicMember.isAuthorityWriter())
                {
                    //Logged in user has write access on the passed in topic
                    //Great, lets proceed!!!
                    dtoTask.set_id_project(dtoTopicMember.get_id_project());
                }
                else
                {
                    String sMsg = "Access Denied - Topic update by UserID=" + dtoUser.get_id() + " on TopicID=" + dtoTask.get_id_topic();
                    _LOGGER.error(sMsg);
                    throw new RuntimeException("Access Denied, you don't have write permission for this topic");
                }

                //Make sure the user is allowed access the project this task is tied to; since they may be REMOVED
                MembershipAuthEnum enumAuth = authorityCheckForUserAndProject(dtoUser.get_id(), dtoTask.get_id_project());
                if (enumAuth == MembershipAuthEnum.NONE)
                {
                    String sMsg = "Access Denied - Membership for UserID=" + dtoUser.get_id() + " for ProjectID=" + dtoTask.get_id_project();
                    _LOGGER.error(sMsg);
                    throw new RuntimeException(sMsg);
                }

                //Lets take the values from th UI, and set them up for the DB
                dtoTask.set_s_name(mpbTask._s_name);
                dtoTask.set_s_description(mpbTask._s_description);

                //Set the task status from the DDL
                CodeDecodeDTO cdTaskStatusStatus = CodeDecode.getByValue(CodeDecode._CAT_TASK_STATUS, mpbTask._s_cd_status);
                if (cdTaskStatusStatus != null)
                {
                    dtoTask.set_s_cd_status(cdTaskStatusStatus.get_s_code());
                }
                else
                {
                    dtoTask.set_s_cd_status(null);
                }

                //Set the assigned user from the DDL, we can only accept members of this task's project
                Map<String, ProjectMemberDTO> mapProjectMembers = getProjectMembershipMapBysUsername(connection, dtoTask.get_id_project());
                ProjectMemberDTO dtoPM = mapProjectMembers.get(mpbTask._s_assigned_user);
                if (dtoPM != null)
                {
                    dtoTask.set_id_assigned_user(dtoPM.get_id_user_principal());
                }
                else
                {
                    //This wont pass validation, so a warning message will appear on the UI
                    dtoTask.set_id_assigned_user(0);
                }

                //Set the date, from the HTML5 date input; value could be emptyString, but we expect "yyyy-MM-DD"
                try
                {
                    //http://stackoverflow.com/questions/4216745/java-string-to-date-conversion
                    Date dateInput = new java.text.SimpleDateFormat("yyyy-MM-dd").parse(mpbTask._s_dt_due);
                    //This created a date assuming a GMT timezone
                    dtoTask.set_dt_due(dateInput);

                    //Do some date range validation
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(dateInput);
                    int calYear = cal.get(Calendar.YEAR);
                    if (calYear < 1970 || calYear > 2099)
                    {
                        dtoTask.set_dt_due(null);
                    }
                }
                catch (Exception ex)
                {
                    //Something was not right, probably an unexpected format, or emptyString (i.e. user did not select a date)
                    dtoTask.set_dt_due(null);

                    //I would like to see what people from around the world, what their browser sends
                    _LOGGER.error("Could not parse HTML5 date input string = '" + mpbTask._s_dt_due + "'");
                }

                TransResponse tr = new TransResponse();
                dtoTask.validateFields(tr);
                if (tr.hasExceptions())
                {
                    //Just send back the first error, even if there are many; the UI cant handle many in this case
                    throw new Exception(tr.getExceptions().get(0));
                }

                int iCount = 0;
                //Insert or Update the Task to the DB
                if (dtoTask.get_id() == 0)
                {
                    iCount = TaskDAO.insertTask(connection, dtoTask);
                    _LOGGER.info(formatAppEvent(dtoUser, "TASK_NEW", ImmutableMap.of("project_id", String.valueOf(dtoTask.get_id_project()))));
                }
                else
                {
                    iCount = TaskDAO.updateTask(connection, dtoTask);
                    _LOGGER.info(formatAppEvent(dtoUser, "TASK_UPDATE", ImmutableMap.of("project_id", String.valueOf(dtoTask.get_id_project()))));
                }

                if (iCount == 1)
                {
                    response.setStatus(HttpServletResponse.SC_OK);
                    //No longer sending email on add/update, too noisy, just on comments
                }
                else
                {
                    //For some reason we couldnt insert the user
                    throw new Exception("Task insert/update failed");
                }
            }
            catch (Exception ex)
            {
                SuperDuperDAO.close(connection, false);

                //SQLIntegrityConstraintViolationException doesnt get thrown
                if (ex.getMessage().toLowerCase().contains("duplicate"))
                {
                    throw new Exception("Task name already exists in this topic");
                }
                else
                {
                    _LOGGER.error(ex.getMessage());
                    throw ex;
                }
            }
            finally
            {
                SuperDuperDAO.close(connection, true);
            }
        }
        catch (Exception ex)
        {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().print(ex.getMessage());
            return null;
        }

        return dtoTask;
    }


    private Map<String, ProjectMemberDTO> getProjectMembershipMapBysUsername(Connection connection, int iProjectID)
    {
        Map<String, ProjectMemberDTO> mapPMs = new HashMap<String, ProjectMemberDTO>();

        //And select from the DB
        List<ProjectMemberDTO> lstPMs = ProjectMemberDAO.selectUserMembershipsForProject(connection, iProjectID);

        //Put all the ProjectMembers into a map, that we can key on username
        for (ProjectMemberDTO pm: lstPMs)
        {
            mapPMs.put(pm.getUserPrincipal().get_s_username(), pm);
        }

        return mapPMs;
    }

    public void addComment(HttpServletResponse response, UserPrincipalDTO dtoUser, TaskCommentDTO dtoComment) throws ServletException, IOException
    {
        try
        {
            //The comment should never be sent as null
            if (dtoComment == null)
            {
                //For some reason we got a null object
                _LOGGER.error("Posted comment object was null");
                throw new Exception("Posted comment object was null");
            }

            //Get a DB connection, do the work, and close it
            Connection connection = SuperDuperDAO.getConnection();
            try
            {
                if (dtoComment.get_id_task() == 0)
                {
                    //Either the are hacking AJAX, or the GET request originally failed
                    String sMsg = "Missing TaskID, close form and please try again";
                    _LOGGER.error(sMsg);
                    throw new RuntimeException(sMsg);
                }

                //Get the task they are commenting on out of the DB
                TaskDTO dtoTask = TaskDAO.selectTaskByID(connection, dtoComment.get_id_task());

                //Make sure the user is allowed to update the topic
                TopicMemberDTO dtoTopicMember = TopicMemberDAO.selectTopicMemberByUserAndTopic(connection, dtoUser.get_id(), dtoTask.get_id_topic());
                if (dtoTopicMember != null && dtoTopicMember.isAuthorityWriter())
                {
                    //Logged in user has write access on the passed in topic
                    //Great, lets proceed!!!
                    //dtoTask.set_id_project(dtoTopicMember.get_id_project());
                }
                else
                {
                    String sMsg = "Access Denied - Topic update by UserID=" + dtoUser.get_id() + " on TopicID=" + dtoTask.get_id_topic();
                    _LOGGER.error(sMsg);
                    throw new RuntimeException("Access Denied, you don't have write permission for this topic");
                }

                //Make sure the user is allowed access the project this task is tied to; since they may be REMOVED
                MembershipAuthEnum enumAuth = authorityCheckForUserAndProject(dtoUser.get_id(), dtoTask.get_id_project());
                if (enumAuth == MembershipAuthEnum.NONE)
                {
                    String sMsg = "Access Denied - Membership for UserID=" + dtoUser.get_id() + " for ProjectID=" + dtoTask.get_id_project();
                    _LOGGER.error(sMsg);
                    throw new RuntimeException(sMsg);
                }

                //Lets take the values from th UI, and set them up for the DB
                dtoComment.set_id_project(dtoTask.get_id_project());
                dtoComment.set_id_created_by(dtoUser.get_id());
                dtoComment.set_id_updated_by(dtoUser.get_id());

                TransResponse tr = new TransResponse();
                dtoComment.validateFields(tr);
                if (tr.hasExceptions())
                {
                    //Just send back the first error, even if there are many; the UI cant handle many in this case
                    throw new Exception(tr.getExceptions().get(0));
                }

                int iCount = 0;
                //Insert Comment to the DB
                iCount = TaskCommentDAO.insertTaskComment(connection, dtoComment);
                _LOGGER.info(formatAppEvent(dtoUser, "COMMENT", ImmutableMap.of("project_id", String.valueOf(dtoComment.get_id_project()))));

                if (iCount == 1)
                {
                    response.setStatus(HttpServletResponse.SC_OK);
                    sendTaskCommentEmail(connection, dtoUser, dtoTask, dtoComment);
                }
                else
                {
                    //For some reason we couldnt insert the user
                    _LOGGER.error("Comment insert failed");
                    throw new Exception("Comment insert failed");
                }
            }
            finally
            {
                SuperDuperDAO.close(connection, true);
            }
        }
        catch (Exception ex)
        {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().print(ex.getMessage());
        }
    }


    private TransResponse updateMembershipToActive(ProjectMemberDTO dtoMember)
    {
        TransResponse tr = new TransResponse();
        try
        {

            dtoMember.setStatusActive();

            //See if the membership is valid
            dtoMember.validateFieldsAll(tr);

            //So far so good, lets save it up
            if (tr.isSuccess())
            {
                //Get a DB connection, do the work, and close it
                Connection connection = SuperDuperDAO.getConnection();
                try
                {
                    //And update the DB
                    int iCountDB = ProjectMemberDAO.updateMembershipStatus(connection, dtoMember);

                    if (iCountDB != 1)
                    {
                        //For some reason we could not insert the new topic
                        _LOGGER.error("Membership update failed: iCountDB=" + iCountDB);
                        //Do not proceed
                        throw new Exception("Membership could not be activated");
                    }
                    else
                    {
                        //The user has changed from INVITED to ACTIVE
                        //Send email here to: admins, referral, etc
                        sendProjectMembershipActivatedEmail(connection, dtoMember);
                    }
                }
                catch (Exception ex)
                {
                    SuperDuperDAO.close(connection, false);
                    throw ex;
                }
                finally
                {
                    SuperDuperDAO.close(connection, true);
                }
            }
        }
        catch (Exception ex)
        {
            //Hopefully just someone hacking around
            tr.addException(ex.getMessage());
        }
        finally
        {
            return tr;
        }
    }

    ////////////////Email related methods

    private void sendTaskCommentEmail(Connection connection, UserPrincipalDTO dtoUpdatingUser, TaskDTO dtoTask, TaskCommentDTO dtoComment)
    {
        try
        {
            //Get the project
            ProjectDTO dtoProject = ProjectDAO.selectProjectWithMembersByID(connection, dtoTask.get_id_project());

            //Derive the email address TO list
            ArrayList<String> alTo = new ArrayList<String>();

            Map<Integer, ProjectMemberDTO> mapPMs = new HashMap<Integer, ProjectMemberDTO>();
            for (ProjectMemberDTO pm: dtoProject.getMembers())
            {
                //Derive a map for quick access
                mapPMs.put(pm.getUserPrincipal().get_id(), pm);
            }

            List<TopicMemberDTO> alTMs = TopicMemberDAO.selectTopicMembersByTopic(connection, dtoTask.get_id_topic());
            for (TopicMemberDTO dtoTM : alTMs)
            {
                ProjectMemberDTO dtoPM = mapPMs.get(dtoTM.get_id_user_principal());
                if (!dtoTM.isAuthorityNone() && !dtoPM.isStatusRemoved())
                {
                    alTo.add(dtoPM.getUserPrincipal().get_s_username());
                }
            }

            //Get the Topical info
            TopicDTO dtoTopic = TopicDAO.selectTopicByID(connection, dtoTask.get_id_topic());

            //Send the actual email
            Emailer.sendTaskComment(alTo, dtoUpdatingUser, dtoProject, dtoTopic, dtoTask, dtoComment);
        }
        catch (Exception ex)
        {
            //Just log it, but dont re-throw it; we dont want an email to stop the app from functioning
            _LOGGER.error(ex.getMessage());
        }
    }


    private void sendProjectMembershipActivatedEmail(Connection connection, ProjectMemberDTO dtoMember)
    {
        try
        {
            //Get the project
            ProjectDTO dtoProject = ProjectDAO.selectProjectWithMembersByID(connection, dtoMember.get_id_project());

            //Derive the email address TO list
            ArrayList<String> alTo = new ArrayList<String>();

            Map<Integer, ProjectMemberDTO> mapPMs = new HashMap<Integer, ProjectMemberDTO>();
            for (ProjectMemberDTO dtoPM: dtoProject.getMembers())
            {
                //We dont email anyone who is removed from the project, ever if they were the original referral party
                if (!dtoPM.isStatusRemoved())
                {
                    //Send to ADMINs, and the referring party
                    if (dtoPM.get_b_is_project_admin() || (dtoPM.get_id_user_principal() == dtoMember.get_id_created_by()))
                    {
                        alTo.add(dtoPM.getUserPrincipal().get_s_username());
                    }
                }

                //Derive a map for quick access, when we are done looping
                mapPMs.put(dtoPM.getUserPrincipal().get_id(), dtoPM);
            };

            //Get the actual users involved
            UserPrincipalDTO dtoUserActive = mapPMs.get(dtoMember.get_id_user_principal()).getUserPrincipal();
            UserPrincipalDTO dtoUserReferral = mapPMs.get(dtoMember.get_id_created_by()).getUserPrincipal();

            //Send the actual email
            Emailer.sendProjectMembershipActivated(alTo, dtoUserActive, dtoUserReferral, dtoProject);
        }
        catch (Exception ex)
        {
            //Just log it, but dont re-throw it; we dont want an email to stop the app from functioning
            _LOGGER.error(ex.getMessage());
        }
    }



}
