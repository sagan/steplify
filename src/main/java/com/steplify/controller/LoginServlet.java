package com.steplify.controller;

import java.sql.*;
import com.steplify.dao.*;
import com.steplify.dto.*;
import com.steplify.mpb.*;
import com.steplify.utility.*;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import com.google.common.collect.ImmutableMap;

public class LoginServlet extends SuperServlet
{
    private static Logger _LOGGER = Logger.getLogger("LoginServlet");

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //Get the logged in user from SuperServlet > Request
        UserPrincipalDTO loggedInUserDTO = getLoggedInUserDTO(request);

        if (request.getRequestURI().equals("/login/logout/ajax"))
        {
            //This is an AJAX call, so no need for page manipulation
            AppSession.logout(request, response);
            response.setStatus(HttpServletResponse.SC_OK);
            return;
        }
        else if (request.getRequestURI().equals("/login/terms"))
        {
            request.getRequestDispatcher("/terms.jsp").forward(request, response);
            return;
        }
        else
        {
            //Check to see if the current user is already logged in
            if (loggedInUserDTO == null)
            {
                //Check if the request was to SIGNUP directly, else default to LOGIN TAB
                String sSignUp = request.getParameter("signup");
                if ("true".equals(sSignUp))
                {
                    //Set the Javascript/CSS var on the REGISTER tab
                    request.setAttribute("activate_register","active");
                }
                else
                {
                    //Set the Javascript/CSS var on the LOGIN tab
                    request.setAttribute("activate_login","active");
                }
                request.getRequestDispatcher("/login.jsp").forward(request, response);
            }
            else
            {
                //Assume the user was already logged in, default destination
                response.sendRedirect("/dashboard");
            }
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        if (request.getRequestURI().equals("/login/user/ajax"))
        {
            String jsonIn = getRequestBodyPayload(request);
            LoginRegisterMPB mpb = LoginRegisterMPB.fromJson(jsonIn, LoginRegisterMPB.class);
            UserPrincipalDTO dto = loginUser(response, mpb);
            AppSession.login(dto, request, response);
        }
        else if (request.getRequestURI().equals("/login/register/ajax"))
        {
            String jsonIn = getRequestBodyPayload(request);
            LoginRegisterMPB mpb = LoginRegisterMPB.fromJson(jsonIn, LoginRegisterMPB.class);
            UserPrincipalDTO dto = registerNewUser(response, mpb);
            AppSession.logout(request, response);
        }
        else if (request.getRequestURI().equals("/login/lostpassword/ajax"))
        {
            String jsonIn = getRequestBodyPayload(request);
            LoginRegisterMPB mpb = LoginRegisterMPB.fromJson(jsonIn, LoginRegisterMPB.class);
            lostPassword(response, mpb);
            AppSession.logout(request, response);
        }
        else
        {
            throw new RuntimeException("URL mismatch");
        }
    }

    ///////////////////Split out for Unit Testing////////////////////

    public UserPrincipalDTO loginUser(HttpServletResponse response, LoginRegisterMPB mpb) throws ServletException, IOException
    {
        //Make sure we have the minimum info to check
        if (mpb._s_email != null && mpb._s_password != null)
        {
            try
            {
                //Using the DTO class to apply business rule checks, i.e. lowercase()
                UserPrincipalDTO dto = new UserPrincipalDTO();
                dto.setNewUsername(mpb._s_email);

                Connection connection = SuperDuperDAO.getConnection();
                try
                {
                    //Go get the user from the DB
                    dto = UserPrincipalDAO.selectUserByName(connection, dto.get_s_username());
                }
                finally
                {
                    SuperDuperDAO.close(connection, false);
                }

                if (dto != null)
                {
                    boolean bPassMatch = dto.verifyPassword(mpb._s_password);
                    if (bPassMatch)
                    {
                        boolean bAccountActive = dto.isAccountActive();
                        if (bAccountActive)
                        {
                            //Send the JS browser to the dashboard page
                            _LOGGER.info(formatAppEvent(dto, "LOGIN_SUCCESS", null));
                            response.setStatus(HttpServletResponse.SC_OK);
                            response.getWriter().print("/dashboard");
                            return dto;
                        }
                        else
                        {
                            //The account is locked
                            _LOGGER.info(formatAppEvent(dto, "LOGIN_LOCKED", null));
                            throw new Exception("This account is locked.");
                        }
                    }
                    else
                    {
                        //NO MATCH ON PASS - Sleep to minimize brute force attack
                        doSleep();
                        //The password didnt match
                        throw new Exception("Email/Password combination invalid.");
                    }
                }
                else
                {
                    //NO MATCH ON EMAIL - Sleep to minimize brute force attack
                    doSleep();
                    //We couldnt find the user/email address
                    throw new Exception("Email/Password combination invalid.");
                }
            }
            catch (Exception ex)
            {
                //Probably bad input data - email address format, or a short password
                _LOGGER.info(formatAppEvent(null, "LOGIN_FAILED", ImmutableMap.of("username", mpb._s_email)));
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().print(ex.getMessage());
                return null;
            }
        }
        else
        {
            //We dont expect this, unless someone is hacking, or the front-end has a bug
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().print("Missing Email/Password data");
            _LOGGER.error("Missing Email/Password data, is there a front-end bug?");
            return null;
        }
    }

    public UserPrincipalDTO registerNewUser(HttpServletResponse response, LoginRegisterMPB mpb) throws ServletException, IOException
    {

        //Make sure the passwords match
        if (mpb._s_email != null && mpb._s_postal_code != null)
        {
            _LOGGER.info(formatAppEvent(null, "REGISTRATION_ATTEMPT", ImmutableMap.of("username", mpb._s_email)));
            UserPrincipalDTO dto = new UserPrincipalDTO();

            try
            {
                //In case we ever need to turn off new user self registrations
                String sSelfRegOn;
                try
                {
                    sSelfRegOn = getServletContext().getInitParameter("self_registration_on");
                }
                catch (Exception ex)
                {
                    //So that unit test can continue to work, and if the app is mis-configured
                    sSelfRegOn = "true";
                }

                //Lets see if we allow it
                if ("false".equals(sSelfRegOn))
                {
                    String sMsg = "RequestBy: " + mpb._s_email + " - zip:" + mpb._s_postal_code;
                    Emailer.sendRegistrationRequest(sMsg);
                    throw new Exception("Thanks, we will be in touch shortly....");
                }

                //User doesnt pick the initial password during registration; we will generate it, just like lostPassword
                dto.setNewUsername(mpb._s_email);
                String sNewPass = dto.generateNewPasswordBackup();
                dto.setStatusActive();

                //Check the zipcode is not empty
                if (mpb._s_postal_code.trim().isEmpty())
                {
                    throw new Exception("Missing Postal/Zip code");
                }

                UserProfileDTO profileDTO = new UserProfileDTO();
                profileDTO.set_s_postal_code(mpb._s_postal_code);
                dto.setUserProfile(profileDTO);

                //Inputs look good, READY TO REG - Sleep to minimize brute force attack
                doSleep();

                int iCount = 0;

                //Get a DB connection, do the work, and close it
                Connection connection = SuperDuperDAO.getConnection();
                try
                {
                    iCount = UserPrincipalDAO.insert(connection, dto);
                    Emailer.sendPasswordSelfRegistration(dto.get_s_username(), sNewPass);
                }
                catch (Exception ex)
                {
                    SuperDuperDAO.close(connection, false);
                    throw ex;
                }
                finally
                {
                    SuperDuperDAO.close(connection, true);
                }

                if (iCount == 1)
                {
                    //response.setContentType("application/json");
                    response.setStatus(HttpServletResponse.SC_OK);
                    response.getWriter().print("Your account is being created, an email will be sent shortly.");
                    return dto;
                }
                else
                {
                    //For some reason we could insert the new user
                    throw new Exception("Registration Failed, please try again.");
                }
            }
            catch (Exception ex)
            {
                //SQLIntegrityConstraintViolationException doesnt get thrown
                if (ex.getMessage().toLowerCase().contains("duplicate"))
                {
                    //com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException: Duplicate entry '13176370_abc@foo.com' for key '_s_username_UNIQUE'
                    //gets converted to
                    //java.lang.RuntimeException: Duplicate entry '13176370_abc@foo.com' for key '_s_username_UNIQUE'
                    response.getWriter().print("Email address already registered");
                }
                else
                {
                    _LOGGER.error(ex.getMessage());
                    response.getWriter().print(ex.getMessage());
                }

                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return null;
            }
        }
        else
        {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().print("Missing Registration data");
            _LOGGER.error("Missing Registration data, is there a front-end bug?");
            return null;
        }
    }

    public UserPrincipalDTO lostPassword(HttpServletResponse response, LoginRegisterMPB mpb) throws ServletException, IOException
    {
        //Make sure we have the minimum info to check
        if (mpb._s_email != null)
        {
            _LOGGER.info(formatAppEvent(null, "LOST_PASSWORD", ImmutableMap.of("username", mpb._s_email)));

            try
            {
                //Using the DTO class to apply business rule checks, i.e. lowercase()
                UserPrincipalDTO dto = new UserPrincipalDTO();
                dto.setNewUsername(mpb._s_email);

                //Inputs look good, READY TO RESET_PASS - Sleep to minimize brute force attack
                doSleep();

                //Get a DB connection, do the work, and close it
                Connection connection = SuperDuperDAO.getConnection();
                try
                {
                    dto = UserPrincipalDAO.selectUserByName(connection, dto.get_s_username());

                    if (dto != null)
                    {
                        String sNewPass = dto.generateNewPasswordBackup();
                        UserPrincipalDAO.updateUserBackupPassword(connection, dto);
                        Emailer.sendPasswordReset(dto.get_s_username(), sNewPass);
                    }
                }
                catch (Exception ex)
                {
                    SuperDuperDAO.close(connection, false);
                    throw ex;
                }
                finally
                {
                    SuperDuperDAO.close(connection, true);
                }

                //Lets always send back a generic message
                response.setStatus(HttpServletResponse.SC_OK);
                response.getWriter().print("If your account is found, an email will be sent shortly.");
                return dto;
            }
            catch (Exception ex)
            {
                //Probably bad input data - email address format
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().print(ex.getMessage());
                return null;
            }
        }
        else
        {
            //We dont expect this, unless someone is hacking, or the front-end has a bug
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().print("Missing LostPassword data");
            _LOGGER.error("Missing LostPassword data, is there a front-end bug?");
            return null;
        }
    }

    //Use to minimize brute force attacks during login flows
    //There are probably better ways to do this, based on IP, CAPTCHA, etc
    private void doSleep()
    {
        try
        {
            Thread.sleep(1500);
        }
        catch (Exception ex)
        {
            //Keep going...
        }
    }


}
