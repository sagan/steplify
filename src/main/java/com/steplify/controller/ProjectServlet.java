package com.steplify.controller;

import com.google.common.collect.ImmutableMap;
import com.steplify.dao.*;
import com.steplify.dto.*;
import com.steplify.mpb.*;
import com.steplify.utility.*;

import org.apache.log4j.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.*;

public class ProjectServlet extends SuperServlet
{
    private static Logger _LOGGER = Logger.getLogger("ProjectServlet");

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //Get the logged in user from SuperServlet > Request
        UserPrincipalDTO loggedInUserDTO = getLoggedInUserDTO(request);

        if (request.getRequestURI().equals("/project/edit"))
        {
            String sID = request.getParameter("pid");
            if (sID == null)
            {
                //Assume we are planning to add a new project
                request.setAttribute("dtoProject", new ProjectDTO());
                request.setAttribute("bUserHasProjectAdmin", true);
            }
            else
            {
                //Assume we are planning to edit an existing project
                int iProjectID = Integer.parseInt(sID);
                MembershipAuthEnum enumAuth = authorityCheckForUserAndProject(loggedInUserDTO.get_id(), iProjectID);
                ProjectDTO dtoProject = getProject(enumAuth, loggedInUserDTO, iProjectID);
                request.setAttribute("dtoProject", dtoProject);
                request.setAttribute("bUserHasProjectAdmin", enumAuth==MembershipAuthEnum.ADMIN ? true : false);
            }

            List<CodeDecodeDTO> alCodesProjectType = CodeDecode.getList(CodeDecode._CAT_PROJECT_TYPE);
            request.setAttribute("alCodesProjectType", alCodesProjectType);
            request.setAttribute("objTransResponse", new TransResponse());
            request.getRequestDispatcher("/project_edit.jsp").forward(request, response);
            return;
        }
        else
        {
            throw new RuntimeException("URL mismatch");
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //Get the logged in user from SuperServlet > Request
        UserPrincipalDTO loggedInUserDTO = getLoggedInUserDTO(request);

        if (request.getRequestURI().equals("/project/edit"))
        {
            String jsonIn = getPostParametersAsJSON(request);
            ProjectDTO dtoProject = ProjectDTO.fromJson(jsonIn, ProjectDTO.class);
            MembershipAuthEnum enumAuth;

            TransResponse tr = null;
            boolean bIsEditNotAdd = false;
            if (dtoProject.get_id() == 0)
            {
                bIsEditNotAdd = false;
                enumAuth = MembershipAuthEnum.ADMIN;
                tr = postAddProject(loggedInUserDTO, dtoProject);
            }
            else
            {
                bIsEditNotAdd = true;
                //We are about to update, see if the user actually has permission
                enumAuth = authorityCheckForUserAndProject(loggedInUserDTO.get_id(), dtoProject.get_id());
                tr = postEditProject(enumAuth, loggedInUserDTO, dtoProject);
            }

            if (tr.isSuccess())
            {
                if (bIsEditNotAdd)
                {
                    //Editing an existing is done from the Dashboard
                    response.sendRedirect("/dashboard");
                }
                else
                {
                    //After a new add, sed them straight to the the Setup Wizard
                    response.sendRedirect("/admin_topic/wizard?pid=" + dtoProject.get_id());
                }
            }
            else
            {
                List<CodeDecodeDTO> alCodesProjectType = CodeDecode.getList(CodeDecode._CAT_PROJECT_TYPE);
                request.setAttribute("alCodesProjectType", alCodesProjectType);
                request.setAttribute("objTransResponse", tr);
                request.setAttribute("dtoProject", dtoProject);
                request.setAttribute("bUserHasProjectAdmin", enumAuth==MembershipAuthEnum.ADMIN ? true : false);
                request.getRequestDispatcher("/project_edit.jsp").forward(request, response);
            }
        }
        else
        {
            throw new RuntimeException("URL mismatch");
        }
    }

    ///////////////////Split out for Unit Testing////////////////////

    public ProjectDTO getProject(MembershipAuthEnum enumAuth, UserPrincipalDTO dtoUser, int iProjectID)
    {
        ProjectDTO dtoProject = null;

        //Only the project ADMIN can read this page
        if (enumAuth != MembershipAuthEnum.ADMIN)
        {
            String sMsg = "Access Denied - Membership for UserID=" + dtoUser.get_id() + " for ProjectID=" + iProjectID;
            _LOGGER.error(sMsg);
            throw new RuntimeException(sMsg);
        }

        //Get a DB connection, do the work, and close it
        Connection connection = SuperDuperDAO.getConnection();
        try
        {
            //And select from the DB
            dtoProject = ProjectDAO.selectProjectByID(connection, iProjectID);
            if (dtoProject == null)
            {
                //For some reason we could not insert the new project
                _LOGGER.error("Project could not be found=" + iProjectID);
                //Do not proceed
                throw new RuntimeException("Project could not be found, please try again");
            }
        }
        finally
        {
            SuperDuperDAO.close(connection, false);
        }

        return dtoProject;
    }


    public TransResponse postAddProject(UserPrincipalDTO dtoUser, ProjectDTO dtoProject)
    {
        //No need for AUTH check, all users can add a project to their account; which will make them the ADMIN

        TransResponse tr = new TransResponse();
        try
        {
            if (dtoProject == null)
            {
                _LOGGER.error("UNEXPECTED - ProjectDTO was null");
                throw new Exception("An unexpected error occurred, please try again");
            }

            //Here is where I need to do any last UI minute conversions
            CodeDecodeDTO cdProjectType = CodeDecode.getByValue(CodeDecode._CAT_PROJECT_TYPE, dtoProject.get_s_cd_project_type());
            if (cdProjectType != null)
            {
                dtoProject.set_s_cd_project_type(cdProjectType.get_s_code());
            }
            else
            {
                dtoProject.set_s_cd_project_type(null);
            }

            //See if the project is valid
            dtoProject.validateFieldsForNew(tr);

            //So far so good, lets save it up
            if (tr.isSuccess())
            {
                //Get a DB connection, do the work, and close it
                Connection connection = SuperDuperDAO.getConnection();
                try
                {
                    //Set the current user to be the creator
                    dtoProject.set_id_created_by(dtoUser.get_id());
                    dtoProject.set_id_updated_by(dtoUser.get_id());
                    //And insert to DB
                    int iCountPP = ProjectDAO.insertProject(connection, dtoProject);
                    _LOGGER.info(formatAppEvent(dtoUser, "PROJECT_NEW", ImmutableMap.of("project_id", String.valueOf(dtoProject.get_id()))));

                    if (iCountPP != 1)
                    {
                        //For some reason we could not insert the new project
                        _LOGGER.error("Project insert failed: iCountPP=" + iCountPP);
                        //Do not proceed
                        throw new Exception("Project could not be saved, please try again");
                    }

                    //And create a Membership link, between the user and the project
                    ProjectMemberDTO dtoPM = new ProjectMemberDTO();
                    dtoPM.setStatusActive();
                    dtoPM.set_b_is_project_admin(true);

                    //We dont actually know what their role is
                    dtoPM.setRoleToOther();

                    dtoPM.set_id_user_principal(dtoUser.get_id());
                    dtoPM.set_id_created_by(dtoUser.get_id());
                    dtoPM.set_id_updated_by(dtoUser.get_id());
                    dtoPM.set_id_project(dtoProject.get_id());

                    //And insert to DB
                    int iCountPM = ProjectMemberDAO.insertMembership(connection, dtoPM);
                    if (iCountPM != 1)
                    {
                        //For some reason we could insert the new project
                        _LOGGER.error("Membership insert failed: iCountPM=" + iCountPM);
                        //Do not proceed
                        throw new Exception("Membership could not be saved, please try again");
                    }
                }
                catch (Exception ex)
                {
                    SuperDuperDAO.close(connection, false);
                    throw ex;
                }
                finally
                {
                    SuperDuperDAO.close(connection, true);
                }
            }
        }
        catch (Exception ex)
        {
            //Hopefully just bad input data
            tr.addException(ex.getMessage());
        }
        finally
        {
            return tr;
        }
    }

    public TransResponse postEditProject(MembershipAuthEnum enumAuth, UserPrincipalDTO dtoUser, ProjectDTO dtoProject)
    {
        //Make sure the user is a project ADMIN
        if (enumAuth != MembershipAuthEnum.ADMIN)
        {
            String sMsg = "Access Denied - Membership for UserID=" + dtoUser.get_id() + " for ProjectID=" + dtoProject.get_id();
            _LOGGER.error(sMsg);
            throw new RuntimeException(sMsg);
        }

        TransResponse tr = new TransResponse();
        try
        {
            if (dtoProject == null)
            {
                _LOGGER.error("UNEXPECTED - ProjectDTO was null");
                throw new Exception("An unexpected error occurred, please try again");
            }

            //Here is where I need to do any last minute UI conversions
            CodeDecodeDTO cdProjectType = CodeDecode.getByValue(CodeDecode._CAT_PROJECT_TYPE, dtoProject.get_s_cd_project_type());
            if (cdProjectType != null)
            {
                dtoProject.set_s_cd_project_type(cdProjectType.get_s_code());
            }
            else
            {
                dtoProject.set_s_cd_project_type(null);
            }

            //See if the project is valid
            dtoProject.validateFieldsForNew(tr);

            //So far so good, lets save it up
            if (tr.isSuccess())
            {
                //Get a DB connection, do the work, and close it
                Connection connection = SuperDuperDAO.getConnection();
                try
                {
                    //Set the current user to be the updater
                    dtoProject.set_id_updated_by(dtoUser.get_id());
                    //And update the DB
                    int iCountPP = ProjectDAO.updateProject(connection, dtoProject);
                    _LOGGER.info(formatAppEvent(dtoUser, "PROJECT_EDIT", ImmutableMap.of("project_id", String.valueOf(dtoProject.get_id()))));

                    if (iCountPP != 1)
                    {
                        //For some reason we could not insert the new project
                        _LOGGER.error("Project update failed: iCountPP=" + iCountPP);
                        //Do not proceed
                        throw new Exception("Project could not be saved, please try again");
                    }
                }
                catch (Exception ex)
                {
                    SuperDuperDAO.close(connection, false);
                    throw ex;
                }
                finally
                {
                    SuperDuperDAO.close(connection, true);
                }
            }
        }
        catch (Exception ex)
        {
            //Hopefully just bad input data
            tr.addException(ex.getMessage());
        }
        finally
        {
            return tr;
        }
    }

}
