package com.steplify.controller;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

//This class is a standalone, it is used for debugging non application specifics
//Otherwise users have to be logged in, or require DB access
public class DebugServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        if (request.getRequestURI().equals("/debug/cookies"))
        {
            response.getWriter().println("<html><body><pre>");
            Date dt = new Date();
            response.getWriter().println("Cookies Found: " + dt.toString());
            response.getWriter().println("");

            Cookie[] cookies = request.getCookies();
            for(int i = 0; i < cookies.length; i++)
            {
                Cookie cookie = cookies[i];
                StringBuilder sb = new StringBuilder();
                sb.append("Name=" + cookie.getName());
                sb.append(" | Value=" + cookie.getValue());
                sb.append(" | Domain=" + cookie.getDomain());
                sb.append(" | Path=" + cookie.getPath());
                sb.append(" | MaxAge=" + cookie.getMaxAge());
                sb.append(" | Secure=" + cookie.getSecure());
                sb.append(" | Version=" + cookie.getVersion());
                sb.append(" | Comment=" + cookie.getComment());

                response.getWriter().println(sb.toString());
                response.getWriter().println("");
            }

            response.getWriter().println("</pre></body></html>");
            //No need for a JSP in this case
            return;
        }
        else
        {
            throw new RuntimeException("URL mismatch");
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        if (request.getRequestURI().equals("/debug/abc123"))
        {
            throw new RuntimeException("Not implemented yet");
        }
        else
        {
            throw new RuntimeException("URL mismatch");
        }
    }


}
