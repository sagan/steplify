package com.steplify.controller;

import com.google.common.collect.ImmutableMap;
import com.steplify.dao.*;
import com.steplify.dto.*;
import com.steplify.mpb.*;
import com.steplify.utility.CodeDecode;
import com.steplify.utility.TransResponse;
import com.steplify.utility.UI;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class AdminTopicServlet extends SuperServlet
{
    private static Logger _LOGGER = Logger.getLogger("AdminTopicServlet");

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //Get the logged in user from SuperServlet > Request
        UserPrincipalDTO loggedInUserDTO = getLoggedInUserDTO(request);

        if (request.getRequestURI().equals("/admin_topic/view"))
        {
            AdminTopicViewMPB mpbAdminTopicView = new AdminTopicViewMPB();
            String sID = request.getParameter("pid");
            if (sID == null)
            {
                throw new RuntimeException("Missing input PID");
            }

            //Assume we are planning to edit an existing project
            int iProjectID = Integer.parseInt(sID);
            MembershipAuthEnum enumAuth = authorityCheckForUserAndProject(loggedInUserDTO.get_id(), iProjectID);

            TransResponse tr = new TransResponse();
            forwardBaseDataToJSP(request, response, "/admin_topic.jsp", tr, enumAuth, iProjectID);
            return;
        }
        if (request.getRequestURI().equals("/admin_topic/members"))
        {
            AdminTopicViewMPB mpbAdminTopicView = new AdminTopicViewMPB();
            String sID = request.getParameter("pid");
            if (sID == null)
            {
                throw new RuntimeException("Missing input PID");
            }

            //Assume we are planning to edit an existing project
            int iProjectID = Integer.parseInt(sID);
            MembershipAuthEnum enumAuth = authorityCheckForUserAndProject(loggedInUserDTO.get_id(), iProjectID);

            TransResponse tr = new TransResponse();
            forwardBaseDataToJSP(request, response, "/admin_topic_members.jsp", tr, enumAuth, iProjectID);
            return;
        }
        if (request.getRequestURI().equals("/admin_topic/wizard"))
        {
            AdminTopicViewMPB mpbAdminTopicView = new AdminTopicViewMPB();
            String sID = request.getParameter("pid");
            if (sID == null)
            {
                throw new RuntimeException("Missing input PID");
            }

            //Assume we are planning to edit an existing project
            int iProjectID = Integer.parseInt(sID);
            MembershipAuthEnum enumAuth = authorityCheckForUserAndProject(loggedInUserDTO.get_id(), iProjectID);

            TransResponse tr = new TransResponse();
            forwardWizardDataToJSP(request, response, tr, iProjectID);
            return;
        }
        else
        {
            throw new RuntimeException("URL mismatch");
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //Get the logged in user from SuperServlet > Request
        UserPrincipalDTO loggedInUserDTO = getLoggedInUserDTO(request);

        if (request.getRequestURI().equals("/admin_topic/update_topic"))
        {
            String jsonIn = getPostParametersAsJSON(request);
            TopicMPB mpbTopic = TopicMPB.fromJson(jsonIn, TopicMPB.class);

            //Vars used in both add and update scenarios
            MembershipAuthEnum enumAuth = null;
            TransResponse tr = null;
            int iProjectID;

            //Check if this is an add or update
            if (mpbTopic._id == 0)
            {
                //Add a new topic, verify they have AUTH to add to this project
                iProjectID = mpbTopic._id_project;
                enumAuth = authorityCheckForUserAndProject(loggedInUserDTO.get_id(), iProjectID);
                tr = postAddTopic(enumAuth, loggedInUserDTO, mpbTopic);
            }
            else
            {
                //Update an existing topic
                TopicDTO dtoTopicFromDB = null;
                Connection conn = SuperDuperDAO.getConnection();
                try
                {
                    //Get the Topic from the DB, since we cant trust the user's didn't manipulate the hidden-fields
                    dtoTopicFromDB = TopicDAO.selectTopicByID(conn, mpbTopic._id);
                }
                finally
                {
                    SuperDuperDAO.close(conn, false);
                }

                //Use the ProjectID from the DB to verify they have AUTH to update this project
                //This way, the Membership they want to update is guaranteed to be part of the Project we use for the AUTH check
                iProjectID = dtoTopicFromDB.get_id_project();
                enumAuth = authorityCheckForUserAndProject(loggedInUserDTO.get_id(), iProjectID);
                tr = postUpdateTopic(enumAuth, loggedInUserDTO, mpbTopic, dtoTopicFromDB);
            }

            //If there were no errors, lets notify the user all was good
            if (tr.isSuccess())
            {
                tr.addWarning("Updated on: " + UI.datetimeString(new Date()));
            }

            //When save is complete, leave the user on the same page, in case they need to do more edits
            forwardBaseDataToJSP(request, response, "/admin_topic.jsp", tr, enumAuth, iProjectID);
            return;
        }
        else if (request.getRequestURI().equals("/admin_topic/update_topic_member"))
        {
            String jsonIn = getPostParametersAsJSON(request);
            TopicMemberDTO dtoTopicMember = TopicMemberDTO.fromJson(jsonIn, TopicMemberDTO.class);

            MembershipAuthEnum enumAuth = null;
            TransResponse tr = null;
            int iProjectID;

            //Update an existing topic
            TopicMemberDTO dtoTopicMemberFromDB = null;
            Connection conn = SuperDuperDAO.getConnection();
            try
            {
                //Get the TopicMember from the DB, since we cant trust the user's didn't manipulate the hidden-fields
                dtoTopicMemberFromDB = TopicMemberDAO.selectTopicMemberByID(conn, dtoTopicMember.get_id());
            }
            finally
            {
                SuperDuperDAO.close(conn, false);
            }

            //Use the ProjectID from the DB to verify they have AUTH to update this project
            //This way, the Membership they want to update is guaranteed to be part of the Project we use for the AUTH check
            iProjectID = dtoTopicMemberFromDB.get_id_project();
            enumAuth = authorityCheckForUserAndProject(loggedInUserDTO.get_id(), iProjectID);
            tr = postUpdateTopicMember(enumAuth, loggedInUserDTO, dtoTopicMember, dtoTopicMemberFromDB);

            //If there were no errors, lets notify the user all was good
            if (tr.isSuccess())
            {
                tr.addWarning("Updated on: " + UI.datetimeString(new Date()));
            }

            //When save is complete, leave the user on the same page, in case they need to do more edits
            forwardBaseDataToJSP(request, response, "/admin_topic_members.jsp", tr, enumAuth, iProjectID);
            return;
        }
        else if (request.getRequestURI().equals("/admin_topic/wizard/ajax"))
        {
            //This is an AJAX call, so no need for page manipulation
            String jsonIn = getRequestBodyPayload(request);
            AdminTopicWizardMPB mpbWizard = AdminTopicWizardMPB.fromJson(jsonIn, AdminTopicWizardMPB.class);
            postTopicWizard(response, loggedInUserDTO, mpbWizard);
        }
        else
        {
            throw new RuntimeException("URL mismatch");
        }
    }

    private void forwardBaseDataToJSP(HttpServletRequest request, HttpServletResponse response, String sJspPage, TransResponse tr, MembershipAuthEnum enumAuth, int iProjectID) throws ServletException, IOException
    {
        //Get the logged in user from SuperServlet > Request
        UserPrincipalDTO loggedInUserDTO = getLoggedInUserDTO(request);

        AdminTopicViewMPB mpbAdminTopicView = new AdminTopicViewMPB();
        mpbAdminTopicView.iProjectID = iProjectID;

        //This will set the Memberships and Users on the MPB
        mpbAdminTopicView.lstTopics = getTopicsForProject(enumAuth, loggedInUserDTO, iProjectID);

        //Get the necessary CodeDecodes for the UI
        List<CodeDecodeDTO> alCodesTopicAuthority = CodeDecode.getList(CodeDecode._CAT_TOPIC_AUTHORITY);
        mpbAdminTopicView.alCodesTopicAuthority = alCodesTopicAuthority;

        //Return everything to the JSP to render
        request.setAttribute("mpbAdminTopicView", mpbAdminTopicView);
        request.setAttribute("objTransResponse", tr);
        request.setAttribute("dtoLoggedInUser", loggedInUserDTO);
        request.getRequestDispatcher(sJspPage).forward(request, response);
    }

    private void forwardWizardDataToJSP(HttpServletRequest request, HttpServletResponse response, TransResponse tr, int iProjectID) throws ServletException, IOException
    {
        //Get the logged in user from SuperServlet > Request
        UserPrincipalDTO loggedInUserDTO = getLoggedInUserDTO(request);

        AdminTopicViewMPB mpbAdminTopicView = new AdminTopicViewMPB();
        mpbAdminTopicView.iProjectID = iProjectID;

        //Get the TopicTaskTemplate list for this project type
        List<TopicDTO> lstTopicTaskTemplates = null;
        Connection conn = SuperDuperDAO.getConnection();
        try
        {
            //Get the info from the DB, since we cant trust the user's didn't manipulate the hidden-fields
            ProjectDTO dtoProject = ProjectDAO.selectProjectByID(conn, iProjectID);
            CompanyUserDTO dtoCompanyUser = CompanyDAO.selectCompanyUserByUserID(conn, loggedInUserDTO.get_id());
            lstTopicTaskTemplates = CompanyDAO.selectCompanyTopicTaskList(conn, dtoCompanyUser.get_id_company(), dtoProject.get_s_cd_project_type());
        }
        catch (Exception ex)
        {
            _LOGGER.error(ex.getMessage());
        }
        finally
        {
            SuperDuperDAO.close(conn, false);
        }

        //Return everything to the JSP to render
        request.setAttribute("mpbAdminTopicView", mpbAdminTopicView);
        request.setAttribute("lstTopicTaskTemplates", lstTopicTaskTemplates);
        request.setAttribute("objTransResponse", tr);
        request.setAttribute("dtoLoggedInUser", loggedInUserDTO);
        request.getRequestDispatcher("/admin_topic_wizard.jsp").forward(request, response);
    }

    ///////////////////Split out for Unit Testing////////////////////

    public List<TopicDTO> getTopicsForProject(MembershipAuthEnum enumAuth, UserPrincipalDTO dtoUser, int iProjectID)
    {
        List<TopicDTO> lstTopics;

        //Make sure the user is allowed to at least read this project
        if (enumAuth != MembershipAuthEnum.ADMIN)
        {
            String sMsg = "Access Denied - Membership for UserID=" + dtoUser.get_id() + " for ProjectID=" + iProjectID;
            _LOGGER.error(sMsg);
            throw new RuntimeException(sMsg);
        }

        //Get a DB connection, do the work, and close it
        Connection connection = SuperDuperDAO.getConnection();
        try
        {
            //And select from the DB, those who are REMOVED will still be in the ResultSet
            lstTopics = TopicDAO.selectTopicsWithMembersForProject(connection, iProjectID);
            if (lstTopics == null)
            {
                //For some reason we could not find any memberships
                _LOGGER.error("There were null topics = " + iProjectID);
                //Do not proceed
                throw new RuntimeException("There were null topics, this was unexpected");
            }
            else
            {
                //Looks like we got what we need, lets go...
                //Its ok to have empty list of topics for a project, maybe they just created the project
            }
        }
        finally
        {
            SuperDuperDAO.close(connection, false);
        }

        return lstTopics;
    }

    public TransResponse postAddTopic(MembershipAuthEnum enumAuth, UserPrincipalDTO dtoUserCurrent, TopicMPB mpbTopic)
    {
        //Make sure the user is allowed to update the membership
        if (enumAuth != MembershipAuthEnum.ADMIN)
        {
            String sMsg = "Access Denied - Topic addition by UserID=" + dtoUserCurrent.get_id() + " on ProjectID=" + mpbTopic._id_project;
            _LOGGER.error(sMsg);
            throw new RuntimeException(sMsg);
        }

        TransResponse tr = new TransResponse();
        try
        {
            TopicDTO dtoTopic = new TopicDTO();
            dtoTopic.set_s_name(mpbTopic._s_name);
            dtoTopic.set_s_description(mpbTopic._s_description);
            dtoTopic.set_s_url_attachments(mpbTopic._s_url_attachments);
            dtoTopic.set_b_auto_add_members(mpbTopic._b_auto_add_members);
            dtoTopic.set_id_project(mpbTopic._id_project);

            //Here is where I need to do any last minute UI conversions
            try
            {
                int iValue = Integer.parseInt(mpbTopic._s_order);
                if (iValue < 1 || iValue > 99)
                {
                    throw new Exception("_s_order is useless");
                }
                dtoTopic.set_i_order(iValue);
            }
            catch (Exception ex)
            {
                //Just use a default value
                dtoTopic.set_i_order(1);
            }

            //See if the membership is valid
            dtoTopic.validateFields(tr);

            //So far so good, lets save it up
            if (tr.isSuccess())
            {
                //Get a DB connection, do the work, and close it
                Connection connection = SuperDuperDAO.getConnection();
                try
                {
                    //Set the CURRENT user to be the creator
                    dtoTopic.set_id_created_by(dtoUserCurrent.get_id());
                    dtoTopic.set_id_updated_by(dtoUserCurrent.get_id());

                    //And update the DB
                    int iCountDB = TopicDAO.insertTopic(connection, dtoTopic);
                    _LOGGER.info(formatAppEvent(dtoUserCurrent, "TOPIC_ADD", ImmutableMap.of("project_id", String.valueOf(dtoTopic.get_id_project()))));


                    if (iCountDB != 1)
                    {
                        //For some reason we could not insert the new topic
                        throw new Exception("Topic could not be saved, please try again");
                    }
                    else
                    {
                        //For the new topic, add all project members
                        addProjectMembersToNewTopic(tr, connection, dtoTopic);
                    }
                }
                catch (Exception ex)
                {
                    SuperDuperDAO.close(connection, false);

                    //SQLIntegrityConstraintViolationException doesnt get thrown
                    if (ex.getMessage().toLowerCase().contains("duplicate"))
                    {
                        throw new Exception("Topic name already exists in this project");
                    }
                    else
                    {
                        _LOGGER.error(ex.getMessage());
                        throw ex;
                    }
                }
                finally
                {
                    SuperDuperDAO.close(connection, true);
                }
            }
        }
        catch (Exception ex)
        {
            //Hopefully just bad input data
            tr.addException(ex.getMessage());
        }
        finally
        {
            return tr;
        }
    }

    public TransResponse postUpdateTopic(MembershipAuthEnum enumAuth, UserPrincipalDTO dtoUserCurrent, TopicMPB mpbTopic, TopicDTO dtoTopicFromDB)
    {
        //Make sure the user is allowed to update the membership
        if (enumAuth != MembershipAuthEnum.ADMIN)
        {
            String sMsg = "Access Denied - Topic update by UserID=" + dtoUserCurrent.get_id() + " on TopicID=" + dtoTopicFromDB.get_id();
            _LOGGER.error(sMsg);
            throw new RuntimeException(sMsg);
        }

        TransResponse tr = new TransResponse();
        try
        {
            //Here is where I need to do any last minute UI conversions
            dtoTopicFromDB.set_s_name(mpbTopic._s_name);
            dtoTopicFromDB.set_s_description(mpbTopic._s_description);
            dtoTopicFromDB.set_s_url_attachments(mpbTopic._s_url_attachments);
            dtoTopicFromDB.set_b_auto_add_members(mpbTopic._b_auto_add_members);

            //Here is where I need to do any last minute UI conversions
            try
            {
                int iValue = Integer.parseInt(mpbTopic._s_order);
                if (iValue < 1 || iValue > 99)
                {
                    throw new Exception("_s_order is useless");
                }
                dtoTopicFromDB.set_i_order(iValue);
            }
            catch (Exception ex)
            {
                //Just use a default value
                dtoTopicFromDB.set_i_order(1);
            }

            //See if the membership is valid
            dtoTopicFromDB.validateFields(tr);

            //So far so good, lets save it up
            if (tr.isSuccess())
            {
                //Get a DB connection, do the work, and close it
                Connection connection = SuperDuperDAO.getConnection();
                try
                {
                    //Set the current user to be the updater
                    dtoTopicFromDB.set_id_updated_by(dtoUserCurrent.get_id());
                    //And update the DB
                    int iCountDB = TopicDAO.updateTopic(connection, dtoTopicFromDB);
                    _LOGGER.info(formatAppEvent(dtoUserCurrent, "TOPIC_UPDATE", ImmutableMap.of("project_id", String.valueOf(dtoTopicFromDB.get_id_project()))));

                    if (iCountDB != 1)
                    {
                        //For some reason we could not update the topic
                        _LOGGER.error("Topic update failed: iCountPP=" + iCountDB);
                        //Do not proceed
                        throw new Exception("Topic could not be saved, please try again");
                    }
                    else
                    {
                        //Do not add all project members, that was done on AddTopic
                        //Nor should we update their topic memberships
                    }
                }
                catch (Exception ex)
                {
                    SuperDuperDAO.close(connection, false);
                    throw ex;
                }
                finally
                {
                    SuperDuperDAO.close(connection, true);
                }
            }
        }
        catch (Exception ex)
        {
            //Hopefully just bad input data
            tr.addException(ex.getMessage());
        }
        finally
        {
            return tr;
        }
    }

    public TransResponse postUpdateTopicMember(MembershipAuthEnum enumAuth, UserPrincipalDTO dtoUserCurrent, TopicMemberDTO dtoTopicMember, TopicMemberDTO dtoTopicMemberFromDB)
    {
        //Make sure the user is allowed to update the membership
        if (enumAuth != MembershipAuthEnum.ADMIN)
        {
            String sMsg = "Access Denied - Topic update by UserID=" + dtoUserCurrent.get_id() + " on TopicID=" + dtoTopicMemberFromDB.get_id();
            _LOGGER.error(sMsg);
            throw new RuntimeException(sMsg);
        }

        TransResponse tr = new TransResponse();
        try
        {
            //Here is where I need to do any last minute UI conversions
            CodeDecodeDTO cdTopicAuth = CodeDecode.getByValue(CodeDecode._CAT_TOPIC_AUTHORITY, dtoTopicMember.get_s_cd_authority());
            if (cdTopicAuth != null)
            {
                dtoTopicMemberFromDB.set_s_cd_authority(cdTopicAuth.get_s_code());
            }
            else
            {
                dtoTopicMemberFromDB.set_s_cd_authority(null);
            }

            //See if the membership is valid
            dtoTopicMemberFromDB.validateFields(tr);

            //So far so good, lets save it up
            if (tr.isSuccess())
            {
                //Get a DB connection, do the work, and close it
                Connection connection = SuperDuperDAO.getConnection();
                try
                {
                    //Set the current user to be the updater
                    dtoTopicMemberFromDB.set_id_updated_by(dtoUserCurrent.get_id());
                    //And update the DB
                    int iCountDB = TopicMemberDAO.updateTopicMember(connection, dtoTopicMemberFromDB);
                    _LOGGER.info(formatAppEvent(dtoUserCurrent, "TOPIC_MEMBER_AUTH_UPDATE", ImmutableMap.of("project_id", String.valueOf(dtoTopicMemberFromDB.get_id_project()))));

                    if (iCountDB != 1)
                    {
                        //For some reason we could not update the topic
                        _LOGGER.error("TopicMember update failed: iCountPP=" + iCountDB);
                        //Do not proceed
                        throw new Exception("TopicMember could not be saved, please try again");
                    }
                    else
                    {
                        //All looks good, lets get out of here...
                    }
                }
                catch (Exception ex)
                {
                    SuperDuperDAO.close(connection, false);
                    throw ex;
                }
                finally
                {
                    SuperDuperDAO.close(connection, true);
                }
            }
        }
        catch (Exception ex)
        {
            //Hopefully just bad input data
            tr.addException(ex.getMessage());
        }
        finally
        {
            return tr;
        }
    }

    public void postTopicWizard(HttpServletResponse response, UserPrincipalDTO dtoUser, AdminTopicWizardMPB mpbWizard) throws ServletException, IOException
    {
        try
        {
            //The mpbWizard should never be sent as null
            if (mpbWizard == null)
            {
                //For some reason we got a null object
                _LOGGER.error("Posted wizard object was null");
                throw new Exception("Posted wizard object was null");
            }

            //Make sure the user is allowed to use the wizard on this project
            MembershipAuthEnum enumAuth = authorityCheckForUserAndProject(dtoUser.get_id(), mpbWizard.iProjectID);
            if (enumAuth != MembershipAuthEnum.ADMIN)
            {
                String sMsg = "Access Denied - Wizard access for UserID=" + dtoUser.get_id() + " on ProjectID=" + mpbWizard.iProjectID;
                _LOGGER.error(sMsg);
                throw new RuntimeException(sMsg);
            }

            //Get a DB connection, do the work, and close it
            Connection connection = SuperDuperDAO.getConnection();
            try
            {
                //Get the users company, to validate the requested TopicTasks belong to them
                CompanyUserDTO dtoCompanyUser = CompanyDAO.selectCompanyUserByUserID(connection, dtoUser.get_id());

                int iNewTopics = 0;
                int iNewTasks = 0;

                //Now lets add each of these items in the TT
                for (String sTopicTaskID : mpbWizard.alTopicTasks)
                {
                    try
                    {
                        //Format of sTopicTask = "TK#"
                        int iTopicTaskID = Integer.valueOf(sTopicTaskID.substring(2));
                        CompanyTopicTaskDTO dtoCTT = CompanyDAO.selectCompanyTopicTaskByIDs(connection, iTopicTaskID, dtoCompanyUser.get_id_company());

                        //First see if the topic already exists on this project
                        TopicDTO dtoTopic = TopicDAO.selectTopicByProjectIDAndTopicName(connection, mpbWizard.iProjectID, dtoCTT.get_s_topic_name());
                        if (dtoTopic == null)
                        {
                            dtoTopic = new TopicDTO();
                            dtoTopic.set_id_created_by(dtoUser.get_id());
                            dtoTopic.set_id_updated_by(dtoUser.get_id());
                            dtoTopic.set_s_name(dtoCTT.get_s_topic_name());
                            //dtoTopic.set_s_description(dtoCTT.get_s_description());
                            dtoTopic.set_i_order(dtoCTT.get_i_topic_order());
                            //dtoTopic.set_s_url_attachments(dtoCTT.get_s_url_attachments());
                            //dtoTopic.set_b_auto_add_members(dtoCTT.get_b_auto_add_members());
                            dtoTopic.set_id_project(mpbWizard.iProjectID);

                            TransResponse tr = new TransResponse();
                            dtoTopic.validateFields(tr);

                            //Add it to the DB
                            if (tr.isSuccess())
                            {
                                TopicDAO.insertTopic(connection, dtoTopic);
                                iNewTopics++;

                                //For the new topic, add all project members; just like a manual add
                                addProjectMembersToNewTopic(tr, connection, dtoTopic);
                            }
                        }

                        //Now lets see if the task exists on the topic
                        TaskDTO dtoTask = TaskDAO.selectTaskByTopicIDAndTaskName(connection, dtoTopic.get_id(), dtoCTT.get_s_task_name());
                        if (dtoTask == null)
                        {
                            dtoTask = new TaskDTO();
                            dtoTask.set_id_topic(dtoTopic.get_id());
                            dtoTask.set_id_created_by(dtoUser.get_id());
                            dtoTask.set_id_updated_by(dtoUser.get_id());
                            dtoTask.set_s_name(dtoCTT.get_s_task_name());
                            //dtoTask.set_s_description(dtoCTT.get_s_description());
                            //dtoTask.set_i_order(dtoCTT.get_i_order());
                            dtoTask.setStatusNew();
                            dtoTask.set_id_assigned_user(dtoUser.get_id());
                            dtoTask.set_dt_due(new Date());
                            dtoTask.set_id_project(mpbWizard.iProjectID);

                            TransResponse tr = new TransResponse();
                            dtoTask.validateFields(tr);

                            //Add it to the DB
                            if (tr.isSuccess())
                            {
                                TaskDAO.insertTask(connection, dtoTask);
                                iNewTasks++;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //Even if a topic/task insert fails, keep going tp completion; dont throw the error up
                        String sMsg = "Unexpected exception processing - Wizard sTopicTask=" + sTopicTaskID + " on ProjectID=" + mpbWizard.iProjectID;
                        _LOGGER.error(sMsg);
                        _LOGGER.error(ex.getMessage());
                    }
                } //end of for-loop

                //All done
                response.setStatus(HttpServletResponse.SC_OK);

                _LOGGER.info(formatAppEvent(dtoUser, "WIZARD_COMPLETE",
                        ImmutableMap.of("project_id", String.valueOf(mpbWizard.iProjectID),
                                        "num_new_topics", String.valueOf(iNewTopics),
                                        "num_new_tasks", String.valueOf(iNewTasks))));

                String sSummary = "Complete (" + iNewTopics + " new topics, " + iNewTasks + " new tasks, were created)";
                response.getWriter().print(sSummary);
            }
            catch (Exception ex)
            {
                SuperDuperDAO.close(connection, false);
                _LOGGER.error("Wizard failed unexpectedly, please try again");
                _LOGGER.error(ex.getMessage());
                throw new Exception("Wizard failed unexpectedly, please try again");
            }
            finally
            {
                SuperDuperDAO.close(connection, true);
            }
        }
        catch (Exception ex)
        {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().print(ex.getMessage());
        }
    }

    public static TransResponse addProjectMembersToNewTopic(TransResponse tr, Connection connection, TopicDTO dtoTopic)
    {
        List<ProjectMemberDTO> lstPMs = ProjectMemberDAO.selectMembershipsByProjectID(connection, dtoTopic.get_id_project());

        for (ProjectMemberDTO pm : lstPMs)
        {
            TopicMemberDTO dtoTM = new TopicMemberDTO();
            dtoTM.set_id_created_by(dtoTopic.get_id_created_by());
            dtoTM.set_id_updated_by(dtoTopic.get_id_updated_by());
            dtoTM.set_id_user_principal(pm.get_id_user_principal());
            dtoTM.set_id_topic(dtoTopic.get_id());
            dtoTM.set_id_project(dtoTopic.get_id_project());

            //Rule based Membership permission on TopicAdd
            //if (dtoTopic.get_b_auto_add_members())
            if(pm.get_b_is_project_admin())
            {
                dtoTM.setAuthorityWriter();
            }
            else
            {
                //You are basically a nobody
                dtoTM.setAuthorityNone();
            }

            dtoTM.validateFields(tr);

            //If all fields are ok, insert the row
            if (tr.isSuccess())
            {
                int iCount = TopicMemberDAO.insertTopicMember(connection, dtoTM);
                if (iCount != 1)
                {
                    throw new RuntimeException("TopicMember could not be saved, please try again");
                }
            }
            else
            {
                throw new RuntimeException("TopicMember could not be validated, please try again");
            }
        }

        return tr;
    }

}
