package com.steplify.dto;


import com.steplify.utility.TransResponse;

import java.util.ArrayList;
import java.util.Date;

public class ProjectMemberDTO extends SuperDuperDTO {

    private int _id_user_principal;
    private String _s_cd_status;
    private String _s_cd_role;
    private boolean _b_is_project_admin;
    private int _id_project;
    private ProjectDTO _dto_project;
    private UserPrincipalDTO _dto_user_principal;
    private Date _dt_archive_date;

    public int get_id_user_principal()
    {
        return this._id_user_principal;
    }

    public void set_id_user_principal(int new_id_user_principal)
    {
        this._id_user_principal = new_id_user_principal;
    }

    public String get_s_cd_status()
    {
        return this._s_cd_status;
    }

    public void set_s_cd_status(String new_s_cd_status)
    {
        this._s_cd_status = new_s_cd_status;
    }

    public String get_s_cd_role()
    {
        return this._s_cd_role;
    }

    public void set_s_cd_role(String new_s_cd_role)
    {
        this._s_cd_role = new_s_cd_role;
    }

    public boolean get_b_is_project_admin()
    {
        return this._b_is_project_admin;
    }

    public void set_b_is_project_admin(boolean bIsAdmin)
    {
        this._b_is_project_admin = bIsAdmin;
    }

    public int get_id_project()
    {
        return this._id_project;
    }

    public void set_id_project(int new_id_project)
    {
        this._id_project = new_id_project;
    }

    public Date get_dt_archive_date()
    {
        return this._dt_archive_date;
    }

    public void set_dt_archive_date(Date new_dt_archive_date)
    {
        this._dt_archive_date = new_dt_archive_date;
    }

    ////////////////////////////

    public ProjectDTO getProject()
    {
        return this._dto_project;
    }

    public void setProject(ProjectDTO new_dto_project)
    {
        this._dto_project = new_dto_project;
    }

    public UserPrincipalDTO getUserPrincipal()
    {
        return this._dto_user_principal;
    }

    public void setUserPrincipal(UserPrincipalDTO new_dto_user_principal)
    {
        this._dto_user_principal = new_dto_user_principal;
    }

    ///////////CODE_DECODE - STATUS////////////////

    //These values match the CODEDECODE values
    public void setStatusActive()
    {
        this._s_cd_status = "ACTIVE";
    }

    public void setStatusInvited()
    {
        this._s_cd_status = "INVITED";
    }

    public void setStatusRemoved()
    {
        this._s_cd_status = "REMOVED";
    }

    public boolean isStatusActive()
    {
        if("ACTIVE".equals(this._s_cd_status))
            return true;
        else
            return false;
    }

    public boolean isStatusInvited()
    {
        if("INVITED".equals(this._s_cd_status))
            return true;
        else
            return false;
    }

    public boolean isStatusRemoved()
    {
        if("REMOVED".equals(this._s_cd_status))
            return true;
        else
            return false;
    }

    ///////////CODE_DECODE - ROLE////////////////

    //These values match the CODEDECODE values
    public void setRoleToOther()
    {
        this._s_cd_role = "OTHER";
    }

    public boolean isRoleOther()
    {
        if("OTHER".equals(this._s_cd_role))
            return true;
        else
            return false;
    }

    ///////////////////Helpers

    public TransResponse validateFieldsAll(TransResponse tr)
    {
        validateFieldsBase(tr);

        if (_id_user_principal == 0)
        {
            tr.addException("Required Field: User");
        }

        if (_id_project == 0)
        {
            tr.addException("Required Field: Project");
        }

        return tr;
    }

    public TransResponse validateFieldsBase(TransResponse tr)
    {
        if (_s_cd_status == null || _s_cd_status.isEmpty())
        {
            tr.addException("Required Field: Status");
        }

        if (_s_cd_role == null || _s_cd_role.isEmpty())
        {
            tr.addException("Required Field: Role");
        }

        return tr;
    }

}
