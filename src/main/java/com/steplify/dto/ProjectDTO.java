package com.steplify.dto;

import com.steplify.utility.TransResponse;
import com.steplify.utility.UI;
import java.util.*;

public class ProjectDTO extends SuperDuperDTO {

    private String _s_address;
    private String _s_city;
    private String _s_state;
    private String _s_country;
    private String _s_postal_code;
    private String _s_url_website;
    private String _s_url_image;
    private String _s_description;
    private String _s_cd_project_type;
    private String _s_url_attachments;
    private List<TopicDTO> _lst_topics = new ArrayList<TopicDTO>();
    private List<ProjectMemberDTO> _lst_members = new ArrayList<ProjectMemberDTO>();

    public String get_s_address()
    {
        return this._s_address;
    }

    public void set_s_address(String sAddress)
    {
        this._s_address = sAddress;
    }

    public String get_s_city()
    {
        return this._s_city;
    }

    public void set_s_city(String sCity)
    {
        this._s_city = sCity;
    }

    public String get_s_state()
    {
        return this._s_state;
    }

    public void set_s_state(String sState)
    {
        this._s_state = sState;
    }

    public String get_s_country()
    {
        return this._s_country;
    }

    public void set_s_country(String sCountry)
    {
        this._s_country = sCountry;
    }

    public String get_s_postal_code()
    {
        return this._s_postal_code;
    }

    public void set_s_postal_code(String sPostalCode)
    {
        this._s_postal_code = sPostalCode;
    }

    public String get_s_url_website()
    {
        return this._s_url_website;
    }

    public void set_s_url_website(String sURL)
    {
        this._s_url_website = sURL;
    }

    public String get_s_url_image()
    {
        return this._s_url_image;
    }

    public void set_s_url_image(String sURL)
    {
        this._s_url_image = sURL;
    }

    public String get_s_description()
    {
        return this._s_description;
    }

    public void set_s_description(String sDescription)
    {
        this._s_description = sDescription;
    }

    public String get_s_cd_project_type()
    {
        return this._s_cd_project_type;
    }

    public void set_s_cd_project_type(String sProjectTypeCD)
    {
        this._s_cd_project_type = sProjectTypeCD;
    }

    public String get_s_url_attachments()
    {
        return this._s_url_attachments;
    }

    public void set_s_url_attachments(String new_s_url_attachments)
    {
        this._s_url_attachments = new_s_url_attachments;
    }

    ///////////CODE_DECODE - STATUS////////////////

    //These values match the CODEDECODE values
    public boolean isTypeOther()
    {
        if("OTHER".equals(this._s_cd_project_type))
            return true;
        else
            return false;
    }

    public boolean isTypeRemodel()
    {
        if("REMODEL".equals(this._s_cd_project_type))
            return true;
        else
            return false;
    }

    public boolean isTypeSale()
    {
        if("HOME_SALE".equals(this._s_cd_project_type))
            return true;
        else
            return false;
    }

    public boolean isTypeBuy()
    {
        if("HOME_BUY".equals(this._s_cd_project_type))
            return true;
        else
            return false;
    }

    ////////////////////////

    public List<TopicDTO> getTopics()
    {
        return this._lst_topics;
    }

    public void addTopic(TopicDTO dtoTopic)
    {
        dtoTopic.set_id_project(this.get_id());
        this._lst_topics.add(dtoTopic);
    }

    public List<ProjectMemberDTO> getMembers()
    {
        return this._lst_members;
    }

    public void addMember(ProjectMemberDTO dtoMember)
    {
        dtoMember.set_id_project(this.get_id());
        this._lst_members.add(dtoMember);
    }

    ///////////////////Helpers

    public TransResponse validateFieldsForNew(TransResponse tr)
    {
        if (_s_address == null || _s_address.isEmpty())
        {
            tr.addException("Required Field: Address");
        }

        if (_s_city == null || _s_city.isEmpty())
        {
            tr.addException("Required Field: City");
        }

        if (_s_state == null || _s_state.isEmpty())
        {
            tr.addException("Required Field: State");
        }

//        if (_s_postal_code == null || _s_postal_code.isEmpty())
//        {
//            tr.addException("Required Field: Postal Code");
//        }

        if (_s_country == null || _s_country.isEmpty())
        {
            tr.addException("Required Field: Country");
        }

        if (_s_cd_project_type == null)
        {
            tr.addException("Required Field: Project Type");
        }

        if (_s_description == null || _s_description.isEmpty())
        {
            tr.addException("Required Field: Client Name");
        }

        if (_s_url_website != null && !_s_url_website.isEmpty() && !UI.startsWithHttp(_s_url_website))
        {
            tr.addException("Invalid: Website URL");
        }

        if (_s_url_image != null && !_s_url_image.isEmpty() && !UI.startsWithHttp(_s_url_image))
        {
            tr.addException("Invalid: Image URL");
        }

        //tr.addWarning("Test Warning");

        return tr;
    }

    ///////////Purely for JSP UI convenience - Usually a no no to put this here

    public String getMapURL()
    {
        try
        {
            StringBuilder sbFormattedAddress = new StringBuilder();

            if (_s_address != null && !_s_address.isEmpty())
            {
                sbFormattedAddress.append(_s_address).append(", ");
            }

            if (_s_city != null && !_s_city.isEmpty())
            {
                sbFormattedAddress.append(_s_city).append(", ");
            }

            if (_s_state != null && !_s_state.isEmpty())
            {
                sbFormattedAddress.append(_s_state).append(", ");
            }

            if (_s_postal_code != null && !_s_postal_code.isEmpty())
            {
                sbFormattedAddress.append(_s_postal_code).append(", ");
            }


            if (_s_country != null && !_s_country.isEmpty())
            {
                sbFormattedAddress.append(_s_country);
            }

            //Get the results so far
            String sCompleteAddress = sbFormattedAddress.toString();

            //URL Encode the parameter string
            try
            {
                sCompleteAddress = java.net.URLEncoder.encode(sCompleteAddress, "UTF-8");
            }
            catch (Exception ex)
            {
                //What would you like me to do???
            }

            String sMapLink = String.format("http://maps.google.com/?q=%s", sCompleteAddress);
            return sMapLink;
        }
        catch (Exception ex)
        {
            //Forget it, we tried our best
        }

        //Hopefully we never make it here
        return "http://maps.google.com/";
    }

}
