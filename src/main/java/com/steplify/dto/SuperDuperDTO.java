package com.steplify.dto;

import com.google.gson.Gson;

import java.util.Date;


public class SuperDuperDTO {

    //Every DTO will have option to persist these
    private int _id;
    private int _id_created_by;
    private int _id_updated_by;
    private Date _dt_create_date;
    private Date _dt_last_update;

    public int get_id()
    {
        return this._id;
    }

    public void set_id(int iID)
    {
        this._id = iID;
    }

    public int get_id_created_by()
    {
        return this._id_created_by;
    }

    public void set_id_created_by(int iID)
    {
        this._id_created_by = iID;
    }

    public int get_id_updated_by()
    {
        return this._id_updated_by;
    }

    public void set_id_updated_by(int iID)
    {
        this._id_updated_by = iID;
    }

    public Date get_dt_create_date()
    {
        return this._dt_create_date;
    }

    public void set_dt_create_date(Date dt)
    {
        this._dt_create_date = dt;
    }

    public Date get_dt_last_update()
    {
        return this._dt_last_update;
    }

    public void set_dt_last_update(Date dt)
    {
        this._dt_last_update = dt;
    }

    //Used for communicating with UI Layer and REST calls
    public String toJSON()
    {
        Gson gson = new Gson();
        String json = gson.toJson(this);
        return json;
    }

    public static <T> T fromJson(String json, Class<T> classOfT)
    {
        Gson gson = new Gson();
        return gson.fromJson(json, classOfT);
    }

    public static String nullJSON()
    {
        //This is just a way to make sure we get something to the client, helps debuging
        return "{\"null\":\"null\"}";
    }

}
