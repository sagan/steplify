package com.steplify.dto;

public class UserProfileDTO extends SuperDuperDTO {

    private String _s_first_name;
    private String _s_last_name;
    private String _s_url_personal_img;
    private String _s_company_name;
    private String _s_url_company_img;
    private String _s_address;
    private String _s_city;
    private String _s_state;
    private String _s_country;
    private String _s_postal_code;
    private String _s_phone_number;
    private String _s_url_website;

    public String get_s_first_name()
    {
        return this._s_first_name;
    }

    public void set_s_first_name(String sFirstName)
    {
        this._s_first_name = sFirstName;
    }

    public String get_s_last_name()
    {
        return this._s_last_name;
    }

    public void set_s_last_name(String sLastName)
    {
        this._s_last_name = sLastName;
    }

    public String get_s_url_personal_img()
    {
        return this._s_url_personal_img;
    }

    public void set_s_url_personal_img(String sURL)
    {
        this._s_url_personal_img = sURL;
    }

    public String get_s_company_name()
    {
        return this._s_company_name;
    }

    public void set_s_company_name(String sCompanyName)
    {
        this._s_company_name = sCompanyName;
    }

    public String get_s_url_company_img()
    {
        return this._s_url_company_img;
    }

    public void set_s_url_company_img(String sURL)
    {
        this._s_url_company_img = sURL;
    }

    public String get_s_address()
    {
        return this._s_address;
    }

    public void set_s_address(String sAddress)
    {
        this._s_address = sAddress;
    }

    public String get_s_city()
    {
        return this._s_city;
    }

    public void set_s_city(String sCity)
    {
        this._s_city = sCity;
    }

    public String get_s_state()
    {
        return this._s_state;
    }

    public void set_s_state(String sState)
    {
        this._s_state = sState;
    }

    public String get_s_country()
    {
        return this._s_country;
    }

    public void set_s_country(String sCountry)
    {
        this._s_country = sCountry;
    }

    public String get_s_postal_code()
    {
        return this._s_postal_code;
    }

    public void set_s_postal_code(String sPostalCode)
    {
        this._s_postal_code = sPostalCode;
    }

    public String get_s_phone_number()
    {
        return this._s_phone_number;
    }

    public void set_s_phone_number(String sPhoneNumber)
    {
        this._s_phone_number = sPhoneNumber;
    }

    public String get_s_url_website()
    {
        return this._s_url_website;
    }

    public void set_s_url_website(String sURL)
    {
        this._s_url_website = sURL;
    }

    ///////////////////////////////////////////

    public boolean isIncomplete()
    {
        if (_s_first_name == null || "".equals(_s_first_name))
        {
            //We could add more checks if we wanted
            return true;
        }
        else
        {
            return false;
        }
    }

}
