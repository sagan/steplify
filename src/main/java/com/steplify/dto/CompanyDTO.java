package com.steplify.dto;

import com.steplify.utility.TransResponse;
import com.steplify.utility.UI;

import java.util.ArrayList;
import java.util.List;

public class CompanyDTO extends SuperDuperDTO {

    private String _s_name;
    private String _s_description;

    public String get_s_name()
    {
        return this._s_name;
    }

    public void set_s_name(String new_s_name)
    {
        this._s_name = new_s_name;
    }

    public String get_s_description()
    {
        return this._s_description;
    }

    public void set_s_description(String new_s_description)
    {
        this._s_description = new_s_description;
    }

}
