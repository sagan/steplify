package com.steplify.dto;

import com.steplify.utility.TransResponse;
import com.steplify.utility.UI;

import java.util.*;

public class TaskDTO extends SuperDuperDTO {

    private String _s_name;
    private String _s_description;
    private int _i_order;
    private String _s_cd_status;
    private Date _dt_due;
    private int _id_assigned_user;
    private int _id_project;
    private int _id_topic;
    private List<TaskCommentDTO> _lst_task_comments = new ArrayList<TaskCommentDTO>();
    private UserPrincipalDTO _dto_user_principal_assigned;

    public String get_s_name()
    {
        return this._s_name;
    }

    public void set_s_name(String new_s_name)
    {
        this._s_name = new_s_name;
    }

    public String get_s_description()
    {
        return this._s_description;
    }

    public void set_s_description(String new_s_description)
    {
        this._s_description = new_s_description;
    }

    public int get_i_order()
    {
        return this._i_order;
    }

    public void set_i_order(int new_i_order)
    {
        this._i_order = new_i_order;
    }

    public String get_s_cd_status()
    {
        return this._s_cd_status;
    }

    public void set_s_cd_status(String new_s_cd_status)
    {
        this._s_cd_status = new_s_cd_status;
    }

    public Date get_dt_due()
    {
        return this._dt_due;
    }

    public void set_dt_due(Date new_dt_due)
    {
        this._dt_due = new_dt_due;
    }

    public int get_id_assigned_user()
    {
        return this._id_assigned_user;
    }

    public void set_id_assigned_user(int new_id_assigned_user)
    {
        this._id_assigned_user = new_id_assigned_user;
    }

    public int get_id_project()
    {
        return this._id_project;
    }

    public void set_id_project(int new_id_project)
    {
        this._id_project = new_id_project;
    }

    public int get_id_topic()
    {
        return this._id_topic;
    }

    public void set_id_topic(int new_id_topic)
    {
        this._id_topic = new_id_topic;
    }

    ///////////DERIVATIONS/////////////////////////
    public boolean isOverdue()
    {
        try
        {
            Date dtNow = new Date();
            if (dtNow.after(get_dt_due()) && !isFinished())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            //Assume there is no due date???
            return false;
        }
    }

    public boolean isFinished()
    {
        return isStatusCompleted() || isStatusDeleted();
    }

    ///////////CODE_DECODE - STATUS////////////////

    //These values match the CODEDECODE values
    public void setStatusNew()
    {
        this._s_cd_status = "NEW";
    }

    public void setStatusAcknowledged()
    {
        this._s_cd_status = "ACKNOWLEDGED";
    }

    public void setStatusWorking()
    {
        this._s_cd_status = "WORKING";
    }

    public void setStatusPaused()
    {
        this._s_cd_status = "PAUSE";
    }

    public void setStatusStuck()
    {
        this._s_cd_status = "STUCK";
    }

    public void setStatusAlmostDone()
    {
        this._s_cd_status = "ALMOST_DONE";
    }

    public void setStatusCompleted()
    {
        this._s_cd_status = "COMPLETE";
    }

    public void setStatusReopened()
    {
        this._s_cd_status = "REOPEN";
    }

    public void setStatusDeleted()
    {
        this._s_cd_status = "DELETE";
    }

    public boolean isStatusNew()
    {
        if("NEW".equals(this._s_cd_status))
            return true;
        else
            return false;
    }

    public boolean isStatusAcknowledged()
    {
        if("ACKNOWLEDGED".equals(this._s_cd_status))
            return true;
        else
            return false;
    }

    public boolean isStatusWorking()
    {
        if("WORKING".equals(this._s_cd_status))
            return true;
        else
            return false;
    }

    public boolean isStatusPaused()
    {
        if("PAUSE".equals(this._s_cd_status))
            return true;
        else
            return false;
    }

    public boolean isStatusStuck()
    {
        if("STUCK".equals(this._s_cd_status))
            return true;
        else
            return false;
    }

    public boolean isStatusAlmostDone()
    {
        if("ALMOST_DONE".equals(this._s_cd_status))
            return true;
        else
            return false;
    }

    public boolean isStatusCompleted()
    {
        if("COMPLETE".equals(this._s_cd_status))
            return true;
        else
            return false;
    }

    public boolean isStatusReopened()
    {
        if("REOPEN".equals(this._s_cd_status))
            return true;
        else
            return false;
    }

    public boolean isStatusDeleted()
    {
        if("DELETE".equals(this._s_cd_status))
            return true;
        else
            return false;
    }

    ////////////////////////

    public List<TaskCommentDTO> getTaskComments()
    {
        return this._lst_task_comments;
    }

    public void addTaskComment(TaskCommentDTO dtoTaskComment)
    {
        dtoTaskComment.set_id_task(this.get_id());
        dtoTaskComment.set_id_project(this.get_id_project());
        this._lst_task_comments.add(dtoTaskComment);
    }

    public UserPrincipalDTO getUserPrincipalAssigned()
    {
        return this._dto_user_principal_assigned;
    }

    public void setUserPrincipalAssigned(UserPrincipalDTO new_dto_user_principal)
    {
        this._dto_user_principal_assigned = new_dto_user_principal;
    }

    public String getUserPrincipalAssignedName()
    {
        if (this._dto_user_principal_assigned == null)
        {
            return "";
        }
        else
        {
            return this._dto_user_principal_assigned.get_s_username();
        }
    }

    //////////////Helpers/////////////

    public TransResponse validateFields(TransResponse tr)
    {

        if (UI.isNullOrEmpty(_s_name))
        {
            tr.addException("Required Field: Name");
        }

        if (UI.isNullOrEmpty(_s_cd_status))
        {
            tr.addException("Required Field: Status");
        }

        if (_id_assigned_user == 0)
        {
            tr.addException("Required Field: Assigned Member");
        }

        if (_dt_due == null)
        {
            tr.addException("Required Field: Due Date missing or invalid format");
        }

        if (_id_topic == 0)
        {
            tr.addException("Required Field: TopicID");
        }

        if (_id_project == 0)
        {
            tr.addException("Required Field: ProjectID");
        }

        return tr;
    }

}
