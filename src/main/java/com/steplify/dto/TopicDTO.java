package com.steplify.dto;

import com.steplify.utility.TransResponse;
import com.steplify.utility.UI;

import java.util.*;

public class TopicDTO extends SuperDuperDTO {

    private String _s_name;
    private String _s_description;
    private String _s_url_attachments;
    private boolean _b_auto_add_members;
    private int _i_order;
    private int _id_project;
    private List<TopicMemberDTO> _lst_topic_members = new ArrayList<TopicMemberDTO>();
    private List<TaskDTO> _lst_tasks = new ArrayList<TaskDTO>();

    public String get_s_name()
    {
        return this._s_name;
    }

    public void set_s_name(String new_s_name)
    {
        this._s_name = new_s_name;
    }

    public String get_s_description()
    {
        return this._s_description;
    }

    public void set_s_description(String new_s_description)
    {
        this._s_description = new_s_description;
    }

    public String get_s_url_attachments()
    {
        return this._s_url_attachments;
    }

    public void set_s_url_attachments(String new_s_url_attachments)
    {
        this._s_url_attachments = new_s_url_attachments;
    }

    public boolean get_b_auto_add_members()
    {
        return this._b_auto_add_members;
    }

    public void set_b_auto_add_members(boolean bAutoAdd)
    {
        this._b_auto_add_members = bAutoAdd;
    }

    public int get_i_order()
    {
        return this._i_order;
    }

    public void set_i_order(int new_i_order)
    {
        this._i_order = new_i_order;
    }

    public int get_id_project()
    {
        return this._id_project;
    }

    public void set_id_project(int new_id_project)
    {
        this._id_project = new_id_project;
    }

    ////////////////////////

    public List<TopicMemberDTO> getTopicMembers()
    {
        return this._lst_topic_members;
    }

    public void addTopicMember(TopicMemberDTO dtoTopicMember)
    {
        dtoTopicMember.set_id_topic(this.get_id());
        dtoTopicMember.set_id_project(this.get_id_project());
        this._lst_topic_members.add(dtoTopicMember);
    }

    public List<TaskDTO> getTasks()
    {
        return this._lst_tasks;
    }

    public void addTask(TaskDTO dtoTask)
    {
        dtoTask.set_id_topic(this.get_id());
        dtoTask.set_id_project(this.get_id_project());
        this._lst_tasks.add(dtoTask);
    }

    //////////////Helpers/////////////

    public TransResponse validateFields(TransResponse tr)
    {

        if (UI.isNullOrEmpty(_s_name))
        {
            tr.addException("Required Field: Topic Name");
        }

        if (_s_url_attachments != null && !_s_url_attachments.isEmpty() && !UI.startsWithHttp(_s_url_attachments))
        {
            tr.addException("Required Field: URL must start with http");
        }

        if (_i_order < 1 || _i_order > 99)
        {
            tr.addException("Required Field: Order should be between 1 and 99");
        }

        if (_id_project == 0)
        {
            tr.addException("Required Field: Project");
        }

        return tr;
    }

}
