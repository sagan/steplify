package com.steplify.dto;

import com.steplify.utility.TransResponse;
import com.steplify.utility.UI;

public class TaskCommentDTO extends SuperDuperDTO {

    private String _s_comment;
    private int _id_project;
    private int _id_task;
    private UserPrincipalDTO _dto_user_principal_created_by;

    public String get_s_comment()
    {
        return this._s_comment;
    }

    public void set_s_comment(String new_s_comment)
    {
        this._s_comment = new_s_comment;
    }

    public int get_id_project()
    {
        return this._id_project;
    }

    public void set_id_project(int new_id_project)
    {
        this._id_project = new_id_project;
    }

    public int get_id_task()
    {
        return this._id_task;
    }

    public void set_id_task(int new_id_task)
    {
        this._id_task = new_id_task;
    }

    /////////////////////////////////

    public UserPrincipalDTO getUserPrincipalCreatedBy()
    {
        return this._dto_user_principal_created_by;
    }

    public void setUserPrincipalCreatedBy(UserPrincipalDTO new_dto_user_principal)
    {
        this._dto_user_principal_created_by = new_dto_user_principal;
    }

    public String getUserPrincipalCreatedByName()
    {
        if (this._dto_user_principal_created_by == null)
        {
            return "";
        }
        else
        {
            return this._dto_user_principal_created_by.get_s_username();
        }
    }

    //////////////Helpers/////////////

    public TransResponse validateFields(TransResponse tr)
    {

        if (UI.isNullOrEmpty(_s_comment))
        {
            tr.addException("Required Field: Comment");
        }

        if (_id_task == 0)
        {
            tr.addException("Required Field: TaskID");
        }

        if (_id_project == 0)
        {
            tr.addException("Required Field: ProjectID");
        }

        if (get_id_created_by() == 0)
        {
            tr.addException("Required Field: UserCreatedByID");
        }

        return tr;
    }

}
