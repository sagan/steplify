package com.steplify.dto;


import com.steplify.utility.TransResponse;
import com.steplify.utility.UI;

public class TopicMemberDTO extends SuperDuperDTO {

    private int _id_user_principal;
    private String _s_cd_authority;
    private int _id_project;
    private int _id_topic;
    private UserPrincipalDTO _dto_user_principal;

    public int get_id_user_principal()
    {
        return this._id_user_principal;
    }

    public void set_id_user_principal(int new_id_user_principal)
    {
        this._id_user_principal = new_id_user_principal;
    }

    public String get_s_cd_authority()
    {
        return this._s_cd_authority;
    }

    public void set_s_cd_authority(String new_s_cd_authority)
    {
        this._s_cd_authority = new_s_cd_authority;
    }

    public int get_id_project()
    {
        return this._id_project;
    }

    public void set_id_project(int new_id_project)
    {
        this._id_project = new_id_project;
    }

    public int get_id_topic()
    {
        return this._id_topic;
    }

    public void set_id_topic(int new_id_topic)
    {
        this._id_topic = new_id_topic;
    }

    ///////////////////////////////////////

    public UserPrincipalDTO getUserPrincipal()
    {
        return this._dto_user_principal;
    }

    public void setUserPrincipal(UserPrincipalDTO new_dto_user_principal)
    {
        this._dto_user_principal = new_dto_user_principal;
    }

    ///////////CODE_DECODE - AUTHORITY////////////////

    //These values match the CODEDECODE values
    public void setAuthorityWriter()
    {
        this._s_cd_authority = "WRITER";
    }

    public void setAuthorityReader()
    {
        this._s_cd_authority = "READER";
    }

    public void setAuthorityNone()
    {
        this._s_cd_authority = "NONE";
    }

    public boolean isAuthorityWriter()
    {
        if("WRITER".equals(this._s_cd_authority))
            return true;
        else
            return false;
    }

    public boolean isAuthorityReader()
    {
        if("READER".equals(this._s_cd_authority))
            return true;
        else
            return false;
    }

    public boolean isAuthorityNone()
    {
        if(this._s_cd_authority == null || "NONE".equals(this._s_cd_authority))
            return true;
        else
            return false;
    }

    //////////////Helpers/////////////

    public TransResponse validateFields(TransResponse tr)
    {

        if (_id_user_principal == 0)
        {
            tr.addException("Required Field: TopicMember User");
        }

        if (_s_cd_authority == null || _s_cd_authority.isEmpty())
        {
            tr.addException("Required Field: TopicMember Authority");
        }

        if (_id_project == 0)
        {
            tr.addException("Required Field: TopicMember Project");
        }

        if (_id_topic == 0)
        {
            tr.addException("Required Field: TopicMember Topic");
        }

        return tr;
    }

}
