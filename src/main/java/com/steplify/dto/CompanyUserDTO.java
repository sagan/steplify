package com.steplify.dto;

public class CompanyUserDTO extends SuperDuperDTO {

    private int _id_company;
    private int _id_user_principal;

    public int get_id_company()
    {
        return this._id_company;
    }

    public void set_id_company(int new_id_company)
    {
        this._id_company = new_id_company;
    }

    public int get_id_user_principal()
    {
        return this._id_user_principal;
    }

    public void set_id_user_principal(int new_id_user_principal)
    {
        this._id_user_principal = new_id_user_principal;
    }

    /////////////////////////////////////////

    //Everyone gets a CompanyUser record, but 0 means it means its not a real/explicit assignment
    //See CompanyDAO > selectCompanyUserByUserID()
    public boolean isAssignedCompanyUser()
    {
        return get_id() != 0;
    }

}
