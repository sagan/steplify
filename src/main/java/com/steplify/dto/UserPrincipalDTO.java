package com.steplify.dto;

import com.steplify.utility.UI;
import org.apache.commons.codec.digest.DigestUtils;
import java.util.Date;
import java.util.Random;
import java.util.StringTokenizer;
import org.apache.commons.validator.routines.*;


public class UserPrincipalDTO extends SuperDuperDTO {

    private String _s_username;
    private String _s_password_hash;
    private String _s_password_backup_hash;
    private String _s_cd_status;
    private UserProfileDTO _dto_user_profile;

    public String get_s_username()
    {
        return this._s_username;
    }

    //Use the public API instead, only for DB ORM
    public void set_s_username(String sUsername)
    {
        this._s_username = sUsername;
    }

    public String get_s_password_hash()
    {
        return this._s_password_hash;
    }

    //Use the public API instead, only for DB ORM
    public void set_s_password_hash(String sPasswordHash)
    {
        this._s_password_hash = sPasswordHash;
    }

    public String get_s_password_backup_hash()
    {
        return this._s_password_backup_hash;
    }

    //Use the public API instead, only for DB ORM
    public void set_s_password_backup_hash(String sPasswordHash)
    {
        this._s_password_backup_hash = sPasswordHash;
    }

    public String get_s_cd_status()
    {
        return this._s_cd_status;
    }

    //Use the public API instead, only for DB ORM
    public void set_s_cd_status(String sStatus)
    {
        this._s_cd_status = sStatus;
    }

    //////////////////////////

    public UserProfileDTO getUserProfile()
    {
        return _dto_user_profile;
    }

    public void setUserProfile(UserProfileDTO dto)
    {
        //The Profile now belongs to this UserPrincipal
        dto.set_id_created_by(this.get_id());
        _dto_user_profile = dto;
    }

    //This allows the Tomcat Session Manager to tell us something useful
    public String toString()
    {
        //In case of null, ensure a string
        return "" + this._s_username;
    }

    /////////////MISC APIs/////////////////

    public String prettyPrint()
    {
        String sDerivedName = null;
        if (_dto_user_profile != null &&
                _dto_user_profile.get_s_first_name() != null  && _dto_user_profile.get_s_last_name() != null)
        {
            sDerivedName = "" + _dto_user_profile.get_s_first_name() + " " + _dto_user_profile.get_s_last_name();
        }

        if (UI.isNullOrEmpty(sDerivedName))
        {
            return _s_username;
        }
        else
        {
            return sDerivedName;
        }
    }

    public boolean setNewUsername(String sUsername)
    {
        if (isUsernameValid(sUsername))
        {
            String sName = sUsername.trim().toLowerCase();
            this._s_username = sName;
            return true;
        }
        else
        {
            //Password is too insecure
            throw new RuntimeException("Not a valid email address");
        }
    }

    public boolean setNewPassword(String sClearPassword)
    {
        if (isPasswordSecure(sClearPassword))
        {
            //Generate the HASH, and set it!
            this._s_password_hash = generatePassHash(sClearPassword);
            return true;
        }
        else
        {
            //Password is too insecure
            throw new RuntimeException("Password length should be 5-32 characters, with no spaces");
        }
    }

    public boolean setNewPasswordBackup(String sClearPassword)
    {
        if (isPasswordSecure(sClearPassword))
        {
            //Generate the HASH, and set it!
            this._s_password_backup_hash = generatePassHash(sClearPassword);
            return true;
        }
        else
        {
            //Password is too insecure
            throw new RuntimeException("Password length should be at least 5 characters, with no spaces");
        }
    }

    //Use this to compare a login password, with the DB source password
    public boolean verifyPassword(String sClearPassword)
    {
        //To avoid any mishap of null or empty string password injections, or unexpected DB state
        if (!isPasswordSecure(sClearPassword))
        {
            return false;
        }

        //Lets get the hashed values
        String tempPassword = generatePassHash(sClearPassword);
        String sourcePassword = this.get_s_password_hash();
        String sourcePasswordBackup = this.get_s_password_backup_hash();

        //See if the password matches either Primary or Backup
        if ((tempPassword.equals(sourcePassword)) || (tempPassword.equals(sourcePasswordBackup)))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public String generateNewPasswordBackup()
    {
        //generate a new large random number
        Random rnd = new Random((new Date()).getTime());
        int basePass = rnd.nextInt(1000000000);
        basePass = basePass + 100000;

        //Convert to something alpha-numeric, and clean it up for a human to consume
        String sClearPassword = DigestUtils.sha1Hex(String.valueOf(basePass));
        sClearPassword = sClearPassword.toLowerCase().substring(5,15);

        //Set the new password as the backup
        this.setNewPasswordBackup(sClearPassword);
        return sClearPassword;
    }

    //These values match the CODEDECODE values
    public void setStatusActive()
    {
        this._s_cd_status = "ACTIVE";
    }

    public void setStatusLocked()
    {
        this._s_cd_status = "LOCKED";
    }

    public boolean isAccountActive()
    {
        if("ACTIVE".equals(this._s_cd_status))
            return true;
        else
            return false;
    }

    /////////////Private Helpers/////////////////

    private String generatePassHash(String sClearPassword)
    {
        //Use Apache Commons Codecs for Encryption
        String sSalt = "BetterThanNothing";
        String sPassHash = DigestUtils.md5Hex(sSalt + sClearPassword);
        return sPassHash;
    }

    private boolean isPasswordSecure(String sClearPassword)
    {
        if (sClearPassword == null)
            return false;

        //Make them at least 5 char long
        if (sClearPassword.length() < 5)
            return false;

        //Limit them at 32 char long
        if (sClearPassword.length() > 32)
            return false;

        //This is why we dont have to trim() it anywhere else before this
        if (sClearPassword.contains(" "))
            return false;

        return true;
    }

    private boolean isUsernameValid(String sUsername)
    {
        boolean result = true;

        if (sUsername == null)
            return false;

        //a@b.tv
        if (sUsername.length() < 6)
            return false;

        //This is a DB column length constraint
        if (sUsername.length() > 100)
            return false;

        StringTokenizer stPeriod = new StringTokenizer(sUsername, ".");
        if (stPeriod.countTokens() < 2)
            return false;

        StringTokenizer stAt = new StringTokenizer(sUsername, "@");
        if (stAt.countTokens() < 2)
            return false;

        //Use Apache Commons to do the Validation, javaMail isnt very strict
        boolean bIsValid = EmailValidator.getInstance().isValid(sUsername);
        if (!bIsValid)
        {
            return false;
        }

        //You made it to the end of all the checks!
        return true;
    }

}
