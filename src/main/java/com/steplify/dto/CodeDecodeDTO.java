package com.steplify.dto;

public class CodeDecodeDTO extends SuperDuperDTO {

    private int _i_order;
    private String _s_category;
    private String _s_code;
    private String _s_decode;
    private boolean _b_active;

    public CodeDecodeDTO()
    {
        //Default Constructor
    }

    public CodeDecodeDTO(int iOrder, String sCat, String sCode, String sDecode, boolean bActive)
    {
        this._i_order = iOrder;
        this._s_category = sCat;
        this._s_code = sCode;
        this._s_decode = sDecode;
        this._b_active = bActive;
    }

    public int get_i_order()
    {
        return this._i_order;
    }

    public void set_i_order(int iNum)
    {
        this._i_order = iNum;
    }

    public String get_s_category()
    {
        return this._s_category;
    }

    public void set_s_category(String sCat)
    {
        this._s_category = sCat;
    }

    public String get_s_code()
    {
        return this._s_code;
    }

    public void set_s_code(String sCode)
    {
        this._s_code = sCode;
    }

    public String get_s_decode()
    {
        return this._s_decode;
    }

    public void set_s_decode(String sDecode)
    {
        this._s_decode = sDecode;
    }

    public boolean get_b_active()
    {
        return this._b_active;
    }

    public void set_b_active(boolean bActive)
    {
        this._b_active = bActive;
    }

}
