package com.steplify.dto;

import com.steplify.utility.UI;

public class CompanyTopicTaskDTO extends SuperDuperDTO {

    private String _s_topic_name;
    private int _i_topic_order;
    private String _s_task_name;
    private int _id_company;
    private String _s_cd_project_type;

    public String get_s_topic_name()
    {
        return this._s_topic_name;
    }

    public void set_s_topic_name(String new_s_topic_name)
    {
        this._s_topic_name = new_s_topic_name;
    }

    public int get_i_topic_order()
    {
        return this._i_topic_order;
    }

    public void set_i_topic_order(int new_i_topic_order)
    {
        this._i_topic_order = new_i_topic_order;
    }

    public String get_s_task_name()
    {
        return this._s_task_name;
    }

    public void set_s_task_name(String new_s_task_name)
    {
        this._s_task_name = new_s_task_name;
    }

    public int get_id_company()
    {
        return this._id_company;
    }

    public void set_id_company(int new_id_company)
    {
        this._id_company = new_id_company;
    }

    public String get_s_cd_project_type()
    {
        return this._s_cd_project_type;
    }

    public void set_s_cd_project_type(String new_s_cd_project_type)
    {
        this._s_cd_project_type = new_s_cd_project_type;
    }

    //////////////////Helpers

    public boolean isValid()
    {
        if (UI.isNullOrEmpty(_s_topic_name))
        {
            return false;
        }

        if (_i_topic_order < 1 || _i_topic_order > 99)
        {
            return false;
        }

        if (UI.isNullOrEmpty(_s_task_name))
        {
            return false;
        }

        if (UI.isNullOrEmpty(_s_cd_project_type))
        {
            return false;
        }

        //Dont validate the company, since 0 is real

        return true;
    }

}
