package com.steplify.utility;

// Import required java libraries

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//This class is invoked based on the 'filter-mapping' in web.xml
// Manage cache requests here
public class FilterNoCache implements Filter
{
    public void  init(FilterConfig config) throws ServletException{
        // Get init parameter
        //String testParam = config.getInitParameter("test-param");
    }

    //The iPhone ios6 seems to cache AJAX post calls, so we are forcing new requests for all
    public void  doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException
    {
        //http://www.einternals.com/blog/web-development/ios6-0-caching-ajax-post-requests
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        httpResponse.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1
        httpResponse.setHeader("Pragma", "no-cache"); // HTTP 1.0
        httpResponse.setDateHeader("Expires", 0); // Proxies.

        // Pass request back down the filter chain
        chain.doFilter(request,response);
    }

    public void destroy( ){
      /* Called before the Filter instance is removed
      from service by the web container*/
    }
}