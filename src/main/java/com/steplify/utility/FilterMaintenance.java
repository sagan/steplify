package com.steplify.utility;

// Import required java libraries

import javax.servlet.*;
import java.io.IOException;

//This class is invoked based on the 'filter-mapping' in web.xml
//Should be last, since we dont continue down the FilterChain if TRUE
public class FilterMaintenance implements Filter
{
    private boolean bUnderMaintain = false;

    public void  init(FilterConfig config) throws ServletException{
        //Get init parameters, one at a time
        try
        {
            String sUnderMaintenance = config.getInitParameter("under_maintenance");
            if ("true".equals(sUnderMaintenance))
            {
                bUnderMaintain = true;
            }
        }
        catch (Exception ex)
        {
            bUnderMaintain = false;
        }
    }

    //This call happens because the web.xml configures this class to handle * requests
    public void  doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException
    {
        //Should we show the maintenance page?
        if (bUnderMaintain)
        {
            request.getRequestDispatcher("/jsp_errors/maintenance.jsp").forward(request, response);
            //Stop here or else tomcat error: "Cannot forward after response has been committed"
        }
        else
        {
            //Not under maintenance, do the normal flow
            chain.doFilter(request,response);
        }
    }

    public void destroy( ){
      /* Called before the Filter instance is removed
      from service by the web container*/
    }
}