package com.steplify.utility;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.SimpleEmail;
import org.apache.log4j.Logger;
import java.util.*;
import com.steplify.dto.*;

//TODO: This should probably relay via a localhost queue or something like that
//For now lets just spin off a new thread and hope
public class Emailer extends Thread {

    private static Logger _LOGGER = Logger.getLogger("Emailer");
    private List<String> _al_to_addresses;
    private boolean _b_send_as_bcc;
    private String _s_subject;
    private String _s_msg;

    public Emailer(List<String> alTo, boolean bBCC, String subject, String msg)
    {
        this._al_to_addresses = alTo;
        this._b_send_as_bcc = bBCC;
        this._s_subject = subject;
        this._s_msg = msg;
    }

    //Call run() directly from unit tests; vs start() from app code - because of threading
    public void run()
    {
        //Try up to 3 times
        sendNow(2);
    }

    private void sendNow(int iAttemptsLeft)
    {
        try
        {
            //Only send email when config is on
            if (!Config._EMAIL_ON)
            {
                _LOGGER.info("Email no-op to: " + this._al_to_addresses.get(0));
                return;
            }

            //Make a list of all recipients, for logging purposes only
            StringBuilder sbEmailList = new StringBuilder();
            for (String sEmail : this._al_to_addresses)
            {
                sbEmailList.append(sEmail).append(", ");
            }

            _LOGGER.info("Email attempt to: " + sbEmailList.toString());
            Email email = new SimpleEmail();
            //email.setDebug(true);

            //Set the base config
            email.setHostName(Config._EMAIL_SMTP);

            //Config for TLS, SSL, or BASIC
            if (Config._EMAIL_SMTP_TLS)
            {
                email.setSmtpPort(Config._EMAIL_SMTP_PORT);
                email.setStartTLSEnabled(true);
            }
            else if (Config._EMAIL_SMTP_SSL)
            {
                email.setSslSmtpPort(String.valueOf(Config._EMAIL_SMTP_PORT));
                email.setSSLOnConnect(true);
            }
            else
            {
                email.setSmtpPort(Config._EMAIL_SMTP_PORT);
            }

            //Send user and password
            email.setAuthenticator(new DefaultAuthenticator(Config._EMAIL_USER, Config._EMAIL_PASS));

            //Set the content
            email.setFrom(Config._EMAIL_FROM);
            email.addReplyTo(Config._EMAIL_FROM);
            email.setSubject(this._s_subject);
            email.setMsg(this._s_msg);

            //Set the TO/BCC addresses, 1 by 1
            if (this._al_to_addresses != null)
            {
                for (String sAddress : this._al_to_addresses)
                {
                    if (_b_send_as_bcc)
                    {
                        email.addBcc(sAddress);
                    }
                    else
                    {
                        email.addTo(sAddress);
                    }
                }
            }

            //Always copy ourselves, so we have a copy to easily examine (bcc sends to gmail spam)
            email.addTo(Config._EMAIL_FROM);

            //Send it
            email.send();
            _LOGGER.info("Email successfully sent to: " + sbEmailList.toString());
        }
        catch (Exception ex)
        {
            //Should we be writing to a DB Error table, since we a re not queueing?
            _LOGGER.error(ex.getMessage());
            //Cause usually contains protocol detail errors to help debug
            _LOGGER.error(ex.getCause());

            //If there was an error, lets try to send again
            try
            {
                _LOGGER.error("Email retry ATTEMPTS_LEFT=" + iAttemptsLeft);
                if (iAttemptsLeft > 0)
                {
                    //Just hold up for a bit
                    Thread.sleep(10000);
                    sendNow(iAttemptsLeft-1);
                }
                else
                {
                    //All retries have been tried. give up
                    _LOGGER.error("Email retry FINAL_STATUS=FAILED");
                }
            }
            catch (Exception threadEX)
            {
                //Dont bother, call it a day...
                _LOGGER.error(threadEX.getMessage());
            }
        }
    }

    ///////////////////////Public APIs the app will call

    public static void sendRegistrationRequest(String sRegistrationMsg)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("A registration request was made.");
        sb.append("\n");
        sb.append(sRegistrationMsg);
        sb.append("\n");
        sb.append("Reminder: Someone just went to try and create a new account from the home page.");

        sb.append("\n\n");
        sb.append(Config._WEB_ADDRESS);

        //Conform to the API, it needs a List
        ArrayList<String> alTo = new ArrayList<String>();
        alTo.add(Config._SUPPORT_EMAIL);

        //No need to BCC anyone
        Emailer email = new Emailer(alTo, false, "Registration Request", sb.toString());
        email.start();
    }

    public static void sendPasswordSelfRegistration(String toEmail, String newPass)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("Welcome to Steplify, your password is: ");
        sb.append(newPass);
        sb.append("\n");
        sb.append("If you did not signup, don't worry, no one can login without this.");

        sb.append("\n\n");
        sb.append(Config._WEB_ADDRESS);

        //Conform to the API, it needs a List
        ArrayList<String> alTo = new ArrayList<String>();
        alTo.add(toEmail);

        //No need to BCC anyone
        Emailer email = new Emailer(alTo, false, "Account Created", sb.toString());
        email.start();
    }

    public static void sendPasswordReset(String toEmail, String newPass)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("Your new Steplify password is: ");
        sb.append(newPass);
        sb.append("\n");
        sb.append("If you did not request a new password, don't worry, your old one will continue to work.");

        sb.append("\n\n");
        sb.append(Config._WEB_ADDRESS);

        //Conform to the API, it needs a List
        ArrayList<String> alTo = new ArrayList<String>();
        alTo.add(toEmail);

        //No need to BCC anyone
        Emailer email = new Emailer(alTo, false, "Password Reset", sb.toString());
        email.start();
    }

    public static void sendNewMembership(String toEmail, String referralEmail, String newPassword)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("Welcome to Steplify.");
        sb.append("\n\n");
        sb.append(referralEmail);
        sb.append(" has added you as a member to their project.");

        if (newPassword != null)
        {
            sb.append("\n\n");
            sb.append("Your temp password is ");
            sb.append(newPassword);
        }

        sb.append("\n\n");
        sb.append(Config._WEB_ADDRESS);

        //Conform to the API, it needs a List
        ArrayList<String> alTo = new ArrayList<String>();
        alTo.add(toEmail);

        //No need to BCC anyone
        Emailer email = new Emailer(alTo, false, "New Membership from " + referralEmail, sb.toString());
        email.start();
    }

    public static void sendTaskComment(ArrayList<String> alTo, UserPrincipalDTO dtoUpdatingUser, ProjectDTO dtoProject, TopicDTO dtoTopic, TaskDTO dtoTask, TaskCommentDTO dtoComment)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("Comment made by: ");
        sb.append(dtoUpdatingUser.get_s_username());
        sb.append("\n\n");

        sb.append("Address: ");
        sb.append(dtoProject.get_s_address());
        sb.append("\n");
        sb.append("Client: ");
        sb.append(dtoProject.get_s_description());
        sb.append("\n");
        sb.append("ID: ");
        sb.append(dtoProject.get_id());
        sb.append("\n\n");

        sb.append("Topic: ");
        sb.append(dtoTopic.get_s_name());
        sb.append("\n");
        sb.append("Task: ");
        sb.append(dtoTask.get_s_name());
        sb.append("\n\n");

        sb.append("Comment: ");
        sb.append(dtoComment.get_s_comment());
        sb.append("\n\n");

        sb.append(Config._WEB_ADDRESS + "/task_manager/view?pid=" + dtoProject.get_id());

        String sSubject = dtoProject.get_s_address() + " - " + dtoProject.get_s_description();

        //No need to BCC anyone
        Emailer email = new Emailer(alTo, true, sSubject, sb.toString());
        email.start();
    }

    public static void sendProjectMembershipActivated(ArrayList<String> alTo, UserPrincipalDTO dtoActiveUser, UserPrincipalDTO dtoReferralUser, ProjectDTO dtoProject)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("A project member is now viewing your project for the first time: ");
        sb.append(dtoActiveUser.get_s_username());
        sb.append("\n\n");

        sb.append("If you are the project administrator, configure their membership so they can access necessary project topics.");
        sb.append("\n\n");

        sb.append("Address: ");
        sb.append(dtoProject.get_s_address());
        sb.append("\n");
        sb.append("Client: ");
        sb.append(dtoProject.get_s_description());
        sb.append("\n");
        sb.append("ID: ");
        sb.append(dtoProject.get_id());
        sb.append("\n\n");

        sb.append("Referral from: ");
        sb.append(dtoReferralUser.get_s_username());
        sb.append("\n\n");

        sb.append(Config._WEB_ADDRESS + "/task_manager/view?pid=" + dtoProject.get_id());

        String sSubject = dtoProject.get_s_address() + " - " + dtoProject.get_s_description();

        //No need to BCC anyone
        Emailer email = new Emailer(alTo, true, sSubject, sb.toString());
        email.start();
    }

    public static void sendProjectMembershipAlertOthers(ArrayList<String> alTo, UserPrincipalDTO dtoActiveUser, UserPrincipalDTO dtoReferralUser, ProjectDTO dtoProject)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("A new member has been added to your project: ");
        sb.append(dtoActiveUser.get_s_username());
        sb.append("\n\n");

        sb.append("If you are the project administrator, configure their membership so they can access necessary project topics.");
        sb.append("\n\n");

        sb.append("Address: ");
        sb.append(dtoProject.get_s_address());
        sb.append("\n");
        sb.append("Client: ");
        sb.append(dtoProject.get_s_description());
        sb.append("\n");
        sb.append("ID: ");
        sb.append(dtoProject.get_id());
        sb.append("\n\n");

        sb.append("Referral from: ");
        sb.append(dtoReferralUser.get_s_username());
        sb.append("\n\n");
        sb.append(Config._WEB_ADDRESS + "/task_manager/view?pid=" + dtoProject.get_id());

        String sSubject = dtoProject.get_s_address() + " - " + dtoProject.get_s_description();

        //No need to BCC anyone
        Emailer email = new Emailer(alTo, true, sSubject, sb.toString());
        email.start();
    }


}
