package com.steplify.utility;

//This should really all move to a CONFIG file of some sort

public class Config {
    //Release Settings
    public static final String _APP_VERSION = "1.2.8"; //BUMP THIS FOR A RELEASE DEPLOYMENT
    public static boolean _EMAIL_ON = true;
    public static String _WEB_ADDRESS = "http://steplify.homenet.org"; //"http://www.steplify.com"

    //DB Settings
    public static String _DB_HOST = "hpub"; //"steplifyproddb.cfltvuzrkhqx.us-west-2.rds.amazonaws.com"
    public static String _DB_NAME = "steplify";
    public static String _DB_USER = "DEPLOYMENT_SECRET";
    public static String _DB_PASS = "DEPLOYMENT_SECRET";

    //Email SMTP Settings
    //Outlook.com ["smtp.live.com", "DEPLOYMENT_SECRET", "DEPLOYMENT_SECRET"]
    //AWS-SES ["email-smtp.us-west-2.amazonaws.com", "DEPLOYMENT_SECRET", "DEPLOYMENT_SECRET"]
    public static String _EMAIL_SMTP = "email-smtp.us-west-2.amazonaws.com";
    public static int _EMAIL_SMTP_PORT = 587;
    public static boolean _EMAIL_SMTP_SSL = false;
    public static boolean _EMAIL_SMTP_TLS = true;
    public static String _EMAIL_USER = "DEPLOYMENT_SECRET";
    public static String _EMAIL_PASS = "DEPLOYMENT_SECRET";
    public static String _EMAIL_FROM = "Steplify Admin <admin@steplify.com>";

    //App UI Settings
    public static String FOOTER_LEFT = "&copy;2014 Steplify";
    public static String FOOTER_RIGHT = "Version: " + _APP_VERSION;
    //We dont currently capture the user's local TZ, so lets default to one for display consistency
    public static String _TIMEZONE_DISPLAY = "America/Los_Angeles";  //"America/Los_Angeles", "Etc/GMT", "Pacific/Fiji"
    public static String _SUPPORT_EMAIL = "support@steplify.com";


    //API for when running the Unit Tests
    public static void useTestDB()
    {
        //Modify this when TESTING vs CREATING your DB tables
        _DB_HOST = "localhost";
        _DB_NAME = "steplify";
        _DB_USER = "DEPLOYMENT_SECRET";
        _DB_PASS = "DEPLOYMENT_SECRET";
        _EMAIL_ON = false;
        System.out.println("Using TEST DB: " + _DB_NAME);
    }

    //So when we deploy to different envs, the change is in 1 place, not 10 JSPs
    public static String getGoogleAnalyticsScript()
    {
        return String.format("%s%n%s%n%s%n%s%n%s%n%s%n",
                "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){",
                "(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),",
                "m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)",
                "})(window,document,'script','//www.google-analytics.com/analytics.js','ga');",
                "ga('create', 'UA-46683024-1', 'homenet.org');",
                //"ga('create', 'UA-46683024-2', 'steplify.com');",
                "ga('send', 'pageview');");
    }
	
	//Encryption Key
	//This is a 16 byte secret key for AES
    public static final byte[] _AES_KEY = {
        //DEPLOYMENT_SECRET
    };

}
