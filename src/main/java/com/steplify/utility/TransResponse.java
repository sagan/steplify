package com.steplify.utility;

import java.util.ArrayList;

/**
 * When calling any sort of atomic function (i.e. a transaction), there is a result.
 * The result can have an overall status.
 * Along with informational-warnings, as well as exceptions.
 */
public class TransResponse {

    private ArrayList<String> _alWarningMessages;
    private ArrayList<String> _alExceptionMessages;

    public void addWarning(String sMsg)
    {
        if (_alWarningMessages == null)
        {
            _alWarningMessages = new ArrayList<String>();
        }

        _alWarningMessages.add(sMsg);
    }

    public void addException(String sMsg)
    {
        if (_alExceptionMessages == null)
        {
            _alExceptionMessages = new ArrayList<String>();
        }

        _alExceptionMessages.add(sMsg);
    }

    public boolean hasWarnings()
    {
        if (_alWarningMessages == null || _alWarningMessages.isEmpty())
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean hasExceptions()
    {
        if (_alExceptionMessages == null || _alExceptionMessages.isEmpty())
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean isSuccess()
    {
        return !hasExceptions();
    }

    public ArrayList<String> getWarnings()
    {
        if (_alWarningMessages == null)
        {
            _alWarningMessages = new ArrayList<String>();
        }

        return _alWarningMessages;
    }

    public ArrayList<String> getExceptions()
    {
        if (_alExceptionMessages == null)
        {
            _alExceptionMessages = new ArrayList<String>();
        }

        return _alExceptionMessages;
    }

}
