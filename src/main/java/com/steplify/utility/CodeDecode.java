package com.steplify.utility;

import com.steplify.dao.SuperDuperDAO;
import com.steplify.dto.CodeDecodeDTO;
import com.steplify.dao.CodeDecodeDAO;
import java.sql.*;
import java.util.*;

public class CodeDecode {

    public static final String _CAT_USER_STATUS = "USER_ACCOUNT_STATUS";
    public static final String _CAT_PROJECT_TYPE = "PROJECT_TYPE";
    public static final String _CAT_MEMBERSHIP_ROLE = "PROJECT_MEMBER_ROLE";
    public static final String _CAT_MEMBERSHIP_STATUS = "PROJECT_MEMBER_STATUS";
    public static final String _CAT_TASK_STATUS = "TASK_STATUS";
    public static final String _CAT_TOPIC_AUTHORITY = "TOPIC_AUTHORITY";

    //Use this to GET a specific CodeDecode row, usually to match/validate a prior selection
    public static CodeDecodeDTO getByCode(String sCategory, String sCode)
    {
        //What is the row you are looking for?
        if (sCode == null)
        {
            return null;
        }

        List<CodeDecodeDTO> alDTO = CodeDecodeDAO.select(sCategory, sCode, null);
        if (alDTO != null && alDTO.size()==1)
        {
            return alDTO.get(0);
        }
        else
        {
            //Either we got no results, or there was more than 1
            return null;
        }
    }

    //Use this to GET a specific CodeDecode row, usually to match/validate a prior selection
    public static CodeDecodeDTO getByValue(String sCategory, String sDecode)
    {
        //What is the row you are looking for?
        if (sDecode == null)
        {
            return null;
        }

        List<CodeDecodeDTO> alDTO = CodeDecodeDAO.select(sCategory, null, sDecode);
        if (alDTO != null && alDTO.size()==1)
        {
            return alDTO.get(0);
        }
        else
        {
            //Either we got no results, or there was more than 1
            return null;
        }
    }

    //Use this to get a list of CodeDecodes, usually to populate a list of options
    public static List<CodeDecodeDTO> getList(String sCategory)
    {
        return CodeDecodeDAO.select(sCategory, null, null);
    }

    //Use this to get a hash of CodeToDecode, usually to display a prior selection
    public static Map<String, String> getLookupMap(String sCategory)
    {
        return CodeDecodeDAO.selectKVs(sCategory);
    }


    ///////////////////MAIN//////////////////////

    //Run this to populate the DB with CodeDecode
    //You should delete all the documents in the collection first
    public static void main(String[] args)
    {
        System.out.println("CodeDecode: Uncomment exit() if you want to really build the DB collection");
        System.exit(1);

        System.out.println("CodeDecode: Using TestDB");
        Config.useTestDB();

        //Get a connection, and start a transaction
        Connection conn = SuperDuperDAO.getConnection();

        try
        {
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(1, _CAT_USER_STATUS, "ACTIVE", "Active", true));
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(2, _CAT_USER_STATUS, "LOCKED", "Locked", true));

        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(1, _CAT_PROJECT_TYPE, "HOME_BUY", "Buying", true));
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(2, _CAT_PROJECT_TYPE, "HOME_SALE", "Selling / Listing", true));
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(3, _CAT_PROJECT_TYPE, "REMODEL", "Remodeling", true));
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(99, _CAT_PROJECT_TYPE, "OTHER", "Other / Custom", true));

        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(5, _CAT_MEMBERSHIP_ROLE, "TRANSACTION_COORDINATOR", "Transaction Coordinator", true));
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(8, _CAT_MEMBERSHIP_ROLE, "AGENT_ASSISTANT", "Agent Assistant", true));
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(12, _CAT_MEMBERSHIP_ROLE, "BUYER", "Buyer", true));
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(13, _CAT_MEMBERSHIP_ROLE, "BUYER_AGENT", "Buyer Agent", true));
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(14, _CAT_MEMBERSHIP_ROLE, "OWNER", "Owner/Seller", true));
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(15, _CAT_MEMBERSHIP_ROLE, "OWNER_AGENT", "Seller Agent", true));
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(16, _CAT_MEMBERSHIP_ROLE, "MORTGAGE", "Mortgage Broker", true));
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(17, _CAT_MEMBERSHIP_ROLE, "BANK", "Bank/Loan Agent", true));
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(18, _CAT_MEMBERSHIP_ROLE, "INSPECTOR_PUBLIC", "Inspector (Public/City)", true));
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(19, _CAT_MEMBERSHIP_ROLE, "INSPECTOR_PRIVATE", "Inspector (Private)", true));
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(20, _CAT_MEMBERSHIP_ROLE, "LEGAL", "Attorney", true));
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(21, _CAT_MEMBERSHIP_ROLE, "BUILDER", "Builder/Construction", true));
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(22, _CAT_MEMBERSHIP_ROLE, "LANDSCAPE", "Landscape Company", true));
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(23, _CAT_MEMBERSHIP_ROLE, "MOVING", "Moving Company", true));
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(99, _CAT_MEMBERSHIP_ROLE, "OTHER", "Other", true));

        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(1, _CAT_MEMBERSHIP_STATUS, "INVITED", "Invited", true));
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(2, _CAT_MEMBERSHIP_STATUS, "ACTIVE", "Active", true));
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(3, _CAT_MEMBERSHIP_STATUS, "REMOVED", "Removed", true));

        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(1, _CAT_TOPIC_AUTHORITY, "NONE", "No Access", true));
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(2, _CAT_TOPIC_AUTHORITY, "READER", "Read-Only", true));
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(3, _CAT_TOPIC_AUTHORITY, "WRITER", "Collaborator", true));

        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(1, _CAT_TASK_STATUS, "NEW", "New", true));
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(2, _CAT_TASK_STATUS, "ACKNOWLEDGED", "Haven't started, but I know what to do", true));
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(3, _CAT_TASK_STATUS, "WORKING", "I am working on it", true));
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(4, _CAT_TASK_STATUS, "PAUSE", "Taking a break for a bit", true));
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(5, _CAT_TASK_STATUS, "STUCK", "Need help, I am stuck", true));
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(6, _CAT_TASK_STATUS, "ALMOST_DONE", "Almost Done", true));
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(7, _CAT_TASK_STATUS, "COMPLETE", "Completed", true));
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(8, _CAT_TASK_STATUS, "REOPEN", "Reopened / Incomplete", true));
        CodeDecodeDAO.insert(conn, new CodeDecodeDTO(99, _CAT_TASK_STATUS, "DELETE", "Deleted / Duplicate", true));
        }
        catch (Exception ex)
        {
            System.out.println(ex);
        }
        finally
        {
            SuperDuperDAO.close(conn, true);
        }
    }

}
