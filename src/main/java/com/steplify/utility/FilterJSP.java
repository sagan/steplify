package com.steplify.utility;

// Import required java libraries
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;

//This class is invoked based on the 'filter-mapping' in web.xml
// If anyone tries to call a JSP page directly, lets kick them back to the homepage
public class FilterJSP implements Filter
{
    public void  init(FilterConfig config) throws ServletException{
        // Get init parameter
        //String testParam = config.getInitParameter("test-param");
    }

    //Basically we don't want anyone to try and call a JSP directly, only call a servlet.
    //This call happens because the web.xml configures this class to handle *.jsp requests
    public void  doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws java.io.IOException, ServletException
    {
        //This is the default homepage of the app
        request.getRequestDispatcher("/index.jsp").forward(request, response);

        //We are done in this case, stop the filter chain; or container 404 page will conflict
        //chain.doFilter(request,response);
    }

    public void destroy( ){
      /* Called before the Filter instance is removed
      from service by the web container*/
    }
}