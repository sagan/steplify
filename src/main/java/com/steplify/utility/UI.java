package com.steplify.utility;

import com.steplify.dto.CodeDecodeDTO;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;

import java.util.*;
import java.text.*;

/**
 * The UI = UserInterface
 * Provide utility functions for rendering HTML, JSP, etc
 */
public class UI {

    private static Logger _LOGGER = Logger.getLogger("UI");

    //Escapes complex scenarios of JS inside of HTML; hints that front end pattern could use a makeover
    public static String printAndEscape(String aString)
    {
        if (aString == null)
        {
            return "";
        }
        else
        {
            //The JS escape is wrapped last because its the first to get unwrapped in browser evaluation
            String escapedString = StringEscapeUtils.escapeHtml4(aString);
            String escapedString2 = StringEscapeUtils.escapeEcmaScript(escapedString);
            return escapedString2;
        }
    }

    //If the string has a single quote in it, it may screw up your JS, depending on how you use it (i.e. ng-init="scopvar='foo's'")
    public static String printAndEscapeForJS(String aString)
    {
        if (aString == null)
        {
            return "";
        }
        else
        {
            String escapedString = StringEscapeUtils.escapeEcmaScript(aString);
            return escapedString;
        }
    }

    //Prevents use of quotes and ancle brackets to disrupt the HTML
    public static String printAndEscapeForHTML(String aString)
    {
        if (aString == null)
        {
            return "";
        }
        else
        {
            String escapedString = StringEscapeUtils.escapeHtml4(aString);
            return escapedString;
        }
    }

    public static boolean isNullOrEmpty(String aString)
    {
        if (aString == null || "".equals(aString.trim()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static String dropDownOptions(List<CodeDecodeDTO> alCodesProjectType, String sCode)
    {
        StringBuilder sb = new StringBuilder();
        String sTemplate = "<option>%s</option>";
        String sTemplateSelection = "<option selected>%s</option>";

        for(CodeDecodeDTO cdDTO : alCodesProjectType)
        {
            if (cdDTO.get_s_code().equals(sCode))
            {
                sb.append(String.format(sTemplateSelection, (cdDTO.get_s_decode())));
            }
            else
            {
                sb.append(String.format(sTemplate, (cdDTO.get_s_decode())));
            }
        }

        return sb.toString();
    }

    public static String dropDownOptionsWithStrings(List<String> alStrings)
    {
        StringBuilder sb = new StringBuilder();
        String sTemplate = "<option>%s</option>";

        for(String sValue : alStrings)
        {
            sb.append(String.format(sTemplate, (sValue)));
        }

        return sb.toString();
    }

    public static String getCodeDecodeValue(List<CodeDecodeDTO> alCodesProjectType, String sCode)
    {
        //Dont bother if the input is null
        if (sCode == null)
        {
            return null;
        }

        //Look in the list to if its there
        for(CodeDecodeDTO cdDTO : alCodesProjectType)
        {
            if (cdDTO.get_s_code().equals(sCode))
            {
                return cdDTO.get_s_decode();
            }
        }

        //If we couldnt find it, default to nothing selected
        return null;
    }

    public static Date createDate(int YYYY, int MM, int DD)
    {
        Calendar cal = Calendar.getInstance();
        cal.set(YYYY, MM-1, DD);
        return cal.getTime();
    }

    //The TZ usage is so everyone sees the same moment in time, not the same date
    public static String dateString(Date dtDate)
    {
        if (dtDate == null)
        {
            return "";
        }
        else
        {
            DateFormat dateFormat = new SimpleDateFormat("MMM d, yyyy");
            TimeZone tz = TimeZone.getTimeZone(Config._TIMEZONE_DISPLAY);
            dateFormat.setTimeZone(tz);
            return dateFormat.format(dtDate);
        }
    }

    //This is so everyone sees the same date on the screen, regardless of where they are in the world
    //Because if I create a (task due) date of 3/15/2013 @ 12:00:00 GMT, and then display it in PST, it will come back as 3/14/2013 @ 18:00:00 PST
    //Use when a user is date picking, so it will now render in GMT, just as it was created
    public static String dateNoTzString(Date dtDate)
    {
        if (dtDate == null)
        {
            return "";
        }
        else
        {
            DateFormat dateFormat = new SimpleDateFormat("MMM d, yyyy");
            return dateFormat.format(dtDate);
        }
    }

    public static String datetimeString(Date dtDate)
    {
        if (dtDate == null)
        {
            return "";
        }
        else
        {
            //DateFormat dateFormat = new SimpleDateFormat("MMM d, yyyy @ HH:mm:ss Z");
            //DateFormat dateFormat = new SimpleDateFormat("MMM d, yyyy @ HH:mm:ss z");
            DateFormat dateFormat = new SimpleDateFormat("MMM d, yyyy @ hh:mm aaa");
            TimeZone tz = TimeZone.getTimeZone(Config._TIMEZONE_DISPLAY);
            dateFormat.setTimeZone(tz);
            return dateFormat.format(dtDate);
        }
    }

    public static boolean startsWithHttp(String sURL)
    {
        if (isNullOrEmpty(sURL))
        {
            return false;
        }
        else if (sURL.startsWith("http://"))
        {
            return true;
        }
        else if (sURL.startsWith("https://"))
        {
             return true;
        }
        else
        {
            return false;
        }
    }

    //http://email.about.com/library/misc/blmailto_encoder.htm
    public static String htmlMailToHref(String to, String subject, String body)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("mailto:");

        //The to field is optional
        if (to != null)
        {
            sb.append(to);
        }

        //Always put a ? after the email address, even if there is none
        sb.append("?");

        if (subject != null)
        {
            byte[] paramBytes = org.apache.commons.codec.net.URLCodec.encodeUrl(null, subject.getBytes());
            String paramString = new String(paramBytes);
            paramString = paramString.replace("+", "%20");
            sb.append("subject=");
            sb.append(paramString);
        }

        if (body != null)
        {
            byte[] paramBytes = org.apache.commons.codec.net.URLCodec.encodeUrl(null, body.getBytes());
            String paramString = new String(paramBytes);
            paramString = paramString.replace("+", "%20");
            sb.append("&body=");
            sb.append(paramString);
        }

        return sb.toString();

    }


}
