package com.steplify.utility;

import com.steplify.dao.SuperDuperDAO;
import com.steplify.dao.UserPrincipalDAO;
import com.steplify.dto.UserPrincipalDTO;
import javax.crypto.*;
import javax.crypto.spec.*;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import java.util.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class AppSession
{
    private static Logger _LOGGER = Logger.getLogger("AppSession");
    private static String _REMEMBER_ME = "RMID";

    public static void login(UserPrincipalDTO dto, HttpServletRequest request, HttpServletResponse response)
    {
        if (dto != null)
        {
            //Give the user 10 years to not have to re-login
            String encryptedUID = AppSession.encryptToken(String.valueOf(dto.get_id()));
            AppSession.setCookie(response, _REMEMBER_ME, encryptedUID, 3650);
        }
        else
        {
            AppSession.logout(request, response);
        }
    }

    public static void logout(HttpServletRequest request, HttpServletResponse response)
    {
        //Expire the RememberMe cookie
        AppSession.setCookie(response, _REMEMBER_ME, "null", 0);
    }

    public static UserPrincipalDTO getCurrentUser(HttpServletRequest request, HttpServletResponse response)
    {
        UserPrincipalDTO dto = null;

        //Use the RememberMe cookie to pull the USER from the DB
        Map<String, Cookie> hmCookies = AppSession.getCookies(request);
        Cookie cookie = hmCookies.get(_REMEMBER_ME);

        //If the cookie is not found, assume no logged in user
        if (cookie != null)
        {
            String encryptedUID = cookie.getValue();
            String decryptedUID = AppSession.decryptToken(encryptedUID);
            dto = selectUserByID(decryptedUID);

            //If the account has been set to INACTIVE, after they logged in, stop per request
            if (dto != null && !dto.isAccountActive())
            {
                dto = null;
                AppSession.logout(request, response);
            }
        }

        return dto;
    }

    ///////////////Public Helpers

    //Get all the cookies from the HTTPRequest; this should be a normal Servlet API!
    public static Map<String, Cookie> getCookies(HttpServletRequest request)
    {
        Map<String, Cookie> mapCookies = new HashMap<String, Cookie>();
        Cookie[] cookies = request.getCookies();

        //When there are no cookies, this [] evaluates to null
        if (cookies != null)
        {
            for(int i = 0; i < cookies.length; i++)
            {
                Cookie cookie = cookies[i];
                mapCookies.put(cookie.getName(), cookie);
            }
        }

        return mapCookies;
    }

    //Use to set a cookie back to the client, simple API
    public static void setCookie(HttpServletResponse response, String key, String value, int iDaysExpire)
    {
        Cookie cookie = new Cookie(key, value);
        cookie.setMaxAge(iDaysExpire*24*60*60);
        cookie.setPath("/");
        response.addCookie(cookie);
    }

    //////////////Private Helpers

    private static UserPrincipalDTO selectUserByID(String sUID)
    {
        //Get a DB connection, do the work, and close it
        java.sql.Connection connection = SuperDuperDAO.getConnection();
        try
        {
            int anID = Integer.parseInt(sUID);
            UserPrincipalDTO dto = UserPrincipalDAO.selectUserByID(connection, anID);
            return dto;
        }
        catch (Exception ex)
        {
            //In case the cookie is bad, or blank, assume no user
            return null;
        }
        finally
        {
            SuperDuperDAO.close(connection, false);
        }
    }

    //////////////Encryption, methods are public ONLY for unit testing

    public static String encryptToken(String aToken)
    {
        String encryptedToken = null;

        try
        {
            //The date is like salt for encrypting, and is all ignored on decryption
            Date dtNow = new Date();
            //Assumption is what we are encrypting does NOT have this '|' delimiter
            String sToken = UUID.randomUUID().toString() + "|" + aToken + "|" + dtNow.toString() + "|" + dtNow.getTime();

            //Do the actual encryption
            Cipher c = Cipher.getInstance("AES");
            SecretKeySpec secretKey = new SecretKeySpec(Config._AES_KEY, "AES");
            c.init(Cipher.ENCRYPT_MODE, secretKey);
            encryptedToken = Base64.encodeBase64String(c.doFinal(sToken.getBytes()));
        }
        catch (Exception ex)
        {
            _LOGGER.error("Error trying to encrypt token");
            _LOGGER.error(ex.getMessage());
        }
        return encryptedToken;
    }

    public static String decryptToken(String encryptedToken)
    {
        String originalToken = null;

        try
        {
            //Do the actual decryption
            Cipher c = Cipher.getInstance("AES");
            SecretKeySpec secretKey = new SecretKeySpec(Config._AES_KEY, "AES");
            c.init(Cipher.DECRYPT_MODE, secretKey);
            String decryptedToken = new String(c.doFinal(Base64.decodeBase64(encryptedToken)));

            //The second token is the UID, the rest is the date padding for uniqueness
            StringTokenizer st = new StringTokenizer(decryptedToken, "|");
            originalToken = st.nextToken();
            originalToken = st.nextToken();
        }
        catch (Exception ex)
        {
            _LOGGER.error("Error trying to decrypt token");
            _LOGGER.error(ex.getMessage());
        }

        //Handle the null case
        if ("null".equals(originalToken))
        {
            return null;
        }
        else
        {
            return originalToken;
        }
    }

}
