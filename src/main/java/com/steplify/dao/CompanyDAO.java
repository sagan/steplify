package com.steplify.dao;

import com.steplify.dto.CompanyDTO;
import com.steplify.dto.CompanyTopicTaskDTO;
import com.steplify.dto.CompanyUserDTO;
import com.steplify.dto.TopicDTO;
import com.steplify.dto.TaskDTO;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CompanyDAO extends SuperDuperDAO {

    private static Logger _LOGGER = Logger.getLogger("CompanyDAO");

    //ADD DB CRUD METHODS HERE
    public static int insertCompany(Connection connection, CompanyDTO dto)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        String sSQL = "insert into company " +
                "(_id_created_by, _id_updated_by, _dt_create_date, _dt_last_update, _s_name, _s_description) " +
                "values (?, ?, ?, ?, ?, ?)";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL, Statement.RETURN_GENERATED_KEYS);
            long lTimeNow = (Calendar.getInstance()).getTimeInMillis();

            preparedStatement.setInt(1, dto.get_id_created_by());
            preparedStatement.setInt(2, dto.get_id_updated_by());
            preparedStatement.setTimestamp(3, new Timestamp(lTimeNow));
            preparedStatement.setTimestamp(4, new Timestamp(lTimeNow));
            preparedStatement.setString(5, prepStringForDB(dto.get_s_name(), 100));
            preparedStatement.setString(6, prepStringForDB(dto.get_s_description(), 200));

            //Do it, and then get the rowCount
            iCount = preparedStatement.executeUpdate();

            //Set the auto-increment field back in the DTO
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet != null && resultSet.next())
            {
                dto.set_id(resultSet.getInt(1));
            }

            return iCount;
        }
        catch (Exception ex)
        {
            _LOGGER.error(dto.toJSON());
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static int updateCompany(Connection connection, CompanyDTO dto)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        String sSQL = "update company " +
                "set _id_updated_by=?, _dt_last_update=?, _s_name=?, _s_description=? " +
                "where _id=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            long lTimeNow = (Calendar.getInstance()).getTimeInMillis();

            preparedStatement.setInt(1, dto.get_id_updated_by());
            preparedStatement.setTimestamp(2, new Timestamp(lTimeNow));
            preparedStatement.setString(3, prepStringForDB(dto.get_s_name(), 100));
            preparedStatement.setString(4, prepStringForDB(dto.get_s_description(), 200));
            preparedStatement.setInt(5, dto.get_id());

            //Do it, and then get the rowCount
            iCount = preparedStatement.executeUpdate();

            return iCount;
        }
        catch (Exception ex)
        {
            _LOGGER.error(dto.toJSON());
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static int deleteCompany(Connection connection, int anID)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        String sSQL = "delete from company where _id=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, anID);

            //Do it, and then get the rowCount
            iCount = preparedStatement.executeUpdate();
            return iCount;
        }
        catch (Exception ex)
        {
            _LOGGER.error("anID=" + anID);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static CompanyDTO selectCompanyByID(Connection connection, int anID)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String sSQL = "select _id, _id_created_by, _id_updated_by, _dt_create_date, _dt_last_update, _s_name, _s_description " +
                "from company where _id=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, anID);
            preparedStatement.execute();

            //Parse the results
            resultSet = preparedStatement.getResultSet();
            CompanyDTO dto = null;
            while (resultSet != null && resultSet.next())
            {
                dto = new CompanyDTO();
                dto.set_id(resultSet.getInt(1));
                dto.set_id_created_by(resultSet.getInt(2));
                dto.set_id_updated_by(resultSet.getInt(3));
                dto.set_dt_create_date(resultSet.getTimestamp(4));
                dto.set_dt_last_update(resultSet.getTimestamp(5));
                dto.set_s_name(resultSet.getString(6));
                dto.set_s_description(resultSet.getString(7));
            }

            return dto;
        }
        catch (Exception ex)
        {
            _LOGGER.error("anID=" + anID);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static CompanyUserDTO selectCompanyUserByUserID(Connection connection, int aUserID)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String sSQL = "select _id, _id_created_by, _id_updated_by, _dt_create_date, _dt_last_update, _id_company, _id_user_principal " +
                "from company_user where _id_user_principal=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, aUserID);
            preparedStatement.execute();

            //Parse the results
            resultSet = preparedStatement.getResultSet();
            CompanyUserDTO dto = null;
            while (resultSet != null && resultSet.next())
            {
                dto = new CompanyUserDTO();
                dto.set_id(resultSet.getInt(1));
                dto.set_id_created_by(resultSet.getInt(2));
                dto.set_id_updated_by(resultSet.getInt(3));
                dto.set_dt_create_date(resultSet.getTimestamp(4));
                dto.set_dt_last_update(resultSet.getTimestamp(5));
                dto.set_id_company(resultSet.getInt(6));
                dto.set_id_user_principal(resultSet.getInt(7));
            }

            //This will ensure users who are not tied to a real company get some company representation
            if (dto == null)
            {
                dto = new CompanyUserDTO();
                dto.set_id(0); //will ensure no AUTH is given when used
                dto.set_id_company(0); //will fallback to the default(0) company
                dto.set_id_user_principal(0); //wont match the real user
            }

            return dto;
        }
        catch (Exception ex)
        {
            _LOGGER.error("anID=" + aUserID);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    //////////////////TopicTaskTemplates//////////

    public static int insertCompanyTopicTask(Connection connection, CompanyTopicTaskDTO dtoCTT)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        String sSQL = "insert into company_topic_task " +
                "(_id_created_by, _id_updated_by, _dt_create_date, _dt_last_update, _i_topic_order, _s_topic_name, _s_task_name, _id_company, _s_cd_project_type) " +
                "values (?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL, Statement.RETURN_GENERATED_KEYS);
            long lTimeNow = (Calendar.getInstance()).getTimeInMillis();

            preparedStatement.setInt(1, dtoCTT.get_id_created_by());
            preparedStatement.setInt(2, dtoCTT.get_id_updated_by());
            preparedStatement.setTimestamp(3, new Timestamp(lTimeNow));
            preparedStatement.setTimestamp(4, new Timestamp(lTimeNow));
            preparedStatement.setInt(5, dtoCTT.get_i_topic_order());
            preparedStatement.setString(6, prepStringForDB(dtoCTT.get_s_topic_name(), 100));
            preparedStatement.setString(7, prepStringForDB(dtoCTT.get_s_task_name(), 250));
            preparedStatement.setInt(8, dtoCTT.get_id_company());
            preparedStatement.setString(9, prepStringForDB(dtoCTT.get_s_cd_project_type(), 45));

            //Do it, and then get the rowCount
            iCount = preparedStatement.executeUpdate();

            //Set the auto-increment field back in the DTO
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet != null && resultSet.next())
            {
                dtoCTT.set_id(resultSet.getInt(1));
            }

            return iCount;
        }
        catch (Exception ex)
        {
            _LOGGER.error(dtoCTT.toJSON());
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static CompanyTopicTaskDTO selectCompanyTopicTaskByIDs(Connection connection, int anID, int aCompanyID)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String sSQL = "select _id, _id_created_by, _id_updated_by, _dt_create_date, _dt_last_update, _i_topic_order, _s_topic_name, _s_task_name, _id_company, _s_cd_project_type " +
                "from company_topic_task where _id=? and _id_company=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, anID);
            preparedStatement.setInt(2, aCompanyID);
            preparedStatement.execute();

            //Parse the results
            resultSet = preparedStatement.getResultSet();
            CompanyTopicTaskDTO dto = null;
            while (resultSet != null && resultSet.next())
            {
                dto = new CompanyTopicTaskDTO();
                dto.set_id(resultSet.getInt(1));
                dto.set_id_created_by(resultSet.getInt(2));
                dto.set_id_updated_by(resultSet.getInt(3));
                dto.set_dt_create_date(resultSet.getTimestamp(4));
                dto.set_dt_last_update(resultSet.getTimestamp(5));
                dto.set_i_topic_order(resultSet.getInt(6));
                dto.set_s_topic_name(resultSet.getString(7));
                dto.set_s_task_name(resultSet.getString(8));
                dto.set_id_company(resultSet.getInt(9));
                dto.set_s_cd_project_type(resultSet.getString(10));
            }

            return dto;
        }
        catch (Exception ex)
        {
            _LOGGER.error("anID=" + anID);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    //NOTE: This is a denormalized set of data, to quickly replace a demo version
    public static List<TopicDTO>  selectCompanyTopicTaskList(Connection connection, int aCompanyID, String sProjectTypeCd)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String sSQL = "select _id, _i_topic_order, _s_topic_name, _s_task_name " +
                "from company_topic_task where _id_company=? and _s_cd_project_type=?";

        //Very important sort to get TopicMembers aligned with Topics, in the same order
        sSQL = sSQL + " order by _i_topic_order, _s_topic_name, _id";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, aCompanyID);
            preparedStatement.setString(2, sProjectTypeCd);
            preparedStatement.execute();

            //Parse the results
            resultSet = preparedStatement.getResultSet();
            List<TopicDTO> alDTO = new ArrayList<TopicDTO>();
            TopicDTO dtoTopic = null;
            int iLastTopicID = -1;
            int iCurrentTopicID = 0;
            while (resultSet != null && resultSet.next())
            {
                //A bit non-standard, we are going to pull the Topic data from only the first row of data
                //Abuse the _i_topic_order as a proxy for a TopicID
                iCurrentTopicID = resultSet.getInt(2);
                if (iCurrentTopicID != iLastTopicID)
                {
                    iLastTopicID = iCurrentTopicID;
                    dtoTopic = new TopicDTO();
                    dtoTopic.set_id(resultSet.getInt(2));
                    dtoTopic.set_i_order(resultSet.getInt(2));
                    dtoTopic.set_s_name(resultSet.getString(3));
                    alDTO.add(dtoTopic);
                }

                //From here on, each row is a new Task instance
                TaskDTO dtoTask = new TaskDTO();
                dtoTask.set_id(resultSet.getInt(1));
                dtoTask.set_s_name(resultSet.getString(4));

                //Tie the Task to its Topic
                dtoTopic.addTask(dtoTask);
                //The Topic was added the the alDTO earlier
            }

            return alDTO;
        }
        catch (Exception ex)
        {
            _LOGGER.error("aCompanyID=" + aCompanyID);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static int deleteCompanyTopicTaskList(Connection connection, int aCompanyID, String sProjectTypeCd)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        String sSQL = "delete from company_topic_task where _id_company=? and _s_cd_project_type=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, aCompanyID);
            preparedStatement.setString(2, sProjectTypeCd);

            //Do it, and then get the rowCount
            iCount = preparedStatement.executeUpdate();
            return iCount;
        }
        catch (Exception ex)
        {
            _LOGGER.error("aCompanyID=" + aCompanyID);
            _LOGGER.error("sProjectTypeCd=" + sProjectTypeCd);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }


}
