package com.steplify.dao;

import java.sql.*;
import com.steplify.dto.*;
import org.apache.log4j.Logger;
import java.util.*;

public class TopicDAO extends SuperDuperDAO {

    private static Logger _LOGGER = Logger.getLogger("TopicDAO");

    //ADD DB CRUD METHODS HERE
    public static int insertTopic(Connection connection, TopicDTO dto)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        String sSQL = "insert into topic " +
                "(_id_created_by, _id_updated_by, _dt_create_date, _dt_last_update, _s_name, _s_description, _s_url_attachments, _b_auto_add_members, _i_order, _id_project) " +
                "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL, Statement.RETURN_GENERATED_KEYS);
            long lTimeNow = (Calendar.getInstance()).getTimeInMillis();

            preparedStatement.setInt(1, dto.get_id_created_by());
            preparedStatement.setInt(2, dto.get_id_updated_by());
            preparedStatement.setTimestamp(3, new Timestamp(lTimeNow));
            preparedStatement.setTimestamp(4, new Timestamp(lTimeNow));
            preparedStatement.setString(5, prepStringForDB(dto.get_s_name(), 100));
            preparedStatement.setString(6, prepStringForDB(dto.get_s_description(), 200));
            preparedStatement.setString(7, prepStringForDB(dto.get_s_url_attachments(), 200));
            preparedStatement.setInt(8, dto.get_b_auto_add_members() ? 1 : 0);
            preparedStatement.setInt(9, dto.get_i_order());
            preparedStatement.setInt(10, dto.get_id_project());

            //Do it, and then get the rowCount
            iCount = preparedStatement.executeUpdate();

            //Set the auto-increment field back in the DTO
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet != null && resultSet.next())
            {
                dto.set_id(resultSet.getInt(1));
            }

            return iCount;
        }
        catch (Exception ex)
        {
            _LOGGER.error(dto.toJSON());
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static int updateTopic(Connection connection, TopicDTO dto)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        String sSQL = "update topic " +
                "set _id_updated_by=?, _dt_last_update=?, _s_name=?, _s_description=?, _s_url_attachments=?, _b_auto_add_members=?, _i_order=?, _id_project=? " +
                "where _id=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            long lTimeNow = (Calendar.getInstance()).getTimeInMillis();

            preparedStatement.setInt(1, dto.get_id_updated_by());
            preparedStatement.setTimestamp(2, new Timestamp(lTimeNow));
            preparedStatement.setString(3, prepStringForDB(dto.get_s_name(), 100));
            preparedStatement.setString(4, prepStringForDB(dto.get_s_description(), 200));
            preparedStatement.setString(5, prepStringForDB(dto.get_s_url_attachments(), 200));
            preparedStatement.setInt(6, dto.get_b_auto_add_members() ? 1 : 0);
            preparedStatement.setInt(7, dto.get_i_order());
            preparedStatement.setInt(8, dto.get_id_project());
            preparedStatement.setInt(9, dto.get_id());

            //Do it, and then get the rowCount
            iCount = preparedStatement.executeUpdate();

            return iCount;
        }
        catch (Exception ex)
        {
            _LOGGER.error(dto.toJSON());
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static int deleteTopic(Connection connection, int anID)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        String sSQL = "delete from topic where _id=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, anID);

            //Do it, and then get the rowCount
            iCount = preparedStatement.executeUpdate();
            return iCount;
        }
        catch (Exception ex)
        {
            _LOGGER.error("anID=" + anID);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static TopicDTO selectTopicByID(Connection connection, int anID)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String sSQL = "select _id, _id_created_by, _id_updated_by, _dt_create_date, _dt_last_update, _s_name, _s_description, _s_url_attachments, _b_auto_add_members, _i_order, _id_project " +
                "from topic where _id=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, anID);
            preparedStatement.execute();

            //Parse the results
            resultSet = preparedStatement.getResultSet();
            TopicDTO dto = null;
            while (resultSet != null && resultSet.next())
            {
                dto = new TopicDTO();
                dto.set_id(resultSet.getInt(1));
                dto.set_id_created_by(resultSet.getInt(2));
                dto.set_id_updated_by(resultSet.getInt(3));
                dto.set_dt_create_date(resultSet.getTimestamp(4));
                dto.set_dt_last_update(resultSet.getTimestamp(5));
                dto.set_s_name(resultSet.getString(6));
                dto.set_s_description(resultSet.getString(7));
                dto.set_s_url_attachments(resultSet.getString(8));
                dto.set_b_auto_add_members(resultSet.getInt(9) == 0 ? false : true);
                dto.set_i_order(resultSet.getInt(10));
                dto.set_id_project(resultSet.getInt(11));
            }

            return dto;
        }
        catch (Exception ex)
        {
            _LOGGER.error("anID=" + anID);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static List<TopicDTO> selectTopicsByProjectID(Connection connection, int anID)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String sSQL = "select _id, _id_created_by, _id_updated_by, _dt_create_date, _dt_last_update, _s_name, _s_description, _s_url_attachments, _b_auto_add_members, _i_order, _id_project " +
                "from topic where _id_project=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, anID);
            preparedStatement.execute();

            //Parse the results
            resultSet = preparedStatement.getResultSet();
            List<TopicDTO> alDTO = new ArrayList<TopicDTO>();
            TopicDTO dto = null;
            while (resultSet != null && resultSet.next())
            {
                dto = new TopicDTO();
                dto.set_id(resultSet.getInt(1));
                dto.set_id_created_by(resultSet.getInt(2));
                dto.set_id_updated_by(resultSet.getInt(3));
                dto.set_dt_create_date(resultSet.getTimestamp(4));
                dto.set_dt_last_update(resultSet.getTimestamp(5));
                dto.set_s_name(resultSet.getString(6));
                dto.set_s_description(resultSet.getString(7));
                dto.set_s_url_attachments(resultSet.getString(8));
                dto.set_b_auto_add_members(resultSet.getInt(9) == 0 ? false : true);
                dto.set_i_order(resultSet.getInt(10));
                dto.set_id_project(resultSet.getInt(11));
                alDTO.add(dto);
            }

            return alDTO;
        }
        catch (Exception ex)
        {
            _LOGGER.error("anID=" + anID);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static TopicDTO selectTopicByProjectIDAndTopicName(Connection connection, int anID, String sTopicName)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String sSQL = "select _id, _id_created_by, _id_updated_by, _dt_create_date, _dt_last_update, _s_name, _s_description, _s_url_attachments, _b_auto_add_members, _i_order, _id_project " +
                "from topic where _id_project=? and UPPER(_s_name)=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, anID);
            preparedStatement.setString(2, sTopicName.toUpperCase().trim());
            preparedStatement.execute();

            //Parse the results
            resultSet = preparedStatement.getResultSet();
            TopicDTO dto = null;
            while (resultSet != null && resultSet.next())
            {
                dto = new TopicDTO();
                dto.set_id(resultSet.getInt(1));
                dto.set_id_created_by(resultSet.getInt(2));
                dto.set_id_updated_by(resultSet.getInt(3));
                dto.set_dt_create_date(resultSet.getTimestamp(4));
                dto.set_dt_last_update(resultSet.getTimestamp(5));
                dto.set_s_name(resultSet.getString(6));
                dto.set_s_description(resultSet.getString(7));
                dto.set_s_url_attachments(resultSet.getString(8));
                dto.set_b_auto_add_members(resultSet.getInt(9) == 0 ? false : true);
                dto.set_i_order(resultSet.getInt(10));
                dto.set_id_project(resultSet.getInt(11));
            }

            return dto;
        }
        catch (Exception ex)
        {
            _LOGGER.error("anID=" + anID);
            _LOGGER.error("sTopicName=" + sTopicName);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static List<TopicDTO> selectTopicsWithMembersForProject(Connection connection, int iProjectID)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String sSQL = "select T._id, T._id_created_by, T._id_updated_by, T._dt_create_date, T._dt_last_update, T._s_name, T._s_description, T._s_url_attachments, T._b_auto_add_members, T._i_order, T._id_project, " +
                "TM._id, TM._id_created_by, TM._id_updated_by, TM._dt_create_date, TM._dt_last_update, TM._id_user_principal, TM._s_cd_authority, TM._id_project, TM._id_topic, " +
                "UPAL._id, UPAL._dt_create_date, UPAL._dt_last_update, UPAL._s_username, UPAL._s_password_hash, UPAL._s_password_backup_hash, UPAL._s_cd_status, " +
                "UPF._id, UPF._id_created_by, UPF._dt_create_date, UPF._dt_last_update, UPF._s_first_name, UPF._s_last_name, UPF._s_url_personal_img, UPF._s_company_name, UPF._s_url_company_img, UPF._s_address, UPF._s_city, UPF._s_state, UPF._s_country, UPF._s_postal_code, UPF._s_phone_number, UPF._s_url_website " +
                "from topic T, topic_member TM, user_principal UPAL, user_profile UPF " +
                "where T._id_project=? and TM._id_topic=T._id and TM._id_user_principal=UPAL._id and UPAL._id=UPF._id_created_by ";

        //Very important sort to get TopicMembers aligned with Topics, in the same order
        sSQL = sSQL + " order by T._i_order, T._dt_last_update DESC, T._id, TM._id_user_principal";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, iProjectID);
            preparedStatement.execute();

            //Parse the results
            resultSet = preparedStatement.getResultSet();
            List<TopicDTO> alDTO = new ArrayList<TopicDTO>();
            TopicDTO dtoTopic = null;
            int iLastTopicID = 0;
            int iCurrentTopicID = 0;
            while (resultSet != null && resultSet.next())
            {
                //A bit non-standard, we are going to pull the Topic data from only the first row of TopicMember data
                //Since each row is a TopicMember, not a Topic
                iCurrentTopicID = resultSet.getInt(1);
                if (iCurrentTopicID != iLastTopicID)
                {
                    iLastTopicID = iCurrentTopicID;
                    dtoTopic = new TopicDTO();
                    dtoTopic.set_id(resultSet.getInt(1));
                    dtoTopic.set_id_created_by(resultSet.getInt(2));
                    dtoTopic.set_id_updated_by(resultSet.getInt(3));
                    dtoTopic.set_dt_create_date(resultSet.getTimestamp(4));
                    dtoTopic.set_dt_last_update(resultSet.getTimestamp(5));
                    dtoTopic.set_s_name(resultSet.getString(6));
                    dtoTopic.set_s_description(resultSet.getString(7));
                    dtoTopic.set_s_url_attachments(resultSet.getString(8));
                    dtoTopic.set_b_auto_add_members(resultSet.getInt(9) == 0 ? false : true);
                    dtoTopic.set_i_order(resultSet.getInt(10));
                    dtoTopic.set_id_project(resultSet.getInt(11));
                    alDTO.add(dtoTopic);
                }

                //From here on, each row is a new TopicMember instance
                TopicMemberDTO dtoTM = new TopicMemberDTO();
                dtoTM.set_id(resultSet.getInt(12));
                dtoTM.set_id_created_by(resultSet.getInt(13));
                dtoTM.set_id_updated_by(resultSet.getInt(14));
                dtoTM.set_dt_create_date(resultSet.getTimestamp(15));
                dtoTM.set_dt_last_update(resultSet.getTimestamp(16));
                dtoTM.set_id_user_principal(resultSet.getInt(17));
                dtoTM.set_s_cd_authority(resultSet.getString(18));
                dtoTM.set_id_project(resultSet.getInt(19));
                dtoTM.set_id_topic(resultSet.getInt(20));

                UserPrincipalDTO dtoUPAL = new UserPrincipalDTO();
                dtoUPAL.set_id(resultSet.getInt(21));
                dtoUPAL.set_dt_create_date(resultSet.getTimestamp(22));
                dtoUPAL.set_dt_last_update(resultSet.getTimestamp(23));
                dtoUPAL.set_s_username(resultSet.getString(24));
                dtoUPAL.set_s_password_hash(resultSet.getString(25));
                dtoUPAL.set_s_password_backup_hash(resultSet.getString(26));
                dtoUPAL.set_s_cd_status(resultSet.getString(27));

                UserProfileDTO dtoUPF = new UserProfileDTO();
                dtoUPF.set_id(resultSet.getInt(28));
                dtoUPF.set_id_created_by(resultSet.getInt(29));
                dtoUPF.set_dt_create_date(resultSet.getTimestamp(30));
                dtoUPF.set_dt_last_update(resultSet.getTimestamp(31));
                dtoUPF.set_s_first_name(resultSet.getString(32));
                dtoUPF.set_s_last_name(resultSet.getString(33));
                dtoUPF.set_s_url_personal_img(resultSet.getString(34));
                dtoUPF.set_s_company_name(resultSet.getString(35));
                dtoUPF.set_s_url_company_img(resultSet.getString(36));
                dtoUPF.set_s_address(resultSet.getString(37));
                dtoUPF.set_s_city(resultSet.getString(38));
                dtoUPF.set_s_state(resultSet.getString(39));
                dtoUPF.set_s_country(resultSet.getString(40));
                dtoUPF.set_s_postal_code(resultSet.getString(41));
                dtoUPF.set_s_phone_number(resultSet.getString(42));
                dtoUPF.set_s_url_website(resultSet.getString(43));

                //Tie the User to its profile, then its TopicMembership, then to its Topic
                dtoUPAL.setUserProfile(dtoUPF);
                dtoTM.setUserPrincipal(dtoUPAL);
                dtoTopic.addTopicMember(dtoTM);
                //The Topic was added the the alDTO earlier
            }

            return alDTO;
        }
        catch (Exception ex)
        {
            _LOGGER.error("iProjectID=" + iProjectID);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

}
