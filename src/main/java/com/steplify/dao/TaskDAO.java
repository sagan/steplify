package com.steplify.dao;

import java.sql.*;
import com.steplify.dto.*;
import com.steplify.mpb.*;
import org.apache.log4j.Logger;
import java.sql.*;
import java.util.*;
import java.util.Date;


public class TaskDAO extends SuperDuperDAO {

    private static Logger _LOGGER = Logger.getLogger("TaskDAO");

    //ADD DB CRUD METHODS HERE
    public static int insertTask(Connection connection, TaskDTO dto)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        String sSQL = "insert into task " +
                "(_id_created_by, _id_updated_by, _dt_create_date, _dt_last_update, _s_name, _s_description, _i_order, _s_cd_status, _dt_due, _id_assigned_user, _id_project, _id_topic) " +
                "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL, Statement.RETURN_GENERATED_KEYS);
            long lTimeNow = (Calendar.getInstance()).getTimeInMillis();

            preparedStatement.setInt(1, dto.get_id_created_by());
            preparedStatement.setInt(2, dto.get_id_updated_by());
            preparedStatement.setTimestamp(3, new Timestamp(lTimeNow));
            preparedStatement.setTimestamp(4, new Timestamp(lTimeNow));
            preparedStatement.setString(5, prepStringForDB(dto.get_s_name(), 250));
            preparedStatement.setString(6, prepStringForDB(dto.get_s_description(), 1000));
            preparedStatement.setInt(7, dto.get_i_order());
            preparedStatement.setString(8, dto.get_s_cd_status());
            preparedStatement.setDate(9, new java.sql.Date(dto.get_dt_due().getTime()));
            preparedStatement.setInt(10, dto.get_id_assigned_user());
            preparedStatement.setInt(11, dto.get_id_project());
            preparedStatement.setInt(12, dto.get_id_topic());

            //Do it, and then get the rowCount
            iCount = preparedStatement.executeUpdate();

            //Set the auto-increment field back in the DTO
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet != null && resultSet.next())
            {
                dto.set_id(resultSet.getInt(1));
            }

            return iCount;
        }
        catch (Exception ex)
        {
            _LOGGER.error(dto.toJSON());
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static int updateTask(Connection connection, TaskDTO dto)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        String sSQL = "update task " +
                "set _id_updated_by=?, _dt_last_update=?, _s_name=?, _s_description=?, _i_order=?, _s_cd_status=?, _dt_due=?, _id_assigned_user=?, _id_project=?, _id_topic=? " +
                "where _id=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            long lTimeNow = (Calendar.getInstance()).getTimeInMillis();

            preparedStatement.setInt(1, dto.get_id_updated_by());
            preparedStatement.setTimestamp(2, new Timestamp(lTimeNow));
            preparedStatement.setString(3, prepStringForDB(dto.get_s_name(), 250));
            preparedStatement.setString(4, prepStringForDB(dto.get_s_description(), 1000));
            preparedStatement.setInt(5, dto.get_i_order());
            preparedStatement.setString(6, dto.get_s_cd_status());
            preparedStatement.setDate(7, new java.sql.Date(dto.get_dt_due().getTime()));
            preparedStatement.setInt(8, dto.get_id_assigned_user());
            preparedStatement.setInt(9, dto.get_id_project());
            preparedStatement.setInt(10, dto.get_id_topic());
            preparedStatement.setInt(11, dto.get_id());

            //Do it, and then get the rowCount
            iCount = preparedStatement.executeUpdate();

            return iCount;
        }
        catch (Exception ex)
        {
            _LOGGER.error(dto.toJSON());
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static int deleteTask(Connection connection, int anID)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        String sSQL = "delete from task where _id=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, anID);

            //Do it, and then get the rowCount
            iCount = preparedStatement.executeUpdate();
            return iCount;
        }
        catch (Exception ex)
        {
            _LOGGER.error("anID=" + anID);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static TaskDTO selectTaskByID(Connection connection, int anID)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String sSQL = "select _id, _id_created_by, _id_updated_by, _dt_create_date, _dt_last_update, _s_name, _s_description, _i_order, _s_cd_status, _dt_due, _id_assigned_user, _id_project, _id_topic " +
                "from task where _id=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, anID);
            preparedStatement.execute();

            //Parse the results
            resultSet = preparedStatement.getResultSet();
            TaskDTO dto = null;
            while (resultSet != null && resultSet.next())
            {
                dto = new TaskDTO();
                dto.set_id(resultSet.getInt(1));
                dto.set_id_created_by(resultSet.getInt(2));
                dto.set_id_updated_by(resultSet.getInt(3));
                dto.set_dt_create_date(resultSet.getTimestamp(4));
                dto.set_dt_last_update(resultSet.getTimestamp(5));
                dto.set_s_name(resultSet.getString(6));
                dto.set_s_description(resultSet.getString(7));
                dto.set_i_order(resultSet.getInt(8));
                dto.set_s_cd_status(resultSet.getString(9));
                dto.set_dt_due(resultSet.getDate(10));
                dto.set_id_assigned_user(resultSet.getInt(11));
                dto.set_id_project(resultSet.getInt(12));
                dto.set_id_topic(resultSet.getInt(13));
            }

            return dto;
        }
        catch (Exception ex)
        {
            _LOGGER.error("anID=" + anID);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static TaskDTO selectTaskByTopicIDAndTaskName(Connection connection, int anID, String sTaskName)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String sSQL = "select _id, _id_created_by, _id_updated_by, _dt_create_date, _dt_last_update, _s_name, _s_description, _i_order, _s_cd_status, _dt_due, _id_assigned_user, _id_project, _id_topic " +
                "from task where _id_topic=? and UPPER(_s_name)=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, anID);
            preparedStatement.setString(2, sTaskName.toUpperCase().trim());
            preparedStatement.execute();

            //Parse the results
            resultSet = preparedStatement.getResultSet();
            TaskDTO dto = null;
            while (resultSet != null && resultSet.next())
            {
                dto = new TaskDTO();
                dto.set_id(resultSet.getInt(1));
                dto.set_id_created_by(resultSet.getInt(2));
                dto.set_id_updated_by(resultSet.getInt(3));
                dto.set_dt_create_date(resultSet.getTimestamp(4));
                dto.set_dt_last_update(resultSet.getTimestamp(5));
                dto.set_s_name(resultSet.getString(6));
                dto.set_s_description(resultSet.getString(7));
                dto.set_i_order(resultSet.getInt(8));
                dto.set_s_cd_status(resultSet.getString(9));
                dto.set_dt_due(resultSet.getDate(10));
                dto.set_id_assigned_user(resultSet.getInt(11));
                dto.set_id_project(resultSet.getInt(12));
                dto.set_id_topic(resultSet.getInt(13));
            }

            return dto;
        }
        catch (Exception ex)
        {
            _LOGGER.error("anID=" + anID);
            _LOGGER.error("sTaskName=" + sTaskName);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static List<TaskDTO> selectTasksWithCommentsForTopic(Connection connection, int iTopicID)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String sSQL = "select TK._id, TK._id_created_by, TK._id_updated_by, TK._dt_create_date, TK._dt_last_update, TK._s_name, TK._s_description, TK._i_order, TK._s_cd_status, TK._dt_due, TK._id_assigned_user, TK._id_project, TK._id_topic, " +
                "UPAL._id, UPAL._dt_create_date, UPAL._dt_last_update, UPAL._s_username, UPAL._s_password_hash, UPAL._s_password_backup_hash, UPAL._s_cd_status, " +
                "CM._id, CM._id_created_by, CM._id_updated_by, CM._dt_create_date, CM._dt_last_update, CM._s_comment, CM._id_project, CM._id_task, " +
                "UPAL2._id, UPAL2._dt_create_date, UPAL2._dt_last_update, UPAL2._s_username, UPAL2._s_password_hash, UPAL2._s_password_backup_hash, UPAL2._s_cd_status " +
                "from task TK " +
                "inner join user_principal UPAL on TK._id_assigned_user=UPAL._id " +
                "left join task_comment CM on TK._id=CM._id_task " +
                "left join user_principal UPAL2 on CM._id_created_by=UPAL2._id " +
                "where TK._id_topic=? ";

        //Very important sort to get Tasks aligned with Comments, in the same order every time so its always predictable
        sSQL = sSQL + "order by TK._dt_due, TK._id, CM._dt_create_date, CM._id";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, iTopicID);
            preparedStatement.execute();

            //Parse the results
            resultSet = preparedStatement.getResultSet();
            List<TaskDTO> alDTO = new ArrayList<TaskDTO>();
            TaskDTO dtoTask = null;
            int iLastTaskID = 0;
            int iCurrentTaskID = 0;
            while (resultSet != null && resultSet.next())
            {
                //A bit non-standard, we are going to pull the Task data from only the first row of TaskComment data
                //Since each row is a TaskComment, not a Task
                iCurrentTaskID = resultSet.getInt(1);
                if (iCurrentTaskID != iLastTaskID)
                {
                    iLastTaskID = iCurrentTaskID;
                    dtoTask = new TaskDTO();
                    dtoTask.set_id(resultSet.getInt(1));
                    dtoTask.set_id_created_by(resultSet.getInt(2));
                    dtoTask.set_id_updated_by(resultSet.getInt(3));
                    dtoTask.set_dt_create_date(resultSet.getTimestamp(4));
                    dtoTask.set_dt_last_update(resultSet.getTimestamp(5));
                    dtoTask.set_s_name(resultSet.getString(6));
                    dtoTask.set_s_description(resultSet.getString(7));
                    dtoTask.set_i_order(resultSet.getInt(8));
                    dtoTask.set_s_cd_status(resultSet.getString(9));
                    dtoTask.set_dt_due(resultSet.getTimestamp(10));
                    dtoTask.set_id_assigned_user(resultSet.getInt(11));
                    dtoTask.set_id_project(resultSet.getInt(12));
                    dtoTask.set_id_topic(resultSet.getInt(13));
                    alDTO.add(dtoTask);

                    //Tie the AssignmentOwnerUSer to the Task
                    UserPrincipalDTO dtoAssignedUPAL = new UserPrincipalDTO();
                    dtoAssignedUPAL.set_id(resultSet.getInt(14));
                    dtoAssignedUPAL.set_dt_create_date(resultSet.getTimestamp(15));
                    dtoAssignedUPAL.set_dt_last_update(resultSet.getTimestamp(16));
                    dtoAssignedUPAL.set_s_username(resultSet.getString(17));
                    dtoAssignedUPAL.set_s_password_hash(resultSet.getString(18));
                    dtoAssignedUPAL.set_s_password_backup_hash(resultSet.getString(19));
                    dtoAssignedUPAL.set_s_cd_status(resultSet.getString(20));
                    dtoTask.setUserPrincipalAssigned(dtoAssignedUPAL);
                }

                //From here on, each row is a new TaskComment instance
                //Only if the TaskComment columns exist should we proceed, since its a LEFT join
                if (resultSet.getObject(21) != null)
                {
                    TaskCommentDTO dtoTC = new TaskCommentDTO();
                    dtoTC.set_id(resultSet.getInt(21));
                    dtoTC.set_id_created_by(resultSet.getInt(22));
                    dtoTC.set_id_updated_by(resultSet.getInt(23));
                    dtoTC.set_dt_create_date(resultSet.getTimestamp(24));
                    dtoTC.set_dt_last_update(resultSet.getTimestamp(25));
                    dtoTC.set_s_comment(resultSet.getString(26));
                    dtoTC.set_id_project(resultSet.getInt(27));
                    dtoTC.set_id_task(resultSet.getInt(28));

                    UserPrincipalDTO dtoUPAL = new UserPrincipalDTO();
                    dtoUPAL.set_id(resultSet.getInt(29));
                    dtoUPAL.set_dt_create_date(resultSet.getTimestamp(30));
                    dtoUPAL.set_dt_last_update(resultSet.getTimestamp(31));
                    dtoUPAL.set_s_username(resultSet.getString(32));
                    dtoUPAL.set_s_password_hash(resultSet.getString(33));
                    dtoUPAL.set_s_password_backup_hash(resultSet.getString(34));
                    dtoUPAL.set_s_cd_status(resultSet.getString(35));

                    //Tie the User to its profile, then its TaskComment, then to its Task
                    dtoTC.setUserPrincipalCreatedBy(dtoUPAL);
                    dtoTask.addTaskComment(dtoTC);
                }
            }

            return alDTO;
        }
        catch (Exception ex)
        {
            _LOGGER.error("iTopicID=" + iTopicID);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static Map<Integer, ProjectTaskCountsMPB> selectTaskCountsByProjectForUser(Connection connection, int aUserID, Date lateDate)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        //Not using now() or curdate() due to timezone issues of DB vs AppServer
        String sSQL = "select TASK_TOTALS._id, sum(late_task_user), sum(late_task), sum(total_task) from " +
            "( " +
                "( " +
                "select P._id, 1 late_task_user, 0 late_task, 0 total_task from task T, project_member PM, project P " +
                "where T._id_assigned_user=? and T._id_project=P._id and P._id=PM._id_project and PM._id_user_principal=? and ((T._dt_due<=? and T._s_cd_status not in ('COMPLETE', 'DELETE')) or (T._s_cd_status in ('STUCK', 'REOPEN')))" +
                ") " +
                "UNION ALL " +
                "( " +
                "select P._id, 0 late_task_user, 1 late_task, 0 total_task from task T, project_member PM, project P " +
                "where T._id_project=P._id and P._id=PM._id_project and PM._id_user_principal=? and ((T._dt_due<=? and T._s_cd_status not in ('COMPLETE', 'DELETE')) or (T._s_cd_status in ('STUCK', 'REOPEN')))" +
                ") " +
                "UNION ALL " +
                "( " +
                "select P._id, 0 late_task_user, 0 late_task, 1 total_task from task T, project_member PM, project P " +
                "where T._id_project=P._id and P._id=PM._id_project and PM._id_user_principal=? " +
                ") " +
            ") as TASK_TOTALS " +
            "group by TASK_TOTALS._id ";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, aUserID);
            preparedStatement.setInt(2, aUserID);
            preparedStatement.setDate(3, new java.sql.Date(lateDate.getTime()));
            preparedStatement.setInt(4, aUserID);
            preparedStatement.setDate(5, new java.sql.Date(lateDate.getTime()));
            preparedStatement.setInt(6, aUserID);
            preparedStatement.execute();

            //Parse the results
            resultSet = preparedStatement.getResultSet();
            Map<Integer, ProjectTaskCountsMPB> mapResult = new HashMap<Integer, ProjectTaskCountsMPB>();
            while (resultSet != null && resultSet.next())
            {
                ProjectTaskCountsMPB mpb = new ProjectTaskCountsMPB();
                mpb._id_project = resultSet.getInt(1);
                mpb._id_user = aUserID;
                mpb._i_count_late_for_user = resultSet.getInt(2);
                mpb._i_count_late = resultSet.getInt(3);
                mpb._i_count_total = resultSet.getInt(4);
                mapResult.put(resultSet.getInt(1), mpb);
            }

            return mapResult;
        }
        catch (Exception ex)
        {
            _LOGGER.error("aUserID=" + aUserID);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }


}
