package com.steplify.dao;

import com.steplify.dto.*;
import org.apache.log4j.Logger;
import java.util.*;
import java.sql.*;

public class ProjectDAO extends SuperDuperDAO {

    private static Logger _LOGGER = Logger.getLogger("ProjectDAO");

    //ADD DB CRUD METHODS HERE
    public static int insertProject(Connection connection, ProjectDTO dto)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        String sSQL = "insert into project " +
                "(_id_created_by, _id_updated_by, _dt_create_date, _dt_last_update, _s_address, _s_city, _s_state, _s_country, _s_postal_code, _s_url_website, _s_url_image, _s_description, _s_cd_project_type) " +
                "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL, Statement.RETURN_GENERATED_KEYS);
            long lTimeNow = (Calendar.getInstance()).getTimeInMillis();

            preparedStatement.setInt(1, dto.get_id_created_by());
            preparedStatement.setInt(2, dto.get_id_updated_by());
            preparedStatement.setTimestamp(3, new Timestamp(lTimeNow));
            preparedStatement.setTimestamp(4, new Timestamp(lTimeNow));
            preparedStatement.setString(5, prepStringForDB(dto.get_s_address(), 100));
            preparedStatement.setString(6, prepStringForDB(dto.get_s_city(), 45));
            preparedStatement.setString(7, prepStringForDB(dto.get_s_state(), 45));
            preparedStatement.setString(8, prepStringForDB(dto.get_s_country(), 45));
            preparedStatement.setString(9, prepStringForDB(dto.get_s_postal_code(), 45));
            preparedStatement.setString(10, prepStringForDB(dto.get_s_url_website(), 200));
            preparedStatement.setString(11, prepStringForDB(dto.get_s_url_image(), 200));
            preparedStatement.setString(12, prepStringForDB(dto.get_s_description(), 100));
            preparedStatement.setString(13, dto.get_s_cd_project_type());

            //Do it, and then get the rowCount
            iCount = preparedStatement.executeUpdate();

            //Set the auto-increment field back in the DTO
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet != null && resultSet.next())
            {
                dto.set_id(resultSet.getInt(1));
            }

            return iCount;
        }
        catch (Exception ex)
        {
            _LOGGER.error(dto.toJSON());
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static int updateProject(Connection connection, ProjectDTO dto)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        String sSQL = "update project " +
                "set _id_updated_by=?, _dt_last_update=?, _s_address=?, _s_city=?, _s_state=?, _s_country=?, _s_postal_code=?, _s_url_website=?, _s_url_image=?, _s_description=?, _s_cd_project_type=? " +
                "where _id=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            long lTimeNow = (Calendar.getInstance()).getTimeInMillis();

            preparedStatement.setInt(1, dto.get_id_updated_by());
            preparedStatement.setTimestamp(2, new Timestamp(lTimeNow));
            preparedStatement.setString(3, prepStringForDB(dto.get_s_address(), 100));
            preparedStatement.setString(4, prepStringForDB(dto.get_s_city(), 45));
            preparedStatement.setString(5, prepStringForDB(dto.get_s_state(), 45));
            preparedStatement.setString(6, prepStringForDB(dto.get_s_country(), 45));
            preparedStatement.setString(7, prepStringForDB(dto.get_s_postal_code(), 45));
            preparedStatement.setString(8, prepStringForDB(dto.get_s_url_website(), 200));
            preparedStatement.setString(9, prepStringForDB(dto.get_s_url_image(), 200));
            preparedStatement.setString(10, prepStringForDB(dto.get_s_description(), 100));
            preparedStatement.setString(11, dto.get_s_cd_project_type());
            preparedStatement.setInt(12, dto.get_id());

            //Do it, and then get the rowCount
            iCount = preparedStatement.executeUpdate();

            return iCount;
        }
        catch (Exception ex)
        {
            _LOGGER.error(dto.toJSON());
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static int deleteProject(Connection connection, int anID)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        String sSQL = "delete from project where _id=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, anID);

            //Do it, and then get the rowCount
            iCount = preparedStatement.executeUpdate();
            return iCount;
        }
        catch (Exception ex)
        {
            _LOGGER.error("anID=" + anID);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static ProjectDTO selectProjectByID(Connection connection, int anID)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String sSQL = "select _id, _id_created_by, _id_updated_by, _dt_create_date, _dt_last_update, _s_address, _s_city, _s_state, _s_country, _s_postal_code, _s_url_website, _s_url_image, _s_description, _s_cd_project_type " +
                "from project where _id=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, anID);
            preparedStatement.execute();

            //Parse the results
            resultSet = preparedStatement.getResultSet();
            ProjectDTO dto = null;
            while (resultSet != null && resultSet.next())
            {
                dto = new ProjectDTO();
                dto.set_id(resultSet.getInt(1));
                dto.set_id_created_by(resultSet.getInt(2));
                dto.set_id_updated_by(resultSet.getInt(3));
                dto.set_dt_create_date(resultSet.getTimestamp(4));
                dto.set_dt_last_update(resultSet.getTimestamp(5));
                dto.set_s_address(resultSet.getString(6));
                dto.set_s_city(resultSet.getString(7));
                dto.set_s_state(resultSet.getString(8));
                dto.set_s_country(resultSet.getString(9));
                dto.set_s_postal_code(resultSet.getString(10));
                dto.set_s_url_website(resultSet.getString(11));
                dto.set_s_url_image(resultSet.getString(12));
                dto.set_s_description(resultSet.getString(13));
                dto.set_s_cd_project_type(resultSet.getString(14));
            }

            return dto;
        }
        catch (Exception ex)
        {
            _LOGGER.error("anID=" + anID);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static ProjectDTO selectProjectWithMembersByID(Connection connection, int anID)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String sSQL = "select PR._id, PR._id_created_by, PR._id_updated_by, PR._dt_create_date, PR._dt_last_update, PR._s_address, PR._s_city, PR._s_state, PR._s_country, PR._s_postal_code, PR._s_url_website, PR._s_url_image, PR._s_description, PR._s_cd_project_type, " +
                "MB._id, MB._id_created_by, MB._id_updated_by, MB._dt_create_date, MB._dt_last_update, MB._id_user_principal, MB._s_cd_status, MB._s_cd_role, MB._b_is_project_admin, MB._id_project, " +
                "UPAL._id, UPAL._dt_create_date, UPAL._dt_last_update, UPAL._s_username, UPAL._s_password_hash, UPAL._s_password_backup_hash, UPAL._s_cd_status " +
                "from project PR, project_member MB, user_principal UPAL " +
                "where PR._id=? and PR._id=MB._id_project and MB._id_user_principal=UPAL._id";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, anID);
            preparedStatement.execute();

            //Parse the results
            resultSet = preparedStatement.getResultSet();
            ProjectDTO dto = new ProjectDTO();
            boolean bFirstTime = true;
            while (resultSet != null && resultSet.next())
            {
                if (bFirstTime)
                {
                    dto.set_id(resultSet.getInt(1));
                    dto.set_id_created_by(resultSet.getInt(2));
                    dto.set_id_updated_by(resultSet.getInt(3));
                    dto.set_dt_create_date(resultSet.getTimestamp(4));
                    dto.set_dt_last_update(resultSet.getTimestamp(5));
                    dto.set_s_address(resultSet.getString(6));
                    dto.set_s_city(resultSet.getString(7));
                    dto.set_s_state(resultSet.getString(8));
                    dto.set_s_country(resultSet.getString(9));
                    dto.set_s_postal_code(resultSet.getString(10));
                    dto.set_s_url_website(resultSet.getString(11));
                    dto.set_s_url_image(resultSet.getString(12));
                    dto.set_s_description(resultSet.getString(13));
                    dto.set_s_cd_project_type(resultSet.getString(14));
                    bFirstTime = false;
                }

                ProjectMemberDTO dtoMB = new ProjectMemberDTO();
                dtoMB.set_id(resultSet.getInt(15));
                dtoMB.set_id_created_by(resultSet.getInt(16));
                dtoMB.set_id_updated_by(resultSet.getInt(17));
                dtoMB.set_dt_create_date(resultSet.getTimestamp(18));
                dtoMB.set_dt_last_update(resultSet.getTimestamp(19));
                dtoMB.set_id_user_principal(resultSet.getInt(20));
                dtoMB.set_s_cd_status(resultSet.getString(21));
                dtoMB.set_s_cd_role(resultSet.getString(22));
                dtoMB.set_b_is_project_admin(resultSet.getInt(23) == 0 ? false : true);
                dtoMB.set_id_project(resultSet.getInt(24));

                UserPrincipalDTO dtoUPAL = new UserPrincipalDTO();
                dtoUPAL.set_id(resultSet.getInt(25));
                dtoUPAL.set_dt_create_date(resultSet.getTimestamp(26));
                dtoUPAL.set_dt_last_update(resultSet.getTimestamp(27));
                dtoUPAL.set_s_username(resultSet.getString(28));
                dtoUPAL.set_s_password_hash(resultSet.getString(29));
                dtoUPAL.set_s_password_backup_hash(resultSet.getString(30));
                dtoUPAL.set_s_cd_status(resultSet.getString(31));

                //Add the member to the Project
                dtoMB.setUserPrincipal(dtoUPAL);
                dto.addMember(dtoMB);
            }

            return dto;
        }
        catch (Exception ex)
        {
            _LOGGER.error("anID=" + anID);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

}
