package com.steplify.dao;

import java.sql.*;
import com.steplify.dto.*;
import com.steplify.mpb.*;
import org.apache.log4j.Logger;
import java.util.*;

public class ProjectMemberDAO extends SuperDuperDAO {

    private static Logger _LOGGER = Logger.getLogger("ProjectMemberDAO");

    //ADD DB CRUD METHODS HERE
    public static int insertMembership(Connection connection, ProjectMemberDTO dto)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        String sSQL = "insert into project_member " +
                "(_id_created_by, _id_updated_by, _dt_create_date, _dt_last_update, _id_user_principal, _s_cd_status, _s_cd_role, _b_is_project_admin, _id_project) " +
                "values (?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL, Statement.RETURN_GENERATED_KEYS);
            long lTimeNow = (Calendar.getInstance()).getTimeInMillis();

            preparedStatement.setInt(1, dto.get_id_created_by());
            preparedStatement.setInt(2, dto.get_id_updated_by());
            preparedStatement.setTimestamp(3, new Timestamp(lTimeNow));
            preparedStatement.setTimestamp(4, new Timestamp(lTimeNow));
            preparedStatement.setInt(5, dto.get_id_user_principal());
            preparedStatement.setString(6, dto.get_s_cd_status());
            preparedStatement.setString(7, dto.get_s_cd_role());
            preparedStatement.setInt(8, dto.get_b_is_project_admin() ? 1 : 0);
            preparedStatement.setInt(9, dto.get_id_project());

            //Do it, and then get the rowCount
            iCount = preparedStatement.executeUpdate();

            //Set the auto-increment field back in the DTO
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet != null && resultSet.next())
            {
                dto.set_id(resultSet.getInt(1));
            }

            return iCount;
        }
        catch (Exception ex)
        {
            _LOGGER.error(dto.toJSON());
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static int updateMembership(Connection connection, ProjectMemberDTO dto)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        //Columns "_id_user_principal" and "_id_project" cannot be updated

        String sSQL = "update project_member " +
                "set _id_updated_by=?, _dt_last_update=?, _s_cd_status=?, _s_cd_role=?, _b_is_project_admin=? " +
                "where _id=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            long lTimeNow = (Calendar.getInstance()).getTimeInMillis();

            preparedStatement.setInt(1, dto.get_id_updated_by());
            preparedStatement.setTimestamp(2, new Timestamp(lTimeNow));
            //preparedStatement.setInt(3, dto.get_id_user_principal());
            preparedStatement.setString(3, dto.get_s_cd_status());
            preparedStatement.setString(4, dto.get_s_cd_role());
            preparedStatement.setInt(5, dto.get_b_is_project_admin() ? 1 : 0);
            //preparedStatement.setInt(6, dto.get_id_project());
            preparedStatement.setInt(6, dto.get_id());

            //Do it, and then get the rowCount
            iCount = preparedStatement.executeUpdate();

            return iCount;
        }
        catch (Exception ex)
        {
            _LOGGER.error(dto.toJSON());
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static int updateMembershipStatus(Connection connection, ProjectMemberDTO dto)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        String sSQL = "update project_member " +
                "set _s_cd_status=? " +
                "where _id=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            long lTimeNow = (Calendar.getInstance()).getTimeInMillis();

            preparedStatement.setString(1, dto.get_s_cd_status());
            preparedStatement.setInt(2, dto.get_id());

            //Do it, and then get the rowCount
            iCount = preparedStatement.executeUpdate();

            return iCount;
        }
        catch (Exception ex)
        {
            _LOGGER.error(dto.toJSON());
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static int updateMembershipArchive(Connection connection, ProjectMemberDTO dto)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        String sSQL = "update project_member " +
                "set _dt_archive_date=? " +
                "where _id=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            long lTimeNow = (Calendar.getInstance()).getTimeInMillis();

            preparedStatement.setTimestamp(1, prepDateForTimestampDB(dto.get_dt_archive_date()));
            preparedStatement.setInt(2, dto.get_id());

            //Do it, and then get the rowCount
            iCount = preparedStatement.executeUpdate();

            return iCount;
        }
        catch (Exception ex)
        {
            _LOGGER.error(dto.toJSON());
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static int deleteMembership(Connection connection, int anID)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        String sSQL = "delete from project_member where _id=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, anID);

            //Do it, and then get the rowCount
            iCount = preparedStatement.executeUpdate();
            return iCount;
        }
        catch (Exception ex)
        {
            _LOGGER.error("anID=" + anID);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static ProjectMemberDTO selectMembershipByID(Connection connection, int anID)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String sSQL = "select _id, _id_created_by, _id_updated_by, _dt_create_date, _dt_last_update, _id_user_principal, _s_cd_status, _s_cd_role, _b_is_project_admin, _id_project " +
                "from project_member where _id=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, anID);
            preparedStatement.execute();

            //Parse the results
            resultSet = preparedStatement.getResultSet();
            ProjectMemberDTO dto = null;
            while (resultSet != null && resultSet.next())
            {
                dto = new ProjectMemberDTO();
                dto.set_id(resultSet.getInt(1));
                dto.set_id_created_by(resultSet.getInt(2));
                dto.set_id_updated_by(resultSet.getInt(3));
                dto.set_dt_create_date(resultSet.getTimestamp(4));
                dto.set_dt_last_update(resultSet.getTimestamp(5));
                dto.set_id_user_principal(resultSet.getInt(6));
                dto.set_s_cd_status(resultSet.getString(7));
                dto.set_s_cd_role(resultSet.getString(8));
                dto.set_b_is_project_admin(resultSet.getInt(9) == 0 ? false : true);
                dto.set_id_project(resultSet.getInt(10));
            }

            return dto;
        }
        catch (Exception ex)
        {
            _LOGGER.error("anID=" + anID);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static List<ProjectMemberDTO> selectMembershipsByProjectID(Connection connection, int anID)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String sSQL = "select _id, _id_created_by, _id_updated_by, _dt_create_date, _dt_last_update, _id_user_principal, _s_cd_status, _s_cd_role, _b_is_project_admin, _id_project " +
                "from project_member where _id_project=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, anID);
            preparedStatement.execute();

            //Parse the results
            resultSet = preparedStatement.getResultSet();
            List<ProjectMemberDTO> alPMs = new ArrayList<ProjectMemberDTO>();
            ProjectMemberDTO dto = null;
            while (resultSet != null && resultSet.next())
            {
                dto = new ProjectMemberDTO();
                dto.set_id(resultSet.getInt(1));
                dto.set_id_created_by(resultSet.getInt(2));
                dto.set_id_updated_by(resultSet.getInt(3));
                dto.set_dt_create_date(resultSet.getTimestamp(4));
                dto.set_dt_last_update(resultSet.getTimestamp(5));
                dto.set_id_user_principal(resultSet.getInt(6));
                dto.set_s_cd_status(resultSet.getString(7));
                dto.set_s_cd_role(resultSet.getString(8));
                dto.set_b_is_project_admin(resultSet.getInt(9) == 0 ? false : true);
                dto.set_id_project(resultSet.getInt(10));
                alPMs.add(dto);
            }

            return alPMs;
        }
        catch (Exception ex)
        {
            _LOGGER.error("anID=" + anID);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static ProjectMemberDTO selectMembershipByUserAndProjectID(Connection connection, int aUserID, int aProjectID)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String sSQL = "select _id, _id_created_by, _id_updated_by, _dt_create_date, _dt_last_update, _id_user_principal, _s_cd_status, _s_cd_role, _b_is_project_admin, _id_project " +
                "from project_member where _id_user_principal=? and _id_project=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, aUserID);
            preparedStatement.setInt(2, aProjectID);
            preparedStatement.execute();

            //Parse the results
            resultSet = preparedStatement.getResultSet();
            ProjectMemberDTO dto = null;
            while (resultSet != null && resultSet.next())
            {
                dto = new ProjectMemberDTO();
                dto.set_id(resultSet.getInt(1));
                dto.set_id_created_by(resultSet.getInt(2));
                dto.set_id_updated_by(resultSet.getInt(3));
                dto.set_dt_create_date(resultSet.getTimestamp(4));
                dto.set_dt_last_update(resultSet.getTimestamp(5));
                dto.set_id_user_principal(resultSet.getInt(6));
                dto.set_s_cd_status(resultSet.getString(7));
                dto.set_s_cd_role(resultSet.getString(8));
                dto.set_b_is_project_admin(resultSet.getInt(9) == 0 ? false : true);
                dto.set_id_project(resultSet.getInt(10));
            }

            return dto;
        }
        catch (Exception ex)
        {
            _LOGGER.error("aUserID=" + aUserID);
            _LOGGER.error("aProjectID=" + aProjectID);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static List<ProjectMemberDTO> selectProjectMembershipsForUser(Connection connection, int aUserID, SortDashboardMPB sortMPB, boolean bFetchArchived)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String sSQL = "select MB._id, MB._id_created_by, MB._id_updated_by, MB._dt_create_date, MB._dt_last_update, MB._id_user_principal, MB._s_cd_status, MB._s_cd_role, MB._b_is_project_admin, MB._id_project, MB._dt_archive_date," +
                "PR._id, PR._id_created_by, PR._id_updated_by, PR._dt_create_date, PR._dt_last_update, PR._s_address, PR._s_city, PR._s_state, PR._s_country, PR._s_postal_code, PR._s_url_website, PR._s_url_image, PR._s_description, PR._s_cd_project_type " +
                "from project_member MB, project PR " +
                "where MB._id_user_principal=? and MB._id_project=PR._id";

        //We sort differently depending on normal/non-archived vs archived project memberships
        if (bFetchArchived)
        {
            sSQL = sSQL + " and MB._dt_archive_date is not null order by MB._dt_archive_date desc";
        }
        else
        {
            sSQL = sSQL + " and MB._dt_archive_date is null";

            //Handle the order for the non-archived (i.e. normal) membership rows
            if (sortMPB.bOrderByProjectID)
            {
                sSQL = sSQL + " order by PR._id";
            }
            else if (sortMPB.bOrderByProjectAddress)
            {
                sSQL = sSQL + " order by PR._s_city";
            }
            else if (sortMPB.bOrderByMembershipCreateDate)
            {
                sSQL = sSQL + " order by MB._dt_create_date";
            }
            else
            {
                //Default if all are false
                sSQL = sSQL + " order by MB._dt_create_date";
            }

            //And now for Ascending vs Descending
            if (sortMPB.bOrderReverse)
            {
                sSQL = sSQL + " DESC";
            }
        }

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, aUserID);
            preparedStatement.execute();

            //Parse the results
            resultSet = preparedStatement.getResultSet();
            List<ProjectMemberDTO> alDTO = new ArrayList<ProjectMemberDTO>();
            while (resultSet != null && resultSet.next())
            {
                //Get the Membership data
                ProjectMemberDTO dtoMB = new ProjectMemberDTO();
                dtoMB.set_id(resultSet.getInt(1));
                dtoMB.set_id_created_by(resultSet.getInt(2));
                dtoMB.set_id_updated_by(resultSet.getInt(3));
                dtoMB.set_dt_create_date(resultSet.getTimestamp(4));
                dtoMB.set_dt_last_update(resultSet.getTimestamp(5));
                dtoMB.set_id_user_principal(resultSet.getInt(6));
                dtoMB.set_s_cd_status(resultSet.getString(7));
                dtoMB.set_s_cd_role(resultSet.getString(8));
                dtoMB.set_b_is_project_admin(resultSet.getInt(9) == 0 ? false : true);
                dtoMB.set_id_project(resultSet.getInt(10));
                dtoMB.set_dt_archive_date(resultSet.getTimestamp(11));

                //Get the Project data
                ProjectDTO dtoPR = new ProjectDTO();
                dtoPR.set_id(resultSet.getInt(12));
                dtoPR.set_id_created_by(resultSet.getInt(13));
                dtoPR.set_id_updated_by(resultSet.getInt(14));
                dtoPR.set_dt_create_date(resultSet.getTimestamp(15));
                dtoPR.set_dt_last_update(resultSet.getTimestamp(16));
                dtoPR.set_s_address(resultSet.getString(17));
                dtoPR.set_s_city(resultSet.getString(18));
                dtoPR.set_s_state(resultSet.getString(19));
                dtoPR.set_s_country(resultSet.getString(20));
                dtoPR.set_s_postal_code(resultSet.getString(21));
                dtoPR.set_s_url_website(resultSet.getString(22));
                dtoPR.set_s_url_image(resultSet.getString(23));
                dtoPR.set_s_description(resultSet.getString(24));
                dtoPR.set_s_cd_project_type(resultSet.getString(25));

                //Tie the Project to the Membership
                dtoMB.setProject(dtoPR);
                alDTO.add(dtoMB);
            }

            return alDTO;
        }
        catch (Exception ex)
        {
            _LOGGER.error("aUserID=" + aUserID);
            _LOGGER.error("sortMPB=" + sortMPB.toJSON());
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static List<ProjectMemberDTO> selectUserMembershipsForProject(Connection connection, int iProjectID)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String sSQL = "select MB._id, MB._id_created_by, MB._id_updated_by, MB._dt_create_date, MB._dt_last_update, MB._id_user_principal, MB._s_cd_status, MB._s_cd_role, MB._b_is_project_admin, MB._id_project, " +
                "UPAL._id, UPAL._dt_create_date, UPAL._dt_last_update, UPAL._s_username, UPAL._s_password_hash, UPAL._s_password_backup_hash, UPAL._s_cd_status, " +
                "UPF._id, UPF._id_created_by, UPF._dt_create_date, UPF._dt_last_update, UPF._s_first_name, UPF._s_last_name, UPF._s_url_personal_img, UPF._s_company_name, UPF._s_url_company_img, UPF._s_address, UPF._s_city, UPF._s_state, UPF._s_country, UPF._s_postal_code, UPF._s_phone_number, UPF._s_url_website " +
                "from project_member MB, user_principal UPAL, user_profile UPF " +
                "where MB._id_project=? and MB._id_user_principal=UPAL._id and UPAL._id=UPF._id_created_by ";

        sSQL = sSQL + " order by MB._id_created_by, UPAL._s_username";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, iProjectID);
            preparedStatement.execute();

            //Parse the results
            resultSet = preparedStatement.getResultSet();
            List<ProjectMemberDTO> alDTO = new ArrayList<ProjectMemberDTO>();
            while (resultSet != null && resultSet.next())
            {
                //Get the Membership data
                ProjectMemberDTO dtoMB = new ProjectMemberDTO();
                dtoMB.set_id(resultSet.getInt(1));
                dtoMB.set_id_created_by(resultSet.getInt(2));
                dtoMB.set_id_updated_by(resultSet.getInt(3));
                dtoMB.set_dt_create_date(resultSet.getTimestamp(4));
                dtoMB.set_dt_last_update(resultSet.getTimestamp(5));
                dtoMB.set_id_user_principal(resultSet.getInt(6));
                dtoMB.set_s_cd_status(resultSet.getString(7));
                dtoMB.set_s_cd_role(resultSet.getString(8));
                dtoMB.set_b_is_project_admin(resultSet.getInt(9) == 0 ? false : true);
                dtoMB.set_id_project(resultSet.getInt(10));

                UserPrincipalDTO dtoUPAL = new UserPrincipalDTO();
                dtoUPAL.set_id(resultSet.getInt(11));
                dtoUPAL.set_dt_create_date(resultSet.getTimestamp(12));
                dtoUPAL.set_dt_last_update(resultSet.getTimestamp(13));
                dtoUPAL.set_s_username(resultSet.getString(14));
                dtoUPAL.set_s_password_hash(resultSet.getString(15));
                dtoUPAL.set_s_password_backup_hash(resultSet.getString(16));
                dtoUPAL.set_s_cd_status(resultSet.getString(17));

                UserProfileDTO dtoUPF = new UserProfileDTO();
                dtoUPF.set_id(resultSet.getInt(18));
                dtoUPF.set_id_created_by(resultSet.getInt(19));
                dtoUPF.set_dt_create_date(resultSet.getTimestamp(20));
                dtoUPF.set_dt_last_update(resultSet.getTimestamp(21));
                dtoUPF.set_s_first_name(resultSet.getString(22));
                dtoUPF.set_s_last_name(resultSet.getString(23));
                dtoUPF.set_s_url_personal_img(resultSet.getString(24));
                dtoUPF.set_s_company_name(resultSet.getString(25));
                dtoUPF.set_s_url_company_img(resultSet.getString(26));
                dtoUPF.set_s_address(resultSet.getString(27));
                dtoUPF.set_s_city(resultSet.getString(28));
                dtoUPF.set_s_state(resultSet.getString(29));
                dtoUPF.set_s_country(resultSet.getString(30));
                dtoUPF.set_s_postal_code(resultSet.getString(31));
                dtoUPF.set_s_phone_number(resultSet.getString(32));
                dtoUPF.set_s_url_website(resultSet.getString(33));

                //Tie the User to its profile, then its ProjectMembership
                dtoUPAL.setUserProfile(dtoUPF);
                dtoMB.setUserPrincipal(dtoUPAL);
                alDTO.add(dtoMB);
            }

            return alDTO;
        }
        catch (Exception ex)
        {
            _LOGGER.error("iProjectID=" + iProjectID);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

}
