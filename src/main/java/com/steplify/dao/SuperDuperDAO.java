package com.steplify.dao;

import org.apache.log4j.Logger;
import com.steplify.utility.*;
import java.sql.*;
import java.util.*;

import com.jolbox.bonecp.*;

public abstract class SuperDuperDAO {

    private static Logger _LOGGER = Logger.getLogger("SuperDuperDAO");
    private static BoneCP connectionPool = null;

    //Static Init Block
    private static void initPool()
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver"); 	// load the DB driver

            BoneCPConfig config = new BoneCPConfig();	// create a new configuration object
            config.setJdbcUrl("jdbc:mysql://" +
                                Config._DB_HOST + "/" + Config._DB_NAME +
                                "?user=" + Config._DB_USER +
                                "&password=" + Config._DB_PASS);
            config.setDefaultAutoCommit(false);

            connectionPool = new BoneCP(config);
        }
        catch (Exception ex)
        {
            // handle any errors
            _LOGGER.error(ex.getMessage());
            _LOGGER.error("BoneCP DB ConnectionPool Failed");
            throw new RuntimeException("DB ConnectionPool Failed");
        }
    }

    public static Connection getConnection() throws RuntimeException
    {
        try
        {
            if (connectionPool == null)
            {
                initPool();
            }

            return connectionPool.getConnection();
        }
        catch (SQLException ex)
        {
            _LOGGER.error(ex.getMessage());
            _LOGGER.error("DB Connection Failed");
            throw new RuntimeException("DB Connection Failed");
        }
    }

    public static void close(Connection conn, boolean commitVsRollback) throws RuntimeException
    {
        try
        {
            try
            {
                if (conn.isClosed())
                {
                    return;
                }
            }
            catch (SQLException ex)
            {
                _LOGGER.error(ex.getMessage());
                _LOGGER.error("DB isClosed() Failed");
                //Do not throw this ex
            }

            if (commitVsRollback)
            {
                try
                {
                    conn.commit();
                }
                catch (SQLException ex)
                {
                    _LOGGER.error(ex.getMessage());
                    _LOGGER.error("DB Commit Failed");
                    throw new RuntimeException("DB Commit Failed");
                }
            }
            else
            {
                try
                {
                    conn.rollback();
                }
                catch (SQLException ex)
                {
                    _LOGGER.error(ex.getMessage());
                    _LOGGER.error("DB Rollback Failed");
                    throw new RuntimeException("DB Rollback Failed");
                }
            }
        }
        finally
        {
            try
            {
                conn.close();
            }
            catch (SQLException ex)
            {
                _LOGGER.error(ex.getMessage());
                _LOGGER.error("DB Close Failed");
                throw new RuntimeException("DB Close Failed");
            }
        }
    }


    //Handle null and overflow cases
    public static String prepStringForDB(String inputString, int iMaxLength)
    {
        if (inputString == null || "".equals(inputString.trim()))
        {
            return null;
        }
        else
        {
            String trimString = inputString.trim();
            int iLength = trimString.length();

            if (iLength > iMaxLength)
            {
                return trimString.substring(0, iMaxLength);
            }
            else
            {
                return trimString;
            }
        }
    }

    public static Timestamp prepDateForTimestampDB(java.util.Date inputDate)
    {
        if (inputDate == null)
        {
            return null;
        }
        else
        {
            return new java.sql.Timestamp(inputDate.getTime());
        }
    }

}
