package com.steplify.dao;

import com.steplify.dto.*;
import org.apache.log4j.Logger;
import java.sql.*;
import java.util.*;

public class UserPrincipalDAO extends SuperDuperDAO {

    private static Logger _LOGGER = Logger.getLogger("UserPrincipalDAO");

    //ADD DB CRUD METHODS HERE
    public static int insert(Connection connection, UserPrincipalDTO dto)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        String sSQL = "insert into user_principal " +
                "(_dt_create_date, _dt_last_update, _s_username, _s_password_hash, _s_password_backup_hash, _s_cd_status) " +
                "values (?, ?, ?, ?, ?, ?)";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL, Statement.RETURN_GENERATED_KEYS);

            long lTimeNow = (Calendar.getInstance()).getTimeInMillis();
            preparedStatement.setTimestamp(1, new Timestamp(lTimeNow));
            preparedStatement.setTimestamp(2, new Timestamp(lTimeNow));
            preparedStatement.setString(3, dto.get_s_username().toLowerCase().trim());
            preparedStatement.setString(4, dto.get_s_password_hash());
            preparedStatement.setString(5, dto.get_s_password_backup_hash());
            preparedStatement.setString(6, dto.get_s_cd_status());

            //Do it, and then get the rowCount
            iCount = preparedStatement.executeUpdate();

            //Set the auto-increment field back in the DTO
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet != null && resultSet.next())
            {
                dto.set_id(resultSet.getInt(1));
            }

            //Also insert the userProfile, use a blank one if none is passed in
            insertProfile(connection, dto);

            return iCount;
        }
        catch (Exception ex)
        {
            _LOGGER.error(dto.toJSON());
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    //This is PRIVATE, only call while inserting the parent record
    private static int insertProfile(Connection connection, UserPrincipalDTO dtoUser)
    {
        UserProfileDTO dto = dtoUser.getUserProfile();
        if (dto == null)
        {
            dto = new UserProfileDTO();
            dtoUser.setUserProfile(dto);
        }

        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        String sSQL = "insert into user_profile " +
                "(_id_created_by, _dt_create_date, _dt_last_update, _s_first_name, _s_last_name, _s_url_personal_img, _s_company_name, _s_url_company_img, _s_address, _s_city, _s_state, _s_country, _s_postal_code, _s_phone_number, _s_url_website) " +
                "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL, Statement.RETURN_GENERATED_KEYS);

            long lTimeNow = (Calendar.getInstance()).getTimeInMillis();
            preparedStatement.setInt(1, dtoUser.get_id());
            preparedStatement.setTimestamp(2, new Timestamp(lTimeNow));
            preparedStatement.setTimestamp(3, new Timestamp(lTimeNow));
            preparedStatement.setString(4, prepStringForDB(dto.get_s_first_name(), 45));
            preparedStatement.setString(5, prepStringForDB(dto.get_s_last_name(), 45));
            preparedStatement.setString(6, prepStringForDB(dto.get_s_url_personal_img(), 200));
            preparedStatement.setString(7, prepStringForDB(dto.get_s_company_name(), 100));
            preparedStatement.setString(8, prepStringForDB(dto.get_s_url_company_img(), 200));
            preparedStatement.setString(9, prepStringForDB(dto.get_s_address(), 100));
            preparedStatement.setString(10, prepStringForDB(dto.get_s_city(), 45));
            preparedStatement.setString(11, prepStringForDB(dto.get_s_state(), 45));
            preparedStatement.setString(12, prepStringForDB(dto.get_s_country(), 45));
            preparedStatement.setString(13, prepStringForDB(dto.get_s_postal_code(), 45));
            preparedStatement.setString(14, prepStringForDB(dto.get_s_phone_number(), 45));
            preparedStatement.setString(15, prepStringForDB(dto.get_s_url_website(), 200));

            //Do it, and then get the rowCount
            iCount = preparedStatement.executeUpdate();

            //Set the auto-increment field back in the DTO
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet != null && resultSet.next())
            {
                dto.set_id(resultSet.getInt(1));
            }

            return iCount;
        }
        catch (Exception ex)
        {
            _LOGGER.error(dtoUser.toJSON());
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static int updateUserPassword(Connection connection, UserPrincipalDTO dto)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        String sSQL = "update user_principal " +
                "set _dt_last_update=?, _s_password_hash=? " +
                "where _id=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);

            long lTimeNow = (Calendar.getInstance()).getTimeInMillis();
            preparedStatement.setTimestamp(1, new Timestamp(lTimeNow));
            preparedStatement.setString(2, dto.get_s_password_hash());
            preparedStatement.setInt(3, dto.get_id());

            //Do it, and then get the rowCount
            iCount = preparedStatement.executeUpdate();

            return iCount;
        }
        catch (Exception ex)
        {
            _LOGGER.error(dto.toJSON());
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static int updateUserBackupPassword(Connection connection, UserPrincipalDTO dto)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        String sSQL = "update user_principal " +
                "set _dt_last_update=?, _s_password_backup_hash=? " +
                "where _id=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);

            long lTimeNow = (Calendar.getInstance()).getTimeInMillis();
            preparedStatement.setTimestamp(1, new Timestamp(lTimeNow));
            preparedStatement.setString(2, dto.get_s_password_backup_hash());
            preparedStatement.setInt(3, dto.get_id());

            //Do it, and then get the rowCount
            iCount = preparedStatement.executeUpdate();

            return iCount;
        }
        catch (Exception ex)
        {
            _LOGGER.error(dto.toJSON());
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static int updateUserStatus(Connection connection, UserPrincipalDTO dto)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        String sSQL = "update user_principal " +
                "set _dt_last_update=?, _s_cd_status=? " +
                "where _id=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);

            long lTimeNow = (Calendar.getInstance()).getTimeInMillis();
            preparedStatement.setTimestamp(1, new Timestamp(lTimeNow));
            preparedStatement.setString(2, dto.get_s_cd_status());
            preparedStatement.setInt(3, dto.get_id());

            //Do it, and then get the rowCount
            iCount = preparedStatement.executeUpdate();

            return iCount;
        }
        catch (Exception ex)
        {
            _LOGGER.error(dto.toJSON());
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static int updateUserProfile(Connection connection, UserProfileDTO dto)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        String sSQL = "update user_profile " +
                "set _dt_last_update=?, _s_first_name=?, _s_last_name=?, _s_url_personal_img=?, _s_company_name=?, _s_url_company_img=?, _s_address=?, _s_city=?, _s_state=?, _s_country=?, _s_postal_code=?, _s_phone_number=?, _s_url_website=? " +
                "where _id_created_by=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);

            long lTimeNow = (Calendar.getInstance()).getTimeInMillis();
            preparedStatement.setTimestamp(1, new Timestamp(lTimeNow));
            preparedStatement.setString(2, prepStringForDB(dto.get_s_first_name(), 45));
            preparedStatement.setString(3, prepStringForDB(dto.get_s_last_name(), 45));
            preparedStatement.setString(4, prepStringForDB(dto.get_s_url_personal_img(), 200));
            preparedStatement.setString(5, prepStringForDB(dto.get_s_company_name(), 100));
            preparedStatement.setString(6, prepStringForDB(dto.get_s_url_company_img(), 200));
            preparedStatement.setString(7, prepStringForDB(dto.get_s_address(), 100));
            preparedStatement.setString(8, prepStringForDB(dto.get_s_city(), 45));
            preparedStatement.setString(9, prepStringForDB(dto.get_s_state(), 45));
            preparedStatement.setString(10, prepStringForDB(dto.get_s_country(), 45));
            preparedStatement.setString(11, prepStringForDB(dto.get_s_postal_code(), 45));
            preparedStatement.setString(12, prepStringForDB(dto.get_s_phone_number(), 45));
            preparedStatement.setString(13, prepStringForDB(dto.get_s_url_website(), 200));
            preparedStatement.setInt(14, dto.get_id_created_by());


            //Do it, and then get the rowCount
            iCount = preparedStatement.executeUpdate();

            return iCount;
        }
        catch (Exception ex)
        {
            _LOGGER.error(dto.toJSON());
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static int deleteUser(Connection connection, UserPrincipalDTO dto)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        String sSQL1 = "delete from user_profile where _id=?";
        String sSQL2 = "delete from user_principal where _id=?";

        try
        {
            //UserProfile
            preparedStatement = connection.prepareStatement(sSQL1);
            preparedStatement.setInt(1, dto.getUserProfile().get_id());
            iCount = preparedStatement.executeUpdate();

            //userPrincipal
            preparedStatement = connection.prepareStatement(sSQL2);
            preparedStatement.setInt(1, dto.get_id());
            iCount = preparedStatement.executeUpdate();

            return iCount;
        }
        catch (Exception ex)
        {
            _LOGGER.error(dto.toJSON());
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static UserPrincipalDTO selectUserByName(Connection connection, String sUsername)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        //We always store the username as lowercase, so we should search in lowercase
        sUsername = sUsername.toLowerCase().trim();

        //Join both the USER_PRINCIPAL and USER_PROFILE
        String sSQL = "select UPAL._id, UPAL._dt_create_date, UPAL._dt_last_update, UPAL._s_username, UPAL._s_password_hash, UPAL._s_password_backup_hash, UPAL._s_cd_status, " +
                "UPF._id, UPF._id_created_by, UPF._dt_create_date, UPF._dt_last_update, UPF._s_first_name, UPF._s_last_name, UPF._s_url_personal_img, UPF._s_company_name, UPF._s_url_company_img, UPF._s_address, UPF._s_city, UPF._s_state, UPF._s_country, UPF._s_postal_code, UPF._s_phone_number, UPF._s_url_website " +
                "from user_principal UPAL, user_profile UPF where UPAL._s_username=? and UPAL._id=UPF._id_created_by";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setString(1, sUsername);
            preparedStatement.execute();

            //Parse the results
            resultSet = preparedStatement.getResultSet();
            UserPrincipalDTO dtoUPAL = null;
            while (resultSet != null && resultSet.next())
            {
                dtoUPAL = new UserPrincipalDTO();
                dtoUPAL.set_id(resultSet.getInt(1));
                dtoUPAL.set_dt_create_date(resultSet.getTimestamp(2));
                dtoUPAL.set_dt_last_update(resultSet.getTimestamp(3));
                dtoUPAL.set_s_username(resultSet.getString(4));
                dtoUPAL.set_s_password_hash(resultSet.getString(5));
                dtoUPAL.set_s_password_backup_hash(resultSet.getString(6));
                dtoUPAL.set_s_cd_status(resultSet.getString(7));

                UserProfileDTO dtoUPF = new UserProfileDTO();
                dtoUPF.set_id(resultSet.getInt(8));
                dtoUPF.set_id_created_by(resultSet.getInt(9));
                dtoUPF.set_dt_create_date(resultSet.getTimestamp(10));
                dtoUPF.set_dt_last_update(resultSet.getTimestamp(11));
                dtoUPF.set_s_first_name(resultSet.getString(12));
                dtoUPF.set_s_last_name(resultSet.getString(13));
                dtoUPF.set_s_url_personal_img(resultSet.getString(14));
                dtoUPF.set_s_company_name(resultSet.getString(15));
                dtoUPF.set_s_url_company_img(resultSet.getString(16));
                dtoUPF.set_s_address(resultSet.getString(17));
                dtoUPF.set_s_city(resultSet.getString(18));
                dtoUPF.set_s_state(resultSet.getString(19));
                dtoUPF.set_s_country(resultSet.getString(20));
                dtoUPF.set_s_postal_code(resultSet.getString(21));
                dtoUPF.set_s_phone_number(resultSet.getString(22));
                dtoUPF.set_s_url_website(resultSet.getString(23));

                dtoUPAL.setUserProfile(dtoUPF);
            }

            return dtoUPAL;
        }
        catch (Exception ex)
        {
            _LOGGER.error("sUsername=" + sUsername);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }


    public static UserPrincipalDTO selectUserByID(Connection connection, int anID)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        //Join both the USER_PRINCIPAL and USER_PROFILE
        String sSQL = "select UPAL._id, UPAL._dt_create_date, UPAL._dt_last_update, UPAL._s_username, UPAL._s_password_hash, UPAL._s_password_backup_hash, UPAL._s_cd_status, " +
                "UPF._id, UPF._id_created_by, UPF._dt_create_date, UPF._dt_last_update, UPF._s_first_name, UPF._s_last_name, UPF._s_url_personal_img, UPF._s_company_name, UPF._s_url_company_img, UPF._s_address, UPF._s_city, UPF._s_state, UPF._s_country, UPF._s_postal_code, UPF._s_phone_number, UPF._s_url_website " +
                "from user_principal UPAL, user_profile UPF " +
                "where UPAL._id=? and UPAL._id=UPF._id_created_by";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, anID);
            preparedStatement.execute();

            //Parse the results
            resultSet = preparedStatement.getResultSet();
            UserPrincipalDTO dtoUPAL = null;
            while (resultSet != null && resultSet.next())
            {
                dtoUPAL = new UserPrincipalDTO();
                dtoUPAL.set_id(resultSet.getInt(1));
                dtoUPAL.set_dt_create_date(resultSet.getTimestamp(2));
                dtoUPAL.set_dt_last_update(resultSet.getTimestamp(3));
                dtoUPAL.set_s_username(resultSet.getString(4));
                dtoUPAL.set_s_password_hash(resultSet.getString(5));
                dtoUPAL.set_s_password_backup_hash(resultSet.getString(6));
                dtoUPAL.set_s_cd_status(resultSet.getString(7));

                UserProfileDTO dtoUPF = new UserProfileDTO();
                dtoUPF.set_id(resultSet.getInt(8));
                dtoUPF.set_id_created_by(resultSet.getInt(9));
                dtoUPF.set_dt_create_date(resultSet.getTimestamp(10));
                dtoUPF.set_dt_last_update(resultSet.getTimestamp(11));
                dtoUPF.set_s_first_name(resultSet.getString(12));
                dtoUPF.set_s_last_name(resultSet.getString(13));
                dtoUPF.set_s_url_personal_img(resultSet.getString(14));
                dtoUPF.set_s_company_name(resultSet.getString(15));
                dtoUPF.set_s_url_company_img(resultSet.getString(16));
                dtoUPF.set_s_address(resultSet.getString(17));
                dtoUPF.set_s_city(resultSet.getString(18));
                dtoUPF.set_s_state(resultSet.getString(19));
                dtoUPF.set_s_country(resultSet.getString(20));
                dtoUPF.set_s_postal_code(resultSet.getString(21));
                dtoUPF.set_s_phone_number(resultSet.getString(22));
                dtoUPF.set_s_url_website(resultSet.getString(23));

                dtoUPAL.setUserProfile(dtoUPF);
            }

            return dtoUPAL;
        }
        catch (Exception ex)
        {
            _LOGGER.error("anID=" + anID);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static List<UserPrincipalDTO> selectUsersByCompanyID(Connection connection, int anID)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String sSQL = "select UPAL._id, UPAL._dt_create_date, UPAL._dt_last_update, UPAL._s_username, UPAL._s_password_hash, UPAL._s_password_backup_hash, UPAL._s_cd_status, " +
                "UPF._id, UPF._id_created_by, UPF._dt_create_date, UPF._dt_last_update, UPF._s_first_name, UPF._s_last_name, UPF._s_url_personal_img, UPF._s_company_name, UPF._s_url_company_img, UPF._s_address, UPF._s_city, UPF._s_state, UPF._s_country, UPF._s_postal_code, UPF._s_phone_number, UPF._s_url_website " +
                "from user_principal UPAL, user_profile UPF, company_user CU " +
                "where CU._id_company=? and CU._id_user_principal=UPAL._id and UPAL._id=UPF._id_created_by " +
                "order by UPAL._s_username";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, anID);
            preparedStatement.execute();

            //Parse the results
            resultSet = preparedStatement.getResultSet();
            List<UserPrincipalDTO> alDTO = new ArrayList<UserPrincipalDTO>();
            UserPrincipalDTO dtoUPAL = null;
            while (resultSet != null && resultSet.next())
            {
                dtoUPAL = new UserPrincipalDTO();
                dtoUPAL.set_id(resultSet.getInt(1));
                dtoUPAL.set_dt_create_date(resultSet.getTimestamp(2));
                dtoUPAL.set_dt_last_update(resultSet.getTimestamp(3));
                dtoUPAL.set_s_username(resultSet.getString(4));
                dtoUPAL.set_s_password_hash(resultSet.getString(5));
                dtoUPAL.set_s_password_backup_hash(resultSet.getString(6));
                dtoUPAL.set_s_cd_status(resultSet.getString(7));
                alDTO.add(dtoUPAL);

                UserProfileDTO dtoUPF = new UserProfileDTO();
                dtoUPF.set_id(resultSet.getInt(8));
                dtoUPF.set_id_created_by(resultSet.getInt(9));
                dtoUPF.set_dt_create_date(resultSet.getTimestamp(10));
                dtoUPF.set_dt_last_update(resultSet.getTimestamp(11));
                dtoUPF.set_s_first_name(resultSet.getString(12));
                dtoUPF.set_s_last_name(resultSet.getString(13));
                dtoUPF.set_s_url_personal_img(resultSet.getString(14));
                dtoUPF.set_s_company_name(resultSet.getString(15));
                dtoUPF.set_s_url_company_img(resultSet.getString(16));
                dtoUPF.set_s_address(resultSet.getString(17));
                dtoUPF.set_s_city(resultSet.getString(18));
                dtoUPF.set_s_state(resultSet.getString(19));
                dtoUPF.set_s_country(resultSet.getString(20));
                dtoUPF.set_s_postal_code(resultSet.getString(21));
                dtoUPF.set_s_phone_number(resultSet.getString(22));
                dtoUPF.set_s_url_website(resultSet.getString(23));
                dtoUPAL.setUserProfile(dtoUPF);
            }

            return alDTO;
        }
        catch (Exception ex)
        {
            _LOGGER.error("anID=" + anID);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static List<String> selectUsernamesByCompanyAndNamePrefix(Connection connection, int aCompanyID, String aUsernamePrefix)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        //We always store the username as lowercase, so we should search in lowercase
        aUsernamePrefix = aUsernamePrefix.toLowerCase().trim();

        String sSQL = "select UPAL._id, UPAL._s_username " +
                "from user_principal UPAL, company_user CU " +
                "where CU._id_company=? and CU._id_user_principal=UPAL._id and UPAL._s_username like ?" +
                "order by UPAL._s_username";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, aCompanyID);
            preparedStatement.setString(2, aUsernamePrefix + "%");
            preparedStatement.execute();

            //Parse the results
            resultSet = preparedStatement.getResultSet();
            List<String> alUsernames = new ArrayList<String>();
            while (resultSet != null && resultSet.next())
            {
                //int userID = resultSet.getInt(1);
                alUsernames.add(resultSet.getString(2));
            }

            return alUsernames;
        }
        catch (Exception ex)
        {
            _LOGGER.error("aCompanyID=" + aCompanyID);
            _LOGGER.error("aUsernamePrefix=" + aUsernamePrefix);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }



}
