package com.steplify.dao;

import com.steplify.dto.*;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.*;
import java.util.Calendar;

public class TopicMemberDAO extends SuperDuperDAO {

    private static Logger _LOGGER = Logger.getLogger("TopicMemberDAO");

    //ADD DB CRUD METHODS HERE
    public static int insertTopicMember(Connection connection, TopicMemberDTO dto)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        String sSQL = "insert into topic_member " +
                "(_id_created_by, _id_updated_by, _dt_create_date, _dt_last_update, _id_user_principal, _s_cd_authority, _id_project, _id_topic) " +
                "values (?, ?, ?, ?, ?, ?, ?, ?)";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL, Statement.RETURN_GENERATED_KEYS);
            long lTimeNow = (Calendar.getInstance()).getTimeInMillis();

            preparedStatement.setInt(1, dto.get_id_created_by());
            preparedStatement.setInt(2, dto.get_id_updated_by());
            preparedStatement.setTimestamp(3, new Timestamp(lTimeNow));
            preparedStatement.setTimestamp(4, new Timestamp(lTimeNow));
            preparedStatement.setInt(5, dto.get_id_user_principal());
            preparedStatement.setString(6, dto.get_s_cd_authority());
            preparedStatement.setInt(7, dto.get_id_project());
            preparedStatement.setInt(8, dto.get_id_topic());

            //Do it, and then get the rowCount
            iCount = preparedStatement.executeUpdate();

            //Set the auto-increment field back in the DTO
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet != null && resultSet.next())
            {
                dto.set_id(resultSet.getInt(1));
            }

            return iCount;
        }
        catch (Exception ex)
        {
            _LOGGER.error(dto.toJSON());
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static int updateTopicMember(Connection connection, TopicMemberDTO dto)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        String sSQL = "update topic_member " +
                "set _id_updated_by=?, _dt_last_update=?, _id_user_principal=?, _s_cd_authority=?, _id_project=?, _id_topic=? " +
                "where _id=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            long lTimeNow = (Calendar.getInstance()).getTimeInMillis();

            preparedStatement.setInt(1, dto.get_id_updated_by());
            preparedStatement.setTimestamp(2, new Timestamp(lTimeNow));
            preparedStatement.setInt(3, dto.get_id_user_principal());
            preparedStatement.setString(4, dto.get_s_cd_authority());
            preparedStatement.setInt(5, dto.get_id_project());
            preparedStatement.setInt(6, dto.get_id_topic());
            preparedStatement.setInt(7, dto.get_id());

            //Do it, and then get the rowCount
            iCount = preparedStatement.executeUpdate();

            return iCount;
        }
        catch (Exception ex)
        {
            _LOGGER.error(dto.toJSON());
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static int deleteTopicMember(Connection connection, int anID)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        String sSQL = "delete from topic_member where _id=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, anID);

            //Do it, and then get the rowCount
            iCount = preparedStatement.executeUpdate();
            return iCount;
        }
        catch (Exception ex)
        {
            _LOGGER.error("anID=" + anID);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static TopicMemberDTO selectTopicMemberByID(Connection connection, int anID)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String sSQL = "select _id, _id_created_by, _id_updated_by, _dt_create_date, _dt_last_update, _id_user_principal, _s_cd_authority, _id_project, _id_topic " +
                "from topic_member where _id=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, anID);
            preparedStatement.execute();

            //Parse the results
            resultSet = preparedStatement.getResultSet();
            TopicMemberDTO dto = null;
            while (resultSet != null && resultSet.next())
            {
                dto = new TopicMemberDTO();
                dto.set_id(resultSet.getInt(1));
                dto.set_id_created_by(resultSet.getInt(2));
                dto.set_id_updated_by(resultSet.getInt(3));
                dto.set_dt_create_date(resultSet.getTimestamp(4));
                dto.set_dt_last_update(resultSet.getTimestamp(5));
                dto.set_id_user_principal(resultSet.getInt(6));
                dto.set_s_cd_authority(resultSet.getString(7));
                dto.set_id_project(resultSet.getInt(8));
                dto.set_id_topic(resultSet.getInt(9));
            }

            return dto;
        }
        catch (Exception ex)
        {
            _LOGGER.error("anID=" + anID);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static TopicMemberDTO selectTopicMemberByUserAndTopic(Connection connection, int aUserID, int aTopicID)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String sSQL = "select _id, _id_created_by, _id_updated_by, _dt_create_date, _dt_last_update, _id_user_principal, _s_cd_authority, _id_project, _id_topic " +
                "from topic_member where _id_user_principal=? and _id_topic=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, aUserID);
            preparedStatement.setInt(2, aTopicID);
            preparedStatement.execute();

            //Parse the results
            resultSet = preparedStatement.getResultSet();
            TopicMemberDTO dto = null;
            while (resultSet != null && resultSet.next())
            {
                dto = new TopicMemberDTO();
                dto.set_id(resultSet.getInt(1));
                dto.set_id_created_by(resultSet.getInt(2));
                dto.set_id_updated_by(resultSet.getInt(3));
                dto.set_dt_create_date(resultSet.getTimestamp(4));
                dto.set_dt_last_update(resultSet.getTimestamp(5));
                dto.set_id_user_principal(resultSet.getInt(6));
                dto.set_s_cd_authority(resultSet.getString(7));
                dto.set_id_project(resultSet.getInt(8));
                dto.set_id_topic(resultSet.getInt(9));
            }

            return dto;
        }
        catch (Exception ex)
        {
            _LOGGER.error("aUserID=" + aUserID);
            _LOGGER.error("aTopicID=" + aTopicID);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static List<TopicMemberDTO> selectTopicMembersByTopic(Connection connection, int aTopicID)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String sSQL = "select TM._id, TM._id_created_by, TM._id_updated_by, TM._dt_create_date, TM._dt_last_update, TM._id_user_principal, TM._s_cd_authority, TM._id_project, TM._id_topic " +
                "from topic_member TM where TM._id_topic=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, aTopicID);
            preparedStatement.execute();

            //Parse the results
            resultSet = preparedStatement.getResultSet();
            List<TopicMemberDTO> alDTO = new ArrayList<TopicMemberDTO>();
            TopicMemberDTO dtoTM = null;
            while (resultSet != null && resultSet.next())
            {
                dtoTM = new TopicMemberDTO();
                dtoTM.set_id(resultSet.getInt(1));
                dtoTM.set_id_created_by(resultSet.getInt(2));
                dtoTM.set_id_updated_by(resultSet.getInt(3));
                dtoTM.set_dt_create_date(resultSet.getTimestamp(4));
                dtoTM.set_dt_last_update(resultSet.getTimestamp(5));
                dtoTM.set_id_user_principal(resultSet.getInt(6));
                dtoTM.set_s_cd_authority(resultSet.getString(7));
                dtoTM.set_id_project(resultSet.getInt(8));
                dtoTM.set_id_topic(resultSet.getInt(9));
                alDTO.add(dtoTM);
            }

            return alDTO;
        }
        catch (Exception ex)
        {
            _LOGGER.error("aTopicID=" + aTopicID);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static List<TopicMemberDTO> selectTopicMembersByUserAndProject(Connection connection, int aUserID, int aProjectID)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String sSQL = "select TM._id, TM._id_created_by, TM._id_updated_by, TM._dt_create_date, TM._dt_last_update, TM._id_user_principal, TM._s_cd_authority, TM._id_project, TM._id_topic " +
                "from topic_member TM where TM._id_user_principal=? and TM._id_project=?";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setInt(1, aUserID);
            preparedStatement.setInt(2, aProjectID);
            preparedStatement.execute();

            //Parse the results
            resultSet = preparedStatement.getResultSet();
            List<TopicMemberDTO> alDTO = new ArrayList<TopicMemberDTO>();
            TopicMemberDTO dtoTM = null;
            while (resultSet != null && resultSet.next())
            {
                dtoTM = new TopicMemberDTO();
                dtoTM.set_id(resultSet.getInt(1));
                dtoTM.set_id_created_by(resultSet.getInt(2));
                dtoTM.set_id_updated_by(resultSet.getInt(3));
                dtoTM.set_dt_create_date(resultSet.getTimestamp(4));
                dtoTM.set_dt_last_update(resultSet.getTimestamp(5));
                dtoTM.set_id_user_principal(resultSet.getInt(6));
                dtoTM.set_s_cd_authority(resultSet.getString(7));
                dtoTM.set_id_project(resultSet.getInt(8));
                dtoTM.set_id_topic(resultSet.getInt(9));
                alDTO.add(dtoTM);
            }

            return alDTO;
        }
        catch (Exception ex)
        {
            _LOGGER.error("aUserID=" + aUserID);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

}
