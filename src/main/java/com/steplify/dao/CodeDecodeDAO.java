package com.steplify.dao;

import com.steplify.dto.CodeDecodeDTO;
import org.apache.log4j.Logger;
import java.util.*;
import java.sql.*;


public class CodeDecodeDAO extends SuperDuperDAO {

    private static final Logger _LOGGER = Logger.getLogger("CodeDecodeDAO");

    //ADD DB CRUD METHODS HERE
    public static int insert(Connection connection, CodeDecodeDTO dto)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int iCount = 0;

        String sSQL = "insert into code_decode " +
                "(_dt_create_date, _i_order, _s_category, _s_code, _s_decode, _b_active) " +
                "values (?, ?, ?, ?, ?, ?)";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL, Statement.RETURN_GENERATED_KEYS);

            long lTimeNow = (Calendar.getInstance()).getTimeInMillis();
            preparedStatement.setTimestamp(1, new Timestamp(lTimeNow));
            preparedStatement.setInt(2, dto.get_i_order());
            preparedStatement.setString(3, dto.get_s_category());
            preparedStatement.setString(4, dto.get_s_code());
            preparedStatement.setString(5, dto.get_s_decode());
            preparedStatement.setInt(6, dto.get_b_active() ? 1 : 0);

            //Do it, and then get the rowCount
            iCount = preparedStatement.executeUpdate();

            //Set the auto-increment field back in the DTO
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet != null && resultSet.next())
            {
                dto.set_id(resultSet.getInt(1));
            }

            return iCount;
        }
        catch (Exception ex)
        {
            _LOGGER.error(dto.toJSON());
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    public static List<CodeDecodeDTO> select(String sCategory, String sCode, String sDecode)
    {
        Connection connection = null;

        //You must have a CATEGORY
        if (sCategory == null)
        {
            _LOGGER.error("NULL CodeDecode Category");
            throw new RuntimeException("NULL CodeDecode Category");
        }

        try
        {
            //Get a DB connection, do the work, and close it
            connection = SuperDuperDAO.getConnection();

            if (sCode == null && sDecode == null)
            {
                return selectAllByCategory(connection, sCategory);
            }
            else if (sCode != null)
            {
                return selectByCode(connection, sCategory, sCode);
            }
            else if (sDecode != null)
            {
                return selectByDecode(connection, sCategory, sDecode);
            }
            else
            {
                _LOGGER.error("Invalid Arguments for CodeDecode.select()");
                throw new RuntimeException("Invalid use of CodeDecode API");
            }
        }
        finally
        {
            //There is nothing to commit here, it was just a select
            SuperDuperDAO.close(connection, false);
        }
    }

    //Only should return the selectable (i.e. ACTIVE) rows
    private static List<CodeDecodeDTO> selectAllByCategory(Connection connection, String sCategory)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String sSQL = "select _id, _dt_create_date, _i_order, _s_category, _s_code, _s_decode, _b_active " +
                "from code_decode where _s_category=? and _b_active=1 order by _i_order";

        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setString(1, sCategory);
            preparedStatement.execute();

            //Parse the results
            resultSet = preparedStatement.getResultSet();
            List<CodeDecodeDTO> alDTO = new ArrayList<CodeDecodeDTO>();
            while (resultSet != null && resultSet.next())
            {
                CodeDecodeDTO dto = new CodeDecodeDTO();
                dto.set_id(resultSet.getInt(1));
                dto.set_dt_create_date(resultSet.getTimestamp(2));
                dto.set_i_order(resultSet.getInt(3));
                dto.set_s_category(resultSet.getString(4));
                dto.set_s_code(resultSet.getString(5));
                dto.set_s_decode(resultSet.getString(6));
                dto.set_b_active(resultSet.getInt(7) == 0 ? false : true);
                alDTO.add(dto);
            }

            return alDTO;
        }
        catch (Exception ex)
        {
            _LOGGER.error("sCategory=" + sCategory);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

    private static List<CodeDecodeDTO> selectByCode(Connection connection, String sCategory, String sCode)
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String sSQL = "select _id, _dt_create_date, _i_order, _s_category, _s_code, _s_decode, _b_active " +
                "from code_decode where _s_category=? and _s_code=? order by _i_order";
        try
        {
            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setString(1, sCategory);
            preparedStatement.setString(2, sCode);
            preparedStatement.execute();

            //Parse the results
            resultSet = preparedStatement.getResultSet();
            List<CodeDecodeDTO> alDTO = new ArrayList<CodeDecodeDTO>();
            while (resultSet != null && resultSet.next())
            {
                CodeDecodeDTO dto = new CodeDecodeDTO();
                dto.set_id(resultSet.getInt(1));
                dto.set_dt_create_date(resultSet.getTimestamp(2));
                dto.set_i_order(resultSet.getInt(3));
                dto.set_s_category(resultSet.getString(4));
                dto.set_s_code(resultSet.getString(5));
                dto.set_s_decode(resultSet.getString(6));
                dto.set_b_active(resultSet.getInt(7) == 0 ? false : true);
                alDTO.add(dto);
            }

            return alDTO;
        }
        catch (Exception ex)
        {
            _LOGGER.error("sCategory=" + sCategory);
            _LOGGER.error("sCategory=" + sCode);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
    }

        private static List<CodeDecodeDTO> selectByDecode(Connection connection, String sCategory, String sDecode)
        {
            PreparedStatement preparedStatement = null;
            ResultSet resultSet = null;

            String sSQL = "select _id, _dt_create_date, _i_order, _s_category, _s_code, _s_decode, _b_active " +
                    "from code_decode where _s_category=? and _s_decode=? order by _i_order";

            try
            {
                preparedStatement = connection.prepareStatement(sSQL);
                preparedStatement.setString(1, sCategory);
                preparedStatement.setString(2, sDecode);
                preparedStatement.execute();

                //Parse the results
                resultSet = preparedStatement.getResultSet();
                List<CodeDecodeDTO> alDTO = new ArrayList<CodeDecodeDTO>();
                while (resultSet != null && resultSet.next())
                {
                    CodeDecodeDTO dto = new CodeDecodeDTO();
                    dto.set_id(resultSet.getInt(1));
                    dto.set_dt_create_date(resultSet.getTimestamp(2));
                    dto.set_i_order(resultSet.getInt(3));
                    dto.set_s_category(resultSet.getString(4));
                    dto.set_s_code(resultSet.getString(5));
                    dto.set_s_decode(resultSet.getString(6));
                    dto.set_b_active(resultSet.getInt(7) == 0 ? false : true);
                    alDTO.add(dto);
                }

                return alDTO;
            }
            catch (Exception ex)
            {
                _LOGGER.error("sCategory=" + sCategory);
                _LOGGER.error("sDecode=" + sDecode);
                _LOGGER.error(ex.getMessage());
                throw new RuntimeException(ex.getMessage());
            }
    }

    //Just return the simple KV for the category
    public static Map<String, String> selectKVs(String sCategory)
    {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String sSQL = "select _s_code, _s_decode " +
                "from code_decode where _s_category=?";

        try
        {
            //Get a DB connection, do the work, and close it
            connection = SuperDuperDAO.getConnection();

            preparedStatement = connection.prepareStatement(sSQL);
            preparedStatement.setString(1, sCategory);
            preparedStatement.execute();

            //Parse the results
            resultSet = preparedStatement.getResultSet();
            Map<String, String> mapKV = new HashMap<String, String>();
            while (resultSet != null && resultSet.next())
            {
                mapKV.put(resultSet.getString(1), resultSet.getString(2));
            }

            return mapKV;
        }
        catch (Exception ex)
        {
            _LOGGER.error("sCategory=" + sCategory);
            _LOGGER.error(ex.getMessage());
            throw new RuntimeException(ex.getMessage());
        }
        finally
        {
            //There is nothing to commit here, it was just a select
            SuperDuperDAO.close(connection, false);
        }
    }

}
