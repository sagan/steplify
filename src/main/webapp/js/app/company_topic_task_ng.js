function CompanyTopicTaskCtrl($scope, $http, $timeout) {

    //Setup the default view
    $scope._s_cd_project_type = '';
    $scope.alTopics = null;
    $scope.alTasks = null;

    $scope.hasProjectType = function() {
        if ($scope._s_cd_project_type)
        {
            return true;
        }
        else
        {
            return false;
        }
    };

    //Called 2 times per select, once to deselect the old, and select the new
    $scope.topicSelectionChange = function(data) {
        var row =  $scope.alTopics[data.rowIndex];
        if (data.selected)
        {
            $scope.alTasks = row._lst_tasks;
        }
        else
        {
            //No topic is selected
            $scope.alTasks = null;
        }
    };

    //Called 2 times per select, once to deselect the old, and select the new
    $scope.taskSelectionChange = function(data) {
        var row =  $scope.alTasks[data.rowIndex];
        //do nothing for now; placeholder
    };

    $scope.topicMoveUp = function(row) {
        var objTopic =  row.entity;
        var iTopic = row.rowIndex;

        if (iTopic > 0)
        {
            //Remove it, then add it; if we dont use $timeout ng-grid wont refresh
            $scope.alTopics.splice(iTopic, 1);
            $timeout(function() {$scope.alTopics.splice(iTopic-1, 0, objTopic);}, 0);
        }
    };

    $scope.topicMoveDown = function(row) {
        var objTopic =  row.entity;
        var iTopic = row.rowIndex;

        if (iTopic < $scope.alTopics.length - 1)
        {
            //Remove it, then add it; if we dont use $timeout ng-grid wont refresh
            $scope.alTopics.splice(iTopic, 1);
            $timeout(function() {$scope.alTopics.splice(iTopic+1, 0, objTopic);}, 0);
        }
    };

    $scope.taskMoveUp = function(row) {
        var objTask =  row.entity;
        var iTask = row.rowIndex;

        if (iTask > 0)
        {
            //Remove it, then add it; if we dont use $timeout ng-grid wont refresh
            $scope.alTasks.splice(iTask, 1);
            $timeout(function() {$scope.alTasks.splice(iTask-1, 0, objTask);}, 0);
        }
    };

    $scope.taskMoveDown = function(row) {
        var objTask =  row.entity;
        var iTask = row.rowIndex;

        if (iTask < $scope.alTasks.length - 1)
        {
            //Remove it, then add it; if we dont use $timeout ng-grid wont refresh
            $scope.alTasks.splice(iTask, 1);
            $timeout(function() {$scope.alTasks.splice(iTask+1, 0, objTask);}, 0);
        }
    };

    $scope.topicDelete = function(row) {
        var objTopic =  row.entity;
        var iTopic = row.rowIndex;

        //Remove it
        $scope.alTopics.splice(iTopic, 1);
        $scope.alTasks = null;
    };

    $scope.taskDelete = function(row) {
        var objTask =  row.entity;
        var iTask = row.rowIndex;

        //Remove it
        $scope.alTasks.splice(iTask, 1);
    };

    $scope.topicAdd = function() {
        if ($scope.alTopics)
        {
            var newTopic = {_s_name:'', _b_auto_add_members:false, _i_order:5, _lst_tasks:[]};
            $scope.alTopics.push(newTopic);
            $timeout(function() {$scope.gridTopics.selectItem($scope.alTopics.length-1, true);}, 0);
        }
    };

    $scope.taskAdd = function() {
        if ($scope.alTasks)
        {
            var newTask = {_s_name:''};
            $scope.alTasks.push(newTask);
            $timeout(function() {$scope.gridTasks.selectItem($scope.alTasks.length-1, true);}, 0);
        }
    };

    $scope.gridTopics = { data: 'alTopics',
                            multiSelect: false,
                            enableSorting: false,
                            columnDefs: [{field:'_s_name', displayName:'Topic Name', enableCellEdit:true, width:'75%'},
                                        {field:'edit', displayName:'Tasks', enableCellEdit: false, width:'5%', cellTemplate: '<div><a class="btn" href="#tasks"><i class="fa fa-edit"></i></a></div>'},
                                        {field:'down', displayName:'Down', enableCellEdit: false, width:'5%', cellTemplate: '<div><button ng-click="topicMoveDown(row)"><i class="fa fa-arrow-down"></i></button></div>'},
                                        {field:'up', displayName:'Up', enableCellEdit: false, width:'5%', cellTemplate: '<div><button ng-click="topicMoveUp(row)"><i class="fa fa-arrow-up"></i></button></div>'},
                                        {field:'delete', displayName:'Delete', enableCellEdit: false, width:'8%', cellTemplate: '<div><button ng-click="topicDelete(row)"><i class="fa fa-times"></i></button></div>'}],
                            afterSelectionChange: $scope.topicSelectionChange};

    $scope.gridTasks = { data: 'alTasks',
                            multiSelect: false,
                            enableSorting: false,
                            columnDefs: [{field:'_s_name', displayName:'Task Name', enableCellEdit:true, width:'82%'},
                                        {field:'down', displayName:'Down', enableCellEdit: false, width:'5%', cellTemplate: '<div><button ng-click="taskMoveDown(row)"><i class="fa fa-arrow-down"></i></button></div>'},
                                        {field:'up', displayName:'Up', enableCellEdit: false, width:'5%', cellTemplate: '<div><button ng-click="taskMoveUp(row)"><i class="fa fa-arrow-up"></i></button></div>'},
                                        {field:'delete', displayName:'Delete', enableCellEdit: false, width:'8%', cellTemplate: '<div><button ng-click="taskDelete(row)"><i class="fa fa-times"></i></button></div>'}],
                            afterSelectionChange: $scope.taskSelectionChange};


    //For HTML Usage
    $scope.getTopicTasks = function() {
        //Remove any prior data from the editor
        $scope.alTopics = [];
        $scope.alTasks = null;

         //Send the inputs to the server
         var inputData = null;
         var params = "?_s_cd_project_type=" + $scope._s_cd_project_type + "&ts=" + new Date().getTime();

         $http({
           method : 'GET',
           data: inputData,
           url : '/company/topic_task_export/ajax' + params,
           timeout : 60000
           }).success(function(data, status, headers, config) {
               $scope.alTopics = data;
               $scope.alTasks = null;
           }).error(function(data, status, headers, config) {
               alert("An error occurred, please reload the page and try again. " + data);
         });

     }; //end of getTopicTasks()

     //Save back to server
     $scope.postTopicTasks = function() {
          //Do any validation
          if ($scope.alTopics.length == 0)
          {
            bConfirm = window.confirm("You are removing all Topics/Tasks for the selected project type: " + $scope._s_cd_project_type);

            if (!bConfirm)
            {
                return false;
            }
          }

          //Send the inputs to the server
          var inputData = $scope.alTopics;
          var params = "?_s_cd_project_type=" + $scope._s_cd_project_type + "&ts=" + new Date().getTime();

          $http({
            method : 'POST',
            data: inputData,
            url : '/company/topic_task_import/ajax' + params,
            timeout : 60000
            }).success(function(data, status, headers, config) {
                alert("Save was successful. " + data);
            }).error(function(data, status, headers, config) {
                alert("Save unsuccessful (the most common error is the same task exists 2 times within the same topic). " + data);
          });

      }; //end of postTopicTasks()

    $scope.initView = function(data) {
        $scope._s_cd_project_type = '';
        $scope.alTopics = null;
        $scope.alTasks = null;
    };

    //For CSV Export
    $scope.doExport = function() {
        if ($scope._s_cd_project_type == '') {
            alert("Pick a project type!");
            $("#project_type").focus();
            return false;
        }

        //Do the real form POST
        $( "#form_topic_task_export" ).submit();
    };

}

//JQuery usage
$(document).ready(function() {
    //Initialize Tooltips for Bootstrap CSS
    $('.column_tip').tooltip();
});

//Angular Module, used for defining dependencies
var app = angular.module('myCompanyTopicTaskApp', ['ngGrid']);
app.controller('CompanyTopicTaskCtrl', CompanyTopicTaskCtrl);
