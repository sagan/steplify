function IndexCtrl($scope) {

    $scope.showHIW = function (iIndex)
    {
        return (iIndex == $scope.iHIW);
    };

    $scope.incrementHIW = function ()
    {
        $scope.iHIW = $scope.iHIW + 1;
        $scope.checkBoundaries();
    };

    $scope.decrementHIW = function ()
    {
        $scope.iHIW = $scope.iHIW - 1;
        $scope.checkBoundaries();
    };

    $scope.checkBoundaries = function ()
    {
        if ($scope.iHIW > 10)
        {
            $scope.iHIW = 1;
        }
        else if ($scope.iHIW < 1)
        {
            $scope.iHIW = 1;
        }
    };

    $scope.initHIW = function ()
    {
        $scope.iHIW = 1;
    }

    $scope.initHIW();
};

//JQuery usage
$(document).ready(function() {
    //Hide the HIW Text section on page load
    $('#heroHiwID').hide();

    //When we click the HIW-Start button
    $('#btnHiwStartID').click(function(){
        $( "#heroMainID" ).fadeTo(500, 0.0, flipToHIW);
    });

    //Fade in the HIW background image
    function flipToHIW()
    {
        $( "#heroMainID" ).removeClass( "hero-main" ).addClass( "hero-main-hiw" );
        $( "#heroMainID" ).fadeTo(500, 1.0);
        $('#heroIntroID').hide();
        $('#heroHiwID').show();
    }

    //When we click the HIW-Close button
    $('#btnHiwCloseID').click(function(){
        $( "#heroMainID" ).fadeTo(500, 0.0, flipToMain);
    });

    //Fade in the MAIN background image
    function flipToMain()
    {
        $( "#heroMainID" ).removeClass( "hero-main-hiw" ).addClass( "hero-main" );
        $( "#heroMainID" ).fadeTo(500, 1.0);
        $('#heroHiwID').hide();
        $('#heroIntroID').show();
    }
});
