function ProjectAddCtrl($scope, $http) {

    //So the image is hidden by default
    $scope._s_url_image = '';
    $scope._s_url_website = '';

    $scope.postAddProject = function() {
        //If you want any test validation, this is your last chance

        //Do the real form POST
        $( "#form_add_project" ).submit();
    };

    $scope.startsWithHttp = function(sURL)
    {
        if ($scope.isEmpty(sURL))
        {
            return false;
        }
        else
        {
            return (sURL.indexOf('http://') === 0 || sURL.indexOf('https://') === 0);
        }
    };

    $scope.isEmpty = function(aString)
    {
        if (aString == undefined || aString == null || aString === '')
        {
            return true;
        }
        else
        {
            return false;
        }
    };


    $scope.attemptToShowImage = function() {
        if ($scope.startsWithHttp($scope._s_url_image))
        {
            return true;
        }
        else
        {
            return false;
        }
        };

    $scope.showImageHint = function() {
        if (!$scope.isEmpty($scope._s_url_image) && !$scope.startsWithHttp($scope._s_url_image))
        {
            return true;
        }
        else
        {
            return false;
        }
    };

    $scope.attemptToShowWebsite = function() {
        if ($scope.startsWithHttp($scope._s_url_website))
        {
            return true;
        }
        else
        {
            return false;
        }
        };

    $scope.showWebsiteHint = function() {
        if (!$scope.isEmpty($scope._s_url_website) && !$scope.startsWithHttp($scope._s_url_website))
        {
            return true;
        }
        else
        {
            return false;
        }
    };

}


//JQuery usage
$(document).ready(function() {

    function initGoogleMapsAutoComplete() {
        var input = document.getElementById('maps_address');
        var options = {};
        var autocomplete = new google.maps.places.Autocomplete(input, options);

        //When a user selection is made, lets parse the results if a PLACE was found
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                return;
              }

            //The PLACE stores each address component separately, parse each one for the UI
            for (i = 0; i < place.address_components.length; ++i)
            {
                var component = place.address_components[i];

                if (component.types.indexOf("street_number") >= 0)
                {
                    var sStreetNumber = component.long_name;
                }

                if (component.types.indexOf("route") >= 0)
                {
                    var sRoute = component.long_name;
                }

                if (component.types.indexOf("locality") >= 0)
                {
                    $('#maps_city').val(component.long_name);
                }

                if (component.types.indexOf("administrative_area_level_1") >= 0)
                {
                    $('#maps_state').val(component.long_name);
                }

                if (component.types.indexOf("postal_code") >= 0)
                {
                    $('#maps_zip').val(component.long_name);
                }

                if (component.types.indexOf("country") >= 0)
                {
                    $('#maps_country').val(component.long_name);
                }
            }

            //Derive a street address
            var sStreetAddress = '';
            if (sStreetNumber) {
                sStreetAddress = sStreetNumber;
            }
            if (sRoute) {
                sStreetAddress = sStreetAddress + ' ' + sRoute;
            }

            //Leave the input textbox, since google sets the full address/city/state/etc on blur()
            $('#project_type').focus();
            $('#maps_address').val(sStreetAddress.trim());

            //Overwrite the full PLACE set from blur(), with just the street address; delay after google magic is done
            setTimeout(function(){
                $('#maps_address').val(sStreetAddress);
            }, 100);
        });

    }

    //Kick off the Autocomplete code
    google.maps.event.addDomListener(window, 'load', initGoogleMapsAutoComplete);

});
