function AdminTopicCtrl($scope, $http) {

    $scope.postUpdateTopic = function(iFormIndex) {
        //If you want any test validation, this is your last chance

        //Do the real form POST
        $( "#form_update_topic_" + iFormIndex).submit();
    };

    $scope.postAddTopic = function() {
        //If you want any test validation, this is your last chance

        //Do the real form POST
        $( "#form_add_topic" ).submit();
    };

}

//JQuery usage
$(document).ready(function() {
    //Initialize Tooltips for Bootstrap CSS
    $('.column_tip').tooltip();
});


