function DashboardCtrl($scope, $http) {

    $scope.doArchive = function(iProjectMembershipID, bSetArchived) {
        //Remove any prior error messages
        var selectorDivID = '#project_membership_' + iProjectMembershipID;
        $(selectorDivID).hide();

         //Send the inputs to the server
         var inputData = {_id : iProjectMembershipID, _b_set_archived : bSetArchived};
         var params = "?ts=" + new Date().getTime();

         $http({
           method : 'POST',
           data: inputData,
           url : '/dashboard/archive/ajax' + params,
           timeout : 60000
           }).success(function(data, status, headers, config) {
               //There was NO problem, we are done
           }).error(function(data, status, headers, config) {
               //There was a problem, put it back on the UI
               $(selectorDivID).show();
         });

    }; //end of doArchive()

}


//JQuery usage
$(document).ready(function() {

    //Add a toggle to each individual project section
    $('.accordion').click(function(){
        //Lookup the topic section by ID
        var projectSelector = '#project_membership_' + $(this).attr('id');
        $(projectSelector).slideToggle('fast');
    });

    //Add a toggle to open/close all projects at once
    $('#accordion_all_memberships').click(function(){
        //All topic sections have this class
        $('.project_membership').slideToggle('fast');
    });

    //Default all projects closed
    $('.project_membership').hide();

    //Add a toggle to open/close the FAQs
    $('#accordion_faqs').click(function(){
        //All topic sections have this class
        $('#faqsID').slideToggle('slow');
    });

    //Default the FAQ open
    if (gIntProjectCount == 0 && !gbFetchArchived)
    {
        $('#faqsID').show();
    }
    else
    {
        $('#faqsID').hide();
    }

});
