function TaskManagerCtrl($scope, $http) {

    $scope.initTaskVars = function(iTopicID, iTaskID) {
        //Get the latest data from the server
        $scope.disable_task_editor = true;
        $scope.task_editor_status_txt = 'Fetching data, please wait...';

        if (iTaskID)
        {
            //Go get the task from the server, all enable/disable of buttons happens in here
            $scope.getTask(iTaskID);
        }
        else
        {
            //Create a new default task for the given topic, default to today's date
            $scope.task = {
                _s_name : '',
                _s_cd_status : 'New',
                _s_assigned_user : '',
                _s_dt_due : new Date().toISOString().substring(0, 10),
                _s_comment : '',
                _id_topic : iTopicID
            };

            //Data for new instance is setup, let the user edit it
            $scope.task_editor_status_txt = null;
            $scope.disable_task_editor = false;
        }

    }; //end of initTaskVars()


    $scope.getTask = function(iTaskID) {
        //Remove any prior data from the editor
        $scope.task = null;

         //Send the inputs to the server
         var inputData = null;
         var params = "?tkid=" + iTaskID + "&ts=" + new Date().getTime();

         $http({
           method : 'GET',
           data: inputData,
           url : '/task_manager/task/ajax' + params,
           timeout : 60000
           }).success(function(data, status, headers, config) {
               $scope.task = data;
               $scope.disable_task_editor = false;
               $scope.task_editor_status_txt = null;
           }).error(function(data, status, headers, config) {
               $scope.task = null;
               $scope.disable_task_editor = true;
               $scope.task_editor_status_txt = data;
         });

     }; //end of getTask()


     //No parameters, since the $scope vars already have the specific Topic/Task
    $scope.updateTaskAndOrComment = function() {
        //Remove any prior error messages
         $scope.disable_task_editor = true;
         $scope.task_editor_status_txt = 'Saving data, please wait...';

         //Send the inputs to the server
         var inputData = $scope.task;
         var params = "?ts=" + new Date().getTime();

         $http({
           method : 'POST',
           data: inputData,
           url : '/task_manager/task_andor_comment/ajax' + params,
           timeout : 60000
           }).success(function(data, status, headers, config) {
               $scope.disable_task_editor = false;
               $scope.task_editor_status_txt = "Task updated successfully";
               $scope.refreshPage();
           }).error(function(data, status, headers, config) {
               $scope.disable_task_editor = false;
               $scope.task_editor_status_txt = data;
         });

    }; //end of updateTaskAndOrComment()


    //This will reload the page, use after an edit
    $scope.refreshPage = function() {
            window.location.reload();
        }; //end of refreshPage()

};

//JQuery usage
$(document).ready(function() {
    //Initialize Tooltips for Bootstrap CSS
    $('.column_tip').tooltip();
    var idEditorInvokedLastBy = '';

    //Add a toggle to open/close all topics at once
    $('#accordion_all_topics').click(function(){
        //All topic sections have this class
        $('.topic').slideToggle('fast');
    });

    //Add a toggle to each individual topic section
    $('.accordion').click(function(){
        //Lookup the topic section by ID
        var topicSelector = '#topic_' + $(this).attr('id');
        $(topicSelector).slideToggle('fast');
    });

    //Add a toggle to each individual topic section
    $('.accordion_topic_members').click(function(){
        //Lookup the topic section by ID
        var topicSelector = '#' + $(this).attr('id') + '_list';
        $(topicSelector).slideToggle('fast');
    });

    //Updating Tasks, move the #task_editor div after the selected task
    $('.task_edit').click(function(){
        $('#task_editor').hide();

        //This if-check is so if you click the same button, it will toggle
        var idEditorCurrentlyInvokedBy = $(this).attr('id');
        if (idEditorCurrentlyInvokedBy != idEditorInvokedLastBy)
        {
            //The $this.id looks like 'task_edit_678', parse out the number
            var taskID = $(this).attr('id').split('_')[2];
            var taskSelector = '#task_middle_' + taskID;
            $(taskSelector).after($('#task_editor'));
            $('#task_editor').slideToggle('fast');

            //Remember for next time
            idEditorInvokedLastBy = idEditorCurrentlyInvokedBy;
        }
        else
        {
            //So on the 3rd click, it will reopen
            idEditorInvokedLastBy = '';
        }
    });

    //Adding a new task to an existing topic
    $('.task_new').click(function(){
        $('#task_editor').hide();

        //This if-check is so if you click the same button, it will toggle
        var idEditorCurrentlyInvokedBy = $(this).attr('id');
        if (idEditorCurrentlyInvokedBy != idEditorInvokedLastBy)
        {
            //The $this.id looks like 'task_new_for_topic_123', parse out the number
            var topicID = $(this).attr('id').split('_')[4];
            var topicSelector = '#topic_content_header' + topicID;
            $(topicSelector).after($('#task_editor'));
            $('#task_editor').slideToggle('fast');

            //Remember for next time
            idEditorInvokedLastBy = idEditorCurrentlyInvokedBy;
        }
        else
        {
            //So on the 3rd click, it will reopen
            idEditorInvokedLastBy = '';
        }
    });

    //The close button should do whatelse, close it!
    $('#btn_close_task_editor').click(function(){
            $('#task_editor').hide();
        });

    //Defaults on page load/start
    $('.accordion_topic_members_list').hide();
    $('#task_editor').hide();

});


//If the browser doesnt support HTML5 dates, use a polyfill shim
Modernizr.load({
    test: Modernizr.inputtypes.date,
    nope: ['/js/lib/webshims/js-webshim/minified/polyfiller.js'],
    complete: function () {
        if (!Modernizr.inputtypes.date)
        {
            $.webshims.setOptions({basePath: "/js/lib/webshims/js-webshim/minified/shims/", debug: true});

            //http://afarkas.github.io/webshim/demos/demos/webforms/3-webforms-widgets.html#date-cfg
            $.webshims.setOptions('forms-ext', {
                date: {
                    popover: {
                        position: {
                            my: 'left bottom',
                            collision: "flipfit"
                        }
                    }
                }
            });

            //Limit the polyfill to only this class of widgets
            $.webshims.polyfill('forms-ext');
        }
    }
});