function AdminTopicWizardCtrl($scope, $http) {

    $scope.addWizard = function(iProjectID) {
            //Remove any prior error messages
            $scope.error_wizard_show = false;
            $scope.progressbar_wizard_show = true;

             //Send the inputs to the server
             var alSelections = [];
             for(var sKey in $scope.mapWizard)
             {
                //Only send the selected items
                if ($scope.mapWizard[sKey] === true)
                {
                  alSelections.push(sKey);
                }
             }

             var inputData = {'iProjectID':iProjectID, 'alTopicTasks':alSelections};
             var params = "?ts=" + new Date().getTime();

             $http({
               method : 'POST',
               data: inputData,
               url : '/admin_topic/wizard/ajax' + params,
               timeout : 60000
               }).success(function(data, status, headers, config) {
                   $scope.progressbar_wizard_show = false;
                   $scope.error_wizard_txt = data;
                   $scope.error_wizard_show = false;
                   //auto redirect to the next page
                   window.location.href='/admin_user/view?pid=' + iProjectID;
               }).error(function(data, status, headers, config) {
                   $scope.progressbar_wizard_show = false;
                   $scope.error_wizard_txt = data;
                   $scope.error_wizard_show = true;
             });

        }; //end of addWizard()

        $scope.initWizardVars = function() {
            $scope.error_wizard_show = false;
            $scope.progressbar_wizard_show = false;
            //Reset selections to false
            $scope.mapWizard = {};
        }; //end of initWizardVars()

        $scope.initWizardVars();
}

//JQuery usage
$(document).ready(function() {
    //Initialize Tooltips for Bootstrap CSS
    $('.column_tip').tooltip();

    //Add a toggle to each individual topic section
    $('.select_all').click(function(){
        //Lookup the topic section by ID
        var wizardTopicSelector = '.' + $(this).attr('id');
        $(wizardTopicSelector).click();
    });

});


