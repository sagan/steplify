function AdminUserCtrl($scope, $http) {

    $scope.postUpdateMembership = function(iFormIndex) {
        //If you want any test validation, this is your last chance

        //Do the real form POST
        $( "#form_update_membership_" + iFormIndex).submit();
    };

    $scope.postAddMembership = function() {
        //If you want any test validation, this is your last chance

        //Do the real form POST
        $( "#form_add_membership" ).submit();
    };

}

//JQuery usage
$(document).ready(function() {
    //Initialize Tooltips for Bootstrap CSS
    $('.column_tip').tooltip();

    var loadUsernames = function (request, response) {
        //Get the search term entered so far
    	sEmailPrefix = request.term;

      	$.ajax({
            url: '/admin_user/user_search/ajax',
            type: 'GET',
            data: {_s_username_prefix: sEmailPrefix, ts: new Date().getTime()},
            dataType: 'json'
       	 }).done(function(data){
       	    //process the server result json-array data
            response(data);
       	 });
    };

    //Hook up the AutoComplete
    $("#_s_email").autocomplete({source: loadUsernames, minLength: 3, delay: 500});

});
