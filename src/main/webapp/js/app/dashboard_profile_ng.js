function DashboardProfileCtrl($scope, $http) {

    $scope.initPasswordVars = function() {
        $scope.password_1 = '';
        $scope.password_2 = '';
        $scope.error_password_txt = '';
        $scope.error_password_show = false;
        $scope.progressbar_password_show = false;
        $scope.bSavePasswordButtonEnabled = true;
    };

    $scope.initLogoutVars = function() {
        $scope.error_logout_txt = '';
        $scope.error_logout_show = false;
        $scope.progressbar_logout_show = false;
    };

    $scope.initProfileVars = function() {
        //Get the latest data from the server
        $scope.fetching_profile = true;
        $scope.progressbar_profile_show = true;
        $scope.error_profile_txt = 'Fetching profile, please wait...';
        $scope.error_profile_show = true;
        $scope.bSaveProfileButtonEnabled = true;
        $scope.getProfile();
    };

    $scope.setPassword = function() {
        //Remove any prior error messages
        $scope.error_password_show = false;

        if ($scope.password_1 == "") {
            $scope.error_password_txt = "Password cannot be blank";
            $scope.error_password_show = true;
            $("#txt_password1").focus();
            return false;
        }

        if ($scope.password_2 == "") {
            $scope.error_password_txt = "Password cannot be blank";
            $scope.error_password_show = true;
            $("#txt_password2").focus();
            return false;
        }

        if ($scope.password_1 != $scope.password_2) {
            $scope.error_password_txt = "Passwords are not the same";
            $scope.error_password_show = true;
            $("#txt_password2").focus();
            return false;
        }

        //Lets do it
        $scope.error_password_show = false;
        $scope.progressbar_password_show = true;
        $scope.bSavePasswordButtonEnabled = false;

         //Send the inputs to the server
         var inputData = {'_s_password1':$scope.password_1, '_s_password2':$scope.password_2};
         var params = "?ts=" + new Date().getTime();

         $http({
           method : 'POST',
           data: inputData,
           url : '/dashboard/password/ajax' + params,
           timeout : 60000
           }).success(function(data, status, headers, config) {
               $scope.progressbar_password_show = false;
               $scope.error_password_txt = "Password Updated!";
               $scope.error_password_show = true;
               $scope.bSavePasswordButtonEnabled = true;
           }).error(function(data, status, headers, config) {
               $scope.progressbar_password_show = false;
               $scope.error_password_txt = data;
               $scope.error_password_show = true;
               $scope.bSavePasswordButtonEnabled = true;
         });

    }; //end of setPassword()

    $scope.doLogout = function() {
        //Remove any prior error messages
        $scope.error_logout_show = false;
        $scope.progressbar_logout_show = true;

         //Send the inputs to the server
         var inputData = null;
         var params = "?ts=" + new Date().getTime();

         $http({
           method : 'GET',
           data: inputData,
           url : '/login/logout/ajax' + params,
           timeout : 60000
           }).success(function(data, status, headers, config) {
               $scope.progressbar_logout_show = false;
               window.location.href='/index.jsp';
           }).error(function(data, status, headers, config) {
               $scope.progressbar_logout_show = false;
               $scope.error_logout_txt = "There was an unexpected error";
               $scope.error_logout_show = true;
         });

    }; //end of doLogout()

    $scope.getProfile = function() {
        //Remove any prior error messages
        $scope.error_profile_show = false;
        $scope.progressbar_profile_show = true;
        $scope.fetching_profile = true;

         //Send the inputs to the server
         var inputData = null;
         var params = "?ts=" + new Date().getTime();

         $http({
           method : 'GET',
           data: inputData,
           url : '/dashboard/profile/ajax' + params,
           timeout : 60000
           }).success(function(data, status, headers, config) {
               $scope.profile = data;
               $scope.progressbar_profile_show = false;
               $scope.fetching_profile = false;
           }).error(function(data, status, headers, config) {
               $scope.progressbar_profile_show = false;
               $scope.error_profile_txt = "There was an unexpected error";
               $scope.error_profile_show = true;
               $scope.fetching_profile = false;
         });

    }; //end of getProfile()

    $scope.setProfile = function() {
            //Remove any prior error messages
            $scope.error_profile_show = false;
            $scope.progressbar_profile_show = true;
            $scope.bSaveProfileButtonEnabled = false;

             //Send the inputs to the server
             var inputData = $scope.profile;
             var params = "?ts=" + new Date().getTime();

             $http({
               method : 'POST',
               data: inputData,
               url : '/dashboard/profile/ajax' + params,
               timeout : 60000
               }).success(function(data, status, headers, config) {
                   $scope.progressbar_profile_show = false;
                   $scope.error_profile_txt = "Profile updated successfully";
                   $scope.error_profile_show = true;
                   $scope.bSaveProfileButtonEnabled = true;
               }).error(function(data, status, headers, config) {
                   $scope.progressbar_profile_show = false;
                   $scope.error_profile_txt = data;
                   $scope.error_profile_show = true;
                   $scope.bSaveProfileButtonEnabled = true;
             });

        }; //end of setProfile()

        $scope.startsWithHttp = function(sURL)
        {
            if ($scope.isEmpty(sURL))
            {
                return false;
            }
            else
            {
                return (sURL.indexOf('http://') === 0 || sURL.indexOf('https://') === 0);
            }
        };

        $scope.isEmpty = function(aString)
        {
            if (aString == undefined || aString == null || aString === '')
            {
                return true;
            }
            else
            {
                return false;
            }
        };

        $scope.attemptToShowPersonalImage = function() {
            if ($scope.startsWithHttp($scope.profile._s_url_personal_img))
            {
                return true;
            }
            else
            {
                return false;
            }
        };

        $scope.showPersonalImageHint = function() {
            if (!$scope.isEmpty($scope.profile._s_url_personal_img) && !$scope.startsWithHttp($scope.profile._s_url_personal_img))
            {
                return true;
            }
            else
            {
                return false;
            }
        };

        $scope.attemptToShowCompanyImage = function() {
            if ($scope.startsWithHttp($scope.profile._s_url_company_img))
            {
                return true;
            }
            else
            {
                return false;
            }
        };

        $scope.showCompanyImageHint = function() {
            if (!$scope.isEmpty($scope.profile._s_url_company_img) && !$scope.startsWithHttp($scope.profile._s_url_company_img))
            {
                return true;
            }
            else
            {
                return false;
            }
        };

        $scope.attemptToShowWebsite = function() {
            if ($scope.startsWithHttp($scope.profile._s_url_website))
            {
                return true;
            }
            else
            {
                return false;
            }
        };

        $scope.showWebsiteHint = function() {
            if (!$scope.isEmpty($scope.profile._s_url_website) && !$scope.startsWithHttp($scope.profile._s_url_website))
            {
                return true;
            }
            else
            {
                return false;
            }
        };

    //Initialize, even before the user takes any action
    $scope.profile = {};

}

