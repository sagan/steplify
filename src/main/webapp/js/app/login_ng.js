function LoginCtrl($scope, $http) {

    //Initialize my variables, use 1 email variable for all 3 tabs
    $scope.email = '';
    $scope.password = '';
    $scope.error_login_txt = '';
    $scope.error_login_show = false;
    $scope.progressbar_login_show = false;
    $scope.bLoginButtonEnabled = true;

    $scope.postal_code = '';
    $scope.terms_agree = false;
    $scope.error_register_txt = '';
    $scope.error_register_show = false;
    $scope.progressbar_register_show = false;
    $scope.bRegisterButtonEnabled = true;

    $scope.error_lost_password_txt = '';
    $scope.error_lost_password_show = false;
    $scope.progressbar_lost_password_show = false;
    $scope.bLostPasswordButtonEnabled = true;

    $scope.registerUser = function() {
        //Remove any prior error messages
        $scope.error_register_show = false;

        if ($scope.email == "") {
            $scope.error_register_txt = "Email cannot be blank";
            $scope.error_register_show = true;
            $("#txt_email1").focus();
            return false;
        }

        if ($scope.postal_code == "") {
            $scope.error_register_txt = "Postal/Zip code missing";
            $scope.error_register_show = true;
            $("#txt_postal_code").focus();
            return false;
        }

        //Lets do it
        $scope.error_register_show = false;
        $scope.progressbar_register_show = true;
        $scope.bRegisterButtonEnabled = false;

         //Send the inputs to the server
         var inputData = {'_s_email':$scope.email, '_s_postal_code':$scope.postal_code};
         var params = "?ts=" + new Date().getTime();

         $http({
           method : 'POST',
           data: inputData,
           url : '/login/register/ajax' + params,
           timeout : 60000
           }).success(function(data, status, headers, config) {
               $scope.progressbar_register_show = false;
               $scope.error_register_txt = data;
               $scope.error_register_show = true;
               $scope.bRegisterButtonEnabled = false;
           }).error(function(data, status, headers, config) {
               $scope.progressbar_register_show = false;
               $scope.error_register_txt = data;
               $scope.error_register_show = true;
               $scope.bRegisterButtonEnabled = true;
         });

    }; //end of registerUser()


    $scope.loginUser = function() {
        //Remove any prior error messages
        $scope.error_login_show = false;

        if ($scope.email == "") {
            $scope.error_login_txt = "Email cannot be blank";
            $scope.error_login_show = true;
            $("#txt_email").focus();
            return false;
        }

        if ($scope.password == "") {
            $scope.error_login_txt = "Password cannot be blank";
            $scope.error_login_show = true;
            $("#txt_password").focus();
            return false;
        }

        if ($scope.terms_agree == false) {
            $scope.error_register_txt = "You must agree to the terms first.";
            $scope.error_register_show = true;
            $("#chx_agree").focus();
            return false;
        }

        //Lets do it
        $scope.error_login_show = false;
        $scope.progressbar_login_show = true;
        $scope.bLoginButtonEnabled = false;

         //Send the inputs to the server
         var inputData = {'_s_email':$scope.email, '_s_password':$scope.password};
         var params = "?ts=" + new Date().getTime();

         $http({
           method : 'POST',
           data: inputData,
           url : '/login/user/ajax' + params,
           timeout : 60000
           }).success(function(data, status, headers, config) {
               //redirect to the new page, based on the server data response
               window.location.href=data;
           }).error(function(data, status, headers, config) {
               $scope.progressbar_login_show = false;
               $scope.error_login_txt = data;
               $scope.error_login_show = true;
               $scope.bLoginButtonEnabled = true;
         });

    }; //end of loginUser()

    $scope.lostPassword = function() {
        //Remove any prior error messages
        $scope.error_lost_password_show = false;

        if ($scope.email == "") {
            $scope.error_lost_password_txt = "Email cannot be blank";
            $scope.error_lost_password_show = true;
            $("#txt_email_lostpass").focus();
            return false;
        }

        //Lets do it
        $scope.error_lost_password_show = false;
        $scope.progressbar_lost_password_show = true;
        $scope.bLostPasswordButtonEnabled = false;

         //Send the inputs to the server
         var inputData = {'_s_email':$scope.email};
         var params = "?ts=" + new Date().getTime();

         $http({
           method : 'POST',
           data: inputData,
           url : '/login/lostpassword/ajax' + params,
           timeout : 60000
           }).success(function(data, status, headers, config) {
               $scope.progressbar_lost_password_show = false;
               $scope.error_lost_password_txt = data;
               $scope.error_lost_password_show = true;
               $scope.bLostPasswordButtonEnabled = false;
           }).error(function(data, status, headers, config) {
               $scope.progressbar_lost_password_show = false;
               $scope.error_lost_password_txt = data;
               $scope.error_lost_password_show = true;
               $scope.bLostPasswordButtonEnabled = true;
         });

    }; //end of lostPassword()

}

