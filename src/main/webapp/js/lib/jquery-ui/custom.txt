jquery-ui-1.10.4.custom

UI Core:
CoreThe core of jQuery UI, required for all interactions and widgets.
WidgetProvides a factory for creating stateful widgets with a common API.
MouseAbstracts mouse-based interactions to assist in creating certain widgets.
PositionPositions elements relative to other elements.

Interactions:
DraggableEnables dragging functionality for any element.
DroppableEnables drop targets for draggable elements.
ResizableEnables resize functionality for any element.
SelectableAllows groups of elements to be selected with the mouse.
SortableEnables items in a list to be sorted using the mouse.

Widgets:
AutocompleteLists suggested words as the user is typing.
DatepickerDisplays a calendar from an input or inline for selecting dates.
MenuCreates nestable menus.
