<%@ page import="java.util.*, com.steplify.utility.*, com.steplify.dto.*, com.steplify.mpb.*" %>
<%
    AdminUserViewMPB mpbAdminUserView = (AdminUserViewMPB)request.getAttribute("mpbAdminUserView");
    List<CodeDecodeDTO> alCodesMembershipRole = mpbAdminUserView.alCodesMembershipRole;
    List<CodeDecodeDTO> alCodesMembershipStatus = mpbAdminUserView.alCodesMembershipStatus;
    List<ProjectMemberDTO> lstMemberships = mpbAdminUserView.lstMemberships;
    boolean bUserHasProjectAdmin = mpbAdminUserView.bUserHasProjectAdmin;
    Map<Integer, UserPrincipalDTO> mapUsers = mpbAdminUserView.mapUsers;
    int iProjectID = mpbAdminUserView.iProjectID;
    TransResponse objTransResponse = (TransResponse)request.getAttribute("objTransResponse");
    UserPrincipalDTO dtoLoggedInUser = (UserPrincipalDTO)request.getAttribute("dtoLoggedInUser");
%>
<!doctype html>
<html>
  
  <head>
    <title>STEPLIFY</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">

    <link rel="stylesheet" href="/css/bootstrap_2.3.2/css/themes/united/bootstrap.min.css">
    <link rel="stylesheet" href="/css/bootstrap_2.3.2/css/bootstrap-responsive.min.css">
    <link rel="stylesheet" href="/css/app/main.css?v=8">
    <link rel="stylesheet" href="/css/divshot/divshot-util.css">
    <link rel="stylesheet" href="/css/divshot/divshot-canvas.css">

    <!--Add JS libraries in this order, JQ, ANG, Boot, App-->
    <script src="/js/lib/jquery/jquery.min.js"></script>
    <script src="/js/lib/angular/angular.min.js"></script>
    <script src="/css/bootstrap_2.3.2/js/bootstrap.min.js"></script>

    <!--Add jQuery UI-->
    <script src="/js/lib/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>
    <link rel="stylesheet" href="/js/lib/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.min.css">
    <script src="/js/app/admin_user_ng.js?v=3"></script>

    <!--Add Google Analytics-->
    <script><%= Config.getGoogleAnalyticsScript() %></script>
  </head>
  
<body>
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="/dashboard">| STEPLIFY | <span class="small_font">Beta</span></a>
          <div class="navbar-content">
            <a class="brand" href="/task_manager/view?pid=<%=iProjectID%>">&gt; Task Manager</a>
            <span class="brand">&gt; User Administration</span>
          </div>
        </div>
      </div>
    </div> <!--/navbar-->

    <div class="top_spacer container-fluid offset1 span11" ng-app ng-controller="AdminUserCtrl">
      <div class="row-fluid">
          <div class="span9">
            <h2>Add a new user</h2>

            <div class="row-fluid">
                <form id="form_add_membership" class="form-horizontal" method="post" action="/admin_user/membership">
                    <div class="span12">
                        <input type="text" name="_s_username" placeholder="Add new user/email" id="_s_email">
                        <select type="text" name="_s_cd_role" class="extra_wide"><%= UI.dropDownOptions(alCodesMembershipRole, null) %></select>
                        <a class="btn btn-success" href="#" ng-click="postAddMembership()">Invite</a>
                        <a class="btn pull-right" href="/admin_topic/members?pid=<%=iProjectID%>">Next <i class="icon-chevron-right"></i></a>
                    </div>
                    <input type="hidden" name="_id_project" value="<%=iProjectID%>">
                </form>
            </div>

          </div>
          <div class="span3">
            <% for(String sEx : objTransResponse.getExceptions()) { %>
            <p class="text-info bold"><%= UI.printAndEscapeForHTML(sEx) %></p>
            <% } %>
            <% for(String sWarn : objTransResponse.getWarnings()) { %>
            <p class="text-info"><%= UI.printAndEscapeForHTML(sWarn) %></p>
            <% } %>
          </div>
      </div> <!--/span12row-->

      <hr>

        <div class="row-fluid row_header">
            <strong>
                <div class="span3">Email</div>
                <div class="span2">Status <span class="bold_off"><a href="#" class="column_tip" data-original-title="REMOVED users will not be able to access the project in any way"><i class="icon-info-sign"></i></a></span></div>
                <div class="span2">Role</div>
                <div class="span1">Admin <span class="bold_off"><a href="#" class="column_tip" data-original-title="ADMIN users get full access, and can invite other users or add new topics"><i class="icon-info-sign"></i></a></span></div>
                <div class="span1">Action</div>
                <div class="span3">Created / Referral</div>
            </strong>
        </div>

        <% for (ProjectMemberDTO dtoMembership : lstMemberships) { %>
        <div class="row-fluid row_pill">
            <form id="form_update_membership_<%=dtoMembership.get_id()%>" class="form-horizontal" method="post" action="/admin_user/membership">
                <div class="span3">
                  <% if (dtoMembership.isStatusActive()) { %>
                    <span class=back-green>&nbsp;</span>
                  <% } else if (dtoMembership.isStatusRemoved()) { %>
                    <span class=back-red>&nbsp;</span>
                  <% } else { %>
                    <span class=back-yellow>&nbsp;</span>
                  <% } %>
                  <%= UI.printAndEscapeForHTML(dtoMembership.getUserPrincipal().get_s_username()) %>
                </div>
                <div class="span2"><select type="text" name="_s_cd_status"><%= UI.dropDownOptions(alCodesMembershipStatus, dtoMembership.get_s_cd_status()) %></select></div>
                <div class="span2"><select type="text" name="_s_cd_role" class="wide"><%= UI.dropDownOptions(alCodesMembershipRole, dtoMembership.get_s_cd_role()) %></select></div>
                <div class="span1"><input type="checkbox" name="_b_is_project_admin" value="true" <%= dtoMembership.get_b_is_project_admin() ? "checked" : "unchecked"%>></div>
                <div class="span1"><a class="btn btn-success" href="#" ng-click="postUpdateMembership(<%=dtoMembership.get_id()%>)">Save</a></div>
                <div class="span3">
                    <%= UI.datetimeString(dtoMembership.get_dt_create_date()) %>
                    <br>
                    <%= UI.printAndEscapeForHTML(mapUsers.get(dtoMembership.get_id_created_by()).prettyPrint()) %>
                </div>
                <input type="hidden" name="_id" value="<%=dtoMembership.get_id()%>">
            </form>
        </div>
        <% } //end of for-loop%>

      <hr>
      <footer>
        <span class="pull-left"><%=Config.FOOTER_LEFT%></span>
        <span class="pull-right"><%=Config.FOOTER_RIGHT%></span>
      </footer>
    </div> <!--/.fluid-container-->

</body>

</html>