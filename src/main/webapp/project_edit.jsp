<%@ page import="java.util.*, com.steplify.utility.*, com.steplify.dto.*" %>
<%
    List<CodeDecodeDTO> alCodesProjectType = (List)request.getAttribute("alCodesProjectType");
    TransResponse objTransResponse = (TransResponse)request.getAttribute("objTransResponse");
    ProjectDTO dtoProject = (ProjectDTO)request.getAttribute("dtoProject");
    boolean bUserHasProjectAdmin = (Boolean)request.getAttribute("bUserHasProjectAdmin");
%>
<!doctype html>
<html>
  
  <head>
    <title>STEPLIFY</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">

    <link rel="stylesheet" href="/css/bootstrap_2.3.2/css/themes/united/bootstrap.min.css">
    <link rel="stylesheet" href="/css/bootstrap_2.3.2/css/bootstrap-responsive.min.css">
    <link rel="stylesheet" href="/css/app/main.css?v=8">
    <link rel="stylesheet" href="/css/divshot/divshot-util.css">
    <link rel="stylesheet" href="/css/divshot/divshot-canvas.css">

    <!--Add Google Maps-->
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=false"></script>

    <!--Add JS libraries in this order, JQ, ANG, Boot, App-->
    <script src="/js/lib/jquery/jquery.min.js"></script>
    <script src="/js/lib/angular/angular.min.js"></script>
    <script src="/css/bootstrap_2.3.2/js/bootstrap.min.js"></script>
    <script src="/js/app/project_edit_ng.js?v=4"></script>

    <!--Add Google Analytics-->
    <script><%= Config.getGoogleAnalyticsScript() %></script>
  </head>
  
<body>
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="/dashboard">| STEPLIFY | <span class="small_font">Beta</span></a>
        </div>
      </div>
    </div> <!--/navbar-->

    <div class="top_spacer container-fluid offset1 span10" ng-app ng-controller="ProjectAddCtrl">
      <div class="row-fluid">
          <div class="span9">
            <% if (dtoProject.get_id() == 0) { %>
            <h2>Add Property</h2>
            Once you add a property, you will be able to add people, topics, and tasks.<br>
            (If you are a buyer who does not know your address yet, use your current address, and change it when ready.)
            <% } else  { %>
            <h2>Edit Property</h2>
            Click the save button when complete.<br>
            <% } %>
          </div>
          <div class="span3">
            <% for(String sEx : objTransResponse.getExceptions()) { %>
            <p class="text-info bold"><%= UI.printAndEscapeForHTML(sEx) %></p>
            <% } %>
            <% for(String sWarn : objTransResponse.getWarnings()) { %>
            <p class="text-info"><%= UI.printAndEscapeForHTML(sWarn) %></p>
            <% } %>
          </div>
      </div> <!--/span12row-->

      <hr>

        <div class="well row-fluid">
          <form id="form_add_project" class="form-horizontal" method="post" action="/project/edit">
              <div class="control-group">
                <label class="control-label">Address</label>
                <div class="controls">
                  <input type="text" id="maps_address" class="input-block-level span4" name="_s_address" ng-model="_s_address" ng-init="_s_address='<%=UI.printAndEscape(dtoProject.get_s_address())%>'">
                  <span class="help-inline">Enter the property street address</span>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">City</label>
                <div class="controls">
                  <input type="text" id="maps_city" class="input-block-level span4" name="_s_city" ng-model="_s_city" ng-init="_s_city='<%=UI.printAndEscape(dtoProject.get_s_city())%>'">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">State</label>
                <div class="controls">
                  <input type="text" id="maps_state" class="input-block-level span4" name="_s_state" ng-model="_s_state" ng-init="_s_state='<%=UI.printAndEscape(dtoProject.get_s_state())%>'">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Postal Code</label>
                <div class="controls">
                  <input type="text" id="maps_zip" class="input-block-level span4" name="_s_postal_code" ng-model="_s_postal_code" ng-init="_s_postal_code='<%=UI.printAndEscape(dtoProject.get_s_postal_code())%>'">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Country</label>
                <div class="controls">
                  <input type="text" id="maps_country" class="input-block-level span4" name="_s_country" ng-model="_s_country" ng-init="_s_country='<%=UI.printAndEscape(dtoProject.get_s_country())%>'">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Project Type</label>
                <div class="controls">
                  <select type="text" id="project_type" class="span4" name="_s_cd_project_type" ng-model="_s_cd_project_type" ng-init="_s_cd_project_type='<%=UI.getCodeDecodeValue(alCodesProjectType, dtoProject.get_s_cd_project_type())%>'">
                    <%= UI.dropDownOptions(alCodesProjectType, null) %>
                  </select>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Client Name</label>
                <div class="controls">
                  <input type="text" class="input-block-level span4" name="_s_description" ng-model="_s_description" ng-init="_s_description='<%=UI.printAndEscape(dtoProject.get_s_description())%>'">
                  <span class="help-inline">e.g. Jack and Jill Smith</span>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Website Listing (URL)</label>
                <div class="controls">
                  <input type="text" class="input-block-level span4" name="_s_url_website" ng-model="_s_url_website" ng-init="_s_url_website='<%=UI.printAndEscape(dtoProject.get_s_url_website())%>'">
                  <span class="help-inline">http://www.zillow.com/palo-alto-ca</span>
                  <span class="help-inline alert alert-danger" ng-show="showWebsiteHint()">URL should start with http://</span>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Picture/Image (URL)</label>
                <div class="controls">
                  <input type="text" class="input-block-level span4" name="_s_url_image" ng-model="_s_url_image" ng-init="_s_url_image='<%=UI.printAndEscape(dtoProject.get_s_url_image())%>'">
                  <img style="max-width:20%" ng-show="attemptToShowImage()" alt="Not Found" ng-src='{{_s_url_image}}'>
                  <span class="help-inline" ng-show="!attemptToShowImage()">http://trulia.com/images/home.jpg</span>
                  <span class="help-inline alert alert-danger" ng-show="showImageHint()">URL should start with http://</span>
                </div>
              </div>
              <div class="control-group">
                <div class="controls">
                  <input type="hidden" name="_id" value="<%=dtoProject.get_id()%>">
                  <a class="btn btn-inverse" href="/dashboard"><i class="icon-chevron-left icon-white"></i> Cancel</a>
                  <% if (dtoProject.get_id() == 0) { %>
                    <a class="btn btn-success" href="#" ng-click="postAddProject()">Next <i class="icon-chevron-right icon-white"></i></a>
                  <% } else  { %>
                    <a class="btn btn-success" href="#" ng-click="postAddProject()"><i class="icon-plus-sign icon-white"></i> Save</a>
                  <% } %>
                </div>
              </div>
            </form>

        </div>

      <hr>
      <footer>
        <span class="pull-left"><%=Config.FOOTER_LEFT%></span>
        <span class="pull-right"><%=Config.FOOTER_RIGHT%></span>
      </footer>
    </div> <!--/.fluid-container-->

</body>

</html>