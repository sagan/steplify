<%@ page import="java.util.*, com.steplify.utility.*, com.steplify.dto.*, com.steplify.mpb.*" %>
<%
    AdminTopicViewMPB mpbAdminTopicView = (AdminTopicViewMPB)request.getAttribute("mpbAdminTopicView");
    List<CodeDecodeDTO> alCodesTopicAuthority = mpbAdminTopicView.alCodesTopicAuthority;
    List<TopicDTO> lstTopics = mpbAdminTopicView.lstTopics;
    int iProjectID = mpbAdminTopicView.iProjectID;
    TransResponse objTransResponse = (TransResponse)request.getAttribute("objTransResponse");
    UserPrincipalDTO dtoLoggedInUser = (UserPrincipalDTO)request.getAttribute("dtoLoggedInUser");
%>
<!doctype html>
<html>
  
  <head>
    <title>STEPLIFY</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">

    <link rel="stylesheet" href="/css/bootstrap_2.3.2/css/themes/united/bootstrap.min.css">
    <link rel="stylesheet" href="/css/bootstrap_2.3.2/css/bootstrap-responsive.min.css">
    <link rel="stylesheet" href="/css/app/main.css?v=8">
    <link rel="stylesheet" href="/css/divshot/divshot-util.css">
    <link rel="stylesheet" href="/css/divshot/divshot-canvas.css">

    <!--Add JS libraries in this order, JQ, ANG, Boot, App-->
    <script src="/js/lib/jquery/jquery.min.js"></script>
    <script src="/js/lib/angular/angular.min.js"></script>
    <script src="/css/bootstrap_2.3.2/js/bootstrap.min.js"></script>
    <script src="/js/app/admin_topic_ng.js?v=5"></script>

    <!--Add Google Analytics-->
    <script><%= Config.getGoogleAnalyticsScript() %></script>
  </head>
  
<body ng-app ng-controller="AdminTopicCtrl">
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="/dashboard">| STEPLIFY | <span class="small_font">Beta</span></a>
          <div class="navbar-content">
            <a class="brand" href="/task_manager/view?pid=<%=iProjectID%>">&gt; Task Manager</a>
            <span class="brand">&gt; Topic Administration</span>
          </div>
        </div>
      </div>
    </div> <!--/navbar-->

    <div class="top_spacer container-fluid offset1 span11">
      <div class="row-fluid">
          <div class="span9">
            <h2>Add a new topic</h2>

            <div class="row-fluid">
                <form id="form_add_topic" class="form-horizontal" method="post" action="/admin_topic/update_topic">
                    <div class="span12">
                        <input type="text" name="_s_name" placeholder="Add new topic">
                        <a class="btn btn-success" href="#" ng-click="postAddTopic()">Add</a>
                        <a class="btn pull-right" href="/admin_topic/members?pid=<%=iProjectID%>">Next <i class="icon-chevron-right"></i></a>
                    </div>
                    <input type="hidden" name="_id_project" value="<%=iProjectID%>">
                </form>
            </div>

          </div>
          <div class="span3">
            <% for(String sEx : objTransResponse.getExceptions()) { %>
            <p class="text-info bold"><%= UI.printAndEscapeForHTML(sEx) %></p>
            <% } %>
            <% for(String sWarn : objTransResponse.getWarnings()) { %>
            <p class="text-info"><%= UI.printAndEscapeForHTML(sWarn) %></p>
            <% } %>
          </div>
      </div> <!--/span12row-->

      <hr>

        <div>
            <div class="row-fluid row_header">
                <strong>
                    <div class="span3">Topic Name</div>
                    <div class="span3">Attachments URL <span class="bold_off"><a href="#" class="column_tip" data-original-title="Optional: URL for online document storage, per topic (i.e. Dropbox, Google Drive, etc)"><i class="icon-info-sign"></i></a></span></div>
                    <div class="span2">&nbsp;</div>
                    <div class="span2">Order# <span class="bold_off"><a href="#" class="column_tip" data-original-title="Customize the order of topics listed on the Task Manager."><i class="icon-info-sign"></i></a></span></div>
                    <div class="span2">Action <span class="bold_off"><a href="#" class="column_tip" data-original-title="To delete a topic, go to Advanced Permissions"><i class="icon-info-sign"></i></a></span></div>
                </strong>
            </div>

            <% for (TopicDTO dtoTopic : lstTopics) { %>
            <div class="row-fluid row_pill">
                <form id="form_update_topic_<%=dtoTopic.get_id()%>" class="form-horizontal" method="post" action="/admin_topic/update_topic">
                    <div class="span3"><input type="text" name="_s_name" value="<%=UI.printAndEscapeForHTML(dtoTopic.get_s_name())%>"></div>
                    <div class="span5"><input type="text" class="input span12" name="_s_url_attachments" value="<%=UI.printAndEscapeForHTML(dtoTopic.get_s_url_attachments())%>" placeholder="http://..."></div>
                    <div class="span2"><input type="text" name="_s_order" style="width:20px" value="<%=dtoTopic.get_i_order()%>"></div>
                    <div class="span2"><a class="btn btn-success" href="#" ng-click="postUpdateTopic(<%=dtoTopic.get_id()%>)">Save</a></div>
                    <input type="hidden" name="_id" value="<%=dtoTopic.get_id()%>">
                </form>

            </div>
            <% } //end of TOPICS for-loop%>
        </div>

      <hr>
      <footer>
        <span class="pull-left"><%=Config.FOOTER_LEFT%></span>
        <span class="pull-right"><%=Config.FOOTER_RIGHT%></span>
      </footer>
    </div> <!--/.fluid-container-->

</body>

</html>