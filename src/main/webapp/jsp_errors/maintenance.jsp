<%@ page import="java.util.*, com.steplify.utility.*, com.steplify.dto.*" %>
<%
   //Get your DTO objects here
%>
<!doctype html>
<html>

 <head>
   <title>STEPLIFY</title>
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">

   <link rel="stylesheet" href="/css/bootstrap_2.3.2/css/themes/united/bootstrap.min.css">
   <link rel="stylesheet" href="/css/bootstrap_2.3.2/css/bootstrap-responsive.min.css">
   <link rel="stylesheet" href="/css/app/main.css?v=8">
   <link rel="stylesheet" href="/css/divshot/divshot-util.css">
   <link rel="stylesheet" href="/css/divshot/divshot-canvas.css">

   <!--Add JS libraries in this order, JQ, ANG, Boot, App-->
   <script src="/js/lib/jquery/jquery.min.js"></script>
   <script src="/js/lib/angular/angular.min.js"></script>
   <script src="/css/bootstrap_2.3.2/js/bootstrap.min.js"></script>

   <!--Add Google Analytics-->
   <script><%= Config.getGoogleAnalyticsScript() %></script>
 </head>

<body>
   <div class="navbar navbar-fixed-top">
     <div class="navbar-inner">
       <div class="container">
         <a class="brand" href="/">| STEPLIFY |</a>
         <div class="navbar-content">
           <ul class="nav">
             <li class="active">
               <a href="/dashboard">Dashboard</a>
             </li>
           </ul>
         </div>
       </div>
     </div>
   </div> <!--/navbar-->

   <div class="top_spacer container-fluid offset1 span10">
     <div class="row-fluid">
         <div class="span8">
           <h2>Site Under Maintenance</h2>
           We plan to be back in a few minutes...
         </div>
         <div class="span4">
         </div>
     </div> <!--/span12row-->

     <hr>

       <div>
         <img src="/images/maintenance.jpg">
       </div>

     <hr>
     <footer>
       <span class="pull-left"><%=Config.FOOTER_LEFT%></span>
       <span class="pull-right"><%=Config.FOOTER_RIGHT%></span>
     </footer>
   </div> <!--/.fluid-container-->

</body>

</html>