<%@ page import="java.util.*, com.steplify.utility.*, com.steplify.dto.*, com.steplify.mpb.*" %>
<%
    TransResponse objTransResponse = (TransResponse)request.getAttribute("objTransResponse");
    UserPrincipalDTO dtoLoggedInUser = (UserPrincipalDTO)request.getAttribute("dtoLoggedInUser");
    CompanyDTO dtoCompany = (CompanyDTO)request.getAttribute("dtoCompany");
    List<UserPrincipalDTO> lstUserPrincipal = (List<UserPrincipalDTO>)request.getAttribute("lstUserPrincipal");
%>
<!doctype html>
<html>
  
  <head>
    <title>STEPLIFY</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">

    <link rel="stylesheet" href="/css/bootstrap_2.3.2/css/themes/united/bootstrap.min.css">
    <link rel="stylesheet" href="/css/bootstrap_2.3.2/css/bootstrap-responsive.min.css">
    <link rel="stylesheet" href="/css/app/main.css?v=8">
    <link rel="stylesheet" href="/css/divshot/divshot-util.css">
    <link rel="stylesheet" href="/css/divshot/divshot-canvas.css">

    <!--Add JS libraries in this order, JQ, ANG, Boot, App-->
    <script src="/js/lib/jquery/jquery.min.js"></script>
    <script src="/js/lib/angular/angular.min.js"></script>
    <script src="/css/bootstrap_2.3.2/js/bootstrap.min.js"></script>
    <script src="/js/app/company_users_ng.js?v=2"></script>

    <!--Add Google Analytics-->
    <script><%= Config.getGoogleAnalyticsScript() %></script>
  </head>
  
<body>
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="/dashboard">| STEPLIFY | <span class="small_font">Beta</span></a>
          <div class="navbar-content">
            <span class="brand">&gt; Company User Administration</span>
          </div>
        </div>
      </div>
    </div> <!--/navbar-->

    <div class="top_spacer container-fluid offset1 span11" ng-app ng-controller="CompanyUsersCtrl">
      <div class="row-fluid">
          <div class="span9">
            <h2><%=dtoCompany.get_s_name()%></h2>
            <p>Manage the user accounts for your company.</p>
          </div>
          <div class="span3">
            <p><a class="btn" href="/company"><i class="icon-chevron-left"></i> Company</a></p>
            <% for(String sEx : objTransResponse.getExceptions()) { %>
            <p class="text-info bold"><%= UI.printAndEscapeForHTML(sEx) %></p>
            <% } %>
            <% for(String sWarn : objTransResponse.getWarnings()) { %>
            <p class="text-info"><%= UI.printAndEscapeForHTML(sWarn) %></p>
            <% } %>
          </div>
      </div> <!--/span12row-->

      <hr>

        <div class="row-fluid row_header">
            <strong>
                <div class="span3">Username</div>
                <div class="span1">ID</div>
                <div class="span2">Status</div>
                <div class="span2">Created</div>
            </strong>
        </div>

        <% for (UserPrincipalDTO dtoUserPrincipal : lstUserPrincipal) { %>
        <div class="row-fluid row_pill">
            <div class="span3"><%= UI.printAndEscapeForHTML(dtoUserPrincipal.get_s_username()) %></div>
            <div class="span1"><%= dtoUserPrincipal.get_id() %></div>
            <div class="span2"><%= UI.printAndEscapeForHTML(dtoUserPrincipal.get_s_cd_status()) %></div>
            <div class="span2"><%= UI.datetimeString(dtoUserPrincipal.get_dt_create_date()) %></div>
        </div>
        <% } //end of for-loop%>

      <hr>
      <footer>
        <span class="pull-left"><%=Config.FOOTER_LEFT%></span>
        <span class="pull-right"><%=Config.FOOTER_RIGHT%></span>
      </footer>
    </div> <!--/.fluid-container-->

</body>

</html>