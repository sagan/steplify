<%@ page import="java.util.*, com.steplify.utility.*, com.steplify.dto.*, com.steplify.mpb.*" %>
<%
    TaskManagerViewMPB mpbTaskManagerView = (TaskManagerViewMPB)request.getAttribute("mpbTaskManagerView");
    List<TopicDTO> lstTopics = mpbTaskManagerView.lstTopics;
    Map<Integer, List<TaskDTO>> mapTopicTasks = mpbTaskManagerView.mapTopicTasks;
    ProjectDTO dtoProject = mpbTaskManagerView.dtoProject;
    TransResponse objTransResponse = (TransResponse)request.getAttribute("objTransResponse");
    UserPrincipalDTO dtoLoggedInUser = (UserPrincipalDTO)request.getAttribute("dtoLoggedInUser");

    List<CodeDecodeDTO> alCodesTaskStatus = mpbTaskManagerView.alCodesTaskStatus;
    Map<String, String> mapCDsTopicAuthority = mpbTaskManagerView.mapCDsTopicAuthority;
    CodeDecodeDTO cdProjectType = CodeDecode.getByCode(CodeDecode._CAT_PROJECT_TYPE, dtoProject.get_s_cd_project_type());

    //Build some convenience collections
    Map<Integer, ProjectMemberDTO> mapProjectMembers = new HashMap<Integer, ProjectMemberDTO>();
    List<String> alProjectMemberUsername = new ArrayList();
    for (ProjectMemberDTO dtoMember : dtoProject.getMembers())
    {
        //Build a map of all ProjectMembers by UserID, each topic will access this to see if a user was REMOVED from the Project
        mapProjectMembers.put(dtoMember.get_id_user_principal(), dtoMember);
        //Build a list of all ProjectMembers by Username, for the dropdown list on the TopicEditor
        alProjectMemberUsername.add(dtoMember.getUserPrincipal().get_s_username());
    }

    //Build a map of all TaskStatus by Code=Decode, each task of every topic needs to do a lookup
    Map<String, String> mapTaskStatus = new HashMap<String, String>();
    for (CodeDecodeDTO cdDTO : alCodesTaskStatus)
    {
        mapTaskStatus.put(cdDTO.get_s_code(), cdDTO.get_s_decode());
    }

    //Hold on to the current users membership
    ProjectMemberDTO dtoMemberLoggedIn = mapProjectMembers.get(dtoLoggedInUser.get_id());
    boolean bUserHasProjectAdmin = dtoMemberLoggedIn.get_b_is_project_admin();
%>
<!doctype html>
<html>
  
  <head>
    <title>STEPLIFY</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">

    <link rel="stylesheet" href="/css/bootstrap_2.3.2/css/themes/united/bootstrap.min.css">
    <link rel="stylesheet" href="/css/bootstrap_2.3.2/css/bootstrap-responsive.min.css">
    <link rel="stylesheet" href="/css/app/main.css?v=8">
    <link rel="stylesheet" href="/css/divshot/divshot-util.css">
    <link rel="stylesheet" href="/css/divshot/divshot-canvas.css">

    <!--Add JS libraries in this order, JQ, ANG, Boot, App-->
    <script src="/js/lib/jquery/jquery.min.js"></script>
    <script src="/js/lib/angular/angular.min.js"></script>
    <script src="/css/bootstrap_2.3.2/js/bootstrap.min.js"></script>

    <!--Add JS libraries for HTML5 Modernizer and PolyFills-->
    <script src="/js/lib/modernizr/modernizr.js"></script>
    <script src="/js/app/task_manager_ng.js?v=8"></script>

    <!--Add Google Analytics-->
    <script><%= Config.getGoogleAnalyticsScript() %></script>
  </head>
  
<body class="body_contrast" ng-app ng-controller="TaskManagerCtrl">
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="/dashboard">| STEPLIFY | <span class="small_font">Beta</span></a>
          <div class="navbar-content">
            <a class="brand" href="/dashboard">&gt;</a>
             <span class="brand">Task Manager</span>
          </div>
        </div>
      </div>
    </div> <!--/navbar-->

    <div class="top_spacer container-fluid offset1 span11">
      <div class="row-fluid">
          <div class="span9">
            <h2><%= dtoProject.get_s_address() %>
                <a class="btn btn-mini" href="#" id="accordion_all_topics"><i class="icon-resize-full"></i></a>
            </h2>
            <div class="btn-group">
              <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown">
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                <p><a href="<%= UI.printAndEscapeForHTML(dtoProject.getMapURL()) %>" target="_blank"><i class="icon-map-marker"></i> Map / Directions</a></p>
                <% if (!UI.isNullOrEmpty(dtoProject.get_s_url_website())) { %>
                    <a href="<%= UI.printAndEscapeForHTML(dtoProject.get_s_url_website()) %>" target="_blank"><i class="icon-globe"></i> Website</a>
                <% } %>
              </ul>
            </div>
            <span class="bold">Client:</span> <%= UI.printAndEscapeForHTML(dtoProject.get_s_description()) %><br>
          </div>

          <div class="span3">
            <% if (bUserHasProjectAdmin) { %>
                <div class="btn-group">
                  <button class="btn dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-cog"></i> Users / Topics <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu">
                    <li><a href="/admin_user/view?pid=<%=dtoProject.get_id()%>">Manage Users</a></li>
                    <li><a href="/admin_topic/view?pid=<%=dtoProject.get_id()%>">Manage Topics</a></li>
                  </ul>
                </div>
                <p></p>
            <% } %>

            <% if (mpbTaskManagerView.iFilterUserID == 0) {%>
                <a class="btn" href="/task_manager/view?pid=<%=dtoProject.get_id()%>&filter_user_id=<%=dtoLoggedInUser.get_id()%>"><i class="icon-filter"></i>Show MY tasks</a>
            <% } else { %>
                <a class="btn" href="/task_manager/view?pid=<%=dtoProject.get_id()%>"><i class="icon-filter"></i>Show ALL tasks</a>
            <% } %>


            <% for(String sEx : objTransResponse.getExceptions()) { %>
            <p class="text-info bold"><%= UI.printAndEscapeForHTML(sEx) %></p>
            <% } %>
            <% for(String sWarn : objTransResponse.getWarnings()) { %>
            <p class="text-info"><%= UI.printAndEscapeForHTML(sWarn) %></p>
            <% } %>
          </div>
      </div> <!--/span12row-->

      <hr>
        <% if (lstTopics.isEmpty()) {
            if (bUserHasProjectAdmin) {
        %>
            <p>You have no tasks, use the <a class="btn btn-small" href="/admin_topic/wizard?pid=<%=dtoProject.get_id()%>">Setup Wizard</a> to get started.<p>
        <% } else { %>
            <p>You are not a member of any Topics, please contact your project administrator for access.</p>
        <% } } //end of both ifs %>

        <% for (TopicDTO dtoTopic : lstTopics) { %>
          <h4 id="<%=dtoTopic.get_id()%>" class="accordion"><i class="icon-resize-full"></i> Topic: <%=UI.printAndEscapeForHTML(dtoTopic.get_s_name())%></h4>

          <div class="topic_pill topic" id="topic_<%=dtoTopic.get_id()%>">

            <div class="row-fluid border_bottom">
              <div class="span6">

                <button id="task_new_for_topic_<%=dtoTopic.get_id()%>" class="btn btn-success btn-small task_new" ng-click="initTaskVars(<%=dtoTopic.get_id()%>, null)">
                  <span class="icon-plus-sign icon-white"></span> New Task
                </button>

                <br>
                <% if (!UI.isNullOrEmpty(dtoTopic.get_s_url_attachments())) { %>
                    Attachments: <a href="<%= UI.printAndEscapeForHTML(dtoTopic.get_s_url_attachments()) %>" target="_blank">Open <i class="icon-folder-open"></i></a>
               <% } %>
              </div>
              <div class="span6">
                  <p><span class="accordion_topic_members" id="topic_members_<%=dtoTopic.get_id()%>">Permissions <i class="icon-resize-full"></i></span><p>
                  <div class="accordion_topic_members_list" id="topic_members_<%=dtoTopic.get_id()%>_list">
                    <ul class="nav nav-list">
                    <% for (TopicMemberDTO dtoTM : dtoTopic.getTopicMembers()) {
                        ProjectMemberDTO dtoPM = mapProjectMembers.get(dtoTM.get_id_user_principal());

                        //If the user has been removed from the project, override their auth-status
                        String sMemberAuthCode = null;
                        if (!dtoPM.isStatusRemoved())
                        {
                            sMemberAuthCode = mapCDsTopicAuthority.get(dtoTM.get_s_cd_authority());
                        }
                        else
                        {
                             sMemberAuthCode = "Removed from Project";
                        }
                    %>
                      <li>
                      <% if (bUserHasProjectAdmin) { %>
                        <a href="/admin_topic/members?pid=<%=dtoProject.get_id()%>">
                            <%= UI.printAndEscapeForHTML(dtoTM.getUserPrincipal().get_s_username()) %>
                            <span class="badge pull-right"><%= sMemberAuthCode %></span>
                        </a>
                      <% } else { %>
                        <p>
                            <%= UI.printAndEscapeForHTML(dtoTM.getUserPrincipal().get_s_username()) %>
                            <span class="badge pull-right"><%= sMemberAuthCode %></span>
                        </p>
                      <% } //end of bUserHasProjectAdmin %>
                      </li>
                    <% } //end of MEMBER for-loop %>
                    </ul>
                  </div>
              </div>
            </div> <!--/span12row-->

            <div class="topic_content">
              <div class="row-fluid hidden-phone" id="topic_content_header<%=dtoTopic.get_id()%>">
                <strong>
                  <div class="span5">
                    <p>Task</p>
                  </div>
                  <div class="span3">
                    <p>Assigned To</p>
                  </div>
                  <div class="span2">
                    <p>Status</p>
                  </div>
                  <div class="span2">
                    <p>Due Date</p>
                  </div>
                </strong>
              </div> <!--/span12row header-->

              <% for (TaskDTO dtoTask : mapTopicTasks.get(dtoTopic.get_id())) {
                //If there is a user filter, then only show tasks for that user
                if (mpbTaskManagerView.iFilterUserID != 0 && mpbTaskManagerView.iFilterUserID != dtoTask.get_id_assigned_user())
                {
                    continue;
                }
              %>
              <div class="row-fluid row_pill" id="task_<%=dtoTask.get_id()%>">
                <div class="span5">

                    <button id="task_edit_<%=dtoTask.get_id()%>" class="btn btn-mini task_edit" ng-click="initTaskVars(<%=dtoTopic.get_id()%>, <%=dtoTask.get_id()%>)">
                      <span class="caret"></span>
                    </button>

                    <strong><%= UI.printAndEscapeForHTML(dtoTask.get_s_name()) %></strong>
                </div>
                <div class="span3">
                  <%= UI.printAndEscapeForHTML(dtoTask.getUserPrincipalAssignedName()) %>
                </div>
                <div class="span2">
                    <%=mapTaskStatus.get(dtoTask.get_s_cd_status())%>
                </div>
                <div class="span2">
                    <% if (dtoTask.isFinished()) { %>
                        <span class=back-green><i class="icon-ok"></i></span>
                    <% } else if (dtoTask.isOverdue() || dtoTask.isStatusStuck() || dtoTask.isStatusReopened()) { %>
                        <span class=back-red><i class="icon-flag icon-white"></i></span>
                    <% } else if (dtoTask.isStatusNew()) { %>
                        <span class=back-grey><i class="icon-stop"></i></span>
                    <% } else if (dtoTask.isStatusAcknowledged() || dtoTask.isStatusPaused()) { %>
                        <span class=back-grey><i class="icon-pause"></i></span>
                    <% } else { %>
                        <span class=back-grey><i class="icon-play"></i></span>
                    <% } %>
                    <%= UI.dateNoTzString(dtoTask.get_dt_due()) %>
                </div>

                <div class="row-fluid" id="task_middle_<%=dtoTask.get_id()%>">
                </div>

                <div class="task_comment">
                  <% for (TaskCommentDTO dtoComment : dtoTask.getTaskComments()) { %>
                  <div class="row-fluid">
                    <div class="offset1 span7">
                      <strong><%= UI.printAndEscapeForHTML(dtoComment.getUserPrincipalCreatedByName()) %></strong>
                      <%= UI.printAndEscapeForHTML(dtoComment.get_s_comment()) %>
                    </div>
                    <div class="span3">
                      <i><%=UI.datetimeString(dtoComment.get_dt_create_date())%></i>
                    </div>
                  </div> <!--/comment_11row-->
                  <% } //end of COMMENT for-loop%>
                </div> <!--/task_comment-->
              </div> <!--/task_12row-->
              <% } //end of TASK for-loop%>
            </div> <!--/topic_content-->
          </div> <!--/well-->

        <% } //end of TOPIC for-loop%>
      <hr>
      <footer>
        <span class="pull-left"><%=Config.FOOTER_LEFT%></span>
        <span class="pull-right"><%=Config.FOOTER_RIGHT%></span>
      </footer>
    </div> <!--/.fluid-container-->

    <!--Dynamic DIV that moves around for adding/editing Tasks-->
    <div class="row-fluid" id="task_editor">

        <div class="offset1 span5">
            <label class="control-label" for="_s_name">Name:</label>
            <input type="text" class="input span10" id="_s_name" ng-model="task._s_name" ng-disabled="disable_task_editor">

            <label class="control-label" for="_s_cd_status">Status:</label>
            <select type="text" class="span10" id="_s_cd_status" name="_s_cd_status" ng-model="task._s_cd_status" ng-disabled="disable_task_editor">
                <%= UI.dropDownOptions(alCodesTaskStatus, null) %>
            </select>

            <label class="control-label" for="_s_assigned_user">Assigned Member:</label>
            <select type="text" class="span10" id="_s_assigned_user" name="_s_assigned_user" ng-model="task._s_assigned_user" ng-disabled="disable_task_editor">
                <%= UI.dropDownOptionsWithStrings(alProjectMemberUsername) %>
            </select>

            <label class="control-label" for="_s_dt_due" id="lbl_dt_due">Due Date:</label>
            <input type="date" class="input" id="_s_dt_due" ng-model="task._s_dt_due" ng-disabled="disable_task_editor">
        </div>

        <div class="span6">
            <label class="control-label" for="_s_comment">Add Comment:</label>
            <textarea rows="8" class="input span10" id="_s_comment" ng-model="task._s_comment" ng-disabled="disable_task_editor"></textarea>
            <br>
            <button class="btn" id="btn_close_task_editor">Cancel</button>
            <button class="btn btn-primary" ng-show="!disable_task_editor" ng-click="updateTaskAndOrComment()">Save</button>
            <br><br>
            <div class="progress progress-striped active" ng-show="disable_task_editor">
                <div class="bar" style="width: 80%;"></div>
            </div>
            <br>
            <p class="text-error" id="taskUpdateStatus" ng-show="task_editor_status_txt != null">{{task_editor_status_txt}}</p>
        </div>

    </div>

</body>

</html>