<%@ page import="java.lang.*, com.steplify.utility.*" %>
<!doctype html>
<html>
  
  <head>
    <title>STEPLIFY</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">

    <link rel="stylesheet" href="/css/bootstrap_2.3.2/css/themes/united/bootstrap.min.css">
    <link rel="stylesheet" href="/css/bootstrap_2.3.2/css/bootstrap-responsive.min.css">
    <link rel="stylesheet" href="/css/app/main.css?v=8">
    <link rel="stylesheet" href="/css/divshot/divshot-util.css">
    <link rel="stylesheet" href="/css/divshot/divshot-canvas.css">
    <link rel="stylesheet" href="/css/font-awesome-4.0.3/css/font-awesome.min.css">

    <!--Add JS libraries in this order, JQ, ANG, Boot, App-->
    <script src="/js/lib/jquery/jquery.min.js"></script>
    <script src="/js/lib/angular/angular.min.js"></script>
    <script src="/css/bootstrap_2.3.2/js/bootstrap.min.js"></script>
    <script src="/js/app/login_ng.js?v=2"></script>

    <!--Add Google Analytics-->
    <script><%= Config.getGoogleAnalyticsScript() %></script>
  </head>
  
  <body>
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="/">| STEPLIFY | <span class="small_font">Beta</span></a>
          <div class="navbar-content">
            <ul class="nav">
              <li class="active">
                <a href="/"><i class="fa fa-home"></i> Home</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div class="container top_spacer">
      <div class="row main-features">
        <div class="span5 offset4">
          <h3 id="tabs">Sign In</h3>
          <ul class="nav nav-tabs">
            <li class="${requestScope.activate_login}">
              <a href="#TAB_LOGIN" data-toggle="tab">Login</a>
            </li>
            <li class="${requestScope.activate_register}">
              <a href="#TAB_REGISTER" data-toggle="tab">Register</a>
            </li>
            <li>
              <a href="#TAB_LOSTPASS" data-toggle="tab">Lost Password</a>
            </li>
          </ul>
          <div class="tabbable" ng-app>
            <div class="tab-content" ng-controller="LoginCtrl">
              <div class="tab-pane ${requestScope.activate_login}" id="TAB_LOGIN">
                <form id="form_login">
                  <div class="control-group info well">
                    <label class="control-label" for="txt_email">Email:</label>
                    <input type="text" class="input" id="txt_email" ng-model="email">
                    <label class="control-label" for="txt_password">Password:</label>
                    <input type="password" class="input password" id="txt_password" ng-model="password">
                    <label class="checkbox" for="chx_agree" style="">I agree to the following terms:
                          <input type="checkbox" value="false" id="chx_agree" name="chx_agree" ng-model="terms_agree">
                    </label>
                      <div id="terms" class="alert alert-block alert-info">
                          <p ng-hide="terms_agree">
                            Please read and accept our <a target="_blank" href="/login/terms">Terms of Use</a> before logging in.
                          </p>
                          <p ng-show="terms_agree">Thank you.</p>
                      </div>
                    <div class="progress progress-striped active" ng-show="progressbar_login_show">
                      <div class="bar" style="width: 100%;"></div>
                    </div>
                    <p class="text-error" id="loginError" ng-show="error_login_show">{{error_login_txt}}</p>
                    <p/>
                    <a class="btn" id="btn_login" ng-click="loginUser()" ng-show="terms_agree && bLoginButtonEnabled">Login</a>
                  </div>
                </form>
              </div> <!-- /TAB_LOGIN -->

              <div class="tab-pane ${requestScope.activate_register}" id="TAB_REGISTER">
                <form id="form_register">
                  <div class="control-group info well">
                    <label class="control-label" for="txt_email">Email:</label>
                    <input type="text" class="input" id="txt_email1" ng-model="email">
                    <label class="control-label" for="txt_postal_code">Postal/Zip Code:</label>
                    <input type="text" class="input" id="txt_postal_code" ng-model="postal_code">
                    <div class="progress progress-striped active" ng-show="progressbar_register_show">
                      <div class="bar" style="width: 100%;"></div>
                    </div>
                    <p class="text-error" id="registerError" ng-show="error_register_show">{{error_register_txt}}</p>
                    <p/>
                    <a class="btn" id="btn_register" ng-click="registerUser()" ng-show="bRegisterButtonEnabled">Register</a>
                  </div>
                </form>
              </div> <!-- /TAB_REGISTER -->

              <div class="tab-pane" id="TAB_LOSTPASS">
                <form id="form_lostpass">
                  <div class="control-group info well">
                    <label class="control-label" for="txt_email_lostpass">Email:</label>
                    <input type="text" class="input" id="txt_email_lostpass" ng-model="email">
                    <div class="progress progress-striped active" ng-show="progressbar_lost_password_show">
                      <div class="bar" style="width: 100%;"></div>
                    </div>
                    <p class="text-error" id="lostPassError" ng-show="error_lost_password_show">{{error_lost_password_txt}}</p>
                    <p/>
                    <a class="btn" id="btn_lostpass" ng-click="lostPassword()" ng-show="bLostPasswordButtonEnabled">Submit</a>
                  </div>
                </form>
               </div> <!-- /TAB_LOSTPASS -->

            </div> <!-- /tab-content -->
          </div> <!-- /tabbable -->
        </div> <!-- /span5 -->
      </div> <!-- /row -->
    </div> <!-- /container -->

  </body>

</html>