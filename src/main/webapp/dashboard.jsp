<%@ page import="java.util.*, com.steplify.utility.*, com.steplify.dto.*, com.steplify.mpb.*" %>
<%
    DashboardMPB mpbDashboard = (DashboardMPB)request.getAttribute("mpbDashboard");
    UserPrincipalDTO dtoLoggedInUser = (UserPrincipalDTO)request.getAttribute("dtoLoggedInUser");
    List<ProjectMemberDTO> alUserMemberships = mpbDashboard.alUserMemberships;
    Map<String, String> mapCDsProjectType = mpbDashboard.mapCDsProjectType;
    boolean bFetchArchived = (Boolean)request.getAttribute("bFetchArchived");
    Map<Integer, ProjectTaskCountsMPB> mapProjectTaskCounts = mpbDashboard.mapProjectTaskCounts;
    CompanyUserDTO dtoCompanyUser = mpbDashboard.dtoCompanyUser;
%>
<!doctype html>
<html>
  
  <head>
    <title>STEPLIFY</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">

    <link rel="stylesheet" href="/css/bootstrap_2.3.2/css/themes/united/bootstrap.min.css">
    <link rel="stylesheet" href="/css/bootstrap_2.3.2/css/bootstrap-responsive.min.css">
    <link rel="stylesheet" href="/css/app/main.css?v=8">
    <link rel="stylesheet" href="/css/divshot/divshot-util.css">
    <link rel="stylesheet" href="/css/divshot/divshot-canvas.css">
    <link rel="stylesheet" href="/css/font-awesome-4.0.3/css/font-awesome.min.css">

    <!--Add JS libraries in this order, JQ, ANG, Boot, App-->
    <script src="/js/lib/jquery/jquery.min.js"></script>
    <script src="/js/lib/angular/angular.min.js"></script>
    <script src="/css/bootstrap_2.3.2/js/bootstrap.min.js"></script>
    <script src="/js/app/dashboard_ng.js?v=5"></script>
    <script src="/js/app/dashboard_profile_ng.js?v=2"></script>

    <!--Add Google Analytics-->
    <script><%= Config.getGoogleAnalyticsScript() %></script>

    <!--Add App Specific from JSP to JS, keep this stuff to a minimum -->
    <script type="text/javascript">
        var gIntProjectCount = <%=mpbDashboard.alUserMemberships.size()%>;
        var gbFetchArchived = <%=bFetchArchived%>;
    </script>
  </head>
  
<body ng-app ng-controller="DashboardProfileCtrl">
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="/">| STEPLIFY | <span class="small_font">Beta</span></a>
          <div class="navbar-content">
            <ul class="nav nav-pills">
              <li class="dropdown active">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-user icon-white"></i> Settings<b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="#modalProfile" data-toggle="modal" ng-click="initProfileVars()">Profile</a>
                  </li>
                  <li>
                    <a href="#modalPassword" data-toggle="modal" ng-click="initPasswordVars()">Set Password</a>
                  </li>
                  <% if (dtoCompanyUser.isAssignedCompanyUser()) { %>
                  <li>
                    <a href="/company">Company</a>
                  </li>
                  <% } %>
                  <li>
                    <a href="#" id="accordion_faqs">FAQ / Help <i class="icon-question-sign"></i></a>
                  </li>
                  <li>
                    <a href="#modalLogout" data-toggle="modal" ng-click="initLogoutVars()">Logout</a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div> <!--/navbar-->

    <div class="top_spacer container-fluid offset1 span10" ng-controller="DashboardCtrl">
      <div class="row-fluid">
          <% if (!bFetchArchived) { %>
          <div class="span9">
            <h3><%= UI.printAndEscapeForHTML(dtoLoggedInUser.prettyPrint()) %>
                <a class="btn btn-mini" href="#" id="accordion_all_memberships"><i class="icon-resize-full"></i></a>
            </h3>
            <div>
              <% if (dtoLoggedInUser.getUserProfile().isIncomplete()) { %>
              This is a great time to update your <a href="#modalProfile" data-toggle="modal" ng-click="initProfileVars()">profile</a>.<br>
              <% } %>
              <% if (mpbDashboard.alUserMemberships.size() == 1) { %>
                You are a member of 1 active property.
              <% } else {%>
                You are a member of <%=mpbDashboard.alUserMemberships.size()%> active properties.
              <% } %>
            </div>
          </div>
          <div class="span3">
            <p><a class="btn btn-success" href="/project/edit"><i class="icon-plus-sign icon-white"></i> Add New</a></p>
            <p><a class="btn" href="/dashboard?archive=true" >Project Archive</a></p>
            <div class="btn-group">
              <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                Sort by <span class="caret"></span>
              </a>
              <ul class="dropdown-menu">
                <li><a href="/dashboard?sort=membership_date">Date</a></li>
                <li><a href="/dashboard?sort=project_id">Project#</a></li>
                <li><a href="/dashboard?sort=address">City / Address</a></li>
                <li><a href="/dashboard?reverse=true">Reverse</a></li>
              </ul>
            </div>
          </div>
          <% } else { %>
          <div class="span9">
              <h3>Project Archive</h3>
              <div>
                <p>Your projects are never deleted, and will remain in your archive for historical purposes.<p>
              </div>
            </div>
            <div class="span3">
              <p><a class="btn" href="/dashboard"><i class="icon-chevron-left"></i> Home</a></p>
            </div>
          <% } %>
      </div> <!--/span12row-->

      <hr>

        <div class="well" id="faqsID" >
          <h3>FAQs:</h3>
          <dl>
            <dt>How do I add a new property?</dt>
            <dd>Click the <a class="btn btn-mini btn-success" href="/project/edit"><i class="icon-plus-sign icon-white"></i> Add New</a> button.</dd>
            <br>
            <dt>Someone invited me to Steplify, what should I do?</dt>
            <dd>Look below, find the property, select the Task Manager.  Look for tasks assigned to you.</dd>
            <br>
            <dt>How many properties can I setup?</dt>
            <dd>It is unlimited at this time.  So feel free to setup a test property now.</dd>
            <br>
            <dt>What is a Topic?</dt>
            <dd>A topic is a grouping of tasks.  SECURITY access is administered at the Topic level.</dd>
            <br>
            <dt>What is a Task?</dt>
            <dd>A task is an assignment of work, with an due date, and a current status.</dd>
            <br>
            <dt>What is the Setup Wizard?</dt>
            <dd>When you setup a new property, you can choose from pre-defined topics and tasks to get your project going.</dd>
            <br>
            <dt>Can I add custom Topics or Task later?</dt>
            <dd>Yes.  Not every transaction is the same, it is expected you will customize your project's topics and tasks as it develops.</dd>
            <br>
            <dt>Why can I not edit/delete a comment?</dt>
            <dd>This was done to preserve the integrity of the project status.</dd>
            <br>
            <dt>I am an Agent, Broker, Contractor, or other professional.  Can I customize my Setup Wizard?</dt>
            <dd>Yes.  Email us to convert to a company account, where you customize the Topics and Tasks for your business.</dd>
          </dl>
        </div> <!--/FAQs-->

        <% for (ProjectMemberDTO dtoMembership : alUserMemberships) { %>
        <h4 id="<%=dtoMembership.get_id()%>" class="accordion"><i class="icon-resize-full"></i>
            Client: <%= UI.printAndEscapeForHTML(dtoMembership.getProject().get_s_description()) %> - <%= UI.printAndEscapeForHTML(dtoMembership.getProject().get_s_address()) %>
        </h4>
        <div class="well row-fluid project_membership" id="project_membership_<%=dtoMembership.get_id()%>">
          <div class="span5">
            <p class="bold"><%= UI.printAndEscapeForHTML(dtoMembership.getProject().get_s_description()) %></p>
            <p>#<%= dtoMembership.getProject().get_id() %> - <%= mapCDsProjectType.get(dtoMembership.getProject().get_s_cd_project_type()) %></p>
            <p><a class="btn btn-primary btn-small" href="/task_manager/view?pid=<%=dtoMembership.getProject().get_id()%>"><i class="icon-search icon-white"></i> Task Manager</a></p>
            <p><i>Membership: <%= UI.dateString(dtoMembership.get_dt_create_date()) %></i>
            <% if (dtoMembership.getProject().get_id_created_by() == dtoLoggedInUser.get_id()) { %>
                <span><i class="fa fa-star fore-gold"></i></span>
            <% } %>
            </p>

          </div>
          <div class="span4">
            <p>Address:</p>
            <p><%= UI.printAndEscapeForHTML(dtoMembership.getProject().get_s_address()) %></p>
            <p><%= UI.printAndEscapeForHTML(dtoMembership.getProject().get_s_city()) %>, <%= UI.printAndEscapeForHTML(dtoMembership.getProject().get_s_state()) %></p>
            <p><div class="btn-group">
              <button class="btn btn-mini dropdown-toggle" data-toggle="dropdown">
                <i class="icon-cog"></i> More <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                <% if (dtoMembership.get_b_is_project_admin()) { %>
                  <p><a href="/project/edit?pid=<%=dtoMembership.getProject().get_id()%>"><i class="icon-edit"></i> Edit Project</a></p>
                  <p><a href="/admin_topic/wizard?pid=<%=dtoMembership.getProject().get_id()%>"><i class="icon-star"></i> Setup Wizard</a></p>
                <% } %>
                <% if (bFetchArchived) { %>
                  <a ng-click="doArchive(<%=dtoMembership.get_id()%>, false)"><i class="icon-briefcase"></i> Un-Archive</a>
                <% } else { %>
                  <a ng-click="doArchive(<%=dtoMembership.get_id()%>, true)"><i class="icon-briefcase"></i> Send to Archive</a>
                <% } %>
              </ul>
            </div></p>
          </div>
          <div class="span3">
            <%  ProjectTaskCountsMPB mpbCounts = mapProjectTaskCounts.get(dtoMembership.getProject().get_id());
                //In case the ProjectTaskCounts are not found, just be nice and dont NPE
                if (mpbCounts == null)
                {
                    mpbCounts = new ProjectTaskCountsMPB();
                }

                if (dtoMembership.get_b_is_project_admin()) {
            %>
                <p><span class=back-red><i class="icon-flag icon-white"></i></span>
                    Your=<%= mpbCounts._i_count_late_for_user %> / Team=<%= mpbCounts._i_count_late %><br>(out of <%= mpbCounts._i_count_total %> total Tasks)
                </p>
            <% } else { %>
                <p><span class=back-red><i class="icon-flag icon-white"></i></span>
                    <%= mpbCounts._i_count_late_for_user %> (of your Tasks)
                </p>
            <% } %>
            <p><a href="/task_manager/view?pid=<%=dtoMembership.getProject().get_id()%>">
            <% if (!UI.isNullOrEmpty(dtoMembership.getProject().get_s_url_image())) { %>
                <img id="imgProperty" alt="Picture Not Found" src="<%= UI.printAndEscapeForHTML(dtoMembership.getProject().get_s_url_image()) %>" width="100">
            <% } else if (dtoMembership.getProject().isTypeOther()) { %>
                <img id="imgProperty" alt="No image assigned" src="/images/missing_other.png" width="100">
            <% } else if (dtoMembership.getProject().isTypeRemodel()) { %>
                <img id="imgProperty" alt="No image assigned" src="/images/missing_remodel.png" width="100">
            <% } else { %>
                <img id="imgProperty" alt="No image assigned" src="/images/missing_home.png" width="100">
            <% } %>
            </a></p>
          </div>
        </div> <!--/span12row-->
        <% } //end of for-loop %>

      <hr>
      <footer>
        <span class="pull-left"><%=Config.FOOTER_LEFT%></span>
        <span class="pull-right"><%=Config.FOOTER_RIGHT%></span>
      </footer>
    </div> <!--/.fluid-container-->

    <div id="modals">
    <div id="modalPassword" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalPassword" aria-hidden="true">
      <div class="modal-header">
        <h3 id="myModalPassword">Set Password</h3>
      </div>
      <div class="modal-body">
        <label class="control-label" for="txt_password1">Password:</label>
        <input type="password" class="input password" id="txt_password1" ng-model="password_1">

        <label class="control-label" for="txt_password2">Again:</label>
        <input type="password" class="input password" id="txt_password2" ng-model="password_2">

        <div class="progress progress-striped active" ng-show="progressbar_password_show">
            <div class="bar" style="width: 100%;"></div>
        </div>
        <p class="text-error" id="passwordError" ng-show="error_password_show">{{error_password_txt}}</p>
      </div>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" id="btn_password" ng-click="setPassword()" ng-show="bSavePasswordButtonEnabled">Save</button>
      </div>
    </div>

    <div id="modalLogout" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLogout" aria-hidden="true">
      <div class="modal-header">
        <h3 id="myModalLogout">Logout</h3>
      </div>
      <div class="modal-body">
        <p>Logout Now?</p>
        <div class="progress progress-striped active" ng-show="progressbar_logout_show">
            <div class="bar" style="width: 100%;"></div>
        </div>
        <p class="text-error" id="logoutError" ng-show="error_logout_show">{{error_logout_txt}}</p>
      </div>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <button class="btn btn-primary" id="btn_logout" ng-click="doLogout()">Logout</button>
      </div>
    </div>

    <div id="modalProfile" class="modal large hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalProfile" aria-hidden="true">
      <div class="modal-header">
        <h3 id="myModalProfile">Update Profile</h3>
      </div>
      <div class="modal-body" ng-show="!fetching_profile">
        <label class="control-label" for="_s_first_name">First Name:</label>
        <input type="text" class="input" id="_s_first_name" ng-model="profile._s_first_name">
        <label class="control-label" for="_s_last_name">Last Name:</label>
        <input type="text" class="input" id="_s_last_name" ng-model="profile._s_last_name">

        <label class="control-label" for="_s_url_personal_img">Personal Picture/Image (URL):</label>
        <input type="text" class="input" id="_s_url_personal_img" ng-model="profile._s_url_personal_img">
        <img id="imgPersonal" ng-show="attemptToShowPersonalImage()" alt="Not Found" ng-src='{{profile._s_url_personal_img}}' width="100">
        <span class="help-inline" ng-show="!attemptToShowPersonalImage()">e.g. http://mysite.com/picture.jpg</span>
        <span class="help-inline alert alert-danger" ng-show="showPersonalImageHint()">URL should start with http://</span>

        <label class="control-label" for="_s_company_name">Company Name:</label>
        <input type="text" class="input" id="_s_company_name" ng-model="profile._s_company_name">
        <label class="control-label" for="_s_url_company_img">Company Icon/Image (URL):</label>
        <input type="text" class="input" id="_s_url_company_img" ng-model="profile._s_url_company_img">
        <img id="imgCompany" ng-show="attemptToShowCompanyImage()" alt="Not Found" ng-src='{{profile._s_url_company_img}}' width="100">
        <span class="help-inline" ng-show="!attemptToShowCompanyImage()">e.g. http://mysite.com/icon.png</span>
        <span class="help-inline alert alert-danger" ng-show="showCompanyImageHint()">URL should start with http://</span>

        <label class="control-label" for="_s_address">Street Address:</label>
        <input type="text" class="input" id="_s_address" ng-model="profile._s_address">
        <label class="control-label" for="_s_city">City:</label>
        <input type="text" class="input" id="_s_city" ng-model="profile._s_city">
        <label class="control-label" for="_s_state">State:</label>
        <input type="text" class="input" id="_s_state" ng-model="profile._s_state">
        <label class="control-label" for="_s_postal_code">Postal Code:</label>
        <input type="text" class="input" id="_s_postal_code" ng-model="profile._s_postal_code">
        <label class="control-label" for="_s_country">Country:</label>
        <input type="text" class="input" id="_s_country" ng-model="profile._s_country">
        <label class="control-label" for="_s_phone_number">Phone Number:</label>
        <input type="text" class="input" id="_s_phone_number" ng-model="profile._s_phone_number">

        <label class="control-label" for="_s_url_website">Website (URL):</label>
        <input type="text" class="input" id="_s_url_website" ng-model="profile._s_url_website">
        <span class="help-inline">e.g. http://mysite.com</span>
        <span class="help-inline alert alert-danger" ng-show="showWebsiteHint()">URL should start with http://</span>
        <p ng-show="!showWebsiteHint()">
      </div>
      <div class="modal-footer">
        <div class="progress progress-striped active" ng-show="progressbar_profile_show">
            <div class="bar" style="width: 100%;"></div>
        </div>
        <p class="text-error" id="profileError" ng-show="error_profile_show">{{error_profile_txt}}</p>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary" id="btn_profile" ng-click="setProfile()" ng-show="bSaveProfileButtonEnabled">Save</button>
      </div>
    </div>

    </div> <!-- Modals -->

</body>

</html>