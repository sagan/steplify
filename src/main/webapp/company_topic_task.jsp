<%@ page import="java.util.*, com.steplify.utility.*, com.steplify.dto.*, com.steplify.mpb.*" %>
<%
    TransResponse objTransResponse = (TransResponse)request.getAttribute("objTransResponse");
    UserPrincipalDTO dtoLoggedInUser = (UserPrincipalDTO)request.getAttribute("dtoLoggedInUser");
    CompanyDTO dtoCompany = (CompanyDTO)request.getAttribute("dtoCompany");
    CompanyUserDTO dtoCompanyUser = (CompanyUserDTO)request.getAttribute("dtoCompanyUser");
    List<CodeDecodeDTO> alCodesProjectType = (List)request.getAttribute("alCodesProjectType");
%>
<!doctype html>
<html>
  
  <head>
    <title>STEPLIFY</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">

    <link rel="stylesheet" href="/css/bootstrap_2.3.2/css/themes/united/bootstrap.min.css">
    <link rel="stylesheet" href="/css/bootstrap_2.3.2/css/bootstrap-responsive.min.css">
    <link rel="stylesheet" href="/css/app/main.css?v=8">
    <link rel="stylesheet" href="/css/divshot/divshot-util.css">
    <link rel="stylesheet" href="/css/divshot/divshot-canvas.css">
    <link rel="stylesheet" href="/css/font-awesome-4.0.3/css/font-awesome.min.css">

    <!--Add JS libraries in this order, JQ, ANG, Boot, App-->
    <script src="/js/lib/jquery/jquery.min.js"></script>
    <script src="/js/lib/angular/angular.min.js"></script>
    <script src="/css/bootstrap_2.3.2/js/bootstrap.min.js"></script>
    <script src="/js/app/company_topic_task_ng.js?v=2"></script>

    <!-- ng-grid -->
    <script src="/js/lib/angular_ng-grid/ng-grid.min.js"></script>
    <link rel="stylesheet" href="/js/lib/angular_ng-grid/ng-grid.css">

    <!--Add Google Analytics-->
    <script><%= Config.getGoogleAnalyticsScript() %></script>
  </head>
  
<body>
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="/dashboard">| STEPLIFY | <span class="small_font">Beta</span></a>
          <div class="navbar-content">
            <span class="brand">&gt; Company TopicTask Administration</span>
          </div>
        </div>
      </div>
    </div> <!--/navbar-->

    <div class="top_spacer container-fluid offset1 span11" ng-app="myCompanyTopicTaskApp" ng-controller="CompanyTopicTaskCtrl">
      <div class="row-fluid">
          <div class="span9">
            <h2><%=dtoCompany.get_s_name()%></h2>
            <p>Manage the SetupWizard default Topics/Tasks for your company here.</p>
          </div>
          <div class="span3">
            <p><a class="btn" href="/company"><i class="icon-chevron-left"></i> Company</a></p>
            <% for(String sEx : objTransResponse.getExceptions()) { %>
            <p class="text-info bold"><%= UI.printAndEscapeForHTML(sEx) %></p>
            <% } %>
            <% for(String sWarn : objTransResponse.getWarnings()) { %>
            <p class="text-info"><%= UI.printAndEscapeForHTML(sWarn) %></p>
            <% } %>
          </div>
      </div> <!--/span12row-->

      <hr>
        <a name="topics"></a>
        <form id="form_topic_task_export" class="form-horizontal" method="post" action="/company/topic_task_export">
            Step 1 - Select a PROJECT type:
            <select type="text" id="project_type" name="_s_cd_project_type" class="span3" ng-model="_s_cd_project_type" ng-change="getTopicTasks()">
                <%= UI.dropDownOptions(alCodesProjectType, null) %>
            </select>
            <a class="btn" ng-show="false" ng-click="doExport()"><i class="fa fa-download"></i></a>
        </form>
        <br>
        <div ng-show="hasProjectType()">
        <p>
            Step 2A - Update your company <span style="background-color: #FFFF00">TOPICS</span>, double-click inside a cell to edit:
            <a class="btn btn-small btn-success pull-right" ng-click="topicAdd()"><i class="fa fa-plus"></i> Add Topic</a>
        </p>
        <div class="gridStyle" ng-grid="gridTopics"></div>
        <br>
        <br>
        <a name="tasks"></a>
        <p>
            Step 2B - Update the <span style="background-color: #00FFFF">TASKS</span> (for each topic selected <a href="#topics">above</a>):
            <a class="btn btn-small btn-success pull-right" ng-click="taskAdd()"><i class="fa fa-plus"></i> Add Task</a>
        </p>
        <div class="gridStyle" ng-grid="gridTasks"></div>
        <br>
        <br>
        <p>
            Step 3 - Apply all changes for the selected PROJECT type:
            <a class="btn btn-small btn-success" ng-click="postTopicTasks()"><i class="fa fa-floppy-o"></i> Save Now</a>
            <a class="btn btn-small btn-inverse" ng-click="initView()"><i class="fa fa-stop"></i> Reset</a>
        </p>
        </div>

      <hr>
      <footer>
        <span class="pull-left"><%=Config.FOOTER_LEFT%></span>
        <span class="pull-right"><%=Config.FOOTER_RIGHT%></span>
      </footer>
    </div> <!--/.fluid-container-->

</body>

</html>