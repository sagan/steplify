<%@ page import="java.util.*, com.steplify.utility.*, com.steplify.dto.*, com.steplify.mpb.*" %>
<%
    AdminTopicViewMPB mpbAdminTopicView = (AdminTopicViewMPB)request.getAttribute("mpbAdminTopicView");
    List<CodeDecodeDTO> alCodesTopicAuthority = mpbAdminTopicView.alCodesTopicAuthority;
    List<TopicDTO> lstTopics = mpbAdminTopicView.lstTopics;
    int iProjectID = mpbAdminTopicView.iProjectID;
    TransResponse objTransResponse = (TransResponse)request.getAttribute("objTransResponse");
    UserPrincipalDTO dtoLoggedInUser = (UserPrincipalDTO)request.getAttribute("dtoLoggedInUser");
%>
<!doctype html>
<html>
  
  <head>
    <title>STEPLIFY</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">

    <link rel="stylesheet" href="/css/bootstrap_2.3.2/css/themes/united/bootstrap.min.css">
    <link rel="stylesheet" href="/css/bootstrap_2.3.2/css/bootstrap-responsive.min.css">
    <link rel="stylesheet" href="/css/app/main.css?v=8">
    <link rel="stylesheet" href="/css/divshot/divshot-util.css">
    <link rel="stylesheet" href="/css/divshot/divshot-canvas.css">

    <!--Add JS libraries in this order, JQ, ANG, Boot, App-->
    <script src="/js/lib/jquery/jquery.min.js"></script>
    <script src="/js/lib/angular/angular.min.js"></script>
    <script src="/css/bootstrap_2.3.2/js/bootstrap.min.js"></script>
    <script src="/js/app/admin_topic_members_ng.js?v=2"></script>

    <!--Add Google Analytics-->
    <script><%= Config.getGoogleAnalyticsScript() %></script>
  </head>
  
<body ng-app ng-controller="AdminTopicMembersCtrl">
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="/dashboard">| STEPLIFY | <span class="small_font">Beta</span></a>
          <div class="navbar-content">
            <a class="brand" href="/task_manager/view?pid=<%=iProjectID%>">&gt; Task Manager</a>
            <span class="brand">&gt; Advanced Permissions</span>
          </div>
        </div>
      </div>
    </div> <!--/navbar-->

    <div class="top_spacer container-fluid offset1 span11">
      <div class="row-fluid">
          <div class="span9">
            <h2>Configure Permissions</h2>
            Verify user permissions for each topic.
            <a class="btn btn-inverse pull-right" href="/task_manager/view?pid=<%=iProjectID%>">Done</a>
          </div>
          <div class="span3">
            <% for(String sEx : objTransResponse.getExceptions()) { %>
            <p class="text-info bold"><%= UI.printAndEscapeForHTML(sEx) %></p>
            <% } %>
            <% for(String sWarn : objTransResponse.getWarnings()) { %>
            <p class="text-info"><%= UI.printAndEscapeForHTML(sWarn) %></p>
            <% } %>
          </div>
      </div> <!--/span12row-->

      <hr>

        <div>
            <div class="row-fluid row_header">
                <strong>
                    <div class="span3">Topic Name</div>
                    <div class="span3">User</div>
                    <div class="span3">Permission <span class="bold_off"><a href="#" class="column_tip" data-original-title="NoAccess will remove a topic from a user's Task Manager (i.e. delete)"><i class="icon-info-sign"></i></a></span></div>
                </strong>
            </div>

            <% for (TopicDTO dtoTopic : lstTopics) { %>
            <a class="anchor" name="topic_<%=dtoTopic.get_id()%>">&nbsp;</a>
            <div class="row-fluid row_pill">
                <div class="span3">
                    <span class="bold"><%=UI.printAndEscapeForHTML(dtoTopic.get_s_name())%></span>
                </div>

                <div class="row-fluid">
                </div>

                <div class="user_permission">
                    <% for (TopicMemberDTO dtoTopicMember : dtoTopic.getTopicMembers()) { %>
                    <div class="row-fluid">
                        <form id="form_update_topic_member_<%=dtoTopicMember.get_id()%>" class="form-horizontal" method="post" action="/admin_topic/update_topic_member#topic_<%=dtoTopic.get_id()%>">
                            <div class="offset3 span3">
                                <% if (dtoTopicMember.isAuthorityWriter()) { %>
                                  <span class=back-green>&nbsp;</span>
                                <% } else if (dtoTopicMember.isAuthorityNone()) { %>
                                  <span class=back-red>&nbsp;</span>
                                <% } else { %>
                                  <span class=back-yellow>&nbsp;</span>
                                <% } %>
                                <%=UI.printAndEscapeForHTML(dtoTopicMember.getUserPrincipal().get_s_username())%>
                            </div>
                            <div class="span3"><select type="text" name="_s_cd_authority" class="wide" onchange="this.form.submit()"><%= UI.dropDownOptions(alCodesTopicAuthority, dtoTopicMember.get_s_cd_authority()) %></select></div>
                            <input type="hidden" name="_id" value="<%=dtoTopicMember.get_id()%>">
                        </form>
                    </div>
                    <% } //end of MEMBER for-loop%>
                </div>
            </div>
            <% } //end of TOPICS for-loop%>
        </div>

      <hr>
      <footer>
        <span class="pull-left"><%=Config.FOOTER_LEFT%></span>
        <span class="pull-right"><%=Config.FOOTER_RIGHT%></span>
      </footer>
    </div> <!--/.fluid-container-->

</body>

</html>