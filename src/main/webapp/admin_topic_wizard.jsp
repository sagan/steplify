<%@ page import="java.util.*, com.steplify.utility.*, com.steplify.dto.*, com.steplify.mpb.*" %>
<%
    AdminTopicViewMPB mpbAdminTopicView = (AdminTopicViewMPB)request.getAttribute("mpbAdminTopicView");
    int iProjectID = mpbAdminTopicView.iProjectID;
    TransResponse objTransResponse = (TransResponse)request.getAttribute("objTransResponse");
    UserPrincipalDTO dtoLoggedInUser = (UserPrincipalDTO)request.getAttribute("dtoLoggedInUser");
    List<TopicDTO> lstTopicTaskTemplates = (List<TopicDTO>)request.getAttribute("lstTopicTaskTemplates");
%>
<!doctype html>
<html>
  
  <head>
    <title>STEPLIFY</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">

    <link rel="stylesheet" href="/css/bootstrap_2.3.2/css/themes/united/bootstrap.min.css">
    <link rel="stylesheet" href="/css/bootstrap_2.3.2/css/bootstrap-responsive.min.css">
    <link rel="stylesheet" href="/css/app/main.css?v=8">
    <link rel="stylesheet" href="/css/divshot/divshot-util.css">
    <link rel="stylesheet" href="/css/divshot/divshot-canvas.css">

    <!--Add JS libraries in this order, JQ, ANG, Boot, App-->
    <script src="/js/lib/jquery/jquery.min.js"></script>
    <script src="/js/lib/angular/angular.min.js"></script>
    <script src="/css/bootstrap_2.3.2/js/bootstrap.min.js"></script>
    <script src="/js/app/admin_topic_wizard_ng.js?v=2"></script>

    <!--Add Google Analytics-->
    <script><%= Config.getGoogleAnalyticsScript() %></script>
  </head>
  
<body ng-app ng-controller="AdminTopicWizardCtrl">
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="/dashboard">| STEPLIFY | <span class="small_font">Beta</span></a>
          <div class="navbar-content">
            <a class="brand" href="/task_manager/view?pid=<%=iProjectID%>">&gt; Task Manager</a>
            <span class="brand">&gt; Setup Wizard</span>
          </div>
        </div>
      </div>
    </div> <!--/navbar-->

    <div class="top_spacer container-fluid offset1 span11">
      <div class="row-fluid">
          <div class="span9">
            <h2>Setup Wizard</h2>

            <span>Select pre-defined tasks you would like to add to your project. <a href="#" class="column_tip" data-original-title="Any pre-existing tasks will not be removed or overwritten"><i class="icon-info-sign"></i></a><br>
            Use the <i class="icon-plus-sign"></i> to select/deselect all.</span>
          </div>
          <div class="span3">
            <% for(String sEx : objTransResponse.getExceptions()) { %>
            <p class="text-info bold"><%= UI.printAndEscapeForHTML(sEx) %></p>
            <% } %>
            <% for(String sWarn : objTransResponse.getWarnings()) { %>
            <p class="text-info"><%= UI.printAndEscapeForHTML(sWarn) %></p>
            <% } %>
          </div>
      </div> <!--/span12row-->

      <hr>

        <div id="topicWizard">
          <div>
            <% for (TopicDTO dtoTopic : lstTopicTaskTemplates) {
                String sTopicKey = "TP" + dtoTopic.get_id();
            %>
              <div class="row-fluid border_top">
                <div class="span1 select_all" id="<%=sTopicKey%>"><i class="icon-plus-sign"></i></div>
                <div class="span11"><strong>Topic: <%=UI.printAndEscapeForHTML(dtoTopic.get_s_name())%></strong></div>
              </div>
              <% for (TaskDTO dtoTask : dtoTopic.getTasks()) {
                String sTaskKey = "TK" + dtoTask.get_id();
              %>
                <div class="row-fluid">
                    <div class="span1"><input class="<%=sTopicKey%>" type="checkbox" ng-model="mapWizard['<%=sTaskKey%>']" ng-init="mapWizard['<%=sTaskKey%>'] = true"></div>
                    <div class="span8"><i><%=UI.printAndEscapeForHTML(dtoTask.get_s_name())%></i></div>
                </div>
              <% } //end of TEMPLATE_TASKS for-loop %>
            <% } //end of TEMPLATE_TOPICS for-loop %>
          </div>
          <br>
          <div>
            <div class="progress progress-striped active" ng-show="progressbar_wizard_show">
                <div class="bar" style="width: 100%;"></div>
            </div>
            <p class="text-error" id="wizardError" ng-show="error_wizard_show">{{error_wizard_txt}}</p>
            <a class="btn" href="/task_manager/view?pid=<%=iProjectID%>"><i class="icon-chevron-left"></i> Cancel</a>
            <button class="btn" id="btn_task" ng-click="addWizard(<%=iProjectID%>)">Next <i class="icon-chevron-right"></i></button>
          </div>
        </div>

      <hr>
      <footer>
        <span class="pull-left"><%=Config.FOOTER_LEFT%></span>
        <span class="pull-right"><%=Config.FOOTER_RIGHT%></span>
      </footer>
    </div> <!--/.fluid-container-->

</body>

</html>