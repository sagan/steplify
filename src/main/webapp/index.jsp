<%@ page import="com.steplify.utility.*" %>
<!doctype html>
<html>
  
  <head>
    <title>STEPLIFY - Real Estate Task Checklist Software</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
    <meta name="description" content="Use a simple interactive task checklist to get you through your Real Estate transactions - buying, selling, remodeling, etc.">

    <link rel="stylesheet" href="/css/bootstrap_2.3.2/css/themes/united/bootstrap.min.css">
    <link rel="stylesheet" href="/css/bootstrap_2.3.2/css/bootstrap-responsive.min.css">
    <link rel="stylesheet" href="/css/app/main.css?v=8">
    <link rel="stylesheet" href="/css/font-awesome-4.0.3/css/font-awesome.min.css">

    <!--Add JS libraries in this order, JQ, ANG, Boot, App-->
    <script src="/js/lib/jquery/jquery.min.js"></script>
    <script src="/js/lib/angular/angular.min.js"></script>
    <script src="/css/bootstrap_2.3.2/js/bootstrap.min.js"></script>
    <script src="/js/app/index_ng.js?v=3"></script>

    <!--Add Google Analytics-->
    <script><%= Config.getGoogleAnalyticsScript() %></script>
  </head>
  
  <body>

    <!--Add Facebook Sharing-->
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=700457950033630&version=v2.0";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="/">| STEPLIFY | <span class="small_font">Beta</span></a>
          <div class="navbar-content">
            <ul class="nav">
              <li class="active">
                <a href="/login">Sign In <i class="fa fa-sign-in"></i></a>
              </li>
            </ul>
          </div>
          <div class="fb-like pull-right" data-href="http://steplify.com" data-layout="standard" data-action="like" data-show-faces="false" data-share="true"></div>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="hero-unit hero-main" id="heroMainID" ng-app ng-controller="IndexCtrl">

        <div class="hero-into" id="heroIntroID">
          <br>
          <div class="hero-text" id="heroTextID">
            <div class="row-fluid">
              <h2>Buying or Selling a Home?</h2>
            </div>
            <div class="row-fluid">
              <h3 class="pull-right">Use a simple interactive task checklist designed for Real Estate...</h3>
            </div>
          </div>
          <br>
          <div class="row-fluid">
            <a class="btn hidden-phone btn-inverse btn-large" href="#" id="btnHiwStartID">How it works</a>
          </div>

          <br><br><br>
          <br><br><br>
          <br><br><br>
        </div>

        <div class="hero-hiw" id="heroHiwID">
          <br>
          <div class="row-fluid">
            <button class="btn btn-inverse btn-small" href="#" ng-click="decrementHIW()" ng-disabled="iHIW <= 1"><i class="icon-step-backward icon-white"></i></button>
            <button class="btn btn-inverse btn-small" href="#" ng-click="initHIW()" id="btnHiwCloseID"><i class="icon-stop icon-white"></i></button>
            <button class="btn btn-inverse btn-small" href="#" ng-click="incrementHIW()" ng-disabled="iHIW >= 10"><i class="icon-step-forward icon-white"></i></button>
          </div>
          <div class="hero-text-hiw row-fluid" ng-show="showHIW(1)">
              <h2>So your offer just got accepted.</h2>
              <h2>Congratulations!</h2>
            <br><br><br>
          </div>
          <div class="hero-text-hiw row-fluid" ng-show="showHIW(2)">
            <h2>Now the real work begins.</h2>
            <h2>There are a ton of little things to do, with a deadline you cannot afford to miss.</h2>
            <br><br><br>
          </div>
          <div class="hero-text-hiw row-fluid" ng-show="showHIW(3)">
            <h2>Minute 1:</h2>
            <h2>Sign up, and add a property.</h2>
            <br><br><br>
          </div>
          <div class="hero-text-hiw row-fluid" ng-show="showHIW(4)">
            <h2>Minute 2:</h2>
            <h2>Make an initial plan (from pre-defined tasks) to get you going.</h2>
            <br><br><br>
          </div>
          <div class="hero-text-hiw row-fluid" ng-show="showHIW(5)">
            <h2>Minute 3:</h2>
            <h2>Invite your team: Agent, Family, Mortgage, Contractors, Movers...</h2>
            <br><br><br>
          </div>
          <div class="hero-text-hiw row-fluid" ng-show="showHIW(6)">
            <h2>Minute 4:</h2>
            <h2>Everyone has a job to do - Assign tasks and set due dates.</h2>
            <br><br><br>
          </div>
          <div class="hero-text-hiw row-fluid" ng-show="showHIW(7)">
            <h2>Minute 5:</h2>
            <h2>Feel confident and worry-free, everyone knows what they need to do next; plus you can see it all in real-time.</h2>
            <br><br><br>
          </div>
          <div class="hero-text-hiw row-fluid" ng-show="showHIW(8)">
            <h2>Of course there are privacy controls.</h2>
            <h2>Where you can control what people can and cannot see.</h2>
            <br><br><br>
          </div>
          <div class="hero-text-hiw row-fluid" ng-show="showHIW(9)">
            <h2>Did we mention you can see everything on a single page?</h2>
            <h2>So long paper lists, and random emails!</h2>
            <br><br><br>
          </div>
          <div class="hero-text-hiw row-fluid" ng-show="showHIW(10)">
            <h2>It takes just one mistake to drop the ball.</h2>
            <h2>Work like a pro - Simplify with Steplify!</h2>
            <br><br><br>
          </div>
        </div>

      </div>

      <div class="row main-features">
        <div class="span6">
          <div class="well">
            <h4>Not just another online shared spreadsheet:</h4>
            <ul>
            <li>Track all tasks across multiple parties from a single view.</li>
            <li>Organized comments - No more chaotic and disjointed emails.</li>
            <li>No more need for sticky notes and paper lists.</li>
            <li>Communication based on a "need to know" basis.</li>
            <li>Worry free execution, make sure your  transaction is smooth.</li>
            <li>Of course it works on your phone.</li>
            </ul>
          </div>
        </div>
        <div class="span6">
          <div class="well">
            <h4>Sign up is FREE during our beta period:</h4>
            <ul>
            <li>Individuals - start anytime, and invite your Agent when ready.</li>
            <li>Agents - use it yourself, or <a href="#modalInviteOthers" data-toggle="modal">invite your Clients</a> to self-manage themselves.</li>
            <li>Can be used for any custom transaction that requires secure multi-party coordination (Remodeling, Refinance, Property Management, etc).</li>
            </ul>
            <a class="btn btn-primary" href="/login?signup=true"><span class="btn-label">Sign Up</span></a>
            <br>
          </div>
        </div>
      </div>
      <hr>
    </div>

    <div class="container">
      <ul class="nav nav-pills">
        <li class="dropup">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">More Info <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li>
              <a href="#modalAboutUs" data-toggle="modal">About Us</a>
            </li>
            <li>
              <a href="#modalContactUs" data-toggle="modal">Contact Us</a>
            </li>
          </ul>
        </li>
        <span class="pull-right"><%=Config.FOOTER_RIGHT%></span>
      </ul>
    </div>

    <!-- Modal -->
    <div id="modalAboutUs" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalAboutUs" aria-hidden="true">
      <div class="modal-header">
        <h3 id="myModalAboutUs">About Us</h3>
      </div>
      <div class="modal-body">
        <p>Based in Mountain View, CA, USA.</p>
        <p>
        Real estate transactions require continuous coordination between agents, sellers, buyers, inspectors, plus loan and escrow officers at a minimum.
        Research shows the number one Broker/Agent complaint is - "Our current software is too cumbersome for practical execution, so we use paper files instead".
        Client research shows the number one complaint, even after using a great realtor is - "I get too many disconnected emails, so I can never get a single view of everything that: has happened, is happening, is going to happen".
        </p>
        <p>
        Steplify changes all this by bringing the convenience and security of a cloud service to the real estate transaction business.
        Steplify uses technology to maximize efficiency, along with creating a systematic approach to minimize issues and surprises.
        Quickly create a project plan from predefined tasks with deadlines to keep the parties coordinated from start to finish.
        Real time data exchange, combined with granular security features allow transactions to move efficiently and safely towards closing.
        </p>
      </div>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
      </div>
    </div>

    <!-- Modal -->
    <div id="modalContactUs" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalContact" aria-hidden="true">
      <div class="modal-header">
        <h3 id="myModalContact">Contact Us</h3>
      </div>
      <div class="modal-body">
        <p>Email inquiries to: <%=Config._SUPPORT_EMAIL%></p>
      </div>
      <div class="modal-footer">
        <a class="btn btn-primary" target="_blank" href="<%=UI.htmlMailToHref(Config._SUPPORT_EMAIL, "Steplify Support Request", null)%>">Email Us</a>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
      </div>
    </div>

    <!-- Modal -->
    <div id="modalInviteOthers" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="modalInviteOthers" aria-hidden="true">
      <div class="modal-header">
        <h3 id="modalInviteOthers">Invite</h3>
      </div>
      <div class="modal-body">
        <p>Know someone who needs Steplify?</p>
      </div>
      <div class="modal-footer">
        <a class="btn btn-primary" target="_blank" href="<%=UI.htmlMailToHref(null, "Steplify Invitation", "Please signup for an account and use it to manage your home transaction.\n\nhttp://steplify.com\n\nThanks!")%>">Invite</a>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
      </div>
    </div>

  </body>
</html>