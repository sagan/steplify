<%@ page import="java.util.*, com.steplify.utility.*, com.steplify.dto.*, com.steplify.mpb.*" %>
<%
    //Nothing for now
%>
<!doctype html>
<html>
  
  <head>
    <title>STEPLIFY</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">

    <link rel="stylesheet" href="/css/bootstrap_2.3.2/css/themes/united/bootstrap.min.css">
    <link rel="stylesheet" href="/css/bootstrap_2.3.2/css/bootstrap-responsive.min.css">
    <link rel="stylesheet" href="/css/app/main.css?v=8">
    <link rel="stylesheet" href="/css/divshot/divshot-util.css">
    <link rel="stylesheet" href="/css/divshot/divshot-canvas.css">

    <!--Add JS libraries in this order, JQ, ANG, Boot, App-->
    <script src="/js/lib/jquery/jquery.min.js"></script>
    <script src="/js/lib/angular/angular.min.js"></script>
    <script src="/css/bootstrap_2.3.2/js/bootstrap.min.js"></script>

    <!--Add Google Analytics-->
    <script><%= Config.getGoogleAnalyticsScript() %></script>
  </head>
  
<body>
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="/">| STEPLIFY | <span class="small_font">Beta</span></a>
          <div class="navbar-content">
            <ul class="nav">
              <li class="active">
                <a href="/"><i class="fa fa-home"></i> Home</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div> <!--/navbar-->

    <div class="top_spacer container-fluid offset1 span11">
      <div class="row-fluid">
        <h2>Steplify.com - Terms of Use</h2>
      </div> <!--/span12row-->

      <hr>

        <div class="topic_pill">

        <p>Welcome to Steplify.com. Steplify.com is a real estate collaboration website. We facilitate collaboration and information sharing among people in the real estate market, such as buyers, sellers, brokers, agents, transaction coordinators, or contractors. The collaboration service is provided online through the Steplify.com website. Access to and use of Steplify.com is governed by these Terms of Use.</p>
        <p>You are allowed to use our Services only if you can form a binding contract with us, and only in compliance with our Terms of Use and all applicable laws.</p>
        <p>The following terms and conditions apply to everyone who uses Steplify.com's services (including real estate professionals), and they govern the relationship between Steplify.com and our users during their use of Steplify.com.</p>
        <p>Please read these Terms carefully before using Steplify.com. By accessing and using Steplify.com, you explicitly agree to comply with and be bound by our Terms of Use.</p>

        <h4>The Services We Provide</h4>
        <p><u>Our Services:</u> Our services consist of the collaboration tools provided through Steplify.com.</p>
        <p><u>None of Our Services Is an Offer or Promise to Sell:</u> Nothing contained in the Services is an offer or promise to sell a specific product for a specific price or that any user or advertiser will sell any product or service for any purpose or price or on any specific terms.</p>
        <p><u>Be Honest With Us:</u> When you create an account, you must provide us with accurate and up-to-date information, for which you are solely responsible.</p>
        <p><u>Deal with Us in Good Faith:</u> By creating an account, you agree that you are signing up for Steplify.com in good faith, and that you mean to use them only for their intended purposes as real estate collaboration tools and for no other reasons.</p>
        <p><u>Security:</u> While Steplify.com works to protect the security of your content and account, we cannot guarantee that unauthorized third parties will not be able to circumvent our security measures. Please notify us immediately of any compromise or unauthorized use of your account. You are also responsible for maintaining the confidentiality of your account information and for ensuring that only authorized individuals have access to your account. You are responsible for all actions taken or content added through your account.</p>
        <p>By using Steplify.com, you agree that a user account be created for you on Steplify.com. Like all our accounts, you may delete it at any time by making a deletion request.

        <h4>Our Rules About Your Content</h4>
        <p><u>Your Content Is Your Responsibility:</u> You are solely responsible for any messages, reviews, text, photos, videos, graphics, code, or other content or materials (collectively "User Content") that you post, submit, publish, display or link to through Steplify.com or send to other Steplify.com users.</p>
        <p><u>Steplify.com May Choose to Monitor User Content:</u> Steplify.com does not approve, control, or endorse your or anyone else's User Content, and has no obligation to do so. However, we reserve the right (but assume no obligation) to remove or modify User Content for any reason, at our sole discretion, including User Content that we believe violates our Terms of Use.</p>
        <p><u>Be Truthful:</u> You agree to provide accurate, complete, current, and truthful information when you provide or edit content via Steplify.com.</p>
        <p><u>Do Not Post Illegal or Harmful Content:</u> You agree not to post, submit, or link to any User Content or material that infringes, misappropriates, or violates the rights of any third party (including intellectual property rights), or that is in violation of any federal, state, or local law, rule, or regulation, including Fair Housing Laws. You also agree not to post, submit, or link to any User Content that is defamatory, obscene, pornographic, indecent, harassing, threatening, abusive, inflammatory, or fraudulent, purposely false or misleading, or otherwise harmful.</p>
        <p><u>Do Not Violate Others' Rights :</u> You agree not to post copyrighted material without permission from the owner of the copyright. This includes, for example, photographs or other content you upload via the Services. You also agree not to disclose confidential or sensitive information . This includes but is not limited to information about neighbors or other information that would potentially be viewed as an invasion of privacy.</p>
        <p><u>You Grant Steplify.com a License to Use Your Content:</u> When you provide User Content via Steplify.com, you grant Steplify.com a royalty-free, perpetual, irrevocable, and fully sublicensable license to publish, reproduce, distribute, display, adapt, modify and otherwise use your User Content in connection with our Services. We will not pay you or otherwise compensate you for content you provide to us.</p>
        <p>We reserve the right in our sole discretion to edit or remove information or materials provided by you.</p>

        <h4>Rules for Real Estate Professionals</h4>
        <p><u>Information Provided by Real Estate Professionals</u></p>
        <p>If you are a real estate professional, you agree that you will not claim or submit listings that do not belong to you.</p>
        <p><u>Steplify.com User Information Provided to Real Estate Professionals</u></p>
        <p>If you are a real estate professional, you agree that you will not use any information about Steplify.com users that is transferred to you during your use of Steplify.com (e.g., contact information sent to you when a user collaborates with you through a project page) for any reason except to provide those users with real estate services. You further agree never to use such information for any illegal or harmful purpose.</p>

        <h4>Intellectual Property and Licenses</h4>
        <p><u>Steplify.com Grants You a Limited License to Our Services:</u> Subject to these Terms of Use and any other policies we create, we grant you a limited, non-sublicensable right to access Steplify.com. This license does not allow you to copy or sell Steplify.com's services or Materials; scrape or mine Steplify.com; or frame any part of Steplify.com. Steplify.com reserves the right, without notice and in our sole discretion, to terminate your license to use Steplify.com, and to block or prevent future access to and use of Steplify.com.</p>
        <p>Except for the limited license granted to you, you are not conveyed any other right or license in any way. Any unauthorized use of Steplify.com, and any use that exceeds the license granted to you, will terminate the permission or license granted by these Terms of Use.</p>
        <p><u>Our Materials Are Protected:</u> All materials in Steplify.com, including our branding, trade dress, trade names, logos, design, text, search results, graphics, images, pictures, page headers, custom graphics, icons, scripts, sound files and other files, and the selection and arrangement and compilation of information thereof (collectively "Materials"), are proprietary property of Steplify.com, its suppliers, its licensors, or its users, and are protected by U.S. and international intellectual property laws, including copyright and trademark laws. Our Materials cannot be copied, imitated, or used, in whole or in part, without our prior written permission. You may not use any meta tags or any other "hidden text" utilizing "Steplify.com" or any other name, trademark, or product name of Steplify.com without our permission.</p>
        <p><u>Our Use of Third Parties' Marks Does Not Imply Endorsement:</u> All other trademarks, registered trademarks, product names, and logos appearing on Steplify.com are the property of their respective owners. Reference to any products, services, processes, or other information, by trade name, trademark, manufacturer, supplier, or otherwise does not constitute or imply endorsement, sponsorship or recommendation thereof by us.</p>
        <p><u>Steplify.com Does Not Own Real Estate Source Images:</u> Steplify.com does not assert copyright or grant any rights to the underlying images or descriptions of real estate listings that may be contained in our search results and that we derive from the source Web site or information provided by the Web site owner. Steplify.com uses these images and excerpted descriptions only as necessary to generate search results as navigational tools to direct you to the originating Web site. Any use of the source images or descriptions is subject to the copyright owner's permission and/or the requirements of applicable law. Steplify.com does assert copyright and reserves all right to the search results and associated compilations.</p>

        <h4>Third-Party Links, Sites, and Services</h4>
        <p>Our Site and Services may contain links to third-party Web sites, advertisements, services, special offers, or other events, activities, or content (collectively “Third-Party Materials”). These Third-Party Materials are not under the control of Steplify.com. We are providing these links to you only as a convenience, and the inclusion of any link does not imply affiliation, endorsement, or adoption by us of the site or any information contained therein. Steplify.com is not responsible or liable for your use of or access to any Third-Party Materials. Whenever you leave Steplify.com, be aware that your activity outside Steplify.com will not be governed by our Terms of Use or other agreements.</p>

        <h4>No Warranties</h4>
        <p>THIS SITE, THE MATERIALS, AND THE SERVICES ARE PROVIDED ON AN "AS IS" BASIS WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED. ALL USE OF STEPLIFY.COM, MATERIALS, AND SERVICES IS AT YOUR SOLE RISK.</p>
        <p>WE SPECIFICALLY DISCLAIM ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, Non-Infringement, and any warranties arising out of course of dealing or usage of trade.</p>
        <p>Steplify.com takes no responsibility and assumes no liability for any User Content or other content, including links to web pages, that you or any other user or third party posts or transmits using our Site or Services. You understand and agree that you may be exposed to User Content is inaccurate, objectionable, inappropriate for children, or otherwise unsuited to your purpose.</p>
        <p>Steplify.com is not responsible in any way for, does not monitor, and does not endorse or guarantee anything about any third-party content (including advertisements, offers, and promotions) that may appear in Steplify.com.</p>

        <h4>Indemnification</h4>
        <p>You agree to defend, indemnify, and hold Steplify.com harmless from and against any claims, damages, costs, liabilities, and expenses (including, but not limited to, reasonable attorneys' fees) arising out of or related to any User Content you post, store, or otherwise transmit on or through Steplify.com or your use of Steplify.com, including, without limitation, any actual or threatened suit, demand, or claim made against Steplify.com arising out of or relating to the User Content, your conduct, your violation of these Terms of Use, or your violation of the rights of any third party.</p>

        <h4>Limitation Of Liability</h4>
        <p>IN NO EVENT SHALL STEPLIFY.COM, ITS OFFICERS, DIRECTORS, AGENTS, AFFILIATES, EMPLOYEES, ADVERTISERS, OR DATA PROVIDERS BE LIABLE FOR ANY INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL OR PUNITIVE DAMAGES (INCLUDING BUT NOT LIMITED TO LOSS OF USE, LOSS OF PROFITS, OR LOSS OF DATA) WHETHER IN AN ACTION IN CONTRACT, TORT (INCLUDING BUT NOT LIMITED TO NEGLIGENCE), EQUITY OR OTHERWISE, ARISING OUT OF OR IN ANY WAY CONNECTED WITH THE USE OF THIS SITE, THE MATERIALS, OR OUR SERVICES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY, SO THE ABOVE LIMITATIONS MAY NOT APPLY TO YOU.</p>

        <h4>General Terms</h4>
        <h4>Modification of Steplify.com Services</h4>
        <p>Steplify.com reserves the right to modify or discontinue (completely or in part) our site, services, or any content appearing therein. Steplify.com will not be liable to you or any third party if we exercise this right.</p>

        <h4>Modification of These Terms</h4>
        <p>We reserve the right to change or modify these Terms of Use, or any policy or guideline of Steplify.com, at any time and in our sole discretion. Any changes or modification will be effective immediately upon posting of the revisions to Steplify.com. You waive any right you may have to receive specific notice of such changes or modifications. Your continued use of Steplify.com following the posting of changes or modifications will confirm your acceptance of such changes or modifications. Please review these terms and conditions periodically, and check the version date for changes.</p>

        <h4>Choice of Law</h4>
        <p>These Terms of Use are governed by the laws of the State of California without regard to its conflict of law provisions.</p>

        <h4>No Waiver</h4>
        <p>Steplify.com's failure to exercise or enforce any right or provision of the Terms of Use will not be deemed to be a waiver of such right or provision.</p>

        <h4>No Third-Party Beneficiaries or Rights</h4>
        <p>These Terms of Use do not create any private right of action on the part of any third party or any reasonable expectation that Steplify.com will not contain any content that is prohibited by these Terms of Use.</p>

        <h4>Entire Agreement & Severability</h4>
        <p>These Terms and any amendments and additional agreements you might enter with Steplify.com in connection with our site or services, shall constitute the entire agreement between you and Steplify.com concerning the site or services, and shall supersede any prior terms you had with Steplify.com regarding the site or services.</p>
        <p>If any provision of these Terms of Use is deemed invalid, then that provision will be limited or eliminated to the minimum extent necessary, and the other provisions of these Terms of Use remain in full force and effect.</p>

        <h4>Limitation of Claims</h4>
        <p>Regardless of any statute or law to the contrary, any claim or cause of action arising out of or related to your use of Steplify.com must be filed within one (1) year after such claim or cause of action arose, or else that claim or cause of action will be barred forever.</p>

        <h4>Arbitration</h4>
        <p>Any controversy or claim arising out of or relating to these Terms of Use or Steplify.com will be settled by binding arbitration in accordance with the commercial arbitration rules of the American Arbitration Association ("AAA"). Any such controversy or claim must be arbitrated on an individual basis, and must not be consolidated in any arbitration with any claim or controversy of any other party. The arbitration must be conducted in San Jose, California, and judgment on the arbitration award may be entered into any court having jurisdiction thereof. Either Steplify.com or you may seek any interim or preliminary relief from a court of competent jurisdiction in San Jose, California, as necessary to protect the rights or property of you or Steplify.com.</p>
        <p>ALL RIGHTS RESERVED.</p>
        </div>

      <hr>
      <footer>
        <span class="pull-left"><%=Config.FOOTER_LEFT%></span>
        <span class="pull-right"><%=Config.FOOTER_RIGHT%></span>
      </footer>
    </div> <!--/.fluid-container-->

</body>

</html>