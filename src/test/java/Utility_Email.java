import com.steplify.utility.Emailer;
import com.steplify.utility.Config;
import org.junit.*;

import java.util.ArrayList;
import java.util.Date;

import static org.junit.Assert.assertTrue;

public class Utility_Email {

    @BeforeClass
    public static void oneTimeSetUp() {
        System.out.println("@BeforeClass - oneTimeSetUp");
        Config.useTestDB();
    }

    @AfterClass
    public static void oneTimeTearDown() {
        // one-time cleanup code
        System.out.println("@AfterClass - oneTimeTearDown");
    }

    @Test
    public void sendBasicEmail()
    {
        Date dt = new Date();

        ArrayList<String> alTo = new ArrayList<String>();
        alTo.add("sagans99@gmail.com");

        Emailer email = new Emailer(alTo, false, "Hello World from JUnit", dt.toString());
        //Call run() directly from unit tests; vs start() from app code
        email.run();
    }

    @Test
    public void sendBasicEmailMultipleAsBCC()
    {
        Date dt = new Date();

        ArrayList<String> alTo = new ArrayList<String>();
        alTo.add("sagans99@gmail.com");
        alTo.add("sagans@yahoo.com");

        Emailer email = new Emailer(alTo, true, "Hello World BCC from JUnit", dt.toString());
        //Call run() directly from unit tests; vs start() from app code
        email.run();
    }

}

