import com.steplify.controller.*;
import com.steplify.dao.*;
import com.steplify.dto.*;
import com.steplify.mpb.*;
import com.steplify.utility.Config;
import java.util.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Connection;

import static org.junit.Assert.assertTrue;

public class Servlet_AdminUser {

    @BeforeClass
    public static void oneTimeSetUp() {
        System.out.println("@BeforeClass - oneTimeSetUp");
        Config.useTestDB();
    }

    @AfterClass
    public static void oneTimeTearDown() {
        // one-time cleanup code
        System.out.println("@AfterClass - oneTimeTearDown");
    }

    @Test
    public void getProjectMembers() throws Exception
    {
        UserPrincipalDTO dtoUser;
        int iProjectID = 0;

        //Get a DB connection, do the work, and close it
        Connection connection = SuperDuperDAO.getConnection();
        try
        {
            //Use a users from Example_TestData
            dtoUser = UserPrincipalDAO.selectUserByName(connection, "1001@test.com");
            SortDashboardMPB sortMPB = new SortDashboardMPB();
            sortMPB.bOrderByProjectID = true;
            List<ProjectMemberDTO> lstMembershipsForUser = ProjectMemberDAO.selectProjectMembershipsForUser(connection, dtoUser.get_id(), sortMPB, false);
            ProjectMemberDTO dtoMember = lstMembershipsForUser.get(0);
            iProjectID = dtoMember.get_id_project();
        }
        finally
        {
            SuperDuperDAO.close(connection, false);
        }

        AdminUserServlet adminServlet = new AdminUserServlet();
        AdminUserViewMPB mpbAdminUserView = new AdminUserViewMPB();
        adminServlet.getMembershipsAndUsers(AdminUserServlet.MembershipAuthEnum.ADMIN, mpbAdminUserView, dtoUser, iProjectID);

        //Verify the number of memberships found
        assertTrue(mpbAdminUserView.lstMemberships.size() > 0);
        assertTrue(mpbAdminUserView.mapUsers.size() > 0);

        //TODO: Further tests can/should be created
        //Like check the contents of the returned objects
    }

}
