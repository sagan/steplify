import com.steplify.controller.*;
import com.steplify.dao.*;
import com.steplify.dto.*;
import com.steplify.mpb.*;
import com.steplify.utility.*;
import org.junit.*;

import javax.servlet.http.HttpServletResponse;

import static org.junit.Assert.*;

import java.sql.Connection;

public class Servlet_Dashboard {

    @BeforeClass
    public static void oneTimeSetUp() {
        System.out.println("@BeforeClass - oneTimeSetUp");
        Config.useTestDB();
    }

    @AfterClass
    public static void oneTimeTearDown() {
        // one-time cleanup code
        System.out.println("@AfterClass - oneTimeTearDown");
    }

    @Test
    public void getDashboard() throws Exception
    {
        UserPrincipalDTO dtoUser;

        //Get a DB connection, do the work, and close it
        Connection connection = SuperDuperDAO.getConnection();
        try
        {
            //Use a users from Example_TestData
            dtoUser = UserPrincipalDAO.selectUserByName(connection, "1001@test.com");
        }
        finally
        {
            SuperDuperDAO.close(connection, false);
        }

        DashboardServlet dashServlet = new DashboardServlet();
        SortDashboardMPB sortMPB = new SortDashboardMPB();
        DashboardMPB mpbDash = dashServlet.getDashboardUserData(dtoUser, sortMPB, false);

        //Verify the number of memberships found
        assertTrue(mpbDash.alUserMemberships.size() == 2);

        //TODO: Further tests can/should be created
        //Like check the contents of the returned objects
    }

    @Test
    public void archiveOne() throws Exception
    {
        UserPrincipalDTO dtoUser;

        //Get a DB connection, do the work, and close it
        Connection connection = SuperDuperDAO.getConnection();
        try
        {
            //Use a users from Example_TestData
            dtoUser = UserPrincipalDAO.selectUserByName(connection, "1002@test.com");
        }
        finally
        {
            SuperDuperDAO.close(connection, false);
        }

        DashboardServlet dashServlet = new DashboardServlet();
        SortDashboardMPB sortMPB = new SortDashboardMPB();
        DashboardMPB mpbDash = dashServlet.getDashboardUserData(dtoUser, sortMPB, false);

        if (mpbDash.alUserMemberships.size() > 0)
        {
            ProjectMemberDTO dtoPM = mpbDash.alUserMemberships.get(0);
            ProjectMembershipArchiveMPB mpbPM = new ProjectMembershipArchiveMPB();
            mpbPM._id = dtoPM.get_id();
            mpbPM._b_set_archived = true;

            //Archive it
            MockHttpServletResponse responseArchiveMembership = new MockHttpServletResponse();
            dashServlet.postArchiveProject(responseArchiveMembership, dtoUser, mpbPM);
            assertTrue(responseArchiveMembership.getStatus() == HttpServletResponse.SC_OK);

            //We should now have at least 1 archived project membership
            mpbDash = dashServlet.getDashboardUserData(dtoUser, sortMPB, true);
            assertTrue(mpbDash.alUserMemberships.size() > 0);

            //UnArchive it
            mpbPM._b_set_archived = false;
            MockHttpServletResponse responseUnArchiveMembership = new MockHttpServletResponse();
            dashServlet.postArchiveProject(responseUnArchiveMembership, dtoUser, mpbPM);
            assertTrue(responseUnArchiveMembership.getStatus() == HttpServletResponse.SC_OK);
        }
    }

}
