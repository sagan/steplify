import com.steplify.controller.*;
import com.steplify.mpb.*;
import com.steplify.utility.*;
import org.junit.*;
import static org.junit.Assert.*;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

public class Servlet_Login {

    private static Random _RANDOM_GENERATOR;

    @BeforeClass
    public static void oneTimeSetUp() {
        System.out.println("@BeforeClass - oneTimeSetUp");
        Config.useTestDB();
        _RANDOM_GENERATOR = new Random((new Date()).getTime());
    }

    @AfterClass
    public static void oneTimeTearDown() {
        // one-time cleanup code
        System.out.println("@AfterClass - oneTimeTearDown");
    }

    @Test
    public void registerAndLoginNewUser() throws Exception
    {
        //Very slight possibility you get a duplicate of an prior prefix
        int prefix = _RANDOM_GENERATOR.nextInt(100000000);
        LoginRegisterMPB mpb = new LoginRegisterMPB();
        String sOriginalUsername = String.valueOf(prefix) + "_abc@foo.com";
        mpb._s_email = sOriginalUsername;
        mpb._s_postal_code = "94040";

        //Lets register the new user
        MockHttpServletResponse responseReg = new MockHttpServletResponse();
        LoginServlet servletReg = new LoginServlet();
        servletReg.registerNewUser(responseReg, mpb);
        assertTrue(responseReg.getStatus() == HttpServletResponse.SC_OK);
        assertTrue(responseReg.getResponseOut().indexOf("created") != -1);

        //Lets fail login as the new user - bad password
        MockHttpServletResponse responseLoginFail = new MockHttpServletResponse();
        LoginServlet servletLoginFail = new LoginServlet();
        mpb._s_password = "mismatch";
        servletLoginFail.loginUser(responseLoginFail, mpb);
        assertTrue(responseLoginFail.getStatus() == HttpServletResponse.SC_BAD_REQUEST);
        assertTrue(responseLoginFail.getResponseOut().indexOf("invalid") != -1);

        //Lets fail login as the new user - email not found
        MockHttpServletResponse responseLoginFail2 = new MockHttpServletResponse();
        LoginServlet servletLoginFail2 = new LoginServlet();
        mpb._s_email = "mismatch" + mpb._s_email;
        mpb._s_password = "testing";
        servletLoginFail2.loginUser(responseLoginFail2, mpb);
        assertTrue(responseLoginFail2.getStatus() == HttpServletResponse.SC_BAD_REQUEST);
        assertTrue(responseLoginFail2.getResponseOut().indexOf("invalid") != -1);

        //Lets fail login as the new user - bad email
        MockHttpServletResponse responseLoginFail3 = new MockHttpServletResponse();
        LoginServlet servletLoginFail3 = new LoginServlet();
        mpb._s_email = "no_domain";
        mpb._s_password = "testing";
        servletLoginFail3.loginUser(responseLoginFail3, mpb);
        assertTrue(responseLoginFail3.getStatus() == HttpServletResponse.SC_BAD_REQUEST);
        assertTrue(responseLoginFail3.getResponseOut().indexOf("email") != -1);

        //Lets fail re-register the same user
        MockHttpServletResponse responseRegFail = new MockHttpServletResponse();
        LoginServlet servletRegFail = new LoginServlet();
        mpb._s_email = sOriginalUsername;
        servletRegFail.registerNewUser(responseRegFail, mpb);
        assertTrue(responseRegFail.getStatus() == HttpServletResponse.SC_BAD_REQUEST);
        assertTrue(responseRegFail.getResponseOut().indexOf("already registered") != -1);
    }

    @Test
    public void registerNewUserBadEmail() throws Exception
    {
        //Very slight possibility you get a duplicate of an prior prefix
        int prefix = _RANDOM_GENERATOR.nextInt(100000000);
        LoginRegisterMPB mpb = new LoginRegisterMPB();
        mpb._s_email = String.valueOf(prefix) + "_abc@foo";
        mpb._s_password = "testing";
        mpb._s_postal_code = "94040";

        MockHttpServletResponse response = new MockHttpServletResponse();
        LoginServlet servlet = new LoginServlet();
        servlet.registerNewUser(response, mpb);
        assertTrue(response.getStatus() == HttpServletResponse.SC_BAD_REQUEST);
        assertTrue(response.getResponseOut().toString().toLowerCase().indexOf("email") != -1);
    }

    @Test
    public void registerNewUserBadZipcode() throws Exception
    {
        //Very slight possibility you get a duplicate of an prior prefix
        int prefix = _RANDOM_GENERATOR.nextInt(100000000);
        LoginRegisterMPB mpb = new LoginRegisterMPB();
        mpb._s_email = String.valueOf(prefix) + "_abc@foo.com";
        mpb._s_password = "testing";
        mpb._s_postal_code = "";

        MockHttpServletResponse response = new MockHttpServletResponse();
        LoginServlet servlet = new LoginServlet();
        servlet.registerNewUser(response, mpb);
        assertTrue(response.getStatus() == HttpServletResponse.SC_BAD_REQUEST);
        assertTrue(response.getResponseOut().toString().toLowerCase().indexOf("postal") != -1);
    }

}
