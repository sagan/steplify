import com.steplify.dao.*;
import com.steplify.dto.*;
import com.steplify.utility.*;
import org.junit.*;
import java.sql.*;

import static org.junit.Assert.*;

public class Model_UserPrincipal {

    @BeforeClass
    public static void oneTimeSetUp() {
        System.out.println("@BeforeClass - oneTimeSetUp");
        Config.useTestDB();
    }

    @AfterClass
    public static void oneTimeTearDown() {
        // one-time cleanup code
        System.out.println("@AfterClass - oneTimeTearDown");
    }

    @Test
    public void addValidUserThenDelete() {
        UserPrincipalDTO dto = new UserPrincipalDTO();
        dto.setNewUsername("foo@BAR.com");
        dto.setNewPassword("abc456");
        dto.setStatusActive();

        //Get a DB connection, do the work, and close it
        Connection connection = SuperDuperDAO.getConnection();
        try
        {
            UserPrincipalDAO.insert(connection, dto);
            assertNotNull(dto.get_id());

            //See the user is in the DB, and that mixed case is ok
            UserPrincipalDTO newDTO = UserPrincipalDAO.selectUserByName(connection, "FOO@bar.com");
            assertTrue(newDTO.get_s_username().equals("foo@bar.com"));

            //Verify the password
            assertFalse(newDTO.verifyPassword("abc123"));
            assertTrue(newDTO.verifyPassword("abc456"));

            //Update the user account status
            newDTO.setStatusLocked();
            UserPrincipalDAO.updateUserStatus(connection, newDTO);
            UserPrincipalDTO dbDTO = UserPrincipalDAO.selectUserByName(connection, "FOO@bar.com");
            assertFalse(dbDTO.isAccountActive());

            newDTO.setNewPassword("aNewPass");
            UserPrincipalDAO.updateUserPassword(connection, newDTO);
            dbDTO = UserPrincipalDAO.selectUserByName(connection, "FOO@bar.com");
            assertTrue(dbDTO.verifyPassword("aNewPass"));

            String sBackupPass = newDTO.generateNewPasswordBackup();
            assertTrue(newDTO.verifyPassword(sBackupPass));
            UserPrincipalDAO.updateUserBackupPassword(connection, newDTO);
            dbDTO = UserPrincipalDAO.selectUserByName(connection, "FOO@bar.com");
            assertTrue(dbDTO.verifyPassword(sBackupPass));

            //Cleanup the user from the DB
            int iCountDelete = UserPrincipalDAO.deleteUser(connection, newDTO);
            assertTrue(iCountDelete == 1);
        }
        finally
        {
            SuperDuperDAO.close(connection, true);
        }
    }

    @Test
    public void updateUserProfile() {
        UserPrincipalDTO dto = new UserPrincipalDTO();
        dto.setNewUsername("baz@BAR.com");
        dto.setNewPassword("abc456");
        dto.setStatusActive();

        //Get a DB connection, do the work, and close it
        Connection connection = SuperDuperDAO.getConnection();
        try
        {
            UserPrincipalDAO.insert(connection, dto);

            UserProfileDTO dtoProfile = getStaticProfileDTO();
            dto.setUserProfile(dtoProfile);
            int iCountUpdate = UserPrincipalDAO.updateUserProfile(connection, dtoProfile);
            assertTrue(iCountUpdate == 1);

            //See the user is in the DB, and that mixed case is ok
            UserPrincipalDTO newDTO = UserPrincipalDAO.selectUserByName(connection, "baz@BAR.com");
            assertTrue(newDTO.getUserProfile().get_s_first_name().equals("Mickey"));

            //Cleanup the user from the DB
            int iCountDelete = UserPrincipalDAO.deleteUser(connection, newDTO);
            assertTrue(iCountDelete == 1);
        }
        finally
        {
            SuperDuperDAO.close(connection, true);
        }
    }

    @Test
    public void searchForMissingUser()
    {
        //Get a DB connection, do the work, and close it
        Connection connection = SuperDuperDAO.getConnection();
        try
        {
            //Put any ID you want that is not in the DB
            UserPrincipalDTO dto = UserPrincipalDAO.selectUserByName(connection, "unknown@bar.com");
            assertTrue(dto == null);
        }
        finally
        {
            SuperDuperDAO.close(connection, true);
        }
    }

    @Test(expected=RuntimeException.class)
     public void verifyPasswordRules1()
    {
        UserPrincipalDTO dto = new UserPrincipalDTO();
        dto.setNewPassword(null);
    }

    @Test(expected=RuntimeException.class)
    public void verifyPasswordRules2()
    {
        UserPrincipalDTO dto = new UserPrincipalDTO();
        dto.setNewPassword("abc");
    }

    @Test(expected=RuntimeException.class)
    public void verifyPasswordRules3()
    {
        UserPrincipalDTO dto = new UserPrincipalDTO();
        dto.setNewPassword("abc 123");
    }

    @Test(expected=RuntimeException.class)
    public void verifyEmailRules1()
    {
        UserPrincipalDTO dto = new UserPrincipalDTO();
        dto.setNewUsername(null);
    }

    @Test(expected=RuntimeException.class)
    public void verifyEmailRules2()
    {
        UserPrincipalDTO dto = new UserPrincipalDTO();
        dto.setNewUsername("user");
    }

    @Test(expected=RuntimeException.class)
    public void verifyEmailRules3()
    {
        UserPrincipalDTO dto = new UserPrincipalDTO();
        dto.setNewUsername("userwithnodomain");
    }

    @Test(expected=RuntimeException.class)
    public void verifyEmailRules4()
    {
        UserPrincipalDTO dto = new UserPrincipalDTO();
        dto.setNewUsername("userwithnodomain@");
    }

    @Test(expected=RuntimeException.class)
    public void verifyEmailRules5()
    {
        UserPrincipalDTO dto = new UserPrincipalDTO();
        dto.setNewUsername("@domainonly.com");
    }

    @Test
    public void generateNewPassword()
    {
        UserPrincipalDTO dto = new UserPrincipalDTO();
        String sClearPassword = dto.generateNewPasswordBackup();
        System.out.println("NewPassword=" + sClearPassword);
        assertTrue(sClearPassword != null);

        //Verify the new password is usable
        System.out.println("NewHash=" + dto.get_s_password_backup_hash());
        boolean bMatch = dto.verifyPassword(sClearPassword);
        assertTrue(bMatch);
    }

    //This is a reusable method across the test suite
    public static UserProfileDTO getStaticProfileDTO()
    {
        UserProfileDTO dtoProfile = new UserProfileDTO();
        dtoProfile.set_s_first_name("Mickey");
        dtoProfile.set_s_last_name("Mouse");
        dtoProfile.set_s_url_personal_img("http://upload.wikimedia.org/wikipedia/en/thumb/d/d4/Mickey_Mouse.png/220px-Mickey_Mouse.png");
        dtoProfile.set_s_company_name("Disney");
        dtoProfile.set_s_url_company_img("http://upload.wikimedia.org/wikipedia/commons/thumb/6/65/TWDC_Logo.svg/250px-TWDC_Logo.svg.png");
        dtoProfile.set_s_address("1675 N Buena Vista Dr");
        dtoProfile.set_s_city("Lake Buena Vista");
        dtoProfile.set_s_state("Florida");
        dtoProfile.set_s_country("USA");
        dtoProfile.set_s_postal_code("32830");
        dtoProfile.set_s_phone_number("(407) 934-7639");
        dtoProfile.set_s_url_website("http://disney.com");
        return dtoProfile;
    }

}
