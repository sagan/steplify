import java.util.*;

/*
    This class is used to generate the code for DAO and DTO classes
 */

@SuppressWarnings("unchecked")
public class Example_SQL_Generator {

    private static String _S_DB_TABLE_NAME;
    private static ArrayList<String> _AL_DB_COLUMNS = new ArrayList<String>();
    private static ArrayList<String> _AL_INSERT_COLUMNS;
    private static ArrayList<String> _AL_UPDATE_COLUMNS;
    private static ArrayList<String> _AL_DTO_ATTRIBUTES;
    private static String _S_CLASS_NAME;
    private static String _NL = "\n";   //newline

    public static void main(String[] args)
    {
        //Configure your inputs
        _S_CLASS_NAME = "TopicMemberDTO";
        _S_DB_TABLE_NAME = "topic_authority";
        _AL_DB_COLUMNS.add("_id");
        _AL_DB_COLUMNS.add("_id_created_by");
        _AL_DB_COLUMNS.add("_id_updated_by");
        _AL_DB_COLUMNS.add("_dt_create_date");
        _AL_DB_COLUMNS.add("_dt_last_update");
        _AL_DB_COLUMNS.add("_id_user_principal");
        _AL_DB_COLUMNS.add("_s_cd_authority");
        _AL_DB_COLUMNS.add("_id_project");
        _AL_DB_COLUMNS.add("_id_topic");

        init();
        sqlInsert();
        sqlInsertPS();
        sqlUpdate();
        sqlUpdatePS();
        sqlDelete();
        sqlDeletePS();
        sqlSelect();
        sqlSelectPS();
        dtoAttributes();
        dtoMethods();

    }

    private static void init()
    {
        //Ignore certain fields for INSERT
        _AL_INSERT_COLUMNS = (ArrayList<String>)_AL_DB_COLUMNS.clone();
        _AL_INSERT_COLUMNS.remove("_id");

        //Ignore certain fields for UPDATE
        _AL_UPDATE_COLUMNS = (ArrayList<String>)_AL_DB_COLUMNS.clone();
        _AL_UPDATE_COLUMNS.remove("_id");
        _AL_UPDATE_COLUMNS.remove("_id_created_by");
        _AL_UPDATE_COLUMNS.remove("_dt_create_date");

        _AL_DTO_ATTRIBUTES = (ArrayList<String>)_AL_DB_COLUMNS.clone();
        _AL_DTO_ATTRIBUTES.remove("_id");
        _AL_DTO_ATTRIBUTES.remove("_id_created_by");
        _AL_DTO_ATTRIBUTES.remove("_dt_create_date");
        _AL_DTO_ATTRIBUTES.remove("_id_updated_by");
        _AL_DTO_ATTRIBUTES.remove("_dt_last_update");
    }

    private static void write(String sName, String sResult)
    {
        System.out.println("%%%%%%%%%%%%%" + sName + "%%%%%%%%%%%%%%%%%%%%%");
        System.out.println(sResult);
        System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
    }

    private static String getDbTypeSetter(String sColumnName)
    {
        if (sColumnName.startsWith("_id"))
        {
            return "setInt";
        }
        else if (sColumnName.startsWith("_i_"))
        {
            return "setInt";
        }
        else if (sColumnName.startsWith("_dt_"))
        {
            return "setTimestamp";
        }
        else if (sColumnName.startsWith("_s_"))
        {
            return "setString";
        }
        else if (sColumnName.startsWith("_b_"))
        {
            return "setInt";
        }
        else
        {
            throw new RuntimeException("Unknown Type for getDbTypeSetter() - " + sColumnName);
        }
    }

    private static String getDbTypeGetter(String sColumnName)
    {
        if (sColumnName.startsWith("_id"))
        {
            return "getInt";
        }
        else if (sColumnName.startsWith("_i_"))
        {
            return "getInt";
        }
        else if (sColumnName.startsWith("_dt_"))
        {
            return "getTimestamp";
        }
        else if (sColumnName.startsWith("_s_"))
        {
            return "getString";
        }
        else if (sColumnName.startsWith("_b_"))
        {
            return "getInt(TODO) == 0 ? false : true";
        }
        else
        {
            throw new RuntimeException("Unknown Type for getDbTypeGetter() - " + sColumnName);
        }
    }

    private static String getObjectAttributeGetter(String sColumnName)
    {
        if (sColumnName.startsWith("_dt_"))
        {
            return "new Timestamp(lTimeNow)";
        }
        else if (sColumnName.startsWith("_b_"))
        {
            return String.format("dto.get%s() ? 1 : 0", sColumnName);
        }
        else
        {
            return String.format("dto.get%s()", sColumnName);
        }
    }

    private static String getObjectAttributeSetter(String sColumnName)
    {
        return String.format("dto.set%s", sColumnName);
    }

    private static String getJavaDataType(String sColumnName)
    {
        if (sColumnName.startsWith("_id"))
        {
            return "int";
        }
        else if (sColumnName.startsWith("_i_"))
        {
            return "int";
        }
        else if (sColumnName.startsWith("_dt_"))
        {
            return "Date";
        }
        else if (sColumnName.startsWith("_s_"))
        {
            return "String";
        }
        else if (sColumnName.startsWith("_b_"))
        {
            return "boolean";
        }
        else
        {
            throw new RuntimeException("Unknown Type for getJavaDataType() - " + sColumnName);
        }
    }

    //////////////////////////////////////////////////////////////////////

    private static void sqlInsert()
    {
        StringBuilder sb = new StringBuilder();

        //String sSQL = "insert into user_principal " +
        sb.append(String.format("String sSQL = \"insert into %s \" +", _S_DB_TABLE_NAME));
        sb.append(_NL);

        //"(_dt_create_date, _dt_last_update, _s_username, _s_password_hash, _s_password_backup_hash, _s_cd_status) " +
        sb.append("\"(");
        for (int i=0; i<_AL_INSERT_COLUMNS.size(); i++)
        {
            String sColumn = _AL_INSERT_COLUMNS.get(i);
            if (i != 0)
            {
                sb.append(", ");
            }
            sb.append(sColumn);
        }
        sb.append(") \" +");
        sb.append(_NL);

        //"values (?, ?, ?, ?, ?, ?)";
        sb.append("\"values (");
        for (int i=0; i<_AL_INSERT_COLUMNS.size(); i++)
        {
            if (i != 0)
            {
                sb.append(", ");
            }
            sb.append("?");
        }
        sb.append(String.format(")\";"));

        write("SQL_INSERT", sb.toString());
    }

    private static void sqlInsertPS()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("preparedStatement = connection.prepareStatement(sSQL, Statement.RETURN_GENERATED_KEYS);");
        sb.append(_NL);
        sb.append("long lTimeNow = (Calendar.getInstance()).getTimeInMillis();");
        sb.append(_NL);
        sb.append(_NL);
        //preparedStatement.setInt(1, dtoUser.get_id());
        //preparedStatement.setTimestamp(2, new Timestamp(lTimeNow));
        //preparedStatement.setString(3, dto.get_s_first_name());
        for (int i=0; i<_AL_INSERT_COLUMNS.size(); i++)
        {
            String sColumn = _AL_INSERT_COLUMNS.get(i);
            String sDbTypeSetter = getDbTypeSetter(sColumn);
            String sValueAccessor = getObjectAttributeGetter(sColumn);
            sb.append(String.format("preparedStatement.%s(%s, %s);", sDbTypeSetter, i+1, sValueAccessor));
            sb.append(_NL);
        }

        write("SQL_INSERT_PS", sb.toString());
    }

    //////////////////////////////////////////////////////////////////////

    private static void sqlUpdate()
    {
        StringBuilder sb = new StringBuilder();

        //String sSQL = "update user_principal " +
        sb.append(String.format("String sSQL = \"update %s \" +", _S_DB_TABLE_NAME));
        sb.append(_NL);

        //"set _dt_last_update=?, _s_password_hash=? " +
        sb.append("\"set ");
        for (int i=0; i<_AL_UPDATE_COLUMNS.size(); i++)
        {
            String sColumn = _AL_UPDATE_COLUMNS.get(i);
            if (i != 0)
            {
                sb.append(", ");
            }
            sb.append(String.format("%s=?", sColumn));
        }
        sb.append(" \" +");
        sb.append(_NL);

        //"where _id=?";
        sb.append("\"where _id=?\";");
        sb.append(_NL);

        write("SQL_UPDATE", sb.toString());
    }

    private static void sqlUpdatePS()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("preparedStatement = connection.prepareStatement(sSQL);");
        sb.append(_NL);
        sb.append("long lTimeNow = (Calendar.getInstance()).getTimeInMillis();");
        sb.append(_NL);
        sb.append(_NL);
        //preparedStatement.setTimestamp(1, new Timestamp(lTimeNow));
        //preparedStatement.setString(2, dto.get_s_password_hash());
        for (int i=0; i<_AL_UPDATE_COLUMNS.size(); i++)
        {
            String sColumn = _AL_UPDATE_COLUMNS.get(i);
            String sDbTypeSetter = getDbTypeSetter(sColumn);
            String sValueAccessor = getObjectAttributeGetter(sColumn);
            sb.append(String.format("preparedStatement.%s(%s, %s);", sDbTypeSetter, i+1, sValueAccessor));
            sb.append(_NL);
        }

        //The where column is always '_id'
        //preparedStatement.setInt(3, dto.get_id());
        int iParamIndex = _AL_UPDATE_COLUMNS.size() + 1;
        String sColumn = "_id";
        String sDbTypeSetter = getDbTypeSetter(sColumn);
        String sValueAccessor = getObjectAttributeGetter(sColumn);
        sb.append(String.format("preparedStatement.%s(%s, %s);", sDbTypeSetter, iParamIndex, sValueAccessor));
        sb.append(_NL);

        write("SQL_UPDATE_PS", sb.toString());
    }

    //////////////////////////////////////////////////////////////////////

    private static void sqlDelete()
    {
        StringBuilder sb = new StringBuilder();

        //String sSQL = "delete from user_principal where _id=?";
        sb.append(String.format("String sSQL = \"delete from %s where _id=?\";", _S_DB_TABLE_NAME));
        sb.append(_NL);

        write("SQL_DELETE", sb.toString());
    }

    private static void sqlDeletePS()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("preparedStatement = connection.prepareStatement(sSQL);");
        sb.append(_NL);

        //The where column is always '_id'
        //preparedStatement.setInt(1, anID);
        sb.append("preparedStatement.setInt(1, anID);");
        sb.append(_NL);

        write("SQL_DELETE_PS", sb.toString());
    }

    //////////////////////////////////////////////////////////////////////

    private static void sqlSelect()
    {
        StringBuilder sb = new StringBuilder();

        //String sSQL = "select _id, _dt_create_date, _i_order, _s_category, _s_code, _s_decode, _b_active " +
        sb.append("String sSQL = \"select ");
        for (int i=0; i<_AL_DB_COLUMNS.size(); i++)
        {
            String sColumn = _AL_DB_COLUMNS.get(i);
            if (i != 0)
            {
                sb.append(", ");
            }
            sb.append(sColumn);
        }
        sb.append(" \" +");
        sb.append(_NL);

        //"from code_decode where _s_category=? and _s_code=? order by _id";
        sb.append(String.format("\"from %s where _id=?\";", _S_DB_TABLE_NAME));

        write("SQL_SELECT", sb.toString());
    }

    private static void sqlSelectPS()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("preparedStatement.setInt(1, anID);");
        sb.append(_NL);
        sb.append(_NL);
        sb.append(String.format("%s dto = new %s();", _S_CLASS_NAME, _S_CLASS_NAME));
        sb.append(_NL);
        //dto.set_id(resultSet.getInt(1));
        //dto.set_dt_create_date(resultSet.getTimestamp(2));
        //dto.set_i_order(resultSet.getInt(3));
        //dto.set_s_category(resultSet.getString(4));
        //dto.set_s_code(resultSet.getString(5));
        //dto.set_s_decode(resultSet.getString(6));
        //dto.set_b_active(resultSet.getInt(7) == 0 ? false : true);
        for (int i=0; i<_AL_DB_COLUMNS.size(); i++)
        {
            String sColumn = _AL_DB_COLUMNS.get(i);
            String sDbTypeGetter = getDbTypeGetter(sColumn);
            String sValueSetter = getObjectAttributeSetter(sColumn);
            sb.append(String.format("%s(resultSet.%s(%s));", sValueSetter, sDbTypeGetter, i+1));
            sb.append(_NL);
        }

        write("SQL_SELECT_PS", sb.toString());
    }

    //////////////////////////////////////////////////////////////////////

    private static void dtoAttributes()
    {
        StringBuilder sb = new StringBuilder();
        /*
        private int _i_order;
        private String _s_category;
        private String _s_code;
        private String _s_decode;
        private boolean _b_active;
         */
        for (int i=0; i<_AL_DTO_ATTRIBUTES.size(); i++)
        {
            //First the Getter
            String sColumn = _AL_DTO_ATTRIBUTES.get(i);
            String sJavaType = getJavaDataType(sColumn);
            sb.append(String.format("private %s %s;", sJavaType, sColumn));
            sb.append(_NL);
        }

        write("DTO_ATTRIBUTES", sb.toString());
    }

    private static void dtoMethods()
    {
        StringBuilder sb = new StringBuilder();
        /*
        public String get_s_username()
        {
            return this._s_username;
        }

        public void set_s_username(String sUsername)
        {
            this._s_username = sUsername;
        }
         */
        for (int i=0; i<_AL_DTO_ATTRIBUTES.size(); i++)
        {
            //First the Getter
            String sColumn = _AL_DTO_ATTRIBUTES.get(i);
            String sJavaType = getJavaDataType(sColumn);
            sb.append(String.format("public %s get%s()", sJavaType, sColumn));
            sb.append(_NL);
            sb.append("{");
            sb.append(_NL);
            sb.append(String.format("\treturn this.%s;", sColumn));
            sb.append(_NL);
            sb.append("}");
            sb.append(_NL);
            sb.append(_NL);

            //Then the Setter
            String sParameter = "new" + sColumn;
            sb.append(String.format("public void set%s(%s %s)", sColumn, sJavaType, sParameter));
            sb.append(_NL);
            sb.append("{");
            sb.append(_NL);
            sb.append(String.format("\tthis.%s = %s;", sColumn, sParameter));
            sb.append(_NL);
            sb.append("}");
            sb.append(_NL);
            sb.append(_NL);
        }

        write("DTO_METHODS", sb.toString());
    }

}
