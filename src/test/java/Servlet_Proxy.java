import com.steplify.controller.*;
import com.steplify.utility.*;
import org.junit.*;
import static org.junit.Assert.*;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

public class Servlet_Proxy {

    private static Random _RANDOM_GENERATOR;

    @BeforeClass
    public static void oneTimeSetUp() {
        System.out.println("@BeforeClass - oneTimeSetUp");
        Config.useTestDB();
        _RANDOM_GENERATOR = new Random((new Date()).getTime());
    }

    @AfterClass
    public static void oneTimeTearDown() {
        // one-time cleanup code
        System.out.println("@AfterClass - oneTimeTearDown");
    }

    @Test
    public void getGoogleImage() throws Exception
    {
        String sURL_GoogleImage = "https://www.google.com/images/srpr/logo4w.png";
        MockHttpServletResponse responseGoogleImage = new MockHttpServletResponse();
        ProxyServlet servletProxy = new ProxyServlet();
        servletProxy.getRequest(responseGoogleImage, sURL_GoogleImage);
        assertTrue(responseGoogleImage.getStatus() == HttpServletResponse.SC_OK);
        assertFalse(responseGoogleImage.getResponseOut().toString().equals(""));
        assertTrue(responseGoogleImage.getContentType().indexOf("image") != -1);
    }

}
