import com.steplify.utility.AppSession;
import com.steplify.utility.Config;
import org.junit.*;
import java.util.Date;

import static org.junit.Assert.assertTrue;

public class Utility_AppSession {

    @BeforeClass
    public static void oneTimeSetUp() {
        System.out.println("@BeforeClass - oneTimeSetUp");
        Config.useTestDB();
    }

    @AfterClass
    public static void oneTimeTearDown() {
        // one-time cleanup code
        System.out.println("@AfterClass - oneTimeTearDown");
    }

    @Test
    public void validateCookieEncryption()
    {
        String sClearUserID = "HereIs.1.TestToken";
        System.out.println(sClearUserID);

        String sEncryptedPassword = AppSession.encryptToken(sClearUserID);
        System.out.println(sEncryptedPassword);
        System.out.println("Length=" + sEncryptedPassword.length());

        String sDecryptedPassword = AppSession.decryptToken(sEncryptedPassword);
        System.out.println(sDecryptedPassword);

        assertTrue(sClearUserID.equals(sDecryptedPassword));
    }

    @Test
    public void validateCookieEncryptionForNull()
    {
        String sClearUserID = null;
        System.out.println(sClearUserID);

        String sEncryptedPassword = AppSession.encryptToken(sClearUserID);
        System.out.println(sEncryptedPassword);

        String sDecryptedPassword = AppSession.decryptToken(sEncryptedPassword);
        System.out.println(sDecryptedPassword);

        assert(sDecryptedPassword == null);
    }

    @Test
    public void validateCookieEncryptionSpeedTest()
    {
        String sClearUserID = "ThisIs111For000Speed...And...Performance888Testing444";
        System.out.println(sClearUserID);
        for (int i=0; i<100; i++)
        {
            String sEncryptedPassword = AppSession.encryptToken(sClearUserID);
            String sDecryptedPassword = AppSession.decryptToken(sEncryptedPassword);
            assertTrue(sClearUserID.equals(sDecryptedPassword));
        }
    }

}

