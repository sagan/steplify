import com.steplify.utility.Config;
import com.steplify.utility.UI;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import java.util.*;

import static org.junit.Assert.assertTrue;

public class Utility_UI {

    @BeforeClass
    public static void oneTimeSetUp() {
        System.out.println("@BeforeClass - oneTimeSetUp");
        Config.useTestDB();
    }

    @AfterClass
    public static void oneTimeTearDown() {
        // one-time cleanup code
        System.out.println("@AfterClass - oneTimeTearDown");
    }

    @Test
    public void print()
    {
        assertTrue(UI.printAndEscape(null).equals(""));
        assertTrue(UI.printAndEscape("foo").equals("foo"));
        assertTrue(UI.printAndEscape("First \\ ever's <Apartment> \"for\" us").equals("First \\\\ ever\\'s &lt;Apartment&gt; &quot;for&quot; us"));
    }

    @Test
    public void printHTML()
    {
        assertTrue(UI.printAndEscapeForHTML(null).equals(""));
        assertTrue(UI.printAndEscapeForHTML("foo").equals("foo"));
    }

    @Test
    public void printJS()
    {
        assertTrue(UI.printAndEscapeForJS(null).equals(""));
        assertTrue(UI.printAndEscapeForJS("foo").equals("foo"));
    }

    @Test
    public void nullCheck()
    {
        assertTrue(UI.isNullOrEmpty(null) == true);
        assertTrue(UI.isNullOrEmpty("") == true);
        assertTrue(UI.isNullOrEmpty(" ") == true);
        assertTrue(UI.isNullOrEmpty("foo") == false);
    }

    @Test
    public void createDate()
    {
        Date dt = UI.createDate(2013, 1, 1);
        assertTrue(dt.toString().toLowerCase().contains("jan"));
    }


}

