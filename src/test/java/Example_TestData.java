import com.steplify.dao.*;
import com.steplify.dto.*;
import com.steplify.utility.*;
import org.junit.*;

import java.sql.Connection;

/*
This class is not for actual tests, but to create SEED data for some tests.
It should be run manually before running your tests suite.
 */

public class Example_TestData {

    @BeforeClass
    public static void oneTimeSetUp() {
        System.out.println("@BeforeClass - oneTimeSetUp");
        System.out.println("Make sure to LOAD the TEST data for your tests! Uncomment the @Test method(s) below...");
        Config.useTestDB();
    }

    @AfterClass
    public static void oneTimeTearDown() {
        // one-time cleanup code
        System.out.println("@AfterClass - oneTimeTearDown");
    }

    //Uncomment this when you want to add data; this should be run first
    //@Test
    public void makeTwoUsers()
    {
        //Get a DB connection, do the work, and close it
        Connection connection = SuperDuperDAO.getConnection();
        try
        {
            //User1 + Yes Profile
            UserPrincipalDTO dtoUser1 = new UserPrincipalDTO();
            dtoUser1.setNewUsername("1001@test.com");
            dtoUser1.setNewPassword("just4testing");
            dtoUser1.setStatusActive();
            UserPrincipalDAO.insert(connection, dtoUser1);

            UserProfileDTO dtoProfile1 = dtoUser1.getUserProfile();
            dtoProfile1.set_s_first_name("Mr Unit");
            dtoProfile1.set_s_last_name("Test1");
            dtoProfile1.set_s_postal_code("94040");
            dtoProfile1.set_s_url_personal_img("https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash3/c17.17.216.216/s160x160/481278_10151554454437806_1793516101_n.jpg");
            UserPrincipalDAO.updateUserProfile(connection, dtoProfile1);

            //User2 + No Profile, this will be the referral
            UserPrincipalDTO dtoUser2 = new UserPrincipalDTO();
            dtoUser2.set_id(1002);
            dtoUser2.setNewUsername("1002@test.com");
            dtoUser2.setNewPassword("just4testing");
            dtoUser2.setStatusActive();
            UserPrincipalDAO.insert(connection, dtoUser2);

            //The last thing to do, but not a finally block
            SuperDuperDAO.close(connection, true);
        }
        catch (Exception ex)
        {
            SuperDuperDAO.close(connection, false);
            throw new RuntimeException(ex);
        }
    }

    //Uncomment this when you want to add data
    //@Test
    public void makeTwoPropertiesForUser1()
    {
        //Get a DB connection, do the work, and close it
        Connection connection = SuperDuperDAO.getConnection();
        try
        {
            UserPrincipalDTO dtoUser1 = UserPrincipalDAO.selectUserByName(connection, "1001@test.com");
            UserPrincipalDTO dtoUser2 = UserPrincipalDAO.selectUserByName(connection, "1002@test.com");

            ////////////////////////////////////

            //A first project-property - #A
            ProjectDTO dtoPP1 = new ProjectDTO();
            dtoPP1.set_s_description("First home of Mr 1001");
            dtoPP1.set_s_address("1001 Main Street, UNIT_A");
            dtoPP1.set_s_city("Springfield");
            dtoPP1.set_s_state("Illinois");
            dtoPP1.set_s_country("USA");
            dtoPP1.set_s_postal_code("62704");
            dtoPP1.set_s_url_website("www.cnn.com");
            dtoPP1.set_s_url_image("http://i.cdn.turner.com/cnn/.e/img/3.0/global/header/hdr-main.png");
            dtoPP1.set_s_cd_project_type("HOME_SALE");
            dtoPP1.set_id_created_by(dtoUser1.get_id());
            dtoPP1.set_id_updated_by(dtoUser1.get_id());
            ProjectDAO.insertProject(connection, dtoPP1);

            //User1 membership for Project-Property A, referred by self
            ProjectMemberDTO dtoPM1 = new ProjectMemberDTO();
            dtoPM1.setStatusActive();
            dtoPM1.setRoleToOther();
            dtoPM1.set_b_is_project_admin(true);
            dtoPM1.set_id_user_principal(dtoUser1.get_id());
            dtoPM1.set_id_created_by(dtoUser1.get_id());
            dtoPM1.set_id_updated_by(dtoUser1.get_id());
            dtoPM1.set_id_project(dtoPP1.get_id());
            ProjectMemberDAO.insertMembership(connection, dtoPM1);

            //User2 membership for Project-Property A, referred by User1
            ProjectMemberDTO dtoPM2 = new ProjectMemberDTO();
            dtoPM2.setStatusInvited();
            dtoPM2.setRoleToOther();
            dtoPM1.set_b_is_project_admin(false);
            dtoPM2.set_id_user_principal(dtoUser2.get_id());
            dtoPM2.set_id_created_by(dtoUser1.get_id());
            dtoPM2.set_id_updated_by(dtoUser1.get_id());
            dtoPM2.set_id_project(dtoPP1.get_id());
            ProjectMemberDAO.insertMembership(connection, dtoPM2);

            TopicDTO dtoTopic = new TopicDTO();
            dtoTopic.set_id_created_by(dtoUser1.get_id());
            dtoTopic.set_id_updated_by(dtoUser1.get_id());
            dtoTopic.set_s_name("GENERAL");
            dtoTopic.set_s_description("Here is a common area");
            dtoTopic.set_s_url_attachments("http://dropbox.com");
            dtoTopic.set_b_auto_add_members(true);
            dtoTopic.set_i_order(1);
            dtoPP1.addTopic(dtoTopic);
            TopicDAO.insertTopic(connection, dtoTopic);

            TopicDTO dtoTopic2 = new TopicDTO();
            dtoTopic2.set_id_created_by(dtoUser1.get_id());
            dtoTopic2.set_id_updated_by(dtoUser1.get_id());
            dtoTopic2.set_s_name("FINANCE");
            dtoTopic2.set_s_description("Money is a private area");
            dtoTopic2.set_s_url_attachments(null);
            dtoTopic2.set_b_auto_add_members(false);
            dtoTopic2.set_i_order(2);
            dtoPP1.addTopic(dtoTopic2);
            TopicDAO.insertTopic(connection, dtoTopic2);

            TopicMemberDTO dtoMem = new TopicMemberDTO();
            dtoMem.set_id_created_by(dtoUser1.get_id());
            dtoMem.set_id_updated_by(dtoUser1.get_id());
            dtoMem.set_id_user_principal(dtoUser1.get_id());
            dtoMem.setAuthorityWriter();
            dtoTopic.addTopicMember(dtoMem);
            TopicMemberDAO.insertTopicMember(connection, dtoMem);

            TopicMemberDTO dtoMem2 = new TopicMemberDTO();
            dtoMem2.set_id_created_by(dtoUser1.get_id());
            dtoMem2.set_id_updated_by(dtoUser1.get_id());
            dtoMem2.set_id_user_principal(dtoUser1.get_id());
            dtoMem2.setAuthorityReader();
            dtoTopic2.addTopicMember(dtoMem2);
            TopicMemberDAO.insertTopicMember(connection, dtoMem2);

            TaskDTO dtoTask = new TaskDTO();
            dtoTask.set_id_created_by(dtoUser1.get_id());
            dtoTask.set_id_updated_by(dtoUser1.get_id());
            dtoTask.set_s_name("Public Forum");
            dtoTask.set_s_description("Let all speak here");
            dtoTask.set_i_order(1);
            dtoTask.setStatusStuck();
            dtoTask.set_dt_due(UI.createDate(2011, 11, 19));
            dtoTask.set_id_assigned_user(dtoUser1.get_id());
            dtoTopic.addTask(dtoTask);
            TaskDAO.insertTask(connection, dtoTask);

            TaskDTO dtoTask2 = new TaskDTO();
            dtoTask2.set_id_created_by(dtoUser1.get_id());
            dtoTask2.set_id_updated_by(dtoUser1.get_id());
            dtoTask2.set_s_name("Call Bank");
            dtoTask2.set_s_description("Need to call the bank ASAP");
            dtoTask2.set_i_order(1);
            dtoTask2.setStatusCompleted();
            dtoTask2.set_dt_due(UI.createDate(2013, 8, 29));
            dtoTask2.set_id_assigned_user(dtoUser2.get_id());
            dtoTopic2.addTask(dtoTask2);
            TaskDAO.insertTask(connection, dtoTask2);

            TaskDTO dtoTask3 = new TaskDTO();
            dtoTask3.set_id_created_by(dtoUser1.get_id());
            dtoTask3.set_id_updated_by(dtoUser1.get_id());
            dtoTask3.set_s_name("Get Down Payment");
            dtoTask3.set_s_description("Get the money ready");
            dtoTask3.set_i_order(4);
            dtoTask3.setStatusNew();
            dtoTask3.set_dt_due(UI.createDate(2010, 2, 23));
            dtoTask3.set_id_assigned_user(dtoUser1.get_id());
            dtoTopic2.addTask(dtoTask3);
            TaskDAO.insertTask(connection, dtoTask3);

            TaskCommentDTO dtoComment = new TaskCommentDTO();
            dtoComment.set_id_created_by(dtoUser2.get_id());
            dtoComment.set_id_updated_by(dtoUser2.get_id());
            dtoComment.set_s_comment("I called today, but there was no answer...");
            dtoTask2.addTaskComment(dtoComment);
            TaskCommentDAO.insertTaskComment(connection, dtoComment);

            TaskCommentDTO dtoComment1 = new TaskCommentDTO();
            dtoComment1.set_id_created_by(dtoUser1.get_id());
            dtoComment1.set_id_updated_by(dtoUser1.get_id());
            dtoComment1.set_s_comment("I am looking in the couch!");
            dtoTask3.addTaskComment(dtoComment1);
            TaskCommentDAO.insertTaskComment(connection, dtoComment1);

            TaskCommentDTO dtoComment2 = new TaskCommentDTO();
            dtoComment2.set_id_created_by(dtoUser2.get_id());
            dtoComment2.set_id_updated_by(dtoUser2.get_id());
            dtoComment2.set_s_comment("Hey, I found some 'change', how \"much\" do we need?");
            dtoTask3.addTaskComment(dtoComment2);
            TaskCommentDAO.insertTaskComment(connection, dtoComment2);

            //Pause so there is time between the Project-Property create Timestamp;
            //for sorting testing
            try
            {
                Thread.sleep(2000);
            }
            catch (Exception ex)
            {
                //Forget about it...
            }

            //A second project-property - #B
            ProjectDTO dtoPP2 = new ProjectDTO();
            dtoPP2.set_s_description("Second home of Mr 1001");
            dtoPP2.set_s_address("1001 Park Street, UNIT-B");
            dtoPP2.set_s_city("Mountain View");
            dtoPP2.set_s_state("California");
            dtoPP2.set_s_url_website("www.google.com");
            dtoPP2.set_s_url_image("https://www.google.com/images/srpr/logo6w.png");
            dtoPP2.set_s_cd_project_type("REMODEL");
            dtoPP2.set_id_created_by(dtoUser1.get_id());
            dtoPP2.set_id_updated_by(dtoUser1.get_id());
            ProjectDAO.insertProject(connection, dtoPP2);

            //User1 membership for Project-Property B, referred by self
            ProjectMemberDTO dtoPM3 = new ProjectMemberDTO();
            dtoPM3.setStatusActive();
            dtoPM3.setRoleToOther();
            dtoPM3.set_b_is_project_admin(true);
            dtoPM3.set_id_user_principal(dtoUser1.get_id());
            dtoPM3.set_id_created_by(dtoUser1.get_id());
            dtoPM3.set_id_updated_by(dtoUser1.get_id());
            dtoPM3.set_id_project(dtoPP2.get_id());
            ProjectMemberDAO.insertMembership(connection, dtoPM3);

            //The last thing to do, but not a finally block
            SuperDuperDAO.close(connection, true);
        }
        catch (Exception ex)
        {
            SuperDuperDAO.close(connection, false);
            throw new RuntimeException(ex);
        }
    }

}

