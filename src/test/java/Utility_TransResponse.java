import com.steplify.utility.TransResponse;
import com.steplify.utility.Config;
import org.junit.*;

import static org.junit.Assert.assertTrue;

public class Utility_TransResponse {

    @BeforeClass
    public static void oneTimeSetUp() {
        System.out.println("@BeforeClass - oneTimeSetUp");
        Config.useTestDB();
    }

    @AfterClass
    public static void oneTimeTearDown() {
        // one-time cleanup code
        System.out.println("@AfterClass - oneTimeTearDown");
    }

    @Test
    public void transOK()
    {
        TransResponse tr = new TransResponse();
        assertTrue(tr.isSuccess());
    }

    @Test
    public void transOkWithWarnings()
    {
        TransResponse tr = new TransResponse();
        tr.addWarning("WARN1");
        assertTrue(tr.isSuccess());
    }
    @Test
    public void transFailWithExceptions()
    {
        TransResponse tr = new TransResponse();
        tr.addException("EX1");
        assertTrue(!tr.isSuccess());
    }

    @Test
    public void transFailWithWarningAndExceptions()
    {
        TransResponse tr = new TransResponse();
        tr.addWarning("WARN1");
        tr.addException("EX1");
        assertTrue(!tr.isSuccess());
    }

    @Test
    public void transFailCheckMessages()
    {
        TransResponse tr = new TransResponse();
        tr.addWarning("WARN1");
        tr.addException("EX1");
        assertTrue(tr.hasWarnings());
        assertTrue(tr.hasExceptions());
    }

    @Test
    public void transOkGetMessages()
    {
        TransResponse tr = new TransResponse();
        assertTrue(tr.getWarnings().size() == 0);
        assertTrue(tr.getExceptions().size() == 0);
    }

    @Test
    public void transFailGetMessages()
    {
        TransResponse tr = new TransResponse();
        tr.addWarning("WARN1");
        tr.addException("EX1");
        assertTrue(tr.getWarnings().size() > 0);
        assertTrue(tr.getExceptions().size() > 0);
    }


}

