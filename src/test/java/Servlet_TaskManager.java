import com.steplify.controller.TaskManagerServlet;
import com.steplify.dao.ProjectMemberDAO;
import com.steplify.dao.SuperDuperDAO;
import com.steplify.dao.UserPrincipalDAO;
import com.steplify.dto.ProjectMemberDTO;
import com.steplify.dto.TaskDTO;
import com.steplify.dto.UserPrincipalDTO;
import com.steplify.mpb.TaskManagerViewMPB;
import com.steplify.mpb.SortDashboardMPB;
import com.steplify.utility.Config;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Connection;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class Servlet_TaskManager {

    @BeforeClass
    public static void oneTimeSetUp() {
        System.out.println("@BeforeClass - oneTimeSetUp");
        Config.useTestDB();
    }

    @AfterClass
    public static void oneTimeTearDown() {
        // one-time cleanup code
        System.out.println("@AfterClass - oneTimeTearDown");
    }

    @Test
    public void getProjectWithTopicsTasksAndComments() throws Exception
    {
        UserPrincipalDTO dtoUser;
        int iProjectID = 0;

        //Get a DB connection, do the work, and close it
        Connection connection = SuperDuperDAO.getConnection();
        try
        {
            //Use a users from Example_TestData
            dtoUser = UserPrincipalDAO.selectUserByName(connection, "1001@test.com");
            SortDashboardMPB sortMPB = new SortDashboardMPB();
            sortMPB.bOrderByProjectID = true;
            List<ProjectMemberDTO> lstMembershipsForUser = ProjectMemberDAO.selectProjectMembershipsForUser(connection, dtoUser.get_id(), sortMPB, false);
            ProjectMemberDTO dtoMember = lstMembershipsForUser.get(0);
            iProjectID = dtoMember.get_id_project();
        }
        finally
        {
            SuperDuperDAO.close(connection, false);
        }

        TaskManagerServlet tmServlet = new TaskManagerServlet();
        TaskManagerViewMPB mpbTaskManager = tmServlet.getFullProjectData(TaskManagerServlet.MembershipAuthEnum.ADMIN, dtoUser, iProjectID);

        //Check the Project
        assertTrue(mpbTaskManager.dtoProject.getMembers().size() > 0);

        //Verify the number of topics exists
        assertTrue(mpbTaskManager.lstTopics.size() > 0);
        assertTrue(mpbTaskManager.mapTopicTasks.size() > 0);

        //VERY SPECIFIC TESTS ON THE DATA, intentionally brittle to catch subtle changes including sort order!
        List<TaskDTO> tasksForTopic1 = mpbTaskManager.mapTopicTasks.get(mpbTaskManager.lstTopics.get(0).get_id());
        assertTrue(tasksForTopic1.size() == 1);
        assertTrue(tasksForTopic1.get(0).getTaskComments().size() == 0);

        List<TaskDTO> tasksForTopic4 = mpbTaskManager.mapTopicTasks.get(mpbTaskManager.lstTopics.get(1).get_id());
        assertTrue(tasksForTopic4.size() == 2);
        assertTrue(tasksForTopic4.get(0).getTaskComments().size() == 2);
        assertTrue(tasksForTopic4.get(1).getTaskComments().size() == 1);
    }

}
