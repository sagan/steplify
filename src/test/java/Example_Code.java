import com.steplify.controller.*;
import com.steplify.dao.*;
import com.steplify.dto.*;
import com.steplify.mpb.*;
import com.steplify.utility.*;
import org.junit.*;
import static org.junit.Assert.*;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import com.google.common.collect.*;

public class Example_Code {

    @BeforeClass
    public static void oneTimeSetUp() {
        System.out.println("@BeforeClass - oneTimeSetUp");
        Config.useTestDB();
    }

    @AfterClass
    public static void oneTimeTearDown() {
        // one-time cleanup code
        System.out.println("@AfterClass - oneTimeTearDown");
    }

    @Test
    public void makeArrayListMultimap()
    {
        //http://mvnrepository.com/artifact/com.google.guava/guava/r09
        //http://docs.guava-libraries.googlecode.com/git-history/release/javadoc/index.html
        Multimap<String, String> myMultimap = ArrayListMultimap.create();

        // Adding some key/value
        myMultimap.put("Fruits", "Bannana");
        myMultimap.put("Fruits", "Apple");
        myMultimap.put("Fruits", "Pear");
        myMultimap.put("Vegetables", "Carrot");

        // Getting the size
        int size = myMultimap.size();
        System.out.println(size);  // 4

        // Getting values
        Collection<String> fruits = myMultimap.get("Fruits");
        System.out.println(fruits); // [Bannana, Apple, Pear]

        Collection<String> vegetables = myMultimap.get("Vegetables");
        System.out.println(vegetables); // [Carrot]

        // Iterating over entire Mutlimap
        for(String value : myMultimap.values()) {
            System.out.println(value);
        }

        // Removing a single value
        myMultimap.remove("Fruits","Pear");
        System.out.println(myMultimap.get("Fruits")); // [Bannana, Pear]

        // Remove all values for a key
        myMultimap.removeAll("Fruits");
        System.out.println(myMultimap.get("Fruits")); // [] (Empty Collection!)
    }

    @Test
    public void prepStringForDB()
    {
        String sExample = "   abcdefghijklmnopqrstuvwxyz   ";
        assertTrue(SuperDuperDAO.prepStringForDB(sExample, 26).equals("abcdefghijklmnopqrstuvwxyz"));
        assertTrue(SuperDuperDAO.prepStringForDB(sExample, 25).equals("abcdefghijklmnopqrstuvwxy"));
        assertTrue(SuperDuperDAO.prepStringForDB(sExample, 27).equals("abcdefghijklmnopqrstuvwxyz"));
    }

}

