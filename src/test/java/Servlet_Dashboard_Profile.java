import com.steplify.controller.*;
import com.steplify.dao.*;
import com.steplify.dto.*;
import com.steplify.mpb.*;
import com.steplify.utility.*;
import org.junit.*;
import static org.junit.Assert.*;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.util.*;

public class Servlet_Dashboard_Profile {

    private static Random _RANDOM_GENERATOR;

    @BeforeClass
    public static void oneTimeSetUp() {
        System.out.println("@BeforeClass - oneTimeSetUp");
        Config.useTestDB();
        _RANDOM_GENERATOR = new Random((new Date()).getTime());
    }

    @AfterClass
    public static void oneTimeTearDown() {
        // one-time cleanup code
        System.out.println("@AfterClass - oneTimeTearDown");
    }

    @Test
    public void setNewPassword() throws Exception
    {
        //Lets register a new user
        UserPrincipalDTO dtoUser = insertRandomUser("abc111");

        //Lets reset the password - mismatch
        SetNewPasswordMPB setNewPassMPB = new SetNewPasswordMPB();
        setNewPassMPB._s_password1 = "abc222";
        setNewPassMPB._s_password2 = "abc333";

        MockHttpServletResponse responsePassFail = new MockHttpServletResponse();
        DashboardServlet servletDashboardPassFail = new DashboardServlet();
        servletDashboardPassFail.postSetNewPassword(responsePassFail, dtoUser, setNewPassMPB);
        assertTrue(responsePassFail.getStatus() == HttpServletResponse.SC_BAD_REQUEST);
        assertTrue(responsePassFail.getResponseOut().indexOf("not match") != -1);

        //Lets reset the password - too small
        setNewPassMPB._s_password1 = "tiny";
        setNewPassMPB._s_password2 = "tiny";

        MockHttpServletResponse responsePassFail2 = new MockHttpServletResponse();
        DashboardServlet servletDashboardPassFail2 = new DashboardServlet();
        servletDashboardPassFail2.postSetNewPassword(responsePassFail2, dtoUser, setNewPassMPB);
        assertTrue(responsePassFail2.getStatus() == HttpServletResponse.SC_BAD_REQUEST);
        assertTrue(responsePassFail2.getResponseOut().indexOf("length") != -1);

        //Lets reset the password - has space
        setNewPassMPB._s_password1 = "has space";
        setNewPassMPB._s_password2 = "has space";

        MockHttpServletResponse responsePassFail3 = new MockHttpServletResponse();
        DashboardServlet servletDashboardPassFail3 = new DashboardServlet();
        servletDashboardPassFail3.postSetNewPassword(responsePassFail3, dtoUser, setNewPassMPB);
        assertTrue(responsePassFail3.getStatus() == HttpServletResponse.SC_BAD_REQUEST);
        assertTrue(responsePassFail3.getResponseOut().indexOf("spaces") != -1);

        //Lets reset the password - too long
        setNewPassMPB._s_password1 = "1234567890123456789012345678901234567890";
        setNewPassMPB._s_password2 = "1234567890123456789012345678901234567890";

        MockHttpServletResponse responsePassFail4 = new MockHttpServletResponse();
        DashboardServlet servletDashboardPassFail4 = new DashboardServlet();
        servletDashboardPassFail4.postSetNewPassword(responsePassFail4, dtoUser, setNewPassMPB);
        assertTrue(responsePassFail4.getStatus() == HttpServletResponse.SC_BAD_REQUEST);
        assertTrue(responsePassFail4.getResponseOut().indexOf("length") != -1);

        //Lets reset the password - ok
        setNewPassMPB._s_password1 = "abc444";
        setNewPassMPB._s_password2 = "abc444";

        MockHttpServletResponse responsePassOK = new MockHttpServletResponse();
        DashboardServlet servletDashboardPassOK = new DashboardServlet();
        servletDashboardPassOK.postSetNewPassword(responsePassOK, dtoUser, setNewPassMPB);
        assertTrue(responsePassOK.getStatus() == HttpServletResponse.SC_OK);

        //Lets fail login as the new user - old password
        LoginRegisterMPB mpb = new LoginRegisterMPB();
        mpb._s_email = dtoUser.get_s_username();
        mpb._s_password = "abc111";

        MockHttpServletResponse responseLoginFail = new MockHttpServletResponse();
        LoginServlet servletLoginFail = new LoginServlet();
        servletLoginFail.loginUser(responseLoginFail, mpb);
        assertTrue(responseLoginFail.getStatus() == HttpServletResponse.SC_BAD_REQUEST);
        assertTrue(responseLoginFail.getResponseOut().indexOf("invalid") != -1);

        //Lets login as the new user - OK
        mpb._s_password = "abc444";

        MockHttpServletResponse responseLogin = new MockHttpServletResponse();
        LoginServlet servletLogin = new LoginServlet();
        servletLogin.loginUser(responseLogin, mpb);
        assertTrue(responseLogin.getStatus() == HttpServletResponse.SC_OK);
        assertTrue(responseLogin.getResponseOut().indexOf("/dashboard") != -1);

    }

    @Test
    public void getThenSetProfile() throws Exception
    {
        //First try a user with a NULL profile
        UserPrincipalDTO dtoUser = insertRandomUser("abc111");
        MockHttpServletResponse responseProfileNull = new MockHttpServletResponse();
        DashboardServlet servletDashboardNull = new DashboardServlet();
        UserProfileDTO dtoProfileNull = servletDashboardNull.getProfile(responseProfileNull, dtoUser);
        assertTrue(dtoProfileNull.get_s_first_name() == null);
        assertTrue(responseProfileNull.getStatus() == HttpServletResponse.SC_OK);

        //Now lets add a profile, and verify it is json-ed properly
        UserProfileDTO dtoStaticProfile = Model_UserPrincipal.getStaticProfileDTO();
        dtoUser.setUserProfile(dtoStaticProfile);
        MockHttpServletResponse responseProfile = new MockHttpServletResponse();
        DashboardServlet servletDashboard = new DashboardServlet();
        UserProfileDTO dtoProfile = servletDashboard.getProfile(responseProfile, dtoUser);
        assertTrue(dtoProfile != null);
        assertTrue(responseProfile.getStatus() == HttpServletResponse.SC_OK);
        assertTrue(responseProfile.getResponseOut().indexOf("_s_first_name") != -1);

        //Now lets actually POST the profile and update the DB
        MockHttpServletResponse responseProfilePost = new MockHttpServletResponse();
        dtoProfile.set_s_postal_code("99999");
        servletDashboard.postSetProfile(responseProfilePost, dtoUser, dtoProfile);
        assertTrue(responseProfilePost.getStatus() == HttpServletResponse.SC_OK);
    }

    private UserPrincipalDTO insertRandomUser(String sInitialPass)
    {
        //Lets register a new user
        int prefix = _RANDOM_GENERATOR.nextInt(100000000);
        String sEmail = String.valueOf(prefix) + "_abc@foo.com";

        UserPrincipalDTO dtoUser = new UserPrincipalDTO();
        dtoUser.setNewUsername(sEmail);
        dtoUser.setNewPassword(sInitialPass);
        dtoUser.setStatusActive();

        //Get a DB connection, do the work, and close it
        Connection connection = SuperDuperDAO.getConnection();
        try
        {
            UserPrincipalDAO.insert(connection, dtoUser);
        }
        finally
        {
            SuperDuperDAO.close(connection, true);
        }

        return dtoUser;
    }

}
