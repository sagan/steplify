import com.steplify.controller.*;
import com.steplify.dao.*;
import com.steplify.dto.*;
import com.steplify.utility.*;
import org.junit.*;
import static org.junit.Assert.*;

import java.sql.Connection;

public class Servlet_Project {

    @BeforeClass
    public static void oneTimeSetUp() {
        System.out.println("@BeforeClass - oneTimeSetUp");
        Config.useTestDB();
    }

    @AfterClass
    public static void oneTimeTearDown() {
        // one-time cleanup code
        System.out.println("@AfterClass - oneTimeTearDown");
    }

    @Test
    public void addNewProjectEmpty() throws Exception
    {
        //Get a DB connection, do the work, and close it
        Connection connection = SuperDuperDAO.getConnection();
        try
        {
            ProjectServlet servletProject = new ProjectServlet();

            //Use a user from Example_TestData
            UserPrincipalDTO userDTO = UserPrincipalDAO.selectUserByName(connection, "1002@test.com");

            //Lets save a blank project-property
            ProjectDTO dtoProject = new ProjectDTO();

            TransResponse tr = servletProject.postAddProject(userDTO, dtoProject);
            assertTrue(!tr.isSuccess());
        }
        finally
        {
            SuperDuperDAO.close(connection, true);
        }
    }

    @Test
    public void addNewProjectComplete() throws Exception
    {
        //Get a DB connection, do the work, and close it
        Connection connection = SuperDuperDAO.getConnection();
        try
        {
            ProjectServlet servletProject = new ProjectServlet();

            //Use a user from Example_TestData
            UserPrincipalDTO userDTO = UserPrincipalDAO.selectUserByName(connection, "1002@test.com");

            //Lets save a real project-property
            ProjectDTO dtoProject = new ProjectDTO();
            dtoProject.set_s_description("A not so random home");
            dtoProject.set_s_address("123 My Street");
            dtoProject.set_s_city("Redwood City");
            dtoProject.set_s_state("California");
            dtoProject.set_s_country("USA");
            dtoProject.set_s_cd_project_type("Other / Custom");

            TransResponse tr = servletProject.postAddProject(userDTO, dtoProject);
            assertTrue(tr.isSuccess());
        }
        finally
        {
            SuperDuperDAO.close(connection, true);
        }
    }

}
