import com.steplify.dao.*;
import com.steplify.dto.*;
import com.steplify.utility.*;
import org.junit.*;
import java.sql.*;
import java.util.List;

import static org.junit.Assert.*;

public class Model_Company {

    @BeforeClass
    public static void oneTimeSetUp() {
        System.out.println("@BeforeClass - oneTimeSetUp");
        Config.useTestDB();
    }

    @AfterClass
    public static void oneTimeTearDown() {
        // one-time cleanup code
        System.out.println("@AfterClass - oneTimeTearDown");
    }


    @Test
    public void autoCompleteSearchForCompanyUsers()
    {
        //Get a DB connection, do the work, and close it
        Connection connection = SuperDuperDAO.getConnection();
        try
        {
            //Put any ID you want that is not in the DB
            List lstUsernames = UserPrincipalDAO.selectUsernamesByCompanyAndNamePrefix(connection, 101, "s");
            assertTrue(!lstUsernames.isEmpty());
        }
        finally
        {
            SuperDuperDAO.close(connection, false);
        }
    }


}
