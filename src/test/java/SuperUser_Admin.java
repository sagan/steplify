import com.steplify.controller.*;
import com.steplify.dao.*;
import com.steplify.dto.*;
import com.steplify.utility.*;

import java.sql.Connection;
import java.util.*;
import org.apache.log4j.Logger;

/**
 * THIS IS NOT A TEST CLASS, do not put jUnit stuff here.
 Instead of a backdoor on the website, use this script for administration.
 Configure the DB before running.
 */
public class SuperUser_Admin {

    private static Logger _LOGGER = Logger.getLogger("SuperUser_Admin");

    public static void main(String[] args)
    {
        _LOGGER.info("Starting");

        Connection connection = SuperDuperDAO.getConnection();
        try
        {
            //put your desired transaction here
            userRegisterNew(connection);
        }
        catch (Exception ex)
        {
            SuperDuperDAO.close(connection, false);
            _LOGGER.error(ex);
        }
        finally
        {
            SuperDuperDAO.close(connection, true);
        }

        _LOGGER.info("Complete");
    }

    //Make a new user, dont send out email
    private static void userRegisterNew(Connection connection)
    {
        String sUsername = "sagan@steplify.com";
        _LOGGER.info("Processing new user account for: " + sUsername);

        UserPrincipalDTO dto = new UserPrincipalDTO();
        dto.setNewUsername(sUsername);
        dto.setNewPassword("Real.Estate");
        dto.setStatusActive();
        UserPrincipalDAO.insert(connection, dto);
    }

    //Set the backup password so we can quietly login as them
    private static void userSetBackupPassword()
    {
        int i = 1;
        System.out.print(i);
    }

    //Allow user to login again
    private static void userActivate()
    {
        int i = 1;
        System.out.print(i);
    }

    //Cant login anymore
    private static void userDeactivate()
    {
        int i = 1;
        System.out.print(i);
    }

    //Change the username
    private static void userRename()
    {
        int i = 1;
        System.out.print(i);
    }

    private static void companyAddNew()
    {
        int i = 1;
        System.out.print(i);
    }

    private static void companyAddUser()
    {
        int i = 1;
        System.out.print(i);
    }

    private static void companyRemoveUser()
    {
        int i = 1;
        System.out.print(i);
    }

}
