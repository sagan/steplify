import com.steplify.dto.CodeDecodeDTO;
import com.steplify.utility.CodeDecode;
import com.steplify.utility.Config;
import org.junit.*;
import java.util.*;

import static org.junit.Assert.assertTrue;

public class Utility_CodeDecode {

    @BeforeClass
    public static void oneTimeSetUp() {
        System.out.println("@BeforeClass - oneTimeSetUp");
        Config.useTestDB();
    }

    @AfterClass
    public static void oneTimeTearDown() {
        // one-time cleanup code
        System.out.println("@AfterClass - oneTimeTearDown");
    }

    @Test
    public void getProjectMembershipRoles()
    {
        List<CodeDecodeDTO> alDTO = CodeDecode.getList(CodeDecode._CAT_MEMBERSHIP_ROLE);
        assertTrue(alDTO.size() > 1);
    }

    @Test
    public void getUserTypes()
    {
        List<CodeDecodeDTO> alDTO = CodeDecode.getList(CodeDecode._CAT_USER_STATUS);
        assertTrue(alDTO.size() > 1);
    }

    @Test
    public void getCode()
    {
        CodeDecodeDTO dto = CodeDecode.getByCode(CodeDecode._CAT_USER_STATUS, "ACTIVE");
        assertTrue(dto.get_s_decode().equals("Active"));
    }

    @Test
    public void getBadCode()
    {
        CodeDecodeDTO dto = CodeDecode.getByCode(CodeDecode._CAT_USER_STATUS, "ACTIVEX");
        assertTrue(dto == null);
    }

    @Test
    public void getValue()
    {
        CodeDecodeDTO dto = CodeDecode.getByValue(CodeDecode._CAT_PROJECT_TYPE, "Remodeling");
        assertTrue(dto.get_s_code().equals("REMODEL"));
    }

    @Test
    public void getBadValue()
    {
        CodeDecodeDTO dto = CodeDecode.getByValue(CodeDecode._CAT_PROJECT_TYPE, "RemodelingX");
        assertTrue(dto == null);
    }

    @Test
    public void getLookups()
    {
        Map<String, String> mapCD = CodeDecode.getLookupMap(CodeDecode._CAT_PROJECT_TYPE);
        assertTrue(mapCD.containsKey("OTHER"));
    }

}

