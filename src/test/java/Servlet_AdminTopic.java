import com.steplify.controller.AdminTopicServlet;
import com.steplify.dao.*;
import com.steplify.dto.*;
import com.steplify.mpb.*;
import com.steplify.utility.Config;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Connection;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class Servlet_AdminTopic {

    @BeforeClass
    public static void oneTimeSetUp() {
        System.out.println("@BeforeClass - oneTimeSetUp");
        Config.useTestDB();
    }

    @AfterClass
    public static void oneTimeTearDown() {
        // one-time cleanup code
        System.out.println("@AfterClass - oneTimeTearDown");
    }

    @Test
    public void getTopics() throws Exception
    {
        UserPrincipalDTO dtoUser;
        int iProjectID = 0;

        //Get a DB connection, do the work, and close it
        Connection connection = SuperDuperDAO.getConnection();
        try
        {
            //Use a users from Example_TestData
            dtoUser = UserPrincipalDAO.selectUserByName(connection, "1001@test.com");
            SortDashboardMPB sortMPB = new SortDashboardMPB();
            sortMPB.bOrderByProjectID = true;
            List<ProjectMemberDTO> lstMembershipsForUser = ProjectMemberDAO.selectProjectMembershipsForUser(connection, dtoUser.get_id(), sortMPB, false);
            ProjectMemberDTO dtoMember = lstMembershipsForUser.get(0);
            iProjectID = dtoMember.get_id_project();
        }
        finally
        {
            SuperDuperDAO.close(connection, false);
        }

        AdminTopicServlet adminServlet = new AdminTopicServlet();
        List<TopicDTO> lstTopics = adminServlet.getTopicsForProject(AdminTopicServlet.MembershipAuthEnum.ADMIN, dtoUser, iProjectID);

        //Verify the number of memberships found
        assertTrue(lstTopics.size() > 0);
        assertTrue(lstTopics.get(0).getTopicMembers().get(0).getUserPrincipal().getUserProfile() != null);

        //TODO: Further tests can/should be created
        //Like check the contents of the returned objects
    }

    @Test(expected=RuntimeException.class)
    public void getTopicsFOrNonAdmin() throws Exception
    {
        UserPrincipalDTO dtoUser;
        int iProjectID = 0;

        //Get a DB connection, do the work, and close it
        Connection connection = SuperDuperDAO.getConnection();
        try
        {
            //Use a users from Example_TestData
            dtoUser = UserPrincipalDAO.selectUserByName(connection, "1001@test.com");
            SortDashboardMPB sortMPB = new SortDashboardMPB();
            sortMPB.bOrderByProjectID = true;
            List<ProjectMemberDTO> lstMembershipsForUser = ProjectMemberDAO.selectProjectMembershipsForUser(connection, dtoUser.get_id(), sortMPB, false);
            ProjectMemberDTO dtoMember = lstMembershipsForUser.get(0);
            iProjectID = dtoMember.get_id_project();
        }
        finally
        {
            SuperDuperDAO.close(connection, false);
        }

        AdminTopicServlet adminServlet = new AdminTopicServlet();
        List<TopicDTO> lstTopics = adminServlet.getTopicsForProject(AdminTopicServlet.MembershipAuthEnum.MEMBER, dtoUser, iProjectID);

        //You wont make it this far, because an Exception will be thrown above for non-ADMIN users
        //But in case no Exception is thrown, lets fail
        assertTrue(false);
    }

}
