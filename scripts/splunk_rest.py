#!/usr/bin/env python
import urllib
import urllib2
import base64

#http://docs.splunk.com/Documentation/Storm/latest/User/UseStormsRESTAPI#Example:_Input_data_using_cURL
HOST='api.4gff-7src.data.splunkstorm.com'
ACCESS_TOKEN='DEPLOYMENT_SECRET'
PROJECT_ID='8c7b075cba6311e38030123139097a14'
SOURCETYPE='generic_single_line'
FILENAME='/var/lib/tomcat7/logs/catalina.out'

# Insert the latest logfile
url = 'https://%s/1/inputs/http?index=%s&sourcetype=%s' % (HOST, PROJECT_ID, SOURCETYPE)
values = open(FILENAME,'rb')
base64string = base64.encodestring('%s:%s' % ('x', ACCESS_TOKEN)).replace('\n', '')
headers = { 'Authorization' : ('Basic %s' % base64string),
            'Content-Type' : 'text/plain'}

print url;
print headers;
data = values.read()
req = urllib2.Request(url, data, headers)

response = urllib2.urlopen(req)
result = response.read()
print result
