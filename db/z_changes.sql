//Track changes to the schema for deployments

////////////////////////////////////////////////////////////////

//CHANGES for (20140213) BASE V1.1.62 -> NEW V1.1.63

//PRE-DEPLOYMENT
ALTER TABLE `steplify`.`project_member`
ADD COLUMN `_dt_archive_date` DATETIME NULL AFTER `_id_project`;

ALTER TABLE `steplify`.`project`
ADD COLUMN `_s_url_attachments` VARCHAR(200) NULL DEFAULT NULL AFTER `_s_cd_project_type`;

ALTER TABLE `steplify`.`user_profile`
ADD CONSTRAINT `fk_userprofile_userprincipal`
  FOREIGN KEY (`_id_created_by`)
  REFERENCES `steplify`.`user_principal` (`_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `steplify`.`project_member`
ADD CONSTRAINT `fk_projectmember_project`
  FOREIGN KEY (`_id_project`)
  REFERENCES `steplify`.`project` (`_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `steplify`.`project_member`
ADD CONSTRAINT `fk_projectmember_userprincipal`
  FOREIGN KEY (`_id_user_principal`)
  REFERENCES `steplify`.`user_principal` (`_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `steplify`.`topic`
ADD CONSTRAINT `fk_topic_project`
  FOREIGN KEY (`_id_project`)
  REFERENCES `steplify`.`project` (`_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `steplify`.`topic_member`
ADD CONSTRAINT `fk_topicmember_topic`
  FOREIGN KEY (`_id_topic`)
  REFERENCES `steplify`.`topic` (`_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `steplify`.`task`
ADD CONSTRAINT `fk_task_topic`
  FOREIGN KEY (`_id_topic`)
  REFERENCES `steplify`.`topic` (`_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `steplify`.`task_comment`
ADD CONSTRAINT `fk_taskcomment_task`
  FOREIGN KEY (`_id_task`)
  REFERENCES `steplify`.`task` (`_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


//POST-DEPLOYMENT CHANGES
SELECT * FROM steplify.code_decode;
DELETE FROM `steplify`.`code_decode`WHERE _id > 0;
SELECT * FROM steplify.code_decode;
commit;
////!!!NOW, RERUN THE CODE-DECODE script!!!

////////////////////////////////////////////////////////////////


//CHANGES for (20140322) BASE V1.1.74 -> NEW V1.1.75

//PRE-DEPLOYMENT
ALTER TABLE `steplify`.`task`
CHANGE COLUMN `_s_name` `_s_name` VARCHAR(250) NOT NULL ,
CHANGE COLUMN `_s_description` `_s_description` VARCHAR(1000) NULL ;

//POST-DEPLOYMENT CHANGES
UPDATE `steplify`.`code_decode` SET `_s_decode`='Buying for Client' WHERE `_id`='3';
UPDATE `steplify`.`code_decode` SET `_s_decode`='Listing for Client' WHERE `_id`='4';

////////////////////////////////////////////////////////////////


//CHANGES for (20140329) BASE V1.1.75 -> NEW V1.1.76

//PRE-DEPLOYMENT
CREATE TABLE `company` (
  `_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `_id_created_by` int(10) unsigned DEFAULT NULL,
  `_id_updated_by` int(10) unsigned DEFAULT NULL,
  `_dt_create_date` datetime DEFAULT NULL,
  `_dt_last_update` datetime DEFAULT NULL,
  `_s_name` varchar(100) DEFAULT NULL,
  `_s_description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_s_name` (`_s_name`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8;

CREATE TABLE `company_user` (
  `_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `_id_created_by` int(10) unsigned DEFAULT NULL,
  `_id_updated_by` int(10) unsigned DEFAULT NULL,
  `_dt_create_date` datetime DEFAULT NULL,
  `_dt_last_update` datetime DEFAULT NULL,
  `_id_company` int(10) unsigned NOT NULL,
  `_id_user_principal` int(10) unsigned NOT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_user` (`_id_user_principal`),
  KEY `_id_company` (`_id_company`),
  CONSTRAINT `fk_companyuser_userprincipal` FOREIGN KEY (`_id_user_principal`) REFERENCES `user_principal` (`_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_companyuser_company` FOREIGN KEY (`_id_company`) REFERENCES `company` (`_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8;

CREATE TABLE `company_topic_task` (
  `_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `_id_created_by` int(10) unsigned DEFAULT NULL,
  `_id_updated_by` int(10) unsigned DEFAULT NULL,
  `_dt_create_date` datetime DEFAULT NULL,
  `_dt_last_update` datetime DEFAULT NULL,
  `_i_topic_order` int(11) NOT NULL,
  `_s_topic_name` varchar(100) NOT NULL,
  `_s_task_name` varchar(250) NOT NULL,
  `_id_company` int(10) unsigned NOT NULL,
  `_s_cd_project_type` varchar(45) NOT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_company_type_topic_task` (`_id_company`,`_s_cd_project_type`,`_s_topic_name`,`_s_task_name`),
  CONSTRAINT `fk_companytasktopic_company` FOREIGN KEY (`_id_company`) REFERENCES `company` (`_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8;


//Populate new tables
//make sure the default company id=0 after insert
INSERT INTO `steplify`.`company` (`_id`, `_id_created_by`, `_id_updated_by`, `_dt_create_date`, `_dt_last_update`, `_s_name`, `_s_description`)
VALUES (0, NULL, NULL, NULL, NULL, 'DEFAULT', 'For users that are not tied to a company');
//Import z_ctt.csv

//POST-DEPLOYMENT CHANGES
//Assign users to company

////////////////////////////////////////////////////////////////

//CHANGES for (20140401) BASE V1.1.76 -> NEW V1.1.78

//Add 2 new roles
update code_decode set _i_order=_i_order+10 where _s_category='PROJECT_MEMBER_ROLE' and _i_order<99;
INSERT INTO `steplify`.`code_decode` (`_dt_create_date`, `_i_order`, `_s_category`, `_s_code`, `_s_decode`, `_b_active`) VALUES ('2014-04-01', '5', 'PROJECT_MEMBER_ROLE', 'TRANSACTION_COORDINATOR', 'Transaction Coordinator', '1');
INSERT INTO `steplify`.`code_decode` (`_dt_create_date`, `_i_order`, `_s_category`, `_s_code`, `_s_decode`, `_b_active`) VALUES ('2014-04-01', '8', 'PROJECT_MEMBER_ROLE', 'AGENT_ASSISTANT', 'Agent Assistant', '1');

////////////////////////////////////////////////////////////////
