#Command to export to file
#mysqldbexport --server=steplify_user:steplify_user@hpub steplify >> C:\\Temp\steplify_db_meta_export.txt

#Copy/Paste into MySQL Workbench, but before executing do the following manually (for Referential Integrity FKs).
#Move table:user_principal before:company
#Move table:topic before table:task

################################################################

# Exporting metadata from steplify
DROP DATABASE IF EXISTS `steplify`;
CREATE DATABASE `steplify`;
USE `steplify`;

# TABLE: steplify.code_decode
CREATE TABLE `code_decode` (
  `_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `_dt_create_date` datetime DEFAULT NULL,
  `_i_order` int(11) DEFAULT NULL,
  `_s_category` varchar(45) DEFAULT NULL,
  `_s_code` varchar(45) DEFAULT NULL,
  `_s_decode` varchar(45) DEFAULT NULL,
  `_b_active` int(11) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `cat_and_code` (`_s_category`,`_s_code`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

# TABLE: steplify.company
CREATE TABLE `company` (
  `_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `_id_created_by` int(10) unsigned DEFAULT NULL,
  `_id_updated_by` int(10) unsigned DEFAULT NULL,
  `_dt_create_date` datetime DEFAULT NULL,
  `_dt_last_update` datetime DEFAULT NULL,
  `_s_name` varchar(100) DEFAULT NULL,
  `_s_description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_s_name` (`_s_name`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8;

# TABLE: steplify.company_topic_task
CREATE TABLE `company_topic_task` (
  `_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `_id_created_by` int(10) unsigned DEFAULT NULL,
  `_id_updated_by` int(10) unsigned DEFAULT NULL,
  `_dt_create_date` datetime DEFAULT NULL,
  `_dt_last_update` datetime DEFAULT NULL,
  `_i_topic_order` int(11) NOT NULL,
  `_s_topic_name` varchar(100) NOT NULL,
  `_s_task_name` varchar(250) NOT NULL,
  `_id_company` int(10) unsigned NOT NULL,
  `_s_cd_project_type` varchar(45) NOT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_company_type_topic_task` (`_id_company`,`_s_cd_project_type`,`_s_topic_name`,`_s_task_name`),
  CONSTRAINT `fk_companytasktopic_company` FOREIGN KEY (`_id_company`) REFERENCES `company` (`_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=786 DEFAULT CHARSET=utf8;

# TABLE: steplify.company_user
CREATE TABLE `company_user` (
  `_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `_id_created_by` int(10) unsigned DEFAULT NULL,
  `_id_updated_by` int(10) unsigned DEFAULT NULL,
  `_dt_create_date` datetime DEFAULT NULL,
  `_dt_last_update` datetime DEFAULT NULL,
  `_id_company` int(10) unsigned NOT NULL,
  `_id_user_principal` int(10) unsigned NOT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_user` (`_id_user_principal`),
  KEY `_id_company` (`_id_company`),
  CONSTRAINT `fk_companyuser_company` FOREIGN KEY (`_id_company`) REFERENCES `company` (`_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_companyuser_userprincipal` FOREIGN KEY (`_id_user_principal`) REFERENCES `user_principal` (`_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;

# TABLE: steplify.project
CREATE TABLE `project` (
  `_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `_id_created_by` int(10) unsigned NOT NULL,
  `_id_updated_by` int(10) unsigned NOT NULL,
  `_dt_create_date` datetime NOT NULL,
  `_dt_last_update` datetime NOT NULL,
  `_s_address` varchar(100) DEFAULT NULL,
  `_s_city` varchar(45) DEFAULT NULL,
  `_s_state` varchar(45) DEFAULT NULL,
  `_s_country` varchar(45) DEFAULT NULL,
  `_s_postal_code` varchar(45) DEFAULT NULL,
  `_s_url_website` varchar(200) DEFAULT NULL,
  `_s_url_image` varchar(200) DEFAULT NULL,
  `_s_description` varchar(100) DEFAULT NULL,
  `_s_cd_project_type` varchar(45) DEFAULT NULL,
  `_s_url_attachments` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

# TABLE: steplify.project_member
CREATE TABLE `project_member` (
  `_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `_id_created_by` int(10) unsigned NOT NULL,
  `_id_updated_by` int(10) unsigned NOT NULL,
  `_dt_create_date` datetime NOT NULL,
  `_dt_last_update` datetime NOT NULL,
  `_id_user_principal` int(10) unsigned NOT NULL,
  `_s_cd_status` varchar(45) NOT NULL,
  `_s_cd_role` varchar(45) DEFAULT NULL,
  `_b_is_project_admin` int(11) NOT NULL,
  `_id_project` int(10) unsigned NOT NULL,
  `_dt_archive_date` datetime DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_project_and_id_user` (`_id_project`,`_id_user_principal`),
  KEY `_id_user` (`_id_user_principal`),
  CONSTRAINT `fk_projectmember_project` FOREIGN KEY (`_id_project`) REFERENCES `project` (`_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_projectmember_userprincipal` FOREIGN KEY (`_id_user_principal`) REFERENCES `user_principal` (`_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;

# TABLE: steplify.task
CREATE TABLE `task` (
  `_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `_id_created_by` int(10) unsigned NOT NULL,
  `_id_updated_by` int(10) unsigned NOT NULL,
  `_dt_create_date` datetime NOT NULL,
  `_dt_last_update` datetime NOT NULL,
  `_s_name` varchar(250) NOT NULL,
  `_s_description` varchar(1000) DEFAULT NULL,
  `_i_order` int(11) DEFAULT NULL,
  `_s_cd_status` varchar(45) NOT NULL,
  `_dt_due` datetime NOT NULL,
  `_id_assigned_user` int(10) unsigned NOT NULL,
  `_id_project` int(10) unsigned NOT NULL,
  `_id_topic` int(10) unsigned NOT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_topic_and_s_name` (`_id_topic`,`_s_name`),
  CONSTRAINT `fk_task_topic` FOREIGN KEY (`_id_topic`) REFERENCES `topic` (`_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=644 DEFAULT CHARSET=utf8;

# TABLE: steplify.task_comment
CREATE TABLE `task_comment` (
  `_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `_id_created_by` int(10) unsigned NOT NULL,
  `_id_updated_by` int(10) unsigned NOT NULL,
  `_dt_create_date` datetime NOT NULL,
  `_dt_last_update` datetime NOT NULL,
  `_s_comment` varchar(500) NOT NULL,
  `_id_project` int(10) unsigned NOT NULL,
  `_id_task` int(10) unsigned NOT NULL,
  PRIMARY KEY (`_id`),
  KEY `_task_id` (`_id_task`),
  CONSTRAINT `fk_taskcomment_task` FOREIGN KEY (`_id_task`) REFERENCES `task` (`_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

# TABLE: steplify.topic
CREATE TABLE `topic` (
  `_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `_id_created_by` int(10) unsigned NOT NULL,
  `_id_updated_by` int(10) unsigned NOT NULL,
  `_dt_create_date` datetime NOT NULL,
  `_dt_last_update` datetime NOT NULL,
  `_s_name` varchar(100) NOT NULL,
  `_s_description` varchar(200) DEFAULT NULL,
  `_s_url_attachments` varchar(200) DEFAULT NULL,
  `_b_auto_add_members` int(11) NOT NULL,
  `_i_order` int(11) NOT NULL,
  `_id_project` int(10) unsigned NOT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `project_id_and_s_name` (`_id_project`,`_s_name`),
  CONSTRAINT `fk_topic_project` FOREIGN KEY (`_id_project`) REFERENCES `project` (`_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=241 DEFAULT CHARSET=utf8;

# TABLE: steplify.topic_member
CREATE TABLE `topic_member` (
  `_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `_id_created_by` int(10) unsigned NOT NULL,
  `_id_updated_by` int(10) unsigned NOT NULL,
  `_dt_create_date` datetime NOT NULL,
  `_dt_last_update` datetime NOT NULL,
  `_id_user_principal` int(10) unsigned NOT NULL,
  `_s_cd_authority` varchar(45) NOT NULL,
  `_id_project` int(10) unsigned NOT NULL,
  `_id_topic` int(10) unsigned NOT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_topic_and_id_user` (`_id_topic`,`_id_user_principal`),
  CONSTRAINT `fk_topicmember_topic` FOREIGN KEY (`_id_topic`) REFERENCES `topic` (`_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=658 DEFAULT CHARSET=utf8;

# TABLE: steplify.user_principal
CREATE TABLE `user_principal` (
  `_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `_dt_create_date` datetime NOT NULL,
  `_dt_last_update` datetime NOT NULL,
  `_s_username` varchar(100) NOT NULL,
  `_s_password_hash` varchar(45) DEFAULT NULL,
  `_s_password_backup_hash` varchar(45) DEFAULT NULL,
  `_s_cd_status` varchar(45) NOT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_s_username_UNIQUE` (`_s_username`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

# TABLE: steplify.user_profile
CREATE TABLE `user_profile` (
  `_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `_id_created_by` int(10) unsigned NOT NULL,
  `_dt_create_date` datetime NOT NULL,
  `_dt_last_update` datetime NOT NULL,
  `_s_first_name` varchar(45) DEFAULT NULL,
  `_s_last_name` varchar(45) DEFAULT NULL,
  `_s_url_personal_img` varchar(200) DEFAULT NULL,
  `_s_company_name` varchar(100) DEFAULT NULL,
  `_s_url_company_img` varchar(200) DEFAULT NULL,
  `_s_address` varchar(100) DEFAULT NULL,
  `_s_city` varchar(45) DEFAULT NULL,
  `_s_state` varchar(45) DEFAULT NULL,
  `_s_country` varchar(45) DEFAULT NULL,
  `_s_postal_code` varchar(45) DEFAULT NULL,
  `_s_phone_number` varchar(45) DEFAULT NULL,
  `_s_url_website` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id_created_by_UNIQUE` (`_id_created_by`),
  CONSTRAINT `fk_userprofile_userprincipal` FOREIGN KEY (`_id_created_by`) REFERENCES `user_principal` (`_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

#...done.
