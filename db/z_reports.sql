//Some general report queries
SELECT count(*) FROM steplify.user_principal;

SELECT count(*) FROM steplify.project;

//Which users created which projects
SELECT UP._s_username, P._s_description, P.* FROM project P, user_principal UP
where P._id_created_by = UP._id order by P._dt_create_date;

//Which users belong to which companies
SELECT _id_user_principal, UP._s_username, _id_company, C._s_name
FROM company_user CU, company C, user_principal UP
where CU._id_company=C._id and CU._id_user_principal=UP._id;

//Which users are on a given project
SELECT P._id, P._s_description, UP._s_username, PM._s_cd_status, PM._s_cd_role, PM._b_is_project_admin
FROM project_member PM, project P, user_principal UP
where PM._id_project=P._id and PM._id_user_principal=UP._id
order by P._id, PM._id;

//What comments exist on what tasks
SELECT P._id, P._s_description, TK._s_name, TK._s_cd_status, UP._s_username, TC._dt_create_date, TC._s_comment
FROM project P, user_principal UP, task_comment TC, task TK
where P._id=TC._id_project and UP._id=TC._id_created_by and TC._id_task=TK._id
order by P._id, TK._id, TC._dt_create_date;

//Topics and Tasks per project
SELECT P._id, P._s_description, TP._s_name, TP._i_order, TK._s_name, TK._s_cd_status, TK._dt_due
FROM project P, topic TP, task TK
where P._id=TP._id_project and TP._id=TK._id_topic
order by P._id, TP._i_order, TK._dt_due;

//Group by role usage
SELECT _s_cd_role, count(*)
FROM steplify.project_member
group by _s_cd_role;

//Specific role usage
SELECT * FROM steplify.project_member
where _s_cd_role = 'OWNER';
