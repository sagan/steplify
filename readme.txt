Real Estate application, for Task based management

HowTo Setup:

///////////////////////////

DEV ENV:
-Setup Tomcat
-CONFIG NEW WEBAPP, make admin site only internal access (or for dev only)
-Setup TOMCAT_USERS, so you can remote deploy  (or for dev only)

-Setup Apache
-If you skip Apache, and use Tomcat, set Tomcat to port 80
-VHOST
-SETUP to talk to Tomcat = modjk.so

-Deploy custom to your dev setup, see down below...

///////////////////////////

EC2 ENV:
-Setup VM, Ubuntu 12.04
-Setup security groups (trusted, world, default)
-Setup MySQL instance
-Use apt-get to install javac, tomcat7
-Test the base index.html, it should say "It worked"

-Build a test war
-cd /var/lib/tomcat7/webapps
-Rename/Move the original ROOT dir out of the way
-cp test war to "/var/lib/tomcat7/webapps", it should auto deploy
-Make a softlink from ROOT -> TEST_WAR_DIR
-Test against port 8080

-Configure /var/lib/tomcat7/conf/server.xml to listen on port 80
-Configure /etc/default/tomcat to "AUTHBIND=yes"
-restart tomcat with "sudo service tomcat7 restart"
-test against port 80

-test port 8080 no longer works
-run netstat, and review your security groups
-Setup mysql tables and so on...

///////////////////////////

BUILD / RELEASE:
-git clone (optional if you have already pulled)
-Bump the VERSION number (Config.java)
-Maven clean
-Maven package
-Commit to git
-Configure the code (Config.java and web.xml)
-Maven clean
-Maven package
-Rename the war to ROOT_<VERSION>.war
-pscp -i "C:\Program Files (x86)\putty\keys\steplify_aws_ubuntu12_20131223.ppk" ROOT_P1.1.XX.war ubuntu@steplify.com:

-Login to EC2
-mv war to backup_jars dir
-cp war to "/var/lib/tomcat7/webapps", it should auto deploy
-Move the softlink (sudo rm ROOT; sudo ln -s ROOT_P1.1.XX ROOT)
-Bounce Tomcat (sudo service tomcat7 restart)
-Test your new version number comes up
-Remove 2 versions back
-OneLiner="sudo rm ROOT; sudo ln -s ROOT_P1.1.XX ROOT; sudo service tomcat7 restart"

///////////////////////////

Setup MySQL DB
-Create DB on Server
-Create DB on localhost (for unit tests)
-See Config.java for settings, or changing name

///////////////////////////

Windows Dev Machine:
-JDK 1.7 (pom.xml with <target>1.6</target>)
-IntelliJ
-Maven
-Git (scm)
-MySQL Workbench 6.0 gui client
-BitBucket

///////////////////////////

Configure App:
-Update the web.xml with context-param
-Update the utility.Config class with proper STATIC settings

///////////////////////////

Add MySQL databases: (repeat this section for all DBs listed below)
-steplify

Add users:
-steplify_user

Add tables:
-See the ../db dir for SQL to apply, per table

///////////////////////////

Deploy WAR:
-Run maven 'package' goal
-Rename the war file to ROOT.war
-http://steplify.homenet.org/manager/html
-Undeploy old
-Deploy new

///////////////////////////

Maven Compile Details:
http://stackoverflow.com/questions/2210005/config-maven-2-to-print-out-javac-commands-during-compile-phase

///////////////////////////
